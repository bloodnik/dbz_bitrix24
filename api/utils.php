<?php

use Bitrix\Iblock\Iblock;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Type\DateTime;
use lib\contact\CDbzContact;
use lib\helpers\CDbzConstants;
use lib\order\CDbzOrder;
use lib\packages\CDbzPackages;

class Utils {
	//Здесь можно хранить методы, общие для разных файлов, чтобы использовать их из них

	//установка глобальных переменных
	function set_globals() {
		return array(
			'portal_url'    => 'https://dombezzabot.net',
			'setup_element' => 31213,
			'yes'           => 31194,
			'no'            => 31195,
		);
	}

	//-----Возвращение ответа-----

	//$response - json-объект
	//$message - код сообщения


	//кодирование успешного ответа
	public function getSuccess($response) {
		return json_encode(['success' => true, 'message' => null, 'response' => json_encode($response)]);
	}


	//кодирование неудачного ответа
	public function getFailure($message, $money_amount, $special_error) {
		if ( ! ($money_amount)) {
			$money_amount = null;
		}
		$shouldSelectCity = null;
		$shouldEnterName  = null;
		if ($special_error == "city_error") {
			$shouldSelectCity = true;
		}
		if ($special_error == "name_error") {
			$shouldEnterName = true;
		}

		return json_encode(['success' => false, 'message' => $message, 'response' => "", 'money_amount' => $money_amount, 'should_select_city' => $shouldSelectCity, 'should_enter_name' => $shouldEnterName]);
	}


	//-----Функции обработки запросов-----

	//Отправка кода по смс для авторизации
	function sendLogInCode($phone) {
		$code = random_int(1, 9) . random_int(0, 9) . random_int(0, 9) . random_int(0, 9) . random_int(0, 9);
		//удаление старой записи в таблице авторизации
		$arSelect = array("ID");
		$arFilter = array("IBLOCK_ID" => 69, "PROPERTY_123" => $phone);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if ($arFields["ID"] != 31218) {
				$this->element_delete(69, $arFields["ID"]);
			}
		};
		//запись в таблицу авторизации
		$el                       = new CIBlockElement;
		$PROP                     = array();
		$PROP["TELEFON"]          = $phone;
		$PROP["KOD_AVTORIZATSII"] = $code;
		$arLoadProductArray       = array(
			"IBLOCK_ID"       => 69,
			"NAME"            => "Код авторизации",
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id               = $el->Add($arLoadProductArray);
		//запуск процесса на удаление на записи
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				195,
				array("lists", "BizprocDocument", $element_id),
				array_merge(),
				$arErrorsTmp
			);
		}
		//проверка среди тестовых
		$arSelect = array("ID");
		$arFilter = array("IBLOCK_ID" => 116, "PROPERTY_309" => $phone);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$check_id = $arFields["ID"];
		};
		if ($check_id) {
			return $this->getSuccess("");
			exit;
		}
		//отправка смс с кодом
		$message      = "Ваш%20код%20подтверждения:%20" . $code;
		$site_url     = "www.mcommunicator.ru";
		$site_root    = "/M2M/m2m_api.asmx/SendMessage";
		$get_params   = "msid=" . $phone . "&message=" . $message . "&naming=Dombezzabot&login=dombezzabot&password=FBEEE45F469FC74280755E13803E6306";
		$strQueryText = QueryGetData($site_url, 80, $site_root, $get_params, $error_number, $error_text, "GET");

		return $this->getSuccess("");
	}


	//Авторизация по телефону (проверка кода уже произведена)
	function logInWithPhone($user_type, $phone, $code) {
		$this->user_type_check($user_type);
		include_once('extra_utils.php');
		$extra_utils = new ExtraUtils();
		global $yes;
		global $no;
		//проверяем формат телефона для поиска по базе
		if (strpos($phone, '+') !== false) {
			$phone_substr = substr($phone, 2);
		} else {
			$phone_substr = substr($phone, 1);
		}
		if (strlen($phone_substr) == 10) {
			$phone_format = true;
		} else {
			$phone_format = false;
		}
		//ищем телефон по базе
		CModule::IncludeModule("crm");
		if ($phone_format == true) {
			$contacts = $this->contact_search($phone_substr);
			$contact  = ! empty($contacts) ? min($contacts) : null;
		}
		//собрать все типы пушей
		$push_types = array();
		$arSelect   = array("ID");
		$arFilter   = array("IBLOCK_ID" => 94);
		$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields     = $ob->GetFields();
			$push_types[] = $arFields["ID"];
		};
		//оформлям контакт
		$notifications = false;
		if ($user_type == 0) {
			$user_code        = 31169;
			$user_title       = "Исполнитель";
			$user_type_string = "SUPPLIER";
		} else {
			$user_code        = 31170;
			$user_title       = "Заказчик";
			$user_type_string = "CLIENT";
		}
		if ($contact) {  //нашли контакт по телефону
			//проверка мобильных (тип для приложения) телефонов контакта
			$contact_mobile_phones = array();
			$found_this_phone      = false;
			$dbResult              = CCrmFieldMulti::GetList(
				array(),
				array(
					'ENTITY_ID'  => 'CONTACT',
					'TYPE_ID'    => 'PHONE',
					'ELEMENT_ID' => $contact,
					'VALUE_TYPE' => 'MOBILE',
				)
			);
			while ($ar = $dbResult->Fetch()) {
				$contact_mobile_phones[ $ar["ID"] ] = $ar["VALUE"];
			}
			foreach ($contact_mobile_phones as $phone_id => $phone_value) {
				if (stripos($phone_substr, $phone_value) === false) {
					$this->update_phone_type_contact($contact, $phone_id, 'WORK');
				} else {
					$found_this_phone = true;
				}
			}
			if ( ! ($found_this_phone)) {
				$dbResult = CCrmFieldMulti::GetList(
					array(),
					array(
						'ENTITY_ID'  => 'CONTACT',
						'ELEMENT_ID' => $contact,
						'TYPE_ID'    => 'PHONE',
						'VALUE'      => "%" . $phone_substr,
					)
				);
				while ($ar = $dbResult->Fetch()) {
					$this->update_phone_type_contact($contact, $ar["ID"], 'MOBILE');
				}
			}
			//простановка флага регистрации
			$arOrder          = array();
			$arFilter         = array("=ID" => $contact);
			$arGroupBy        = false;
			$arNavStartParams = false;
			$arSelectFields   = array("UF_CRM_NEW_APP_REGISTER", "TYPE_ID", "UF_CRM_NEW_CITY");
			$res              = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$is_register = $ob["UF_CRM_NEW_APP_REGISTER"];
				$type_id     = $ob["TYPE_ID"];
				$city        = $ob["UF_CRM_NEW_CITY"];
			}
			if ( ! ($is_register)) {
				$entity = new CCrmContact;
				$fields = array(
					"UF_CRM_REGISTER_DATE"  => date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y'),
					"UF_CRM_PUSH_AVAILABLE" => $push_types,
					"NAME"                  => $user_title,
					"LAST_NAME"             => "",
					"SECOND_NAME"           => "",
				);
				$entity->update($contact, $fields);
			}
			if ( ! (in_array($user_code, (array) $is_register))) {
				$is_register[] = $user_code;
				$this->update_field_contact($contact, "UF_CRM_NEW_APP_REGISTER", $is_register);
				$notifications = true;
			}
			if ($user_type == 0 and $type_id != $user_type_string) {  //проставить тип контакта = исполнитель, если не так
				$this->update_field_contact($contact, "TYPE_ID", $user_type_string);
			}
			if ($city) {
				$city_selected = true;
			} else {
				$city_selected = false;
			}
		} else {  //создаем новый
			$entity  = new CCrmContact;
			$fields  = array(
				'NAME'                    => $user_title,
				'TYPE_ID'                 => $user_type_string,
				'UF_CRM_NEW_APP_REGISTER' => array($user_code),
				'UF_CRM_REGISTER_DATE'    => date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y'),
				'UF_CRM_PUSH_AVAILABLE'   => $push_types,
			);
			$contact = $entity->add($fields);
			$this->add_phone_contact($contact, $phone, 'MOBILE');
			$this->active_date_contact($contact);
			$notifications = true;
			$city_selected = false;
		}
		//запись в таблицу токенов
		$new_token          = bin2hex(random_bytes(15));
		$el                 = new CIBlockElement;
		$PROP               = array();
		$PROP["KONTAKT"]    = $contact;
		$PROP["TOKEN"]      = $new_token;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 71,
			"NAME"            => "Токен авторизации",
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id         = $el->Add($arLoadProductArray);
		//вешаем стартовые уведомления
		if ($notifications) {
			$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_211", "PROPERTY_212", "PROPERTY_220", "PROPERTY_216", "PROPERTY_236", "PROPERTY_374");
			$arFilter = array("IBLOCK_ID" => 91, "=PROPERTY_212" => 34318, "=PROPERTY_213" => $user_code, "=PROPERTY_217" => $yes);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields   = $ob->GetFields();
				$time_point = date_create(date('Y-m-d H:i:s', time()));
				date_add($time_point, date_interval_create_from_date_string($arFields["PROPERTY_216_VALUE"] . " days"));
				if ($arFields["PROPERTY_236_VALUE"] == $yes) {
					$send_push = $yes;
				} else {
					$send_push = $no;
				}
				if ($arFields["PROPERTY_374_VALUE"]) {
					$start_active = $no;
				} else {
					$start_active = $yes;
				}
				$el                 = new CIBlockElement;
				$PROP               = array(
					204 => $arFields["~PROPERTY_211_VALUE"]["TEXT"],
					205 => 34315,
					206 => $user_code,
					218 => $arFields["PROPERTY_220_VALUE"],
					208 => $start_active,
					209 => date_format($time_point, 'd.m.Y H:i:s'),
					207 => $contact,
					237 => $send_push,
					372 => $arFields["PROPERTY_374_VALUE"],
				);
				$arLoadProductArray = array(
					"IBLOCK_ID"       => 90,
					"NAME"            => $arFields["NAME"],
					"PROPERTY_VALUES" => $PROP,
				);
				$element_id         = $el->Add($arLoadProductArray);
				//запуск БП отложенной деактивации
				if (CModule::IncludeModule("bizproc")) {
					$wfId = CBPDocument::StartWorkflow(
						202,
						array("lists", "BizprocDocument", $element_id),
						array_merge(),
						$arErrorsTmp
					);
				}
				//отправка пушка
				if ($send_push == $yes) {
					$this->send_push($element_id);
				}
			}
		}
		$should_select_city = ! $city_selected;
		//перенос баланса из СП
		$extra_utils->carryover($contact, $phone);
		//обновление тестового аккаунта
		$this->test_account_update($contact, $phone);
		//формируем ответ
		$response = array(
			"id"               => (int) $contact,
			"token"            => $new_token,
			"shouldSelectCity" => $should_select_city,
		);

		return $this->getSuccess($response);
	}


	//Добавление токена для пуша
	function addPushToken($id, $user_type, $token) {
		$this->user_type_check($user_type);
		if ($user_type == 0) {
			$user_code = 31169;
		} else {
			$user_code = 31170;
		}


		// Удаляем все неактуальные токены пользователя

		/**  @var \Bitrix\Iblock\ORM\CommonElementTable $iblock */
		$iblock   = Iblock::wakeUp(CDbzConstants::DBZ_PUSH_TOKENS_IBLOCK_ID)->getEntityDataClass();
		$obElements = $iblock::getList([
			'select' => ["ID", "NAME", "TOKEN_" => "TOKEN", "DATE_CREATE", "POLZOVATEL_" => "POLZOVATEL", "TIP_POLZOVATELYA_" => "TIP_POLZOVATELYA"],
			'filter' => ["POLZOVATEL.VALUE" => $id, "TIP_POLZOVATELYA.VALUE" => $user_code]
		]);
		while ($element = $obElements->fetchObject()) {
			$element->delete();
		}

		//добавляем
		$newToken = $iblock::createObject();

		$newToken->setName("Токен устройства");
		$newToken->setToken($token);
		$newToken->setPolzovatel($id);
		$newToken->setTipPolzovatelya($user_code);
		$newToken->save();

		return $this->getSuccess("");
	}


	//Обновление данных пользователя
	function setUserData($id, $name_1, $name_2, $name_3) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		CModule::IncludeModule("crm");
		$entity = new CCrmContact;
		$fields = array();
		if ($name_1) {
			$fields["NAME"] = $name_1;
		}
		if ($name_2) {
			$fields["LAST_NAME"] = $name_2;
		}
		if ($name_3) {
			$fields["SECOND_NAME"] = $name_3;
		}
		$entity->update($id, $fields);
		//обновление таблицы регистраций (корректировка)
		$extra_utils->register_update($id);

		return $this->getSuccess("");
	}


	//Получение данных пользователя
	function getUserData($id) {
		CModule::IncludeModule("crm");
		$arOrder          = array();
		$arFilter         = array("=ID" => $id);
		$arGroupBy        = false;
		$arNavStartParams = false;
		$arSelectFields   = array("NAME", "LAST_NAME", "SECOND_NAME");
		$res              = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$name_1 = $ob["NAME"];
			$name_2 = $ob["LAST_NAME"];
			$name_3 = $ob["SECOND_NAME"];
		}
		$response = array(
			"name_1" => $name_1,
			"name_2" => $name_2,
			"name_3" => $name_3,
		);

		return $this->getSuccess($response);
	}


	//Получение данных пользователя - подробно
	function getUserInfo($id, $user_type) {
		global $yes;
		global $no;
		$this->user_type_check($user_type);

		//имя и фамилия, рейтинг
		$obContact   = new CDbzContact($id);
		$arContact   = $obContact->getContactData();
		$name_1      = $arContact["NAME"] ?? "";
		$name_2      = $arContact["LAST_NAME"] ?? "";
		$name_3      = $arContact["SECOND_NAME"] ?? "";
		$city_id     = $arContact["UF_CRM_NEW_CITY"];
		$rating      = $arContact["UF_CRM_RATING_MAIN"];
		$works_count = count((array) $arContact[ CDbzContact::CATEGORIES_USER_FIELD_CODE ]);

		$obUserData = new \lib\userdata\CUserData($id);
		$photo_id   = $obUserData->getUserDataValue(\lib\userdata\CUserData::USER_PHOTO_PROPERTY_CODE);
		$photo_url  = $photo_id ? CDbzConstants::DBZ_PORTAL_URL . CFile::GetPath($photo_id) : null;

		$obPackages = new CDbzPackages($id);
		$arPackages = $obPackages->getUserPackagesInfo();

		// Число оставшихся заявок по тарифу
		$maxTaskCountPackage  = array_reduce($arPackages, function($a, $b) {
			return $a ? ($a["TASK_COUNT"] > $b["TASK_COUNT"] ? $a : $b) : $b;
		});
		$packages_value_count = intval($maxTaskCountPackage["TASK_COUNT"]);

		// Число оставшихся дней по тарифу
		$maxEndDatePackage           = array_reduce($arPackages, function($a, $b) {
			return $a ? ($a["VREMYA_OKONCHANIYA"] > $b["VREMYA_OKONCHANIYA"] ? $a : $b) : $b;
		});
		$obTodayToPackageEndDateDiff = DateTime::createFromPhp(new \DateTime())->toUserTime()->getDiff(DateTime::createFromPhp(new \DateTime($maxEndDatePackage["VREMYA_OKONCHANIYA"])));
		$packages_days_count         = $packages_value_count ? $obTodayToPackageEndDateDiff->days : 0;
		$packages_hours_count        = ($packages_value_count && ! $obTodayToPackageEndDateDiff->invert) ? $obTodayToPackageEndDateDiff->h ?? 1 : 0;

//		PR($obTodayToPackageEndDateDiff);

		// Строка об остатке количества заявок
		$packages_remain_string = null;
		if ( ! ! $packages_value_count && ! $obTodayToPackageEndDateDiff->invert) {
			$packages_remain_string = "{$packages_value_count} заяв. на";
			$packages_remain_string .= $packages_days_count ? " {$packages_days_count} дн." : " {$packages_hours_count} ч.";
		}

		$phone = $this->get_phone($id, false);
		if ( ! ($rating)) {
			$rating = 0;
		}
		$city = $this->city_get($city_id);


		$response = array(
			"name_1"               => $name_1,
			"name_2"               => $name_2,
			"name_3"               => $name_3,
			"phone"                => $phone,
			"city"                 => $city,
			"works_count"          => $works_count,
			"packages_value_count" => (int) $packages_value_count,
			"packages_days_count"  => (int) $packages_days_count,
			"packages_hours_count" => (int) $packages_hours_count,
			"packages_remain"      => $packages_remain_string,
			"photo_url"            => $photo_url
		);

		if ($user_type == 0) { //мастер
			$response["rating"] = (float) $rating;
		} else {
			$response["rating"] = 0;
		}

		//баланс
		$main_balance = 0;
		$worker_bonus = 0;
		$client_bonus = 0;
		$arSelect     = array("ID", "IBLOCK_ID", "PROPERTY_160", "PROPERTY_161");
		$arFilter     = array("IBLOCK_ID" => 80, "=PROPERTY_160" => array(31230, 31231, 31234), "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if ($arFields["PROPERTY_160_VALUE"] == 31230) {
				$main_balance = $arFields["PROPERTY_161_VALUE"]; //основной
			} else if ($arFields["PROPERTY_160_VALUE"] == 31231) {
				$worker_bonus = $arFields["PROPERTY_161_VALUE"]; //бонусный - мастер
			} else if ($arFields["PROPERTY_160_VALUE"] == 31234) {
				$client_bonus = $arFields["PROPERTY_161_VALUE"]; //бонусный - заказчик
			}
		}
		if ($user_type == 0) { //мастер
			$response["money"] = (float) $main_balance;
			$response["bonus"] = (float) $worker_bonus;
		} else {
			$response["money"] = 0;
			$response["bonus"] = (float) $client_bonus;
		}

		$response["refund"] = 0; //В РАЗРАБОТКЕ

		//заказы и отзывы
		$total_orders             = array();
		$total_feedbacks          = array();
		$arOrder                  = array("id" => "asc");
		$arFilter                 = array();
		$arGroupBy                = false;
		$arNavStartParams         = false;
		$arSelectFields           = array("ID", "UF_CRM_RATING_MAIN", "STAGE_ID");
		$arFilter['=CATEGORY_ID'] = '7';
		$arFilter['!=STAGE_ID']   = 'C7:LOSE';
		$lose_stages              = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		if ($user_type == 0) { //мастер
			$arFilter['=UF_CRM_MASTER'] = $id;
		} elseif ($user_type == 1) { //заказчик
			$arFilter['=CONTACT_ID'] = $id;
		}
		$res = CCrmDeal::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		while ($ob = $res->GetNext()) {
			if (in_array($ob["STAGE_ID"], (array) $lose_stages)) {
				continue;
			}
			$total_orders[] = $ob["ID"];
			if ($ob["UF_CRM_RATING_MAIN"]) {
				$total_feedbacks[] = $ob["ID"];
			}
		}
		$response["orders"]    = count($total_orders);
		$response["feedbacks"] = count($total_feedbacks);

		return $this->getSuccess($response);
	}


	//Получение города по координатам
	function getCityByLocation($lat, $lon) {
		$city_id  = $this->city_search($lat, $lon);
		$city     = $this->city_get($city_id);
		$response = array('city' => $city);

		return $this->getSuccess($response);
	}


	//Поиск по городам
	function getCitySearchResults($text) {
		$cities         = array();
		$federal_cities = array(34927, 34928, 34926);
		if ( ! ($text)) {
			$arOrder          = array("PROPERTY_182" => "desc");
			$arNavStartParams = array("nPageSize" => 20);
			$arFilter         = array("IBLOCK_ID" => 84, "ID" => "");
		} else {
			$arOrder          = array();
			$arNavStartParams = false;
			$arFilter         = array("IBLOCK_ID" => 84, "NAME" => $text . "%");
		}
		$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_177", "PROPERTY_178", "PROPERTY_182", "PROPERTY_LATITUDE", "PROPERTY_LONGITUDE");
		$res      = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$city     = array(
				"id"      => (int) $arFields["ID"],
				"title"   => $arFields["NAME"],
				"lat"     => (double) $arFields["PROPERTY_LATITUDE_VALUE"] ?? 0,
				"lon"     => (double) $arFields["PROPERTY_LONGITUDE_VALUE"] ?? 0,
				"summary" => $arFields["PROPERTY_178_VALUE"] . " " . $arFields["PROPERTY_177_VALUE"]
			);
			if (in_array($arFields["ID"], (array) $federal_cities)) {
				$city['summary'] = "";
			}
			$cities[] = $city;
		};
		//формируем ответ
		$response = array('list' => $cities);

		return $this->getSuccess($response);
	}


	//Установка города для пользователя
	function setUserCity($id, $user_type, $city_id) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		CModule::IncludeModule("crm");
		$this->update_field_contact($id, "UF_CRM_NEW_CITY", $city_id);
		$this->set_contact_coordinates($id, false, false);
		$this->event_contact_update_city($id, $city_id);
		//обновление таблицы регистраций (корректировка)
		$extra_utils->register_update($id);

		return $this->getSuccess("");
	}


	//Обновление работ пользователя
	function setUserWorks($id, $user_type, $items) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		$this->master_type_check($user_type);
		$categories = array();
		$unit_works = array();
		foreach ($items as $key => $work) {
			if ($work['type'] == 1 or $work['type'] == 2) {
				if ( ! (in_array($work['id'], (array) $categories))) {
					$categories[] = $work['id'];
				}
			} else if ($work['type'] == 3) {
				//проверяем в работах
				$check               = false;
				$checking_element_id = $work['id'];
				$arSelect            = array("ID");
				$arFilter            = array("IBLOCK_ID" => 66, "=ID" => $checking_element_id);
				$res                 = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$check         = true;
					$local_work_id = $checking_element_id;
				};
				//проверяем в поисковых запросах
				if ( ! ($check)) {
					$local_work_id = $extra_utils->search_requests_work($checking_element_id);
				}
				if ($local_work_id) {
					$unit_works[]  = $local_work_id;
					$main_category = $this->get_main_category($work['type'], $local_work_id);
					if ( ! (in_array($main_category, (array) $categories))) {
						$categories[] = $main_category;
					}
				}
			}
		}
		$entity = new CCrmContact;
		$fields = array(
			"UF_CRM_UNDER_CATEGORY" => $categories,
			"UF_CRM_WORK_TYPE"      => $unit_works,
		);
		$entity->update($id, $fields);
		//обновление таблицы регистраций
		$extra_utils->register_add($id);

		return $this->getSuccess("");
	}


	//Получение работ пользователя
	function getUserWorks($id, $user_type) {
		$this->user_type_check($user_type);
		$works            = array();
		$arOrder          = array();
		$arFilter         = array("ID" => $id);
		$arGroupBy        = false;
		$arNavStartParams = false;
		$arSelectFields   = array("UF_CRM_UNDER_CATEGORY", "UF_CRM_WORK_TYPE");
		$res              = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$categories = $ob["UF_CRM_UNDER_CATEGORY"];
			$unit_works = $ob["UF_CRM_WORK_TYPE"];
		}
		//собираем категории и подкатегории
		foreach ($categories as $key => $category) {
			$arSelect = array("DEPTH_LEVEL", "NAME");
			$arFilter = array("IBLOCK_ID" => 66, "GLOBAL_ACTIVE" => "Y", "DEPTH_LEVEL" => array(1, 2), "ID" => $category);
			$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
			while ($ar_result = $db_list->GetNext()) {
				if ($ar_result["DEPTH_LEVEL"] == 1) {
					$type = 1;
				} else {
					$type = 2;
				}
				$work    = array("id" => (int) $category, "title" => $ar_result["NAME"], "type" => $type);
				$works[] = $work;
			};
		}
		//собираем работы
		foreach ($unit_works as $key => $unit_work) {
			$arSelect = array("NAME");
			$arFilter = array("IBLOCK_ID" => 66, "ACTIVE" => "Y", "ID" => $unit_work);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();
				$work     = array("id" => (int) $unit_work, "title" => $arFields["NAME"], "type" => 3);
				$works[]  = $work;
			};
		}
		$response = array(
			'list' => $works,
		);

		return $this->getSuccess($response);
	}


	//Обновление каналов уведомлений
	function setUserNotificationChannels($id, $user_type, $channels) {
		$this->user_type_check($user_type);
		$true_channels = array(34354, 34355); //проставляю неактивные каналы по умолчанию включенными (если потом активируем) (профиль, прочее)
		foreach ($channels as $key => $channel) {
			if ($channel['is_active']) {
				$true_channels[] = $channel['id'];
			}
		}
		CModule::IncludeModule("crm");
		$this->update_field_contact($id, 'UF_CRM_PUSH_AVAILABLE', $true_channels);

		return $this->getSuccess("");
	}


	//Получение каналов уведомлений
	function getUserNotificationChannels($id, $user_type) {
		$this->user_type_check($user_type);
		global $yes;
		$channels = array();
		//получаем включенные
		$true_channels = array();
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $id);
		$arSelectFields = array("UF_CRM_PUSH_AVAILABLE");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$true_channels = $ob["UF_CRM_PUSH_AVAILABLE"];
		}
		//собираем все возможные
		$arSelect = array("ID", "IBLOCK_ID", "NAME");
		$arFilter = array("IBLOCK_ID" => 94, "=PROPERTY_302" => $yes);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields  = $ob->GetFields();
			$is_active = false;
			if (in_array($arFields["ID"], (array) $true_channels)) {
				$is_active = true;
			}
			$channel    = array(
				'id'        => (int) $arFields["ID"],
				'title'     => $arFields["NAME"],
				'is_active' => $is_active,
			);
			$channels[] = $channel;
		};
		$response = array(
			'list' => $channels,
		);

		return $this->getSuccess($response);
	}


	//Получение списка уведолмений
	function getNotifications($id, $user_type, $page) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		$this->user_type_check($user_type);
		global $yes;
		global $no;
		global $setup_element;
		$notifications = array();
		if ($user_type == 0) {
			$user_type_code = 31169;
		} else {
			$user_type_code = 31170;
		}
		$types = array(0 => 34352, 1 => 34354, 2 => 34355, 3 => 34353, 4 => 34351); // 1 и 2 не используются
		//установка постраничного участка
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_202", "PROPERTY_230");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields          = $ob->GetFields();
			$units_per_page    = $arFields["PROPERTY_202_VALUE"];
			$max_regular_views = $arFields["PROPERTY_230_VALUE"];
		};
		if ($page and $page != 0) {
			$min_unit = $page * $units_per_page;
			$max_unit = ($page * $units_per_page) + $units_per_page - 1;
		} else {
			$min_unit = 0;
			$max_unit = $units_per_page - 1;
		}
		//собираем уведомления
		$arSelect   = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_204", "PROPERTY_205", "PROPERTY_218", "PROPERTY_219", "PROPERTY_229", "DATE_CREATE", "PROPERTY_372"); //, "PROPERTY_210", , "PROPERTY_380"
		$arFilter   = array("IBLOCK_ID" => 90, "=PROPERTY_206" => $user_type_code, "=PROPERTY_208" => $yes);
		$arFilter[] = array(
			"LOGIC" => "OR",
			array("=PROPERTY_205" => 34316),
			array("=PROPERTY_205" => 34315, "=PROPERTY_207" => $id),
		);
		$unit       = -1;
		$res        = CIBlockElement::GetList(array('ID' => 'desc'), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$notification_id = $arFields["ID"];
			/*
            //проверка однотипности старая
            if ($arFields["PROPERTY_205_VALUE"] == 34316 and $arFields["PROPERTY_229_VALUE"]) { //общее по регулярному шаблону
                $regular_views_id = array();
                $arSelect2 = array("ID");
                $arFilter2 = array("IBLOCK_ID"=>90, " < ID"=>$arFields["ID"], "=PROPERTY_206"=>$user_type_code, "=PROPERTY_229"=>$arFields["PROPERTY_229_VALUE"], "=PROPERTY_210"=>$id);
                $res2 = CIBlockElement::GetList(array('ID' => 'desc'), $arFilter2, false, false, $arSelect2);
                while($ob2 = $res2->GetNextElement()) {
                    $arFields2 = $ob2->GetFields();
                    $regular_views_id[] = $arFields2["ID"];
                }
                $regular_views = count($regular_views_id);
                if ($regular_views >= $max_regular_views) {continue;}
            }
            */
			//проверка однотипности новая
			if ($arFields["PROPERTY_205_VALUE"] == 34316 and $arFields["PROPERTY_229_VALUE"]) { //общее по регулярному шаблону
				$notification_issue = $extra_utils->template_notification_issue_check($id, $user_type, $arFields["PROPERTY_229_VALUE"], $arFields["ID"], $max_regular_views);
				if ( ! ($notification_issue)) {
					continue;
				}
			}
			//формируем список
			$unit = $unit + 1;
			if ($unit < $min_unit) {
				continue;
			} else if ($unit > $max_unit) {
				break;
			}
			if ($arFields["PROPERTY_219_VALUE"]) {
				$item_id = $arFields["PROPERTY_219_VALUE"];
			} else {
				$item_id = 0;
			}
			$type = array_search($arFields["PROPERTY_218_VALUE"], $types);
			if ( ! ($type) and $type !== 0) {
				$type = 4;
			}
			$date = date_create($arFields['DATE_CREATE']);
			if ($arFields["PROPERTY_372_VALUE"]) {
				date_add($date, date_interval_create_from_date_string($arFields["PROPERTY_372_VALUE"] . " minutes"));
			}
			$correction_period = -2;
			date_add($date, date_interval_create_from_date_string($correction_period . " hours"));
			$date_timestamp  = date_timestamp_get($date);
			$notification    = array(
				'id'      => (int) $arFields["ID"],
				'item_id' => (int) $item_id,
				'type'    => $type,
				'date'    => $date_timestamp,
				'title'   => $arFields["NAME"],
				'summary' => $this->html_decode($arFields["~PROPERTY_204_VALUE"]["TEXT"]),
			);
			$notifications[] = $notification;
			//отметить просмотренным
			/*
            $watch_list = $arFields["PROPERTY_380_VALUE"];
            if ($id and !(in_array($id, $watch_list))) {
                $watch_list[] = $id;
                $PROP = array(380 => $watch_list);
                CIBlockElement::SetPropertyValuesEx($notification_id, 90, $PROP);
            }
            */
		};
		//сбросить счетчик новых в контакте
		if ($user_type == 0) {
			$this->update_field_contact($id, "UF_CRM_NEW_NOTIF_WORKER", array());
		} else {
			$this->update_field_contact($id, "UF_CRM_NEW_NOTIF_CLIENT", array());
		}
		$response = array('list' => $notifications);

		return $this->getSuccess($response);
	}


	//Поиск по работам
	function getWorkSearchResults($text, $category = null) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		$works       = array();
		$weight      = array();

		//собираем категории
		$arSelect = array();
		if ( ! ($text)) {
			$arFilter = array('IBLOCK_ID' => 66, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1); //"NAME"=>"___".$text." % "
			$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
			while ($ar_result = $db_list->GetNext()) {
				$name = $ar_result['NAME'];
				if (substr($name, 0, 3) == '---') {
					$name = substr($name, 3);
				}
				if (strpos($name, '---')) {
					$name = substr($name, 0, strpos($name, '---'));
				}
				$work = array("id" => (int) $ar_result['ID'], "title" => $name, "type" => 1);
				//узнать количество запросов
				$count     = 0;
				$arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_269");
				$arFilter2 = array("IBLOCK_ID" => 109, "=PROPERTY_267" => $ar_result['ID']);
				$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
				while ($ob2 = $res2->GetNextElement()) {
					$arFields   = $ob2->GetFields();
					$line_count = $arFields["PROPERTY_269_VALUE"];
					$count      = $count + $line_count;
				};
				//пишем 2 параллельных массива, работы и их вес по запросам
				$works[]  = $work;
				$weight[] = $count;
			};
			array_multisort($weight, SORT_DESC, $works);
		}

		//непустой запрос
		if ($text) {
			//собираем категории
			$all_categories = array();
			$search_array   = $extra_utils->search_wide_category($text);
			foreach ($search_array as $key => $search_element) {
				$arFilter = array('IBLOCK_ID' => 66, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
				$arFilter = array_merge($arFilter, $search_element);
				$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
				while ($ar_result = $db_list->GetNext()) {
					if ( ! (in_array($ar_result['ID'], (array) $all_categories))) {
						$all_categories[] = $ar_result['ID'];
					}
				}
			}

			if ($all_categories) {
				$arFilter = array('IBLOCK_ID' => 66, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1); //"NAME"=>"___".$text." % "
				$db_list  = CIBlockSection::GetList(array($by => $order), array('ID' => $all_categories), true, $arSelect);
				while ($ar_result = $db_list->GetNext()) {
					$name = $ar_result['NAME'];
					if (substr($name, 0, 3) == '---') {
						$name = substr($name, 3);
					}
					if (strpos($name, '---')) {
						$name = substr($name, 0, strpos($name, '---'));
					}
					$work = array("id" => (int) $ar_result['ID'], "title" => $name, "type" => 1);
					//узнать количество запросов
					$count     = 0;
					$arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_269");
					$arFilter2 = array("IBLOCK_ID" => 109, "=PROPERTY_267" => $ar_result['ID']);
					$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
					while ($ob2 = $res2->GetNextElement()) {
						$arFields   = $ob2->GetFields();
						$line_count = $arFields["PROPERTY_269_VALUE"];
						$count      = $count + $line_count;
					};
					//пишем 2 параллельных массива, работы и их вес по запросам
					$works[]  = $work;
					$weight[] = $count;
				};
				array_multisort($weight, SORT_DESC, $works);
			}

			//собираем подкатегории
			$subcategory_works = array();
			$arSelect          = array();
			$arFilter          = array('IBLOCK_ID' => 66, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 2, $extra_utils->search_wide($text));
			$db_list           = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
			while ($ar_result = $db_list->GetNext()) {
				$work                = array("id" => (int) $ar_result['ID'], "title" => $ar_result['NAME'], "type" => 2);
				$subcategory_works[] = $work;
			};
			$works = array_merge($works, $subcategory_works);
			//собираем работы
			$unit_works = array();
			$arSelect   = array("ID", "NAME");
			$arFilter   = array("IBLOCK_ID" => 66, "ACTIVE" => "Y", $extra_utils->search_wide($text));
			$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields     = $ob->GetFields();
				$work         = array("id" => (int) $arFields["ID"], "title" => $arFields["NAME"], "type" => 3);
				$unit_works[] = $work;
			};
			$works = array_merge($works, $unit_works);
			//собираем поисковые запросы
			$search_requests_works = $extra_utils->search_requests_find($text);
			if ($search_requests_works) {
				$works = array_merge($works, $search_requests_works);
			}
			//итоговая сортировка
			$works = $extra_utils->search_sort($works, $text);
		}
		//формируем ответ
		$response = array('list' => $works);

		return $this->getSuccess($response);
	}


	//Получение количества мастеров по локации и работе
	function getWorkWorkersCount($id, $work_id, $work_type, $lat, $lon) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		$this->set_contact_coordinates($id, $lat, $lon);
		//поиск города
		CModule::IncludeModule("crm");
		//проверка города
		$real_city = $this->city_search($lat, $lon);
		if ($id) {
			$user_city = $this->get_contact_city($id);
			$city      = $user_city;
		} else {
			$city = $real_city;
		}
		if ($user_city and $user_city != $real_city) {
			$coordinates = $this->get_city_coordinates($user_city);
			$lat         = $coordinates['lat'];
			$lon         = $coordinates['lon'];
		}
		//подмена работы, если это поисковый запрос
		if ($work_type == 3) {
			$local_work_id = $extra_utils->search_requests_work($work_id);
			if ($local_work_id) {
				$work_id = $local_work_id;
			}
		}
		//просчет количества масетров
		$right_workers = array();
		$main_category = $this->get_main_category($work_type, $work_id);
		if ($work_type == 3) {
			$subcategory = $this->get_subcategory($work_id);
		} else if ($work_type == 2) {
			$subcategory = $work_id;
		}
		$arFilter                             = array();
		$arFilter["=UF_CRM_NEW_APP_REGISTER"] = 31169;
		if ($city) {
			$arFilter["=UF_CRM_NEW_CITY"] = $city;
		}
		$arFilter[0] = array(
			"LOGIC" => "OR",
			array("=UF_CRM_UNDER_CATEGORY" => $main_category),
		);
		if ($subcategory) {
			$arFilter[0][] = array("=UF_CRM_UNDER_CATEGORY" => $subcategory);
		}
		if ($work_type == 3) {
			$arFilter[0][] = array("=UF_CRM_WORK_TYPE" => $work_id);
		}
		$arSelectFields = array("ID");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$right_workers[] = $ob['ID'];
		}
		$count_workers = count($right_workers);
		//запись в таблицу запросов
		if ($city and $main_category) {
			//ищем запись
			$time_point = date_create(date('Y-m-d H:i:s', time()));
			$arSelect   = array("ID", "IBLOCK_ID", "PROPERTY_269");
			$arFilter   = array("IBLOCK_ID" => 109, "=PROPERTY_272" => $city, "=PROPERTY_267" => $main_category);
			if ($work_type == 3) {
				$arFilter["=PROPERTY_273"] = $work_id;
			} else {
				$arFilter["=PROPERTY_273"] = false;
			}
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields   = $ob->GetFields();
				$element_id = $arFields["ID"];
				$workers    = $arFields["PROPERTY_269_VALUE"];
			};
			//изменяем текущую
			if ($element_id) {
				$PROP      = array();
				$PROP[269] = $workers + 1;
				$PROP[270] = date_format($time_point, 'd.m.Y H:i:s');
				$PROP[274] = (int) $count_workers;
				CIBlockElement::SetPropertyValuesEx($element_id, 109, $PROP);
			} //создаем новую
			else {
				$el        = new CIBlockElement;
				$PROP      = array();
				$PROP[272] = $city;
				$PROP[267] = $main_category;
				$PROP[269] = 1;
				$PROP[270] = date_format($time_point, 'd.m.Y H:i:s');
				$PROP[274] = (int) $count_workers;
				if ($work_type == 3) {
					$PROP[273] = $work_id;
				}
				$arLoadProductArray = array(
					"IBLOCK_ID"       => 109,
					"NAME"            => "Запись счетчика запросов",
					"PROPERTY_VALUES" => $PROP,
				);
				$element_id         = $el->Add($arLoadProductArray);
			}
			if ($count_workers === 0) {
				if (CModule::IncludeModule("bizproc")) {
					$wfId = CBPDocument::StartWorkflow(
						212,
						array("lists", "BizprocDocument", $element_id),
						array_merge(),
						$arErrorsTmp
					);
				}
			}
		}
		$response = array('count' => (int) $count_workers);

		return $this->getSuccess($response);
	}


	//Получение заказов
	function getOrders($id, $user_type, $type, $page, $lat, $lon, $zoom) {
		$this->user_type_check($user_type);
		$this->set_contact_coordinates($id, $lat, $lon);
		include_once('extra_utils.php');
		$extra_utils = new ExtraUtils();
		global $portal_url;
		global $yes;
		global $no;
		global $setup_element;
		CModule::IncludeModule("crm");
		//основной запрос
		$categories                 = array();
		$unit_works                 = array();
		$under_categories           = array();
		$works                      = array();
		$orders                     = array();
		$weight                     = array();
		$order_key                  = 0;
		$coord                      = array();
		$orders_by_responds_active  = array();
		$orders_by_responds_timeout = array();
		$orders_by_responds_closed  = array();
		$orders_by_responds         = array();
		$all_deals                  = array();
		//проверка аккаунта на тестовый
		$is_test = $this->test_account_check($id);
		//проверка города
		$user_city = $this->get_contact_city($id);
		if ($lat and $lon) {
			$real_city = $this->city_search($lat, $lon);
		}

		//если список - берем город из пользователя, если карта - реальный по координатам
		if ($user_city != $real_city) {
			if ( ! ($zoom)) {
				$coordinates = $this->get_city_coordinates($user_city);
				$lat         = $coordinates['lat'];
				$lon         = $coordinates['lon'];
				$city        = $user_city;
			} else {
				$city = $real_city;
			}
		}

		//установка города для пользователя
		if ( ! ($user_city) and $real_city) {
			$this->update_field_contact($id, "UF_CRM_NEW_CITY", $real_city);
			$this->event_contact_update_city($id, $real_city);
			//обновление таблицы регистраций (корректировка)
			$extra_utils->register_update($id);
		}

		//установка постраничного участка и проч. настроек
		$corrective_units = 2;
		$arSelect         = array("ID", "IBLOCK_ID", "PROPERTY_201", "PROPERTY_318");
		$arFilter         = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res              = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$orders_per_page = $arFields["PROPERTY_201_VALUE"];
			$refund_days     = $arFields["PROPERTY_318_VALUE"];
		};
		if ($page and $page != 0) {
			$min_unit = ($page * $orders_per_page) - $corrective_units;
			$max_unit = ($page * $orders_per_page) + $orders_per_page - 1;
		} else {
			$min_unit = 0;
			$max_unit = $orders_per_page - 1;
		}

		//получаем список заказов
		$arOrder                        = array("UF_CRM_ACTUALTIME" => "desc", "id" => "desc");
		$arFilter                       = array();
		$arGroupBy                      = false;
		$arNavStartParams               = false;
		$arSelectFields                 = array(
			'ID',
			'TITLE',
			'UF_CRM_MASTER',
			'OPPORTUNITY',
			'UF_CRM_UNDER_CATEGORY',
			'UF_CRM_WORK_TYPE',
			'BEGINDATE',
			'STAGE_ID',
			'UF_CRM_BEGINTIME',
			'UF_CRM_ACTUALTIME',
			'UF_CRM_1551801303',
			'UF_CRM_NEW_CITY',
			'UF_CRM_LATITUDE',
			'UF_CRM_LONGITUDE',
			'UF_CRM_1548512221',
			'UF_CRM_ACCEPT_PERCENT',
			'UF_CRM_SERVICES_AVAILABLE',
			'UF_CRM_COUNTER_WATCHEDMASTERS',
			'UF_CRM_COUNTER_HIDEMASTERS',
			'UF_CRM_FEEDBACK_PROCESS',
			'UF_CRM_CLOSED_BY_WORKER',
			'UF_CRM_INWORK_TIME',
			'UF_CRM_CONFIRMATION_ON',
			'UF_CRM_CANCELED_WORKERS',
			'UF_CRM_SPECIAL_SIGN',
			'UF_CRM_1548513476',
			"UF_ADDRESS_NEW",
			"UF_CRM_DEAL_ADDRESS",
			'UF_CRM_MAIN_TYPE'
		);
		$arFilter['=CATEGORY_ID']       = '7';
		$arFilter['!=UF_CRM_MAIN_TYPE'] = 34603;
		//получаем все работы мастера из контакта
		if ($type == 0) {
			$arFilter2       = array("=ID" => $id);
			$arSelectFields2 = array("UF_CRM_UNDER_CATEGORY", "UF_CRM_WORK_TYPE");
			$res2            = CCrmContact::GetListEx(array(), $arFilter2, false, false, $arSelectFields2);
			while ($ob2 = $res2->GetNext()) {
				$categories = $ob2["UF_CRM_UNDER_CATEGORY"];
				$unit_works = $ob2["UF_CRM_WORK_TYPE"];
			}
		}

		//получаем полный список папок и работ (со вложенными)
		if ($categories) {
			$arSelect3 = array("ID");
			$arFilter3 = array("=SECTION_ID" => $categories, "GLOBAL_ACTIVE" => "Y", "DEPTH_LEVEL" => 2);
			$db_list3  = CIBlockSection::GetList(array($by => $order), $arFilter3, true, $arSelect3);
			while ($ar_result3 = $db_list3->GetNext()) {
				$under_categories[] = $ar_result3["ID"];
			};
			if ( ! ($categories)) {
				$categories = array();
			}
			if ( ! ($under_categories)) {
				$under_categories = array();
			}
			$categories = array_values(array_unique(array_merge($categories, $under_categories)));
			$arSelect3  = array("ID");
			$arFilter3  = array("IBLOCK_ID" => 66, "ACTIVE" => "Y", "=SECTION_ID" => $categories);
			$res3       = CIBlockElement::GetList(array(), $arFilter3, false, false, $arSelect3);
			while ($ob3 = $res3->GetNextElement()) {
				$arFields3 = $ob3->GetFields();
				$works[]   = $arFields3["ID"];
			};
			if ( ! ($unit_works)) {
				$unit_works = array();
			}
			if ( ! ($works)) {
				$works = array();
			}
			$unit_works = array_values(array_unique(array_merge($unit_works, $works)));
		}


		array_multisort((array) $categories);
		array_multisort((array) $unit_works);
		//echo("категории ".json_encode($categories)." < br>");
		//echo("работы ".json_encode($unit_works)." < br>");

		//расчет координат
		if ($lat and $lon) {
			$radius = $this->get_radius($zoom);
			if ( ! ($radius)) {
				$arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_183");
				$arFilter2 = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
				$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
				while ($ob2 = $res2->GetNextElement()) {
					$arFields2 = $ob2->GetFields();
					$radius    = $arFields2["PROPERTY_183_VALUE"];
				};
				$page_check = true;
			} else {
				$page_check = false;
			}
			$lat_min = $lat - ($radius / 111.134);
			$lat_max = $lat + ($radius / 111.134);
			$lon_min = $lon - ($radius / (111.321 * cos(deg2rad($lat))));
			$lon_max = $lon + ($radius / (111.321 * cos(deg2rad($lat))));
			$coord   = array(
				'lat'     => $lat,
				'lon'     => $lon,
				'lat_min' => $lat_min,
				'lat_max' => $lat_max,
				'lon_min' => $lon_min,
				'lon_max' => $lon_max,
				'radius'  => $radius
			);
			//echo ('lat '.$lat_min.' to '.$lat_max.'<br>lon '.$lon_min.' to '.$lon_max);
		} else {
			$page_check = true;
		}

		switch ($type) {
			case "0":  //заказы, отображаемые на главной
				if ($lat and $lon) {
					$arFilter["(int)>=UF_CRM_LATITUDE"]  = $lat_min;
					$arFilter["(int)<=UF_CRM_LATITUDE"]  = $lat_max;
					$arFilter["(int)>=UF_CRM_LONGITUDE"] = $lon_min;
					$arFilter["(int)<=UF_CRM_LONGITUDE"] = $lon_max;
				} else {
					$arNavStartParams = array("nPageSize" => 50); //если нет города и координат пользователя - 50 последних заказов
				}
				$arFilter['=STAGE_ID'] = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
				break; // ДЛЯ ВЫДАЧИ ЗАКАЗОВ ТОЛЬКО ПО ВЫБРАННЫМ КАТЕГОРИЯМ - УДАЛИТЬ ЭТУ СТРОКУ
				$arFilter[] = array(
					"LOGIC" => "OR",
					array("=UF_CRM_UNDER_CATEGORY" => $categories),
					array("=UF_CRM_WORK_TYPE" => $unit_works),
				);
				break;
			case "1":  //мои активные заказы
				if ($user_type == 0) { //мастер
					$arFilter['=STAGE_ID']      = array('C7:FINAL_INVOICE', 'C7:2', 'C7:6', 'C7:1');
					$arFilter['=UF_CRM_MASTER'] = $id;
					$state                      = 3;
					$state_title                = "Я являюсь исполнителем заказа, заказ активен";
					$orders_by_responds_active  = $this->get_orders_by_responds($id, true, true);
					$orders_by_responds_timeout = $this->get_orders_by_responds($id, true, false);
					$orders_by_responds         = array_values(array_unique(array_merge($orders_by_responds_active, $orders_by_responds_timeout)));
					//echo "orders_by_responds_active ".json_encode($orders_by_responds_active)." orders_by_responds_timeout ".json_encode($orders_by_responds_timeout);
					array_multisort($orders_by_responds);
				} else if ($user_type == 1) { //заказчик
					$arFilter['=STAGE_ID']    = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING', 'C7:FINAL_INVOICE', 'C7:1', 'C7:2', 'C7:6', 'C7:3');
					$arFilter['!=CONTACT_ID'] = false;
					$arFilter['=CONTACT_ID']  = $id;
				}
				break;
			case "2":  //мои закрытые заказы
				if ($user_type == 0) { //мастер
					$arFilter['=STAGE_ID']      = array('C7:1', 'C7:WON', 'C7:3');
					$arFilter['=UF_CRM_MASTER'] = $id;
					$state                      = 4;
					$state_title                = "Я являюсь исполнителем заказа, заказ завершен";
					$orders_by_responds_closed  = $this->get_orders_by_responds($id, false, false);
					$orders_by_responds         = array_values(array_unique($orders_by_responds_closed));
					array_multisort($orders_by_responds);
				} else if ($user_type == 1) { //заказчик
					$arFilter['=STAGE_ID']    = array('C7:WON', 'C7:3', 'C7:LOSE', 'C7:5', 'C7:1', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
					$arFilter['!=CONTACT_ID'] = false;
					$arFilter['=CONTACT_ID']  = $id;
				}
				break;
		}

		//промежуточный запрос и объединение всех id
		$resLocal = CCrmDeal::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, array("ID"));
		while ($obLocal = $resLocal->GetNext()) {
			$all_deals[] = $obLocal['ID'];
		}
		if ($orders_by_responds) {
			if ( ! ($all_deals)) {
				$all_deals = array();
			}
			$all_deals = array_values(array_unique(array_merge($all_deals, $orders_by_responds)));
			//array_multisort($all_deals, SORT_DESC);
		}

		//дозапрос для ленты - оранжевые заказы
		if ($type == 0 and $lat and $lon and ! ($zoom)) {
			$feed_orange                   = array();
			$arFilter['=STAGE_ID']         = array('C7:FINAL_INVOICE', 'C7:2', 'C7:6', 'C7:1', 'C7:WON', 'C7:3', 'C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
			$time_point_feed               = DateTime::createFromPhp(new \DateTime())->add("-1 days");
			$arFilter['>UF_CRM_BEGINTIME'] = $time_point_feed->format('d.m.Y H:i:s');
			$arFilter['!=UF_CRM_MASTER']   = $id; //УБРАТЬ, ЕСЛИ НАДО ВЫДАВАТЬ СВОИ В ЛЕНТЕ ТОЖЕ			
			$resFeed2                      = CCrmDeal::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, array("ID", "UF_CRM_BEGINTIME"));
			while ($obFeed2 = $resFeed2->GetNext()) {
				if (DateTime::createFromPhp(new \DateTime($obFeed2['UF_CRM_BEGINTIME']))->getTimestamp() < $time_point_feed->getTimestamp()) {
					continue;
				}
				$feed_orange[] = $obFeed2['ID'];
			}
			if ($feed_orange) {
				if ( ! ($all_deals)) {
					$all_deals = array();
				}
				$all_deals = array_values(array_unique(array_merge($all_deals, $feed_orange)));
			}
		}

		//основной запрос по параметрам
		$unit = -1;
		if ($all_deals) {
			$res = CCrmDeal::GetListEx($arOrder, array("=ID" => $all_deals), $arGroupBy, $arNavStartParams, $arSelectFields);
			while ($ob = $res->GetNext()) {
				//echo "id's ".$ob['ID']."<br>";
				$state_color = 0;
				//проверка тестовой системы
				if ($ob['UF_CRM_SPECIAL_SIGN'] == 'тест' and ! ($is_test)) {
					continue;
				}

				//проверка координат и скрытия
				if ($type == 0 and $user_type == 0) {
					if (in_array($id, (array) $ob['UF_CRM_COUNTER_HIDEMASTERS'])) {
						continue;
					}
				}

				if ($type == 0 and $user_type == 0 and $lat and $lon) {
					$distance_lat = (abs($ob['UF_CRM_LATITUDE'] - $lat)) * 111.134;
					$distance_lon = (abs($ob['UF_CRM_LONGITUDE'] - $lon)) * 111.321 * cos(deg2rad($lat));
					$distance_sqr = pow($distance_lat, 2) + pow($distance_lon, 2);
					$distance     = sqrt($distance_sqr);
					//echo("dist ".$distance." rad ".$radius."<br>");
					if ($distance > $radius) {
						continue;
					}
				}

				//проверка попадания неактивных откликов при активных заказах
				$active_stages = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING'); //убираю отсюда стартовые 'C7:new', 'C7:PREPARATION'
				if ($type == 2 and $user_type == 0) {
					if (in_array($ob['STAGE_ID'], (array) $active_stages)) {
						continue;
					}
				}

				//разделение стадий исполнено и оценки для заказчика
				$border_stages = array('C7:1', 'C7:3');
				if ($type == 1 and $user_type == 1 and in_array($ob['STAGE_ID'], (array) $border_stages)) {
					if ($ob['UF_CRM_FEEDBACK_PROCESS'] == 30957) {
						continue;
					}
				}

				if ($type == 2 and $user_type == 1 and in_array($ob['STAGE_ID'], (array) $border_stages)) {
					if ($ob['UF_CRM_FEEDBACK_PROCESS'] != 30957) {
						continue;
					}
				}

				//разделение стадии оценки для мастера
				if ($type == 1 and $user_type == 0 and $ob['STAGE_ID'] == 'C7:1') {
					if ($ob['UF_CRM_CLOSED_BY_WORKER'] == 30957) {
						continue;
					}
				}

				if ($type == 2 and $user_type == 0 and $ob['STAGE_ID'] == 'C7:1') {
					if ($ob['UF_CRM_CLOSED_BY_WORKER'] != 30957) {
						continue;
					}
				}

				//проверка страницы
				if ($page_check) {
					$unit = $unit + 1;
					if ($unit < $min_unit) {
						continue;
					} else if ($unit > $max_unit) {
						break;
					}
				}

				//получаем ссылку на файл
				$url    = null;
				$rsFile = CFile::GetByID($ob['UF_CRM_1551801303']);
				$arFile = $rsFile->Fetch();
				if ($arFile) {
					$url = $portal_url . "/upload/" . $arFile['SUBDIR'] . "/" . $arFile['FILE_NAME'];
				}

				//проверки свойств заказа (что доступно)
				if (in_array(30952, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
					$is_suggest_price_available = true;
				} else {
					$is_suggest_price_available = false;
				}

				if (in_array(30953, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
					$is_chat_available = true;
				} else {
					$is_chat_available = false;
				}


				if (in_array(30954, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
					$is_call_available = true;
				} else {
					$is_call_available = false;
				}

				if (in_array(30955, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
					$is_whatsapp_available = true;
				} else {
					$is_whatsapp_available = false;
				}

				//дата подачи заказа
				if ($ob['UF_CRM_ACTUALTIME']) {
					$date = date_create($ob['UF_CRM_ACTUALTIME']);
				} else if ($ob['UF_CRM_BEGINTIME']) {
					$date = date_create($ob['UF_CRM_BEGINTIME']);
				} else {
					$date = date_create($ob['BEGINDATE']);
				}

				$date_timestamp = date_timestamp_get($date);
				//установка статусов
				if ($user_type == 0) {
					$state_obj = $extra_utils->get_order_status_worker($id, $ob['ID'], $ob['STAGE_ID'], $ob['UF_CRM_MASTER'], $ob['UF_CRM_CLOSED_BY_WORKER'], $ob['UF_CRM_CANCELED_WORKERS']);
				} else {
					$state_obj = $extra_utils->get_order_status_client($id, $ob['ID'], $ob['STAGE_ID'], $ob['UF_CRM_FEEDBACK_PROCESS'], $ob['UF_CRM_CONFIRMATION_ON']);
				}
				$state       = $state_obj['state'];
				$state_color = $state_obj['state_color'];
				$state_title = $state_obj['state_title'];
				//ПРОШЛОЕ ОПРЕДЕЛЕНИЕ СТАТУСОВ БЫЛО ЗДЕСЬ
				//проверка возврата
				$refund_time_check = false;
				if ($ob['UF_CRM_INWORK_TIME']) {
					$inwork_time = date_create($ob['UF_CRM_INWORK_TIME']);
					date_add($inwork_time, date_interval_create_from_date_string($refund_days . " days"));
					$refund_available_before = $inwork_time;
					$time_point              = date_create(date('Y-m-d H:i:s', time()));
					if ($time_point < $refund_available_before) {
						$refund_time_check = true;
					}
				}
				$refund_respond_check = $this->respond_refund_check($id, $ob['ID']);
				$refund_stages        = array('C7:2', 'C7:6');
				if (in_array(49528, (array) $ob['UF_CRM_SERVICES_AVAILABLE']) and $user_type == 0 and $state == 3 and ! (in_array($ob['STAGE_ID'], (array) $refund_stages)) and $refund_time_check and $refund_respond_check) {
					$is_refund_available = true;
				} else {
					$is_refund_available = false;
				}
				//получение названия города
				$city_name = "";
				if ($ob['UF_CRM_NEW_CITY']) {
					$arSelect4 = array("ID", "IBLOCK_ID", "NAME");
					$arFilter4 = array("IBLOCK_ID" => 84, "=ID" => $ob['UF_CRM_NEW_CITY']);
					$res4      = CIBlockElement::GetList(array(), $arFilter4, false, false, $arSelect4);
					while ($ob4 = $res4->GetNextElement()) {
						$arFields4 = $ob4->GetFields();
						$city_name = $arFields4["NAME"];
					};
				}
				//получение числа новых событий
				$counts = $this->get_order_counts($id, $user_type, $ob['ID']);
				//формируем название
				$title          = $ob['TITLE'];
				$category_title = $this->get_section_name(66, $ob['UF_CRM_UNDER_CATEGORY']);
//                if ($title != $category_title) {
//                    $title = $category_title.' - '.$title;
//                }

				//название локации, координаты из поля адреса
				//17.06.2021 - Смена свойства адреса на UF_CRM_DEAL_ADDRESS. Временно ставим проверку на пустое значение
				$location = ! ! $ob["UF_CRM_DEAL_ADDRESS"] ? $ob["UF_CRM_DEAL_ADDRESS"] : $ob['UF_CRM_1548513476'];

				$location_pos   = strpos($location, "|");
				$location_title = substr($location, 0, $location_pos);
				if ( ! ($location_title)) {
					$location_title = $city_name;
				}
				$location_lat = 0;
				$location_lon = 0;
				if ($ob["UF_CRM_MAIN_TYPE"] != 34603 and $location and $location_pos) {
					$location_pos = $location_pos + 1;
					$location     = substr($location, $location_pos);
					$lon_pos      = strpos($location, ";");
					$location_lat = substr($location, 0, $lon_pos);
					$location_lon = substr($location, ($lon_pos + 1));
				}
				if ( ! ($location_lat) or ! ($location_lon)) {
					$location_lat = $ob['UF_CRM_LATITUDE'];
					$location_lon = $ob['UF_CRM_LONGITUDE'];
				}
				//собираем массив заказа
				$order = array(
					'id'                         => (int) $ob['ID'],
					'title'                      => $this->quot_decode($title),
					'category_title'             => $this->quot_decode($category_title),
					'cover'                      => $url,
					'price'                      => (int) $ob['OPPORTUNITY'],
					//'price_accepted'=>0,
					'state'                      => $state,
					'state_title'                => $state_title,
					'state_color'                => $state_color,
					'location'                   => array(
						'title' => $location_title,
						'lat'   => (float) $location_lat,
						'lon'   => (float) $location_lon
					),
					'summary'                    => $this->quot_decode($ob['UF_CRM_1548512221']),
					//'rate'=>round($ob['UF_CRM_ACCEPT_PERCENT'], 1),
					'date'                       => $date_timestamp,
					'is_visited'                 => false,
					'is_suggest_price_available' => $is_suggest_price_available,
					'is_refund_available'        => $is_refund_available,
					//'is_chat_available'=>$is_chat_available,
					//'is_call_available'=>$is_call_available,
					//'is_whatsapp_available'=>$is_whatsapp_available,
					'count_new_messages'         => (int) $counts['messages'],
					'count_new_workers'          => (int) $counts['workers'],
					'count_new_events'           => (int) $counts['events'],
					'phone'                      => array(),
				);

				//сортировка по вхождению в работы
				if ($type == 0) {
					$order_category  = $ob['UF_CRM_UNDER_CATEGORY'];
					$order_work_type = $ob['UF_CRM_WORK_TYPE'];
					if (in_array($order_category, (array) $categories) or in_array($order_work_type, (array) $unit_works)) {
						$weight[] = $order_key;
					} else {
						$weight[] = $order_key + 1000;
					}
					$order_key++;
				}

				//если открывался мастером
				if ($user_type == 0) {
					if (in_array($id, (array) $ob['UF_CRM_COUNTER_WATCHEDMASTERS'])) {
						$order['is_visited'] = true;
					}
				}
				//собираем массив заказов
				$orders[] = $order;
			}
		}
		//итоговая сортировка
		if ($type == 0) {
			array_multisort($weight, SORT_ASC, $orders);
		}
		$response = array(
			'list' => $orders,
		);

		return $this->getSuccess($response);
	}


	//Получение деателей одного заказа
	function getOrderDetails($id, $user_type, $order_id) {
		$this->user_type_check($user_type);
		include_once('extra_utils . php');
		$extra_utils = new ExtraUtils();
		global $portal_url;
		global $yes;
		global $no;
		global $setup_element;
		$response = array();
		$order    = array();
		$details  = array();
		//проверка аккаунта на тестовый
		$is_test = $this->test_account_check($id);
		//получаем данные по заказу
		CModule::IncludeModule("crm");
		$arOrder          = array("id" => "asc");
		$arFilter         = array();
		$arGroupBy        = false;
		$arNavStartParams = false;
		$arSelectFields   = array(
			'ID',
			'TITLE',
			'STAGE_ID',
			'OPPORTUNITY',
			'CONTACT_ID',
			'UF_CRM_MASTER',
			'UF_CRM_1551801303',
			'UF_CRM_1548512221',
			'UF_CRM_ACCEPT_PERCENT',
			'UF_CRM_SERVICES_AVAILABLE',
			'UF_CRM_COUNTER_WATCHEDMASTERS',
			'UF_CRM_COUNTER_ORDERCOMPL',
			'UF_CRM_FEEDBACK_PROCESS',
			'UF_CRM_CONFIRMATION_ON',
			'UF_CRM_ACTUALTIME',
			'UF_CRM_BEGINTIME',
			'BEGINDATE',
			'UF_CRM_NEW_CITY',
			'UF_CRM_LATITUDE',
			'UF_CRM_LONGITUDE',
			'UF_CRM_CLOSED_BY_WORKER',
			'UF_CRM_COUNTER_ORDERCOMPL_WORKER',
			'UF_CRM_INWORK_TIME',
			'UF_CRM_CANCELED_WORKERS',
			'UF_CRM_UNDER_CATEGORY',
			'UF_CRM_SPECIAL_SIGN',
			'UF_CRM_1548513476',
			"UF_CRM_DEAL_ADDRESS",
			"UF_ADDRESS_NEW",
			'UF_CRM_MAIN_TYPE'
		);
		$arFilter['=ID']  = $order_id;
		$res              = CCrmDeal::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		$state_color      = 0;
		while ($ob = $res->GetNext()) {
			//проверка тестовой системы
			if ($ob['UF_CRM_SPECIAL_SIGN'] == 'тест' and ! ($is_test)) {
				return $this->getFailure("Данный заказ не найден", false, null);
			}
			if ($user_type == 0) {
				$phone            = $this->get_phone($ob['CONTACT_ID'], true); //получение телефона заказчика
				$details['phone'] = $phone;
				if ( ! ($phone)) {
					$details['phone'] = array();
				}
				//ФИО
				if ($ob['CONTACT_ID']) {
					$res2 = CCrmContact::GetListEx(array(), array("=ID" => $ob['CONTACT_ID']), false, false, array("NAME", "LAST_NAME", "SECOND_NAME"));
					while ($ob2 = $res2->GetNext()) {
						$details['name'] = $ob2["LAST_NAME"] . ' ' . $ob2["NAME"] . ' ' . $ob2["SECOND_NAME"];
					}
				} else {
					$details['name'] = null;
				}
				//ПРОШЛОЕ ОПРЕДЕЛЕНИЕ СТАТУСОВ БЫЛО ЗДЕСЬ
				//добавить мастера в посмотревших
				$watchlist = array();
				$watchlist = (array) $ob['UF_CRM_COUNTER_WATCHEDMASTERS'];
				if ( ! (in_array($id, $watchlist, false))) {
					$watchlist[] = $id;
					$this->update_field_deal($order_id, "UF_CRM_COUNTER_WATCHEDMASTERS", $watchlist);
				}
				//закрытые отклики - просмотрены мастером
				$arSelect3 = array("ID", "IBLOCK_ID", "PROPERTY_132");
				$arFilter3 = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $id, "=PROPERTY_134" => $no);
				$res3      = CIBlockElement::GetList(array(), $arFilter3, false, false, $arSelect3);
				while ($ob3 = $res3->GetNextElement()) {
					$arFields3                             = $ob3->GetFields();
					$PROP                                  = array();
					$PROP["ZAKRYTIE_PROSMOTRENO_MASTEROM"] = $yes;
					CIBlockElement::SetPropertyValuesEx($arFields3["ID"], 73, $PROP);
				};
			} else {
				$phone            = $this->get_phone($ob['UF_CRM_MASTER'], true); //получение телефона мастера
				$details['phone'] = $phone;
				if ( ! ($phone)) {
					$details['phone'] = array();
				}
				//ФИО
				if ($ob['UF_CRM_MASTER']) {
					$res2 = CCrmContact::GetListEx(array(), array("=ID" => $ob['UF_CRM_MASTER']), false, false, array("NAME", "LAST_NAME", "SECOND_NAME"));
					while ($ob2 = $res2->GetNext()) {
						$details['name'] = $ob2["LAST_NAME"] . ' ' . $ob2["NAME"] . ' ' . $ob2["SECOND_NAME"];
					}
				} else {
					$details['name'] = null;
				}
				//ПРОШЛОЕ ОПРЕДЕЛЕНИЕ СТАТУСОВ БЫЛО ЗДЕСЬ
			}

			//установка статусов
			if ($user_type == 0) {
				$state_obj = $extra_utils->get_order_status_worker($id, $ob['ID'], $ob['STAGE_ID'], $ob['UF_CRM_MASTER'], $ob['UF_CRM_CLOSED_BY_WORKER'], $ob['UF_CRM_CANCELED_WORKERS']);
			} else {
				$state_obj = $extra_utils->get_order_status_client($id, $ob['ID'], $ob['STAGE_ID'], $ob['UF_CRM_FEEDBACK_PROCESS'], $ob['UF_CRM_CONFIRMATION_ON']);
			}

			$state       = $state_obj['state'];
			$state_color = $state_obj['state_color'];
			$state_title = $state_obj['state_title'];

			$accept_percent = $ob['UF_CRM_ACCEPT_PERCENT'];
			$order_price    = $ob['OPPORTUNITY'];
			$client_id      = $ob['CONTACT_ID'];
			//$details['rate'] = round($ob['UF_CRM_ACCEPT_PERCENT'], 1);

			//----------считаем комиссию------------
			$rate  = (round($accept_percent, 1) * 0.01);
			$price = $order_price;

			//чтение общих настроек
			$arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_275", "PROPERTY_318");
			$arFilter2 = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
			$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
			while ($ob2 = $res2->GetNextElement()) {
				$arFields2   = $ob2->GetFields();
				$count_type  = $arFields2["PROPERTY_275_VALUE"];
				$refund_days = $arFields2["PROPERTY_318_VALUE"];
			};

			if ($count_type == 39489) {  //от цены мастера
				$count_price = $price;
			} else {
				$count_price = $order_price;
			}
			$worker_tax = round(($count_price * $rate), 0);

			//-----------------провекра возврата-------------------
			$refund_time_check = false;
			if ($ob['UF_CRM_INWORK_TIME']) {
				$inwork_time = date_create($ob['UF_CRM_INWORK_TIME']);
				date_add($inwork_time, date_interval_create_from_date_string($refund_days . " days"));
				$refund_available_before = $inwork_time;
				$time_point              = date_create(date('Y-m-d H:i:s', time()));
				if ($time_point < $refund_available_before) {
					$refund_time_check = true;
				}
			}

			$refund_respond_check = $this->respond_refund_check($id, $order_id);

			//----------------------------------
			//$details['rate_amount'] = $worker_tax;
			//получаем ссылку на файл
			$url    = "";
			$rsFile = CFile::GetByID($ob['UF_CRM_1551801303']);
			$arFile = $rsFile->Fetch();
			if ($arFile) {
				$url = $portal_url . "/upload/" . $arFile['SUBDIR'] . "/" . $arFile['FILE_NAME'];
			}

			//проверки свойств заказа (что доступно)
			$res2 = CCrmContact::GetListEx(array(), array("=ID" => $client_id), false, false, array("UF_CRM_NEW_APP_REGISTER"));
			while ($ob2 = $res2->GetNext()) {
				$is_register = (array) $ob2["UF_CRM_NEW_APP_REGISTER"];
			}
			if (in_array(31170, $is_register, false)) {
				$is_suggest_price_available = true;
				if ( ! (in_array(30952, $ob['UF_CRM_SERVICES_AVAILABLE']))) {
					$all_services   = $ob['UF_CRM_SERVICES_AVAILABLE'];
					$all_services[] = 30952;
					$this->update_field_deal($order_id, 'UF_CRM_SERVICES_AVAILABLE', $all_services);
				}
			} else {
				$is_suggest_price_available = false;
				if (in_array(30952, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
					$all_services = $ob['UF_CRM_SERVICES_AVAILABLE'];
					unset($all_services[ array_search(30952, $all_services) ]);
					$this->update_field_deal($order_id, 'UF_CRM_SERVICES_AVAILABLE', $all_services);
				}
			}
			if (in_array(30953, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
				$is_chat_available = true;
			} else {
				$is_chat_available = false;
			}
			if (in_array(30954, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
				$is_call_available = true;
			} else {
				$is_call_available = false;
			}
			if (in_array(30955, (array) $ob['UF_CRM_SERVICES_AVAILABLE'])) {
				$is_whatsapp_available = true;
			} else {
				$is_whatsapp_available = false;
			}

			$refund_stages = array('C7:2', 'C7:6');
			if (in_array(49528, (array) $ob['UF_CRM_SERVICES_AVAILABLE']) and $user_type == 0 and $state == 3 and ! (in_array($ob['STAGE_ID'], (array) $refund_stages)) and $refund_time_check and $refund_respond_check) {
				$is_refund_available = true;
			} else {
				$is_refund_available = false;
			}
			//$details['is_suggest_price_available'] = $is_suggest_price_available;
			//$details['is_chat_available'] = $is_chat_available;
			//$details['is_call_available'] = $is_call_available;
			//$details['is_whatsapp_available'] = $is_whatsapp_available;
			if ($user_type == 1) {
				//закрытие просмотрено
				if ($ob['UF_CRM_COUNTER_ORDERCOMPL'] == 30956) {
					$entity = new CCrmDeal;
					$fields = array(
						"UF_CRM_COUNTER_ORDERCOMPL" => 30957,
					);
					$entity->update($order_id, $fields);
				}
			} else {
				//закрытие просмотрено
				if ($ob['UF_CRM_COUNTER_ORDERCOMPL_WORKER'] == 30956) {
					$entity = new CCrmDeal;
					$fields = array(
						"UF_CRM_COUNTER_ORDERCOMPL_WORKER" => 30957,
					);
					$entity->update($order_id, $fields);
				}
			}
			//----------------ДОСБОРКА--------------------------
			//дата подачи заказа
			if ($ob['UF_CRM_ACTUALTIME']) {
				$date = date_create($ob['UF_CRM_ACTUALTIME']);
			} else if ($ob['UF_CRM_BEGINTIME']) {
				$date = date_create($ob['UF_CRM_BEGINTIME']);
			} else {
				$date = date_create($ob['BEGINDATE']);
			}
			$date_timestamp = date_timestamp_get($date);
			//получение названия города
			$city_name = "";
			if ($ob['UF_CRM_NEW_CITY']) {
				$arSelect4 = array("ID", "IBLOCK_ID", "NAME");
				$arFilter4 = array("IBLOCK_ID" => 84, "=ID" => $ob['UF_CRM_NEW_CITY']);
				$res4      = CIBlockElement::GetList(array(), $arFilter4, false, false, $arSelect4);
				while ($ob4 = $res4->GetNextElement()) {
					$arFields4 = $ob4->GetFields();
					$city_name = $arFields4["NAME"];
				};
			}
			//если открывался мастером
			$is_visited = false;
			if ($user_type == 0) {
				if (in_array($id, (array) $ob['UF_CRM_COUNTER_WATCHEDMASTERS'])) {
					$is_visited = true;
				}
			}

			//получение числа новых событий
			$counts = $this->get_order_counts($id, $user_type, $ob['ID']);
			//получение подсказки
			$hint = $this->get_hint($user_type, $order_id, $state);
			//собираем название
			$title          = $ob['TITLE'];
			$category_title = $this->get_section_name(66, $ob['UF_CRM_UNDER_CATEGORY']);
//            if ($title != $category_title) {
//                $title = $category_title.' - '.$title;
//            }

			//название локации
			//17.06.2021 - Смена свойства адреса на UF_CRM_DEAL_ADDRESS. Временно ставим проверку на пустое значение
			$location = ! ! $ob["UF_CRM_DEAL_ADDRESS"] ? $ob["UF_CRM_DEAL_ADDRESS"] : $ob['UF_CRM_1548513476'];

			$location_pos   = strpos($location, "|");
			$location_title = substr($location, 0, $location_pos);
			if ( ! ($location_title)) {
				$location_title = $city_name;
			}
			$location_lat = 0;
			$location_lon = 0;
			if ($ob["UF_CRM_MAIN_TYPE"] != 34603 and $location and $location_pos) {
				$location_pos = $location_pos + 1;
				$location     = substr($location, $location_pos);
				$lon_pos      = strpos($location, ";");
				$location_lat = substr($location, 0, $lon_pos);
				$location_lon = substr($location, ($lon_pos + 1));
			}

			if ( ! ($location_lat) or ! ($location_lon)) {
				$location_lat = $ob['UF_CRM_LATITUDE'];
				$location_lon = $ob['UF_CRM_LONGITUDE'];
			}
			//собираем массив заказа
			$order = array(
				'id'                         => (int) $ob['ID'],
				'title'                      => $this->quot_decode($title),
				'category_title'             => $this->quot_decode($category_title),
				'cover'                      => $url,
				'price'                      => (int) $ob['OPPORTUNITY'],
				'state'                      => $state,
				'state_title'                => $state_title,
				'state_color'                => $state_color,
				'location'                   => array(
					'title' => $location_title,
					'lat'   => (float) $location_lat,
					'lon'   => (float) $location_lon
				),
				'summary'                    => $this->quot_decode($ob['UF_CRM_1548512221']),
				'date'                       => $date_timestamp,
				'is_visited'                 => $is_visited,
				//'is_chat_available'=>$is_chat_available,
				//'is_call_available'=>$is_call_available,
				//'is_whatsapp_available'=>$is_whatsapp_available,
				'count_new_messages'         => 0, //(int)$counts['messages']  - везде 0, посколькку зашли в заказ = уже просмотрели все события
				'count_new_workers'          => 0, //(int)$counts['workers']
				'count_new_events'           => 0, //(int)$counts['events']
				'is_suggest_price_available' => $is_suggest_price_available,
				'is_refund_available'        => $is_refund_available,
				'phone'                      => $details['phone'],
				'name'                       => $details['name'],
				'hint'                       => $hint
//				'hint2'                      => $hint,
			);
		}
		$response = array(
			'order' => $order,
		);
		//возвращаем ответ
		if ($order['id']) {
			return $this->getSuccess($response);
		} else {
			return $this->getFailure("Данный заказ не найден", false, null);
		}
	}


	//Получение списка отозвавшихся мастеров по заказу
	function getWorkers($id, $user_type, $order_id) {
		global $yes;
		global $no;
		$workers = array();
		if ($user_type == 1) {
			//получение списка мастеров
			$arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_130", "PROPERTY_132", "PROPERTY_136");
			$arFilter2 = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_132" => array(31191, 31193));
			$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
			while ($ob2 = $res2->GetNextElement()) {
				$all_deals = array();
				$arFields2 = $ob2->GetFields();
				$respond   = $arFields2["ID"];
				$worker_id = $arFields2["PROPERTY_136_VALUE"];
				$price     = $arFields2["PROPERTY_130_VALUE"];
				//чтение контакта мастера
				$arFilter3       = array("=ID" => $worker_id);
				$arSelectFields3 = array("NAME", "UF_CRM_RATING_MAIN");
				$res3            = CCrmContact::GetListEx(array(), $arFilter3, false, false, $arSelectFields3);
				while ($ob3 = $res3->GetNext()) {
					$name         = $ob3["NAME"];
					$raiting_main = (float) $ob3["UF_CRM_RATING_MAIN"];
				}
				if ( ! ($raiting_main)) {
					$raiting_main = null;
				}
				//считаем заказы мастера
				$arFilter4       = array("=UF_CRM_MASTER" => $worker_id, "=CATEGORY_ID" => '7', "=STAGE_ID" => array('C7:1', 'C7:3', 'C7:WON'));
				$arSelectFields4 = array("ID");
				$res4            = CCrmDeal::GetListEx(array(), $arFilter4, false, false, $arSelectFields4);
				while ($ob4 = $res4->GetNext()) {
					$all_deals[] = $ob4["ID"];
				}
				$orders_amount = count($all_deals);
				//собираем массив отклика мастера
				$worker    = array(
					'id'            => (int) $worker_id,
					'name'          => $name,
					'price'         => (int) $price,
					'orders_amount' => (int) $orders_amount,
					'rating'        => $raiting_main,
				);
				$workers[] = $worker;
				//отклики просмотренны заказчиком
				$PROP                           = array();
				$PROP["PROSMOTREN_ZAKAZCHIKOM"] = $yes;
				CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
			};
		}
		$response = array(
			'list' => $workers,
		);

		return $this->getSuccess($response);
	}


	//Получение списка медиа по заказу
	function getMedia($id, $user_type, $order_id) {
		global $yes;
		global $no;
		global $portal_url;
		$all_media = array();
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("STAGE_ID", "CONTACT_ID", "UF_CRM_1551801303");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			//получаем ссылку на файл
			$url    = "";
			$rsFile = CFile::GetByID($ob['UF_CRM_1551801303']);
			$arFile = $rsFile->Fetch();
			if ($arFile) {
				$url         = $portal_url . "/upload/" . $arFile['SUBDIR'] . "/" . $arFile['FILE_NAME'];
				$media0      = array(
					'type' => 0,
					'url'  => $url,
				);
				$all_media[] = $media0;
			}
		}
		$response = array(
			'list' => $all_media,
		);

		return $this->getSuccess($response);
	}


	//Создание заказа
	function addOrder($id, $work_id, $work_type, $title, $summary, $price, $file, $lat, $lon, $cloud_order = false, $location = false) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		CModule::IncludeModule("crm");
		$this->set_contact_coordinates($id, $lat, $lon);
		global $yes;
		global $setup_element;
		global $portal_url;
		if ( ! ($yes)) {
			$globals       = $this->set_globals();
			$yes           = $globals['yes'];
			$setup_element = $globals['setup_element'];
			$portal_url    = $globals['portal_url'];
		}
		$this->active_date_contact($id);

		//проверка города
		$user_city = $this->get_contact_city($id);
		$real_city = $this->city_search((float) $lat, (float) $lon);
		if ($user_city and $user_city != $real_city) {
			$coordinates = $this->get_city_coordinates($user_city);
			$lat         = $coordinates['lat'];
			$lon         = $coordinates['lon'];
		} else { //ВРЕМЕННО, БЕРЕМ КООРДИНАТЫ ИЗ ГОРОДА СТАНДАРТНЫЕ
			$coordinates = $this->get_city_coordinates($real_city);
			$lat         = $coordinates['lat'];
			$lon         = $coordinates['lon'];
		}

		$city = $user_city;
		if ( ! ($city)) {
			return $this->getFailure("Пожалуйста, укажите город", false, "city_error");
		}

		//подмена работы, если это поисковый запрос
		if ($work_type == 3) {
			$local_work_id = $extra_utils->search_requests_work($work_id);
			if ($local_work_id) {
				$work_id = $local_work_id;
			}
		}
		//установка коэф-та принятия
		$rate = $this->set_acception_tax($work_type, $work_id, $city, $price);
		//время создания
		$time_point = date_create(date('Y-m-d H:i:s', time()));
		$time       = date_format($time_point, 'd.m.Y H:i:s');
		//проверка регистрации заказчика
		$services_available = array(30954);
		$arOrder            = array();
		$arFilter           = array("=ID" => $id);
		$arGroupBy          = false;
		$arNavStartParams   = false;
		$arSelectFields     = array("UF_CRM_NEW_APP_REGISTER");
		$res                = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

		$is_register = [];
		while ($ob = $res->GetNext()) {
			$is_register = $ob["UF_CRM_NEW_APP_REGISTER"];
		}

		if (in_array(31170, (array) $is_register, true)) {
			$services_available[] = 30952;
		}

		//создаем сделку
		if ( ! ($title)) {
			$title = $this->get_work_title($work_type, $work_id);
		}

		if ($location["title"]) {
			$city_title = $location["title"];
		} else {
			$city_title = $this->get_element_name(84, $city);
		}

		$entity = new CCrmDeal;
		$fields = array(
			'TITLE'                     => $title,
			'CATEGORY_ID'               => '7',
			'STAGE_ID'                  => 'C7:new',
			'OPPORTUNITY'               => $price,
			'UF_CRM_1548512221'         => $summary,
			'UF_CRM_1551801303'         => $file,
			'UF_CRM_ACCEPT_PERCENT'     => $rate,
			'CONTACT_ID'                => $id,
			'UF_CRM_NEW_CITY'           => $city,
			'UF_CRM_1564122612033'      => $city_title,
			'UF_CRM_BEGINTIME'          => $time,
			'UF_CRM_ACTUALTIME'         => $time,
			'UF_CRM_LATITUDE'           => $lat,
			'UF_CRM_LONGITUDE'          => $lon,
			'UF_CRM_REFUND_DECISION'    => 34541,
			'UF_CRM_RECOUNT_DECISION'   => 34620,
			'UF_CRM_MAIN_TYPE'          => 34603,
			'UF_CRM_SERVICES_AVAILABLE' => $services_available,
			'UF_CRM_1548513476'         => $city_title . ' | ' . $lat . ';' . $lon,
			'UF_CRM_DEAL_ADDRESS'       => $city_title . ' | ' . $lat . ';' . $lon,
		);

		if ($work_id and $work_type and $summary and $price) {
			$fields['STAGE_ID']                = 'C7:PREPARATION'; //заполнена - в свободную  (временно отключили - $fields['STAGE_ID'] = 'C7:PREPAYMENT_INVOICE';)
			$fields['UF_CRM_FREEREQUEST_TIME'] = date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s');
			$full_order                        = true;
		} else {
			$fields['STAGE_ID'] = 'C7:PREPARATION'; //не заполнена - в уточнение
			$full_order         = false;
		}

		$section_id                     = $this->get_main_category($work_type, $work_id);
		$fields['UF_CRM_1564122491066'] = $this->get_section_name(66, $section_id);
		switch ($work_type) {     //перебираем типы работ - категория, подкатегория или работа
			case 1:
			case 2:
				$fields['UF_CRM_UNDER_CATEGORY'] = $work_id;
				break;
			case 3:
				$fields['UF_CRM_WORK_TYPE']      = $work_id;
				$fields['UF_CRM_UNDER_CATEGORY'] = $section_id;
				break;
		}

		//проверка на тестовую
		$is_test = $this->test_account_check($id);
		if ($is_test) {
			$fields['UF_CRM_SPECIAL_SIGN'] = 'тест';
		}

		$deal_id = $entity->add($fields);

		if ($full_order and $cloud_order) {  //ВЫВЕШИВАЕМ ЗАПУСК ПРОЦЕССОВ ОТ СВОБОДНОЙ ЗАЯВКИ
			//запуск БП отправки сообщения в ватсап
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					238,
					array("crm", "CCrmDocumentDeal", "DEAL_" . $deal_id),
					array_merge(),
					$arErrorsTmp
				);
			}
			//запуск БП перехода в "нет исполнителей"
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					203,
					array("crm", "CCrmDocumentDeal", "DEAL_" . $deal_id),
					array("target" => "noworkers_timer"),
					$arErrorsTmp
				);
				$this->update_field_deal($deal_id, "UF_CRM_ACTIVE_PROCESS", $wfId);
			}
		}

		//коммент в ленту
		$client_title = $this->get_contact_name($id);
		$client_phone = $this->get_phone($id, false);
		$this->timeline_comment($deal_id, 'Создана заявка, заказчик: [url = ' . $portal_url . ' / crm / contact / details / ' . $id . ' /]' . $client_title . '[/url]~BR~Телефон: ' . $client_phone);

		//проверка наличия мастеров - уведомления
		$right_workers = array();
		$main_category = $this->get_main_category($work_type, $work_id);
		if ($work_type == 3) {
			$subcategory = $this->get_subcategory($work_id);
		} else if ($work_type == 2) {
			$subcategory = $work_id;
		}
		$arFilter                             = array();
		$arFilter["=UF_CRM_NEW_APP_REGISTER"] = 31169;
		if ($city) {
			$arFilter["=UF_CRM_NEW_CITY"] = $city;
		}
		$arFilter[0] = array(
			"LOGIC" => "OR",
			array("=UF_CRM_UNDER_CATEGORY" => $main_category),
		);
		if ($subcategory) {
			$arFilter[0][] = array("=UF_CRM_UNDER_CATEGORY" => $subcategory);
		}
		if ($work_type == 3) {
			$arFilter[0][] = array("=UF_CRM_WORK_TYPE" => $work_id);
		}


		$arSelectFields = array("ID");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			if ($is_test) {
				$test_worker_check = $this->test_account_check($ob['ID']);
				if ( ! ($test_worker_check)) {
					continue;
				}
			}
			$right_workers[] = $ob['ID'];
		}
		$count_workers = count($right_workers);
		if ($count_workers == 0) {
			$this->add_notification($id, 1, 34408, $deal_id, $title); //заказчику - нет мстеров
		} else {
			if ($full_order and $cloud_order) {  //ВЫВЕШИВАЕМ ЗАПУСК ПРОЦЕССОВ ОТ СВОБОДНОЙ ЗАЯВКИ
				$this->add_notification($right_workers, 0, 34417, $deal_id, $title); //мастерам - новая заявка
				$this->update_field_deal($deal_id, 'UF_CRM_HAND_ORDER_PUSH', $yes);
			}
		}
		//формируем ответ
		$response = array('id' => (int) $deal_id);

		return $this->getSuccess($response);
	}


	//Принятие заказа
	function acceptOrder($id, $user_type, $order_id, $package_id, $price) {
		CModule::IncludeModule("crm");
		$this->master_type_check($user_type);
		$this->user_name_check($id);
		global $yes;
		global $no;
		global $setup_element;
		//проставить активность в контакт
		$this->active_date_contact($id);
		//проверка наличия активного отклика
		$all_responds = array();
		$arSelect     = array("ID", "IBLOCK_ID", "PROPERTY_132", "PROPERTY_136");
		$arFilter     = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $id, "=PROPERTY_132" => 31191);
		$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields       = $ob->GetFields();
			$all_responds[] = $arFields["ID"];
		}
		if ($all_responds) {
			return $this->getFailure("Вы уже приняли данный заказ", false, null);
		}
		//чтение заказа
		$true_stages      = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
		$arOrder          = array("id" => "asc");
		$arFilter         = array("=ID" => $order_id);
		$arGroupBy        = false;
		$arNavStartParams = false;
		$arSelectFields   = array("OPPORTUNITY", "CONTACT_ID", "UF_CRM_ACCEPT_PERCENT", "STAGE_ID", "TITLE", "UF_CRM_MAIN_TYPE", "UF_CRM_CLOUD_DEAL_ID", "UF_CRM_CANCELED_WORKERS");
		$res              = CCrmDeal::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		$title            = "";
		while ($ob = $res->GetNext()) {
			$order_price      = $ob['OPPORTUNITY'];
			$client           = $ob['CONTACT_ID'];
			$accept_percent   = $ob['UF_CRM_ACCEPT_PERCENT'];
			$title            = $ob['TITLE'];
			$main_type        = $ob['UF_CRM_MAIN_TYPE'];
			$cloud_deal_id    = $ob['UF_CRM_CLOUD_DEAL_ID'];
			$canceled_workers = $ob['UF_CRM_CANCELED_WORKERS'];
			//проверка стадии заказа
			if ( ! (in_array($ob['STAGE_ID'], (array) $true_stages))) {
				return $this->getFailure("Данный заказ уже невозможно принять", false, null);
			}
		}

		//проверка заявки из облака
		if ($main_type == 40626 and $cloud_deal_id and ! ($canceled_workers)) {
			//--------------------проверить стадию в облаке---------------------
			include_once('rest_api/rest_out.php');
			$rest_sender  = new RestSender();
			$check_result = $rest_sender->checkOrder($cloud_deal_id);
			//------------------------------------------------------------------
			if ( ! ($check_result)) {
				$this->cancelOrder($client, 1, $order_id, false, false, true);

				return $this->getFailure("Данный заказ уже невозможно принять", false, null);
			}
		}

		if ($package_id) {
			$accept_type = 31187;
		} else {
			$accept_type = 31188;
		}

		//----------считаем комиссию------------
		$rate = (round($accept_percent, 1) * 0.01);
		if ( ! ($price)) {
			$price = $order_price;
		}

		//чтение общих настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_275", "PROPERTY_FIX_DEFAULT_CONFIRM_PRICE", "PROPERTY_FIX_CONFIRM_PRICE_UP_TO_500");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

		$fix_confirm_price           = 0;
		$fix_default_confirm_price   = 0;
		$fix_confirm_price_up_to_500 = 0;
		while ($ob = $res->GetNextElement()) {
			$arFields                    = $ob->GetFields();
			$count_type                  = $arFields["PROPERTY_275_VALUE"];
			$fix_default_confirm_price   = $arFields["PROPERTY_FIX_DEFAULT_CONFIRM_PRICE_VALUE"];
			$fix_confirm_price_up_to_500 = $arFields["PROPERTY_FIX_CONFIRM_PRICE_UP_TO_500_VALUE"];
		};

		if ($count_type == 39489) {  //от цены мастера
			$count_price = $price;
		} else {
			$count_price = $order_price;
		}

		// Если  сумма заказа до 500, стоимость комиссии = FIX_CONFIRM_PRICE_UP_TO_500
		if ((int) $count_price <= 500) {
			$fix_confirm_price = $fix_confirm_price_up_to_500;
		} else {
			$fix_confirm_price = $fix_default_confirm_price;
		}

		// Если установлена фиксированная стоимость принятия, то применяем эту стоимость, вычисляем процент от стоимости
		if ($fix_confirm_price > 0) {
			$worker_tax = $fix_confirm_price;
		} else {
			$worker_tax = round(($count_price * $rate), 0);
		}

		//------------------------------------
		//проверка баланса
		if ($accept_type == 31188) {  //отклик за рубли
			$percent_respond = true;
			$balance_amount  = 0;
			$bonus_amount    = 0;
			$arSelect        = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter        = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31230, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
			$res             = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields        = $ob->GetFields();
				$balance_element = $arFields["ID"];
				$balance_amount  = $arFields["PROPERTY_161_VALUE"];
			}
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields        = $ob->GetFields();
				$balance_element = $arFields["ID"];
				$bonus_amount    = $arFields["PROPERTY_161_VALUE"];
			}

			$balance_amount = $balance_amount + $bonus_amount;
			if ($worker_tax > $balance_amount) {
				$money_amount = $worker_tax - $balance_amount;

				return $this->getFailure("У вас недостаточно средств для принятия заказа", $money_amount, null);
			}
		} else {  //отклик за пакет
			$percent_respond = false;
			$time_point      = date_create(date('Y-m-d H:i:s', time()));
			$arSelect        = array("ID", "IBLOCK_ID", "PROPERTY_163", "PROPERTY_250");
			$arFilter        = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31232, "=PROPERTY_167" => $yes, "=PROPERTY_162" => $package_id, ">PROPERTY_200" => date_format($time_point, 'Y-m-d H:i:s'), "=PROPERTY_168" => $id);
			$res             = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields        = $ob->GetFields();
				$balance_element = $arFields["ID"];
				$balance_amount  = $arFields["PROPERTY_163_VALUE"];
				$worker_tax      = $arFields["PROPERTY_250_VALUE"];
			}
			if ( ! ($balance_element) or (1 > $balance_amount)) {
				return $this->getFailure("Вы не можете принять данный заказ по этому тарифу", false, null);
			}
			//проверка границ пакета
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_143", "PROPERTY_144");
			$arFilter = array("IBLOCK_ID" => 75, "=ID" => $package_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields  = $ob->GetFields();
				$min_price = $arFields["PROPERTY_143_VALUE"];
				$max_price = $arFields["PROPERTY_144_VALUE"];
			};
			if ($min_price and ($order_price < $min_price)) {
				return $this->getFailure("Вы не можете принять данный заказ по этому тарифу", false, null);
			}
			if ($max_price and ($order_price > $max_price)) {
				return $this->getFailure("Вы не можете принять данный заказ по этому тарифу", false, null);
			}
			//проставить дату принятия в запись по балансу пакета
			$PROP      = array();
			$PROP[281] = date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s');
			CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
		}

		//создание записи
		$el                 = new CIBlockElement;
		$PROP               = array(
			"TIP_OTKLIKA"             => $accept_type,
			"PREDLOZHENNAYA_STOIMOST" => $price,
			"STOIMOST_DLYA_MASTERA"   => $worker_tax,
			"TIP_PAKETA"              => $package_id,
			"AKTIVEN"                 => 31191,
			"PROSMOTREN_ZAKAZCHIKOM"  => $no,
			"ZAKAZ"                   => $order_id,
			"MASTER"                  => $id,
			"ZAKAZCHIK"               => $client,
		);
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 73,
			"NAME"            => "Отклик на заявку",
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id         = $el->Add($arLoadProductArray);

		//запуск отложенной деактивации
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				197,
				array("lists", "BizprocDocument", $element_id),
				array_merge(),
				$arErrorsTmp
			);
		}

		//проверка регистрации заказчика
		$res2 = CCrmContact::GetListEx(array(), array("=ID" => $client), false, false, array("UF_CRM_NEW_APP_REGISTER"));
		while ($ob2 = $res2->GetNext()) {
			$is_register = $ob2["UF_CRM_NEW_APP_REGISTER"];
		}
		if (in_array(31170, (array) $is_register)) {
			//уведомление заказчику
			$this->add_notification($client, 1, 34412, $order_id, $title);
		} else {
			//автопринятие отклика
			$this->timeline_comment($order_id, 'Заказчик не зарегистрирован в новом приложении, принятие произошло автоматически');
			$this->acceptWorker($client, 1, $order_id, $id);
		}

		return $this->getSuccess("");
	}


	//Выбор мастера для исполнения
	function acceptWorker($id, $user_type, $order_id, $worker_id) {
		$this->client_type_check($user_type);
		global $yes;
		global $no;
		global $portal_url;
		CModule::IncludeModule("crm");

		//чтение сделки
		$cloud_order    = false;
		$late_stages    = array('C7:FINAL_INVOICE', 'C7:1', 'C7:2', 'C7:6', 'C7:3', 'C7:WON', 'C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		$arOrder        = array("id" => "asc");
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("TITLE", "STAGE_ID", "UF_CRM_SERVICES_AVAILABLE", "UF_CRM_MAIN_TYPE", "UF_CRM_CLOUD_DEAL_ID");
		$res            = CCrmDeal::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$title         = $ob['TITLE'];
			$cloud_deal_id = $ob['UF_CRM_CLOUD_DEAL_ID'];
			if ($ob['UF_CRM_MAIN_TYPE'] == 40626) {
				$cloud_order = true;
			}
			if (in_array($ob['STAGE_ID'], (array) $late_stages)) {
				return $this->getFailure("Невозможно выбрать мастера - заказ уже в работе или завершен", false, null);
			}
			$services = $ob['UF_CRM_SERVICES_AVAILABLE'];
		}

		//Получение всех активных/истекших откликов
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_129", "PROPERTY_131", "PROPERTY_132", "PROPERTY_136", "PROPERTY_138");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_132" => array(31191, 31212));
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields       = $ob->GetFields();
			$all_responds[] = $arFields["ID"];
			if ($worker_id == $arFields["PROPERTY_136_VALUE"] and $arFields["PROPERTY_132_VALUE"] == 31191) {
				$true_respond  = $arFields["ID"];
				$respond_type  = $arFields["PROPERTY_129_VALUE"];
				$package_type  = $arFields["PROPERTY_131_VALUE"];
				$respond_price = $arFields["PROPERTY_138_VALUE"];
			}
		};

		if ( ! ($true_respond)) {
			return $this->getFailure("Предложение данного мастера больше не актуально", false, null);
		}

		//провекра и изменение баланса
		if ($respond_type == 31188) {  //отклик за рубли
			$package_title  = "Стандарт";
			$feed_log       = 'Заявка принята за процент от стоимости заказа, стоимость отклика ' . $respond_price . ' руб';
			$balance_amount = 0;
			$bonus_amount   = 0;
			$arSelect       = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter       = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31230, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $worker_id);
			$res            = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields        = $ob->GetFields();
				$balance_element = $arFields["ID"];
				$balance_amount  = $arFields["PROPERTY_161_VALUE"];
			}

			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $worker_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields      = $ob->GetFields();
				$bonus_element = $arFields["ID"];
				$bonus_amount  = $arFields["PROPERTY_161_VALUE"];
			}

			$balance_total = $balance_amount + $bonus_amount;

			if ($respond_price > $balance_total) {
				return $this->getFailure("Предложение данного мастера больше не актуально", false, null);
			} else {
				$refund_available = true;
				//расчет деления платежа
				if ($bonus_element and $bonus_amount) {
					if ($bonus_amount >= $respond_price) {
						$bonus_spend  = $respond_price;
						$balance_part = false;
					} else {
						$bonus_spend   = $bonus_amount;
						$balance_spend = $respond_price - $bonus_spend;
						$balance_part  = true;
					}
					$bonus_part = true;
				} else {
					$balance_spend = $respond_price;
					$balance_part  = true;
					$bonus_part    = false;
				}

				//списание бонусов
				if ($bonus_part) {
					$PROP              = array();
					$PROP["SUMMA_RUB"] = $bonus_amount - $bonus_spend;
					CIBlockElement::SetPropertyValuesEx($bonus_element, 80, $PROP);
					//фиксация транзакции - списание бонусного баланса
					if ($bonus_spend != 0) {
						$this->add_transaction(31240, $bonus_spend, false, $bonus_element, 31231, false, $worker_id, $order_id, "Списание средств за заявку " . $title);
					}
				}
				//списание баланса
				if ($balance_part) {
					$PROP              = array();
					$PROP["SUMMA_RUB"] = $balance_amount - $balance_spend;
					CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
					//фиксация транзакции - списание основного баланса
					if ($balance_spend != 0) {
						$this->add_transaction(31236, $balance_spend, false, $balance_element, 31230, false, $worker_id, $order_id, "Списание средств за заявку " . $title);
					}
				}
			}
		} else {  //отклик за пакет
			$package_title    = $this->get_element_name(75, $package_type);
			$feed_log         = 'Заявка принята за пакет  ' . $package_title;
			$refund_available = false;
			$time_point       = date_create(date('Y-m-d H:i:s', time()));
			$arSelect         = array("ID", "IBLOCK_ID", "PROPERTY_163", "PROPERTY_250");
			$arFilter         = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31232, "=PROPERTY_167" => $yes, "=PROPERTY_162" => $package_type, ">PROPERTY_200" => date_format($time_point, 'Y-m-d H:i:s'), "=PROPERTY_168" => $worker_id);
			$res              = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields          = $ob->GetFields();
				$balance_element   = $arFields["ID"];
				$balance_amount    = $arFields["PROPERTY_163_VALUE"];
				$package_unit_cost = $arFields["PROPERTY_250_VALUE"];
			}
			if ( ! ($balance_element) or (1 > $balance_amount)) {
				return $this->getFailure("Предложение данного мастера больше не актуально", false, null);
			} else {
				//списание баланса
				$PROP      = array();
				$PROP[163] = $balance_amount - 1;
				if ($PROP[163] <= 0) {
					$PROP[167] = $no; //если закончился - ложим флаг активности записи баланса
					$PROP[296] = $this->get_element_name(77, $no);
				}
				CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
				//фиксация транзакции - списание баланса пакета
				$transaction_id = $this->add_transaction(31244, false, 1, $balance_element, 31232, false, $worker_id, $order_id, "Списание единицы пакета за заявку  " . $title);
				CIBlockElement::SetPropertyValuesEx($transaction_id, 87, array(359 => $package_unit_cost));
				//$this->add_order_bonus($worker_id, $order_id);
			}
		}

		//отклик - снятие активности, флаг непросмотра мастером, флаг просомтра заказчиком
		foreach ($all_responds as $key => $respond) {
			$PROP = array();
			if ($respond == $true_respond) {
				$PROP["AKTIVEN"] = 31193;  //выбран заказчиком
			} else {
				$PROP["AKTIVEN"] = 31192;  //неактивен - выбран другой мастер
			}
			$PROP["ZAKRYTIE_PROSMOTRENO_MASTEROM"] = $no;
			$PROP["PROSMOTREN_ZAKAZCHIKOM"]        = $yes;
			CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
		}

		//проверка регистрации в новом приложении
		$register = true;
		$res      = CCrmContact::GetListEx(array(), array("=ID" => $id), false, false, array("UF_CRM_NEW_APP_REGISTER"));
		while ($ob = $res->GetNext()) {
			if ( ! (in_array(31170, (array) $ob["UF_CRM_NEW_APP_REGISTER"]))) {
				$register = false;
			}
		}

		$res = CCrmContact::GetListEx(array(), array("=ID" => $worker_id), false, false, array("UF_CRM_NEW_APP_REGISTER"));
		while ($ob = $res->GetNext()) {
			if ( ! (in_array(31169, (array) $ob["UF_CRM_NEW_APP_REGISTER"]))) {
				$register = false;
			}
		}

		//дебаг возможных косяков с обязательными полями
		$this->update_field_deal($order_id, 'UF_CRM_REFUND_DECISION', 34541);
		$this->update_field_deal($order_id, 'UF_CRM_RECOUNT_DECISION', 34620);

		//проставить мастера в сделку, продвинуть сделку
		$entity = new CCrmDeal;
		$fields = array(
			"UF_CRM_MASTER"       => $worker_id,
			"STAGE_ID"            => "C7:FINAL_INVOICE",
			"UF_CRM_INWORK_TIME"  => date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s'),
			"UF_CRM_REPORT_TARIF" => $package_title,
			"UF_CRM_REPORT_TAX"   => $respond_price,
		);

		if ($register and ! (in_array(30953, (array) $services))) {
			$services[]                          = 30953; //оба есть в приложении -> чат доступен
			$fields["UF_CRM_SERVICES_AVAILABLE"] = $services;
		}

		$worker_title = $this->get_contact_name($worker_id);
		$worker_phone = $this->get_phone($worker_id, false);
		$this->timeline_comment($order_id, 'Работу принял исполнитель: [url = ' . $portal_url . ' / crm / contact / details / ' . $worker_id . ' /]' . $worker_title . '[/url]~BR~Телефон: ' . $worker_phone . '~BR~' . $feed_log);

		//доступность возврата
		if ($refund_available) {
			if ( ! (in_array(49528, (array) $services))) {
				$services[]                          = 49528;
				$fields["UF_CRM_SERVICES_AVAILABLE"] = $services;
			}
		}

		//флаг тревоги
		if ($id == $worker_id) {
			$fields["UF_CRM_ALERT_ACTION"] = 34929;
		}
		$entity->update($order_id, $fields);
		$this->stop_deal_process($order_id);

		//создание записи в заказах мастеров
		$el                 = new CIBlockElement;
		$PROP               = array(
			"ZAKAZ"  => $order_id,
			"MASTER" => $worker_id,
		);
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 82,
			"NAME"            => $title,
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id         = $el->Add($arLoadProductArray);
		$this->add_notification($worker_id, 0, 34423, $order_id, $title);

		//запуск БП сообщения в ватсап
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				239,
				array("crm", "CCrmDocumentDeal", "DEAL_" . $order_id),
				array_merge(),
				$arErrorsTmp
			);
		}

		//запуск БП перехода в "нет исполнителей"
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				203,
				array("crm", "CCrmDocumentDeal", "DEAL_" . $order_id),
				array("target" => "inwork_timer"),
				$arErrorsTmp
			);
			$this->update_field_deal($order_id, "UF_CRM_ACTIVE_PROCESS", $wfId);
		}

		//проверка откликов мастера
		$this->responds_check($worker_id);
		//----------------сделку в облаке - в отказ---------------------
		if ($cloud_order and $cloud_deal_id) {
			include_once('rest_api/rest_out.php');
			$rest_sender = new RestSender();
			$result      = $rest_sender->cancelOrder($cloud_deal_id, '35');
		}

		//--------------------------------------------------------------
		return $this->getSuccess("");
	}


	//Отмена заказа
	function cancelOrder($id, $user_type, $order_id, $reason_text, $forced_cancel, $forced_title, $rest_cancel = false) {
		$this->user_type_check($user_type);
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		//чтение заказа
		$cloud_order = false;
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $order_id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("TITLE", "STAGE_ID", "CONTACT_ID", "UF_CRM_MAIN_TYPE", "UF_CRM_CLOUD_DEAL_ID", "UF_CRM_CANCELED_WORKERS");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$title         = $ob['TITLE'];
			$stage         = $ob['STAGE_ID'];
			$client_id     = $ob['CONTACT_ID'];
			$cloud_deal_id = $ob['UF_CRM_CLOUD_DEAL_ID'];
			if ($ob['UF_CRM_MAIN_TYPE'] == 40626) {
				$cloud_order = true;
			}
			$canceled_workers = $ob['UF_CRM_CANCELED_WORKERS'];
		}
		if ($forced_title) {
			$title = $forced_title;
		}
		if ($user_type == 0) {
			//Получение всех активных и истекших откликов данного мастера + в работе
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_132");
			$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $id, "=PROPERTY_132" => array(31191, 31212, 31193));
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();
				if ($arFields["PROPERTY_132_VALUE"] == 31193) {
					$active_respond = $arFields["ID"];
				}
				$all_responds[] = $arFields["ID"];
			}
			$active_stages = array('C7:new', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
			$inwork_stages = array('C7:FINAL_INVOICE');
			if (in_array($stage, (array) $active_stages)) {
				//$this->add_notification($client_id, 1, 34745, $order_id, $title);
			} else if (in_array($stage, (array) $inwork_stages)) {
				$this->stop_deal_process($order_id);
				if ($reason_text == 'Заказчик отменил заказ сам по телефону') {
					//сценарий как для возврата
					//регистрация запроса
					$el                 = new CIBlockElement;
					$PROP               = array(
						252 => $order_id,
						253 => $id,
						254 => $active_respond
					);
					$arLoadProductArray = array(
						"IBLOCK_ID"       => 103,
						"NAME"            => "Запрос на возврат по заказу " . $title,
						"PROPERTY_VALUES" => $PROP,
					);
					$element_id         = $el->Add($arLoadProductArray);
					//изменение сделки
					$entity = new CCrmDeal;
					$fields = array(
						"STAGE_ID"              => "C7:6",
						"UF_CRM_REFUND_REQUEST" => 30956,
						"UF_CRM_REFUND_ELEMENT" => $element_id,
					);
					$entity->update($order_id, $fields);
					$this->forced_update_field_deal($order_id, "UF_CRM_REFUND_DECISION");
					$this->timeline_comment($order_id, 'Мастер отказался выполнять данный заказ, заявка автоматически перемещена в запрос возврата~BR~Причина: ' . $reason_text . '~BR~Возврат может не пройти при несоблюдении необходимых для него условий');
				} else {
					//проставить отклики отмененными
					foreach ($all_responds as $key => $respond) {
						$PROP                           = array();
						$PROP["AKTIVEN"]                = 31211;  //отменен мастером
						$PROP["PROSMOTREN_ZAKAZCHIKOM"] = $yes;
						$PROP["PRICHINA_OTMENY"]        = $reason_text;
						CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
					}
					//сценарий возврата в свободные
					$this->timeline_comment($order_id, 'Мастер отказался выполнять данный заказ, заявка возвращена в свободные~BR~Причина: ' . $reason_text);
					$canceled_workers[]                = $id;
					$time_point                        = date_create(date('Y-m-d H:i:s', time()));
					$entity                            = new CCrmDeal;
					$fields                            = array(
						"STAGE_ID"                      => "C7:PREPAYMENT_INVOICE",
						"UF_CRM_CANCELED_WORKERS"       => $canceled_workers,
						"UF_CRM_MASTER"                 => "",
						"UF_CRM_ACTUALTIME"             => date_format($time_point, 'd.m.Y H:i:s'),
						"UF_CRM_COUNTER_WATCHEDMASTERS" => array(),
						"UF_CRM_REPORT_TAX"             => false,
						"UF_CRM_REPORT_TARIF"           => false,
					);
					$fields['UF_CRM_FREEREQUEST_TIME'] = date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s');
					$entity->update($order_id, $fields);
					//удалить запись в заказах мастеров
					$arSelect = array("ID", "IBLOCK_ID");
					$arFilter = array("IBLOCK_ID" => 82, "=PROPERTY_169" => $order_id, "=PROPERTY_170" => $id);
					$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
					while ($ob = $res->GetNextElement()) {
						$arFields   = $ob->GetFields();
						$element_id = $arFields["ID"];
					};
					$this->element_delete(82, $element_id);
					//запуск БП перехода в "нет исполнителей"
					if (CModule::IncludeModule("bizproc")) {
						$wfId = CBPDocument::StartWorkflow(
							203,
							array("crm", "CCrmDocumentDeal", "DEAL_" . $order_id),
							array("target" => "noworkers_timer"),
							$arErrorsTmp
						);
						$this->update_field_deal($order_id, "UF_CRM_ACTIVE_PROCESS", $wfId);
					}
				}
			} else {
				return $this->getFailure("В данный момент отменить заказ невозможно", false, null);
			}
		} else {
			//сделку в отказ
			$workers      = array();
			$all_responds = array();
			$entity       = new CCrmDeal;
			$fields       = array(
				"STAGE_ID"             => "C7:11",
				"UF_CRM_LOSE_REASON"   => 2996920,
				"UF_CRM_CANCEL_REASON" => $reason_text,
			);
			if ($forced_cancel) {
				$fields["UF_CRM_LOSE_REASON"] = 35156;
				$fields["STAGE_ID"]           = "C7:LOSE";
			} else if ($rest_cancel) {
				$fields["STAGE_ID"]           = 'C7:5';
				$fields["UF_CRM_LOSE_REASON"] = 46879;
			}
			$entity->update($order_id, $fields);
			//коммент в ленту
			if ($forced_cancel) {
				$this->timeline_comment($order_id, "Заявка была автоматически перемещена в отказ из-за отсутствия исполнителей");
			} else {
				$this->timeline_comment($order_id, "Заказчик отменил заявку из приложения~BR~Причина: " . $reason_text);
			}
			//Получение всех активных откликов
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_136");
			$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_132" => 31191);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields       = $ob->GetFields();
				$all_responds[] = $arFields["ID"];
				$workers[]      = $arFields["PROPERTY_136_VALUE"];
			}
			//проставить неактивными
			foreach ($all_responds as $key => $respond) {
				$PROP                                  = array();
				$PROP["AKTIVEN"]                       = 31192;  //нактивен
				$PROP["ZAKRYTIE_PROSMOTRENO_MASTEROM"] = $no;
				$PROP["PROSMOTREN_ZAKAZCHIKOM"]        = $yes;
				CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
			}
			if ( ! ($rest_cancel)) {
				$this->add_notification($workers, 0, 34424, $order_id, $title);
			}
			if ($forced_cancel) {
				$this->add_notification($id, 1, 34447, $order_id, $title);
			} else {
				//----------------сделку в облаке - в отказ---------------------
				if ($cloud_order and $cloud_deal_id and ! ($rest_cancel)) {
					include_once('rest_api/rest_out.php');
					$rest_sender = new RestSender();
					$result      = $rest_sender->cancelOrder($cloud_deal_id, '6');
				}
				//--------------------------------------------------------------
			}
			$this->stop_deal_process($order_id);
		}

		return $this->getSuccess("");
	}


	//Завершение заказа
	function completeOrder($id, $user_type, $order_id, $price_fact) {
		$this->user_type_check($user_type);
		$worker_type = 31169;
		$client_type = 31170;
		$time_point  = date_create(date('Y-m-d H:i:s', time()));
		CModule::IncludeModule("crm");
		//чтение заказа
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("CONTACT_ID", "TITLE", "ASSIGNED_BY_ID", "UF_CRM_MASTER", "OPPORTUNITY", "STAGE_ID", "UF_CRM_MAIN_TYPE", "UF_CRM_1607157907", "UF_CRM_FEEDBACK_PROCESS", "UF_CRM_CLOSED_BY_WORKER");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$client      = $ob['CONTACT_ID'];
			$title       = $ob['TITLE'];
			$worker      = $ob['UF_CRM_MASTER'];
			$price_start = $ob['OPPORTUNITY'];
			$stage       = $ob['STAGE_ID'];
			$main_type   = $ob['UF_CRM_MAIN_TYPE'];
			$assigned_id = $ob['ASSIGNED_BY_ID'];
			$assigned_id = str_replace('user_', '', $assigned_id);
			if ( ! ($price_fact) or $price_fact == -1) {
				$price_fact = $ob['UF_CRM_1607157907'];
			}
			if ( ! ($price_fact) or $price_fact == -1) {
				$price_fact = $price_start;
			}
		}
		$refund_stages = array('C7:2', 'C7:6');
		if (in_array($stage, (array) $refund_stages)) {
			return $this->getFailure("По данному заказу запрашивается возврат средств, завершение в данный момент невозможно", false, null);
		}
		$close_stages = array('C7:3', 'C7:WON', 'C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		if (in_array($stage, (array) $close_stages) and ! ($user_type == 1 and $stage == 'C7:3' and $ob['UF_CRM_FEEDBACK_PROCESS'] != 30957) and ! ($user_type == 0 and $stage == 'C7:1' and $ob['UF_CRM_CLOSED_BY_WORKER'] != 30957)) {
			return $this->getFailure("Данный заказ уже был завершен", false, null);
		}
		//сделку в оценку
		$this->stop_deal_process($order_id);
		$process_start = null;
		$entity        = new CCrmDeal;
		if ($user_type == 0) {
			//сценарий - уже завершен заказчиком, лежит в оценке, ждет завершения мастером
			if ($user_type == 0 and $stage == 'C7:1' and $ob['UF_CRM_CLOSED_BY_WORKER'] != 30957) {
				$fields = array(
					"UF_CRM_1607157907"       => $price_fact,
					"STAGE_ID"                => "C7:WON",
					"UF_CRM_CLOSED_BY_WORKER" => 30957,
				);
			} //сценарий - штатное первое завершение мастером
			else {
				$fields = array(
					"STAGE_ID"                  => "C7:1",
					"UF_CRM_1607157907"         => $price_fact,
					"UF_CRM_WHO_COMPLETE"       => $worker_type,
					"UF_CRM_COMPLETE_TIME"      => date_format($time_point, 'd.m.Y H:i:s'),
					"UF_CRM_COUNTER_ORDERCOMPL" => 30956,
					"UF_CRM_CLOSED_BY_WORKER"   => 30957,
				);
				$this->add_notification($client, 1, 34422, $order_id, $title);
				$process_start = 'client_waiting';
			}
			//бонусы за заказы мастеру
			$this->add_order_bonus($worker, $order_id);
		} else {
			//сценарий - уже завершен мастером, лежит в оценке, ждет завершения заказчиком
			if ($user_type == 1 and ($stage == 'C7:3' or $stage == 'C7:1') and $ob['UF_CRM_FEEDBACK_PROCESS'] != 30957) {
				if ($stage == 'C7:3') { //исполнено - нельзя передвинуть стадию, не заполн. обязат. поле перерасчета
					$fields = array();
					$this->update_field_deal($order_id, 'UF_CRM_FEEDBACK_PROCESS', 30957);
				} else { //остальные стадии - перемещение в успех
					$fields = array(
						"STAGE_ID"                => "C7:WON",
						"UF_CRM_FEEDBACK_PROCESS" => 30957,
					);
				}
			} //сценарий - штатное первое завершение заказчиком
			else {
				$fields = array(
					"STAGE_ID"                         => "C7:1",
					"UF_CRM_COUNTER_ORDERCOMPL_WORKER" => 30956,
					"UF_CRM_WHO_COMPLETE"              => $client_type,
					"UF_CRM_COMPLETE_TIME"             => date_format($time_point, 'd.m.Y H:i:s'),
					"UF_CRM_FEEDBACK_PROCESS"          => 30957,
				);
				$this->add_notification($worker, 0, 34425, $order_id, $title);
				$process_start = 'worker_waiting';
			}
			//комментарий о стоимости
			if ($price_fact != $price_start) {
				$this->timeline_comment($order_id, "Заказчиком была указана итоговая стоимость работ, отличная от изначальной: " . $price_fact . " руб");
			}
		}
		//проверка перерасчета
		$recount = false;
		if ($user_type == 0 and $main_type == 34604 and $price_start > $price_fact) { //заявка была заведена вручную
			//проверка типа отклика
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_129");
			$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $id, "=PROPERTY_132" => 31193);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields     = $ob->GetFields();
				$respond_type = $arFields["PROPERTY_129_VALUE"];
			}
			if ($respond_type != 31187) { //только если отклик не за пакет
				$recount = true;
			}
		}
		//реализация перерасчета
		if ($recount) {
			$recount_check = $this->respond_recount($order_id, $price_fact);
			if ($recount_check) {
				$fields["STAGE_ID"]               = "C7:3";
				$fields["UF_CRM_RECOUNT_REQUEST"] = 30956;
			}
		}
		//доп. защита завершения
		if ( ! ($fields["STAGE_ID"])) {
			$fields["STAGE_ID"] = "C7:1";
		}
		$entity->update($order_id, $fields);
		if ($recount) {
			$this->forced_update_field_deal($order_id, "UF_CRM_RECOUNT_DECISION");
		}
		//отложенный перенос - ждем отзыв клиента
		if ($process_start == 'client_waiting') {
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					203,
					array("crm", "CCrmDocumentDeal", "DEAL_" . $order_id),
					array("target" => "feedback_timer"),
					$arErrorsTmp
				);
			}
			$this->update_field_deal($order_id, 'UF_CRM_ACTIVE_PROCESS', $wfId);
		}
		//отложенный перенос - ждем завершение мастером
		if ($process_start == 'worker_waiting') {
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					203,
					array("crm", "CCrmDocumentDeal", "DEAL_" . $order_id),
					array("target" => "feedback_timer_worker"),
					$arErrorsTmp
				);
			}
			$this->update_field_deal($order_id, 'UF_CRM_ACTIVE_PROCESS', $wfId);
		}

		return $this->getSuccess("");
	}


	//Актуализация заказа
	function confirmOrder($id, $user_type, $order_id) {
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		$this->client_type_check($user_type);
		$this->active_date_contact($id);
		CModule::IncludeModule("crm");
		$replacing_times = 0;
		//чтение заказа
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("CONTACT_ID", "STAGE_ID", "OPPORTUNITY", "UF_CRM_MASTER", "UF_CRM_1548512221", "UF_CRM_NEW_CITY", "UF_CRM_REPLACING_TIMES", "UF_CRM_UNDER_CATEGORY", "UF_CRM_WORK_TYPE");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$replacing_times = $ob['UF_CRM_REPLACING_TIMES'];
			$stage_id        = $ob['STAGE_ID'];
			$category        = $ob['UF_CRM_UNDER_CATEGORY'];
			$work            = $ob['UF_CRM_WORK_TYPE'];
			$price           = $ob['OPPORTUNITY'];
			$summary         = $ob['UF_CRM_1548512221'];
			$city            = $ob['UF_CRM_NEW_CITY'];
			$worker_id       = $ob['UF_CRM_MASTER'];
		}
		$lose_stages = array('C7:LOSE', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		if (in_array($stage_id, (array) $lose_stages)) {
			//полный возврат
			if ($worker_id) {
				//получить отклик активного мастера
				$arSelect = array("ID", "IBLOCK_ID");
				$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $worker_id, "=PROPERTY_132" => 31193);
				$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields = $ob->GetFields();
					$respond  = $arFields["ID"];
				}
				//отбраковка отклика
				$PROP            = array();
				$PROP["AKTIVEN"] = 31192;  //неактивен
				CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
				//удалить запись в заказах мастеров
				$arSelect = array("ID", "IBLOCK_ID");
				$arFilter = array("IBLOCK_ID" => 82, "=PROPERTY_169" => $order_id, "=PROPERTY_170" => $worker_id);
				$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields   = $ob->GetFields();
					$element_id = $arFields["ID"];
				};
				$this->element_delete(82, $element_id);
			}
			$time_point = date_create(date('Y-m-d H:i:s', time()));
			$entity     = new CCrmDeal;
			$fields     = array(
				"UF_CRM_REPLACING_TIMES"        => 0,
				"UF_CRM_ACTUALTIME"             => date_format($time_point, 'd.m.Y H:i:s'),
				"UF_CRM_CONFIRMATION_ON"        => $no,
				"UF_CRM_COUNTER_WATCHEDMASTERS" => array(),
				"UF_CRM_MASTER"                 => "",
			);
			if (($category or $work) and $city and $summary and $price) {
				$fields['STAGE_ID']                = 'C7:PREPAYMENT_INVOICE'; //заполнена - в свободную
				$fields['UF_CRM_FREEREQUEST_TIME'] = date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s');
				$full_order                        = true;
			} else {
				$fields['STAGE_ID'] = 'C7:PREPARATION'; //не заполнена - в уточнение
				$full_order         = false;
			}
			$entity->update($order_id, $fields);
			//погасить прошлый процесс
			$this->stop_deal_process($order_id);
			//запуск БП перехода в "нет исполнителей"
			if ($full_order) {
				if (CModule::IncludeModule("bizproc")) {
					$wfId = CBPDocument::StartWorkflow(
						203,
						array("crm", "CCrmDocumentDeal", "DEAL_" . $order_id),
						array("target" => "noworkers_timer"),
						$arErrorsTmp
					);
					$this->update_field_deal($order_id, "UF_CRM_ACTIVE_PROCESS", $wfId);
				}
			}
		} else if ($stage_id == 'C7:5') {
			return $this->getFailure("Данный заказ нельзя актуализовать, пожалуйста, сформируйте новый", false, null);
		} else {
			//стандартное переразмещение
			$time_point = date_create(date('Y-m-d H:i:s', time()));
			$entity     = new CCrmDeal;
			$fields     = array(
				"UF_CRM_REPLACING_TIMES"        => $replacing_times + 1,
				"UF_CRM_ACTUALTIME"             => date_format($time_point, 'd.m.Y H:i:s'),
				"UF_CRM_CONFIRMATION_ON"        => $no,
				"UF_CRM_COUNTER_WATCHEDMASTERS" => array(),
			);
			$entity->update($order_id, $fields);
			//погасить прошлый процесс
			$this->stop_deal_process($order_id);
			//запустить новый процесс
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					203,
					array("crm", "CCrmDocumentDeal", "DEAL_" . $order_id),
					array("target" => "replacing"),
					$arErrorsTmp
				);
			}
			$this->update_field_deal($order_id, "UF_CRM_ACTIVE_PROCESS", $wfId);
		}

		return $this->getSuccess("");
	}


	//Скрыть заказ
	function hideOrder($id, $user_type, $order_id) {
		$this->master_type_check($user_type);
		CModule::IncludeModule("crm");
		//чтение заказа
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("UF_CRM_COUNTER_HIDEMASTERS");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$hide_masters = $ob['UF_CRM_COUNTER_HIDEMASTERS'];
		}
		//добавление и перезапись
		if ( ! (in_array($id, (array) $hide_masters))) {
			$hide_masters[] = $id;
		}
		$this->update_field_deal($order_id, "UF_CRM_COUNTER_HIDEMASTERS", $hide_masters);

		return $this->getSuccess("");
	}


	//Запрос возврата по заказу
	function refundOrder($id, $user_type, $order_id, $reason_text) {
		$this->master_type_check($user_type);
		global $yes;
		global $setup_element;
		CModule::IncludeModule("crm");
		//чтение общих настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_318");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields    = $ob->GetFields();
			$refund_days = $arFields["PROPERTY_318_VALUE"];
		}
		//проверить исполнителя, стадию
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("UF_CRM_MASTER", "STAGE_ID", "TITLE", "UF_CRM_INWORK_TIME");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$worker      = $ob['UF_CRM_MASTER'];
			$stage       = $ob['STAGE_ID'];
			$title       = $ob['TITLE'];
			$inwork_time = date_create($ob['UF_CRM_INWORK_TIME']);
		}
		date_add($inwork_time, date_interval_create_from_date_string($refund_days . " days"));
		$refund_available_before = $inwork_time;
		$time_point              = date_create(date('Y-m-d H:i:s', time()));
		$refund_stages           = array('C7:2', 'C7:6');
		if ($worker != $id) {
			return $this->getFailure("Вы не являетесь исполнителем данного заказа, возврат невозможен", false, null);
		}
		if (in_array($stage, (array) $refund_stages)) {
			return $this->getFailure("Вы уже запросили возврат по данному заказу", false, null);
		}
		if ($time_point > $refund_available_before) {
			return $this->getFailure("Время на запрос возврата по данной заявке истекло", false, null);
		}
		//Получение выбранного отклика данного мастера
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_129", "PROPERTY_138", "PROPERTY_249");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $id, "=PROPERTY_132" => 31193);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields     = $ob->GetFields();
			$respond      = $arFields["ID"];
			$respond_type = $arFields["PROPERTY_129_VALUE"];
			$last_refund  = $arFields["PROPERTY_249_VALUE"];
			$worker_tax   = $arFields["PROPERTY_138_VALUE"];
		}
		if ($respond_type != 31188 or $worker_tax == 0) {
			return $this->getFailure("Возврат возможен только при принятии заявки за ненулевой процент от стоимости работ - тариф стандарт", false, null);
		}
		if ($last_refund == $yes) {
			return $this->getFailure("По данной заявке возврат уже был проведен, повторный возврат невозможен", false, null);
		}
		//запрос на возврат прошел
		$this->stop_deal_process($order_id);
		//регистрация запроса
		$el                 = new CIBlockElement;
		$PROP               = array(
			252 => $order_id,
			253 => $id,
			254 => $respond
		);
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 103,
			"NAME"            => "Запрос на возврат по заказу " . $title,
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id         = $el->Add($arLoadProductArray);
		//изменение сделки
		$entity = new CCrmDeal;
		$fields = array(
			"STAGE_ID"              => "C7:2",
			"UF_CRM_REFUND_REQUEST" => 30956,
			"UF_CRM_REFUND_ELEMENT" => $element_id,
			"COMMENTS"              => $reason_text,
		);
		$entity->update($order_id, $fields);
		$this->forced_update_field_deal($order_id, "UF_CRM_REFUND_DECISION");
		$this->timeline_comment($order_id, "Комментарий мастера: " . $reason_text);

		return $this->getSuccess("");
	}


	//Получение списка всех сообщений
	function getMessages($id, $user_type, $chat_id) {
		$messages = array();
		if ( ! ($messages)) {
			$messages = null;
		}
		$response = array('list' => $messages);

		return $this->getFailure("", false, null);
		//return $this->getSuccess($response);
	}


	//Получение списка доступных пакетов по заказу
	function getPackages($id, $user_type, $order_id, $price) {
		$this->master_type_check($user_type);
		global $yes;
		global $no;
		global $setup_element;
		$packages = array();
		CModule::IncludeModule("crm");
		//активные пакеты мастера
		$arSelect      = array("ID", "IBLOCK_ID", "PROPERTY_162", "PROPERTY_163", "PROPERTY_VREMYA_OKONCHANIYA"); //пакет, осталось ед., время заверш.
		$arFilter      = array("IBLOCK_ID" => CDbzConstants::DBZ_BALANCE_IBLOCK_ID, "=PROPERTY_KONTAKT" => $id, "=PROPERTY_AKTIVEN" => $yes, "=PROPERTY_TIP_ZAPISI" => 31232);
		$res           = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		$have_packages = array();
		$balans        = array();
		while ($ob = $res->GetNextElement()) {
			$arFields    = $ob->GetFields();
			$time_point  = DateTime::createFromPhp(new \DateTime())->toUserTime();
			$time_point2 = DateTime::createFromPhp(new \DateTime($arFields["PROPERTY_VREMYA_OKONCHANIYA_VALUE"]));
			$interval    = $time_point->getDiff($time_point2);
			$days        = $interval->d;
			$hours       = $interval->h;
			$total_hours = (int) $days * 24 + (int) $hours;
			//echo (date_format($time_point, 'd.m.Y H:i:s')."   ".date_format($time_point2, 'd.m.Y H:i:s')."  total hours ".$total_hours."<br>");
			if ($time_point2 <= $time_point) {
				continue;
			}
			$have_packages[]           = $arFields["PROPERTY_162_VALUE"];
			$balans[ $arFields["ID"] ] = array(
				'package' => $arFields["PROPERTY_162_VALUE"],
				'amount'  => $arFields["PROPERTY_163_VALUE"],
				'date'    => $arFields["PROPERTY_164_VALUE"],
				'days'    => $days,
				'hours'   => $total_hours,
			);
		};
		//узнать город мастера
		$arFilter       = array("=ID" => $id);
		$arSelectFields = array("UF_CRM_NEW_CITY");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$city = $ob["UF_CRM_NEW_CITY"];
		}
		//вытаскиваем количество заказов в городе по выборке
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_156");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields         = $ob->GetFields();
			$selection_period = $arFields["PROPERTY_156_VALUE"];
		};
		$selection_period = $selection_period * (-1);
		//$time_point       =  DateTime::createFromPhp(new \DateTime())->toUserTime();//date_create(date('Y-m-d', time()));
		//date_add($time_point, date_interval_create_from_date_string($selection_period . " days"));


		$time_point = DateTime::createFromPhp(new \DateTime())->toUserTime();//date_create(date('Y-m-d', time()));
		$time_point = $time_point->add("2 days");

		$loose_stages = array(); //C7:LOSE
		$arOrder      = array("id" => "asc");
		$arFilter     = array(
			"=CATEGORY_ID" => '7',
			">=BEGINDATE"  => $time_point->format("d.m.Y"),
		);
		if ($city) {
			$arFilter["=UF_CRM_NEW_CITY"] = $city;
		}
		$arSelectFields = array("ID", "STAGE_ID");
		$res            = CCrmDeal::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			if (in_array($ob['STAGE_ID'], (array) $loose_stages)) {
				continue;
			}
			$all_deals[] = $ob['ID'];
		}
		$count_deals = count((array) $all_deals);
		//подготавливаем участки
		$interval_settings_fields = array("PROPERTY_193", "PROPERTY_194", "PROPERTY_195", "PROPERTY_196", "PROPERTY_197"); //поля таблицы "Участки активности пакетов"
		$interval_activity_fields = array("PROPERTY_149", "PROPERTY_150", "PROPERTY_151", "PROPERTY_198", "PROPERTY_199"); //поля таблицы "Пакеты заявок"
		$intervals_common         = array();
		$arSelect                 = array("ID", "IBLOCK_ID");
		$arSelect                 = array_merge($arSelect, $interval_settings_fields);
		$arFilter                 = array("IBLOCK_ID" => 88, "=ID" => 32403);
		$res                      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$min_border = 0;
			foreach ($interval_settings_fields as $key => $field) {
				$max_border               = $arFields[ $field . "_VALUE" ];
				$intervals_common[ $key ] = array(
					'min_border' => $min_border,
					'max_border' => $max_border,
				);
				$min_border               = $max_border;
			}
		};

		//если все пакеты для покупки - собираем имеющиеся
		if ($order_id == 0) {
			//перебор имеющихся
			foreach ($balans as $key => $balans_unit) {
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"NAME",
					"PROPERTY_140",
					"PROPERTY_141",
					"PROPERTY_142",
					"PROPERTY_143",
					"PROPERTY_144",
					"PROPERTY_145",
					"PROPERTY_146",
					"PROPERTY_147",
					"PROPERTY_148",
					"PROPERTY_149",
					"PROPERTY_150",
					"PROPERTY_151",
					"PROPERTY_198",
					"PROPERTY_199",
					"PROPERTY_361"
				);
				$arFilter = array("IBLOCK_ID" => 75, "=ID" => $balans_unit['package']);
				$res      = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields = $ob->GetFields();
					if ($arFields["PROPERTY_361_VALUE"] == $no) {
						$is_refund_available = false;
					} else {
						$is_refund_available = true;
					}
					$price_start = $arFields["PROPERTY_143_VALUE"];
					if ( ! ($price_start)) {
						$price_start = 0;
					}
					$package    = array(
						'id'                  => (int) $arFields["ID"],
						'is_active'           => true,
						'title'               => $arFields["NAME"],
						'summary'             => $this->html_decode($arFields["PROPERTY_140_VALUE"]["TEXT"]),
						'amount'              => (int) $arFields["PROPERTY_141_VALUE"],
						'amount_remain'       => (int) $balans_unit['amount'],
						'days'                => (int) $arFields["PROPERTY_142_VALUE"],
						'hours_remain'        => (int) $balans_unit['hours'],
						'price_start'         => (int) $price_start,
						'price_end'           => (int) $arFields["PROPERTY_144_VALUE"],
						'package_price'       => (int) $arFields["PROPERTY_145_VALUE"],
						'price_option'        => (int) $arFields["PROPERTY_146_VALUE"],
						'option_summary'      => $this->html_decode($arFields["PROPERTY_147_VALUE"]["TEXT"]),
						'is_refund_available' => $is_refund_available,
						"title_options"       => "{$arFields['PROPERTY_141_VALUE']} заявки на {$arFields["PROPERTY_142_VALUE"]} дней",
						"title_remain"        => CDbzPackages::getPackageRemainTitle((int) $balans_unit['amount'], $balans_unit['days'], $balans_unit['hours']),
						"title_orders_range"  => CDbzPackages::getPackageRangeTitle((int) $price_start, (int) $arFields["PROPERTY_144_VALUE"]),
					);
					$packages[] = $package;
				}
			}
			$response_rate = null;
			$worker_tax    = null;
		} //если по конкретному заказу - собираем имеющиеся
		else {
			//вытаскиваем цену заказа
			$arOrder        = array("id" => "asc");
			$arFilter       = array("=ID" => $order_id);
			$arSelectFields = array("OPPORTUNITY", "UF_CRM_ACCEPT_PERCENT", "UF_CRM_MAIN_TYPE");
			$res            = CCrmDeal::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);
			$orderType      = CDbzOrder::ORDER_MAIN_TYPE_LIST["MANUAL_ORDER"];
			while ($ob = $res->GetNext()) {
				$order_price   = $ob["OPPORTUNITY"];
				$response_rate = round($ob['UF_CRM_ACCEPT_PERCENT'], 1);
				$orderType     = (int) $ob["UF_CRM_MAIN_TYPE"];
			}

			$button_title = $orderType === CDbzOrder::ORDER_MAIN_TYPE_LIST["MANUAL_ORDER"] ? "Принять за 1 единицу" : "Предложить услугу за 1 единицу";

			//----------считаем комиссию------------
			$rate = ($response_rate * 0.01);
			if ( ! ($price)) {
				$price = $order_price;
			}
			//чтение общих настроек
			$arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_275", "PROPERTY_276");
			$arFilter2 = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
			$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
			while ($ob2 = $res2->GetNextElement()) {
				$arFields2           = $ob2->GetFields();
				$count_type          = $arFields2["PROPERTY_275_VALUE"];
				$package_choose_type = $arFields2["PROPERTY_276_VALUE"];
			};
			if ($count_type == 39489) {  //от цены мастера
				$count_price = $price;
			} else {
				$count_price = $order_price;
			}
			$worker_tax = round(($count_price * $rate), 0);
			//------------------------------------

			if ($price and $package_choose_type == 39489) {
				$order_price = $price;  //Если считать пакеты по предложенной цене мастера
			}

			//перебор имеющихся
			foreach ($balans as $key => $balans_unit) {
				$arSelect = array(
					"ID",
					"IBLOCK_ID",
					"NAME",
					"PROPERTY_140",
					"PROPERTY_141",
					"PROPERTY_142",
					"PROPERTY_143",
					"PROPERTY_144",
					"PROPERTY_145",
					"PROPERTY_146",
					"PROPERTY_147",
					"PROPERTY_148",
					"PROPERTY_149",
					"PROPERTY_150",
					"PROPERTY_151",
					"PROPERTY_198",
					"PROPERTY_199",
					"PROPERTY_361"
				);
				$arFilter = array("IBLOCK_ID" => 75, "=ID" => $balans_unit['package']);
				$res      = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields = $ob->GetFields();
					if ($arFields["PROPERTY_361_VALUE"] == $no) {
						$is_refund_available = false;
					} else {
						$is_refund_available = true;
					}
					$price_start = $arFields["PROPERTY_143_VALUE"];
					if ( ! ($price_start)) {
						$price_start = 0;
					}
					$price_end = $arFields["PROPERTY_144_VALUE"];
					if (($order_price >= $price_start) and ($order_price <= $price_end)) {
						$is_active = true;
					} else if (($order_price >= $price_start) and ! ($price_end)) {
						$is_active = true;
					} else {
						$is_active = false;
					}
					$package    = array(
						'id'                  => (int) $arFields["ID"],
						'is_active'           => $is_active,
						'title'               => $arFields["NAME"],
						'summary'             => $this->html_decode($arFields["PROPERTY_140_VALUE"]["TEXT"]),
						'amount'              => (int) $arFields["PROPERTY_141_VALUE"],
						'amount_remain'       => (int) $balans_unit['amount'],
						'days'                => (int) $arFields["PROPERTY_142_VALUE"],
						'hours_remain'        => (int) $balans_unit['hours'],
						'price_start'         => (int) $price_start,
						'price_end'           => (int) $price_end,
						'package_price'       => (int) $arFields["PROPERTY_145_VALUE"],
						'price_option'        => (int) $arFields["PROPERTY_146_VALUE"],
						'option_summary'      => $this->html_decode($arFields["PROPERTY_147_VALUE"]["TEXT"]),
						'is_refund_available' => $is_refund_available,
						"title_options"       => "{$arFields['PROPERTY_141_VALUE']} заявки на {$arFields["PROPERTY_142_VALUE"]} дней",
						"title_remain"        => CDbzPackages::getPackageRemainTitle((int) $balans_unit['amount'], $balans_unit['days'], $balans_unit['hours']),
						"title_orders_range"  => CDbzPackages::getPackageRangeTitle((int) $price_start, (int) $price_end),
						"button_title"        => $button_title
					);
					$packages[] = $package;
				}
			}
		}

		//перебор прочих доступных (которых у мастера нет)
		//запрос пакетов из таблицы
		$arSelect = array(
			"ID",
			"IBLOCK_ID",
			"NAME",
			"PROPERTY_140",
			"PROPERTY_141",
			"PROPERTY_142",
			"PROPERTY_143",
			"PROPERTY_144",
			"PROPERTY_145",
			"PROPERTY_146",
			"PROPERTY_147",
			"PROPERTY_148",
			"PROPERTY_149",
			"PROPERTY_150",
			"PROPERTY_151",
			"PROPERTY_198",
			"PROPERTY_199",
			"PROPERTY_360",
			"PROPERTY_361"
		);
		$arFilter = array("IBLOCK_ID" => 75, "=PROPERTY_152" => $yes);
		$res      = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$package_id = $arFields["ID"];
			if ($arFields["PROPERTY_361_VALUE"] == $no) {
				$is_refund_available = false;
			} else {
				$is_refund_available = true;
			}
			$max_activations = $arFields["PROPERTY_360_VALUE"];
			if ($max_activations) {
				$elements_found = 0;
				//если ограничено число активаций - поиск в балансовых записях
				$arSelect2 = array("ID", "IBLOCK_ID"); //пакет, осталось ед., время заверш.
				$arFilter2 = array("IBLOCK_ID" => 80, "=PROPERTY_168" => $id, "=PROPERTY_160" => 31232, "=PROPERTY_162" => $package_id);
				$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
				while ($ob2 = $res2->GetNextElement()) {
					$arFields2 = $ob2->GetFields();
					$elements_found++;
				}
				if ($elements_found >= $max_activations) {
					continue;
				}
			}
			$price_start = $arFields["PROPERTY_143_VALUE"];
			if ( ! ($price_start)) {
				$price_start = 0;
			}
			$price_end = $arFields["PROPERTY_144_VALUE"];
			if (in_array($arFields["ID"], (array) $have_packages)) { //уже был в имеющихся
				continue;
			}
			if ($order_id and (($price_start and $order_price < $price_start) or ($price_end and $order_price > $price_end))) { //запрос по конкретному заказу, не подходит по цене
				continue;
			}
			if ($arFields["PROPERTY_148_VALUE"] != $yes) { //включен не везде - заходим на проверку количества
				$package_active  = false;
				$intervals_local = $intervals_common;
				foreach ($interval_activity_fields as $key => $field) {
					$is_active                            = $arFields[ $field . "_VALUE" ];
					$intervals_local[ $key ]['is_active'] = $is_active;
				}
				foreach ($intervals_local as $key => $interval_data) {
					$min_border = $interval_data['min_border'];
					$max_border = $interval_data['max_border'];
					$is_active  = $interval_data['is_active'];
					if ($count_deals >= $min_border and $count_deals < $max_border and $is_active == $yes) {
						$package_active = true; //попали в активный интервал
					}
				}
				if ( ! ($package_active)) {
					continue;
				}
			}
			$package    = array(
				'id'                  => (int) $package_id,
				'is_active'           => true,
				'title'               => $arFields["NAME"],
				'summary'             => $this->html_decode($arFields["PROPERTY_140_VALUE"]["TEXT"]),
				'amount'              => (int) $arFields["PROPERTY_141_VALUE"],
				'amount_remain'       => 0,
				'days'                => (int) $arFields["PROPERTY_142_VALUE"],
				'hours_remain'        => 0,
				'price_start'         => (int) $price_start,
				'price_end'           => (int) $price_end,
				'package_price'       => (int) $arFields["PROPERTY_145_VALUE"],
				'price_option'        => (int) $arFields["PROPERTY_146_VALUE"],
				'option_summary'      => $this->html_decode($arFields["PROPERTY_147_VALUE"]["TEXT"]),
				'is_refund_available' => $is_refund_available,
				"title_options"       => "{$arFields['PROPERTY_141_VALUE']} заявки на {$arFields["PROPERTY_142_VALUE"]} дней",
				"title_remain"        => null,
				"title_orders_range"  => CDbzPackages::getPackageRangeTitle((int) $price_start, (int) $price_end),
				"button_title"        => $button_title
			);
			$packages[] = $package;
		}

		if ($orderType === CDbzOrder::ORDER_MAIN_TYPE_LIST["CLIENT_APP_ORDER"]) {
			$packages = [];
		}

		$response = array(
			'list' => $packages,
		);

		return $this->getSuccess($response);
	}


	//получение информации по принятию за проценты
	function getDefaultPackage($id, $user_type, $order_id, $price) {
		$this->master_type_check($user_type);
		global $yes;
		global $no;
		global $setup_element;

		//вытаскиваем цену заказа
		$arOrder        = array("id" => "asc");
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("OPPORTUNITY", "UF_CRM_ACCEPT_PERCENT", "UF_CRM_MAIN_TYPE", "CONTACT_ID");
		$res            = CCrmDeal::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);
		$orderType      = CDbzOrder::ORDER_MAIN_TYPE_LIST["MANUAL_ORDER"];
		$contactId      = 0;
		while ($ob = $res->GetNext()) {
			$order_price   = $ob["OPPORTUNITY"];
			$response_rate = round($ob['UF_CRM_ACCEPT_PERCENT'], 1);
			$orderType     = (int) $ob["UF_CRM_MAIN_TYPE"];
			$contactId     = (int) $ob["CONTACT_ID"];
		}
		//----------считаем комиссию------------
		$rate = ($response_rate * 0.01);
		if ( ! ($price)) {
			$price = $order_price;
		}
		//чтение общих настроек
		$arSelect2                   = array("ID", "IBLOCK_ID", "PROPERTY_275", "PROPERTY_276", "PROPERTY_304", "PROPERTY_FIX_DEFAULT_CONFIRM_PRICE", "PROPERTY_FIX_CONFIRM_PRICE_UP_TO_500");
		$arFilter2                   = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res2                        = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
		$fix_confirm_price           = 0;
		$fix_default_confirm_price   = 0;
		$fix_confirm_price_up_to_500 = 0;
		while ($ob2 = $res2->GetNextElement()) {
			$arFields2                   = $ob2->GetFields();
			$count_type                  = $arFields2["PROPERTY_275_VALUE"];
			$max_border                  = $arFields2["PROPERTY_304_VALUE"];
			$fix_default_confirm_price   = $arFields2["PROPERTY_FIX_DEFAULT_CONFIRM_PRICE_VALUE"];
			$fix_confirm_price_up_to_500 = $arFields2["PROPERTY_FIX_CONFIRM_PRICE_UP_TO_500_VALUE"];
		};

		if ($count_type == 39489) {  //от цены мастера
			$count_price = $price;
		} else {
			$count_price = $order_price;
		}

		$worker_tax = round(($count_price * $rate), 0);

		//проверка верхней границы
		if ($order_price > $max_border) {
			$rate_amount = null;
		} else {

			if ((int) $order_price <= 500) {
				$fix_confirm_price = $fix_confirm_price_up_to_500;
				$max_border        = 500;
			} else {
				$fix_confirm_price = $fix_default_confirm_price;
			}

			// Если установлена фиксированная стоимость принятия, то возвращаем эту стоимость, иначе возарвщаем процент от стоимости
			if ($fix_confirm_price > 0) {
				$rate_amount = $fix_confirm_price;
			} else {
				$rate_amount = (int) $worker_tax;
			}
		}

		//------------------------------------
		if ($orderType === CDbzOrder::ORDER_MAIN_TYPE_LIST["MANUAL_ORDER"]) {
			$response = array(
				'rate'         => (float) $response_rate,
				'rate_amount'  => $rate_amount,
				'title'        => "Комиссия",
				'summary'      => "Фиксированная комиссия на заказы до " . $max_border . "₽",
				'button_title' => "Принять за {$rate_amount}₽"
			);
		} else {
			$obContact = new CDbzContact($contactId);

			$response = array(
				'rate'         => (float) 0,
				'rate_amount'  => 0,
				'title'        => "Предложить свою услугу",
				'summary'      => $obContact->isContactHidden() ? "Заказчик свяжется с Вами самостоятельно." : "Вы увидите контакт заказчика, если он Вас выберет сразу свяжитесь с ним!",
				'button_title' => "Предложить услугу"
			);
		}


		return $this->getSuccess($response);
	}


	//Подтверждение платежа
	function confirmPayment($id, $user_type, $version, $token, $amount, $payment_type = null) {
		//чтение данных пользователя
		CModule::IncludeModule("crm");
		$arSelectFields = array("NAME", "LAST_NAME", "SECOND_NAME");
		$res            = CCrmContact::GetListEx(array(), array("=ID" => $id), false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$name_1 = $ob["NAME"];
			$name_2 = $ob["LAST_NAME"];
			$name_3 = $ob["SECOND_NAME"];
		}
		$full_name = $name_2 . ' % 20' . $name_1;
		if ($name_3) {
			$full_name = $full_name . ' % 20' . $name_3;
		}
		$full_name = str_replace(' ', ' % 20', $full_name);
		$phone     = $this->get_phone($id, false);
		include_once($_SERVER["DOCUMENT_ROOT"] . '/api/email_sender.php');
		$email_sender = new EmailSender();
		$email        = $email_sender->get_email($id, false);
		$type         = "createPayment";
		$site_url     = "dombezzabot.net";
		$site_root    = "/api/payment/yookassa_payment_sii.php";
		$get_params   = "type=" . $type . "&user_id=" . $id . "&amount=" . $amount . "&payment_token=" . $token . "&full_name=" . $full_name . "&phone=" . $phone . "&email=" . $email . "&method=" . $payment_type . "&version=" . $version;

		$httpClient = new HttpClient();
		$response   = $httpClient->post("https://" . $site_url . $site_root, [
			"type"          => "createPayment",
			"user_id"       => $id,
			"amount"        => $amount,
			"payment_token" => $token,
			"full_name"     => $full_name,
			"email"         => $email,
			"phone"         => $phone,
			"method"        => $payment_type,
			"version"       => $version,
		]);

		$log_element_id = $this->payment_log(5, $get_params, $id);
		$get_response   = json_decode($response, true);
		$log_element_id = $this->payment_log(3, $get_response, $id);


		if ($get_response["status"] != true) {
			$log_element_id = $this->payment_log(4, $this->getFailure("Платеж не прошел", false, null), $id);

			return $this->getFailure("Платеж не прошел", false, null);
		} else {
			$confirmation_url = $get_response["result"]["confirmation"]["confirmation_url"];
			if ( ! ($confirmation_url)) {
				$confirmation_url = "";
			}
			$response = array('url' => $confirmation_url);

			$confirmation_token = $get_response["result"]["confirmation"]["confirmation_token"];

			if ( ! ! $confirmation_token) {
				$response["confirmation_token"] = $confirmation_token;
			}


			$log_element_id = $this->payment_log(4, $this->getSuccess($response), $id);

			return $this->getSuccess($response);
		}
	}


	//Пополнение баланса
	function addMoney($id, $user_type, $amount, $log_element_id) {
		$globals = $this->set_globals();
		$yes     = $globals['yes'];
		$no      = $globals['no'];
		//ищем запись по балансу
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
		$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31230, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$balance_element = $arFields["ID"];
			$balance_amount  = $arFields["PROPERTY_161_VALUE"];
		}
		//изменяем текущую
		if ($balance_element) {
			$PROP              = array();
			$PROP["SUMMA_RUB"] = $balance_amount + $amount;
			CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
		} //создаем новую
		else {
			$balance_element = $this->add_balance_element(31230, $amount, false, false, $id, "Основной баланс");
		}
		//фиксация транзакции
		$this->add_transaction(31235, $amount, false, $balance_element, 31230, $log_element_id, $id, false, "Пополнение основного баланса");
		$this->active_date_contact($id);

		return $this->getSuccess("");
	}


	//Покупка пакета
	function buyPackage($id, $user_type, $package_id) {
		global $yes;
		global $no;
		$this->master_type_check($user_type);
		$this->active_date_contact($id);
		//ищем активную запись по данному пакету
		$time_point = date_create(date('Y-m-d H:i:s', time()));
		$arSelect   = array("ID", "IBLOCK_ID", "PROPERTY_163", "PROPERTY_200");
		$arFilter   = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31232, "=PROPERTY_162" => $package_id, "!=PROPERTY_163" => 0, ">PROPERTY_200" => date_format($time_point, 'Y-m-d H:i:s'), "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$balance_element = $arFields["ID"];
		}
		//проверка наличия такого пакета у мастера уже
		if ($balance_element) {
			return $this->getFailure("У вас уже есть данный тариф", false, null);
		}
		//узнать баланс мастера
		$balance_amount = 0;
		$bonus_amount   = 0;
		$arSelect       = array("ID", "IBLOCK_ID", "PROPERTY_161");
		$arFilter       = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31230, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		$res            = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$balance_element = $arFields["ID"];
			$balance_amount  = $arFields["PROPERTY_161_VALUE"];
		}
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
		$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$bonus_element = $arFields["ID"];
			$bonus_amount  = $arFields["PROPERTY_161_VALUE"];
		}
		$balance_total = $balance_amount + $bonus_amount;
		//чтение данных о пакете
		$action_price = false;
		$arSelect     = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_141", "PROPERTY_142", "PROPERTY_145", "PROPERTY_146", "PROPERTY_152", "PROPERTY_360");
		$arFilter     = array("IBLOCK_ID" => 75, "=ID" => $package_id);
		$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$max_activations = $arFields["PROPERTY_360_VALUE"];
			if ($arFields["PROPERTY_152_VALUE"] == $no) {
				return $this->getFailure("Данный тариф больше не доступен для покупки", false, null);
			}
			if ($max_activations) {
				$elements_found = 0;
				//если ограничено число активаций - поиск в балансовых записях
				$arSelect2 = array("ID", "IBLOCK_ID"); //пакет, осталось ед., время заверш.
				$arFilter2 = array("IBLOCK_ID" => 80, "=PROPERTY_168" => $id, "=PROPERTY_160" => 31232, "=PROPERTY_162" => $package_id);
				$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
				while ($ob2 = $res2->GetNextElement()) {
					$arFields2 = $ob2->GetFields();
					$elements_found++;
				}
				if ($elements_found >= $max_activations) {
					return $this->getFailure("Данный тариф больше не доступен для покупки", false, null);
				}
			}
			if ($arFields["PROPERTY_146_VALUE"]) {
				$package_price = $arFields["PROPERTY_146_VALUE"];
				$package_title = $arFields["NAME"] . " (акция)";
				$action_price  = true;
			} else {
				$package_price = $arFields["PROPERTY_145_VALUE"];
				$package_title = $arFields["NAME"];
			}
			if ($package_price > $balance_total) {
				$money_amount = $package_price - $balance_total;

				return $this->getFailure("На вашем балансе недостаточно средств", $money_amount, null);
			}
			$package_amount = $arFields["PROPERTY_141_VALUE"];
			$package_days   = $arFields["PROPERTY_142_VALUE"];
		};
		//создаем запись баланса по пакету
		$package_balance_element = $this->add_balance_element(31232, false, $package_id, $package_amount, $id, "Пакет " . $package_title);
		//дополняем запись по пакету
		$time_start_point = date_create(date('Y-m-d H:i:s', time()));
		$time_end_point   = date_create(date('Y-m-d H:i:s', time()));
		date_add($time_end_point, date_interval_create_from_date_string($package_days . " days"));
		$PROP      = array();
		$PROP[200] = date_format($time_end_point, 'd.m.Y H:i:s');
		$PROP[295] = date_format($time_end_point, 'd.m.Y');
		$PROP[280] = date_format($time_start_point, 'd.m.Y H:i:s');
		if ($package_amount != 0) {
			$PROP[250] = round(($package_price / $package_amount), 0);
		} else {
			$PROP[250] = 0;
		}
		CIBlockElement::SetPropertyValuesEx($package_balance_element, 80, $PROP);
		//запуск процесса деактивации пакета
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				200,
				array("lists", "BizprocDocument", $package_balance_element),
				array_merge(),
				$arErrorsTmp
			);
		}
		//фиксация транзакции - покупка пакета
		$transaction_id = $this->add_transaction(31243, false, $package_amount, $package_balance_element, 31232, false, $id, false, "Покупка пакета " . $package_title);
		CIBlockElement::SetPropertyValuesEx($transaction_id, 87, array(359 => $package_price));
		//расчет деления платежа
		if ($bonus_element and $bonus_amount) {
			if ($bonus_amount >= $package_price) {
				$bonus_spend  = $package_price;
				$balance_part = false;
			} else {
				$bonus_spend   = $bonus_amount;
				$balance_spend = $package_price - $bonus_spend;
				$balance_part  = true;
			}
			$bonus_part = true;
		} else {
			$balance_spend = $package_price;
			$balance_part  = true;
			$bonus_part    = false;
		}
		//списание бонусов
		if ($bonus_part) {
			$PROP              = array();
			$PROP["SUMMA_RUB"] = $bonus_amount - $bonus_spend;
			CIBlockElement::SetPropertyValuesEx($bonus_element, 80, $PROP);
			//фиксация транзакции - списание бонусного баланса
			$this->add_transaction(31241, $bonus_spend, false, $bonus_element, 31231, false, $id, false, "Списание средств за покупку пакета " . $package_title);
		}
		//списание баланса
		if ($balance_part) {
			$PROP              = array();
			$PROP["SUMMA_RUB"] = $balance_amount - $balance_spend;
			CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
			//фиксация транзакции - списание основного баланса
			$this->add_transaction(31237, $balance_spend, false, $balance_element, 31230, false, $id, false, "Списание средств за покупку пакета " . $package_title);
		}
		//начисление кешбека
		if ($balance_part) {
			$cashback = $this->add_package_bonus($id, $package_price, $balance_spend, $package_title);
			if ($cashback) {
				$PROP      = array();
				$PROP[277] = $cashback;
				CIBlockElement::SetPropertyValuesEx($package_balance_element, 80, $PROP);
			}
		}
		//проверка откликов мастера
		$this->responds_check($id);

		return $this->getSuccess("");
	}


	//Деактивация пакета
	function deactivatePackage($id, $user_type, $package_id) {
		global $yes;
		global $no;
		global $setup_element;
		if ( ! ($yes)) {
			$globals       = $this->set_globals();
			$yes           = $globals['yes'];
			$no            = $globals['no'];
			$setup_element = $globals['$setup_element'];
		}
		$returning_rate = 0.5; //ДОЛЯ СТОИМОСТИ ЗАЯВОК ПАКЕТА, ВОЗВРАЩАЕМАЯ ПРИ ДЕАКТИВАЦИИ
		$this->master_type_check($user_type);
		//ищем активную запись по данному пакету
		$time_point = date_create(date('Y-m-d H:i:s', time()));
		$arSelect   = array("ID", "IBLOCK_ID", "PROPERTY_163", "PROPERTY_200", "PROPERTY_250", "PROPERTY_277");
		$arFilter   = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31232, "=PROPERTY_162" => $package_id, "!=PROPERTY_163" => 0, ">PROPERTY_200" => date_format($time_point, 'Y-m-d H:i:s'), "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$balance_element = $arFields["ID"];
			$units_left      = $arFields["PROPERTY_163_VALUE"];
			$unit_price      = $arFields["PROPERTY_250_VALUE"];
			$cashback        = $arFields["PROPERTY_277_VALUE"];
		}
		//проверка наличия такого пакета у мастера
		if ( ! ($balance_element)) {
			return $this->getFailure("У вас нет активного данного тарифа", false, null);
		}
		//проверка возможности деактивации
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_361");
		$arFilter = array("IBLOCK_ID" => 75, "=ID" => $package_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if ($arFields["PROPERTY_361_VALUE"] == $no) {
				return $this->getFailure("Деактивация данного тарифа невозможна", false, null);
			}
		}
		//расчеты
		$full_amount      = $units_left * $unit_price;
		$returning_amount = $full_amount * $returning_rate;
		$returning_amount = round($returning_amount, 0);
		$lose_amount      = $full_amount - $returning_amount;
		//чтение общих настроек
		$negative_values = false;
		$arSelect        = array("ID", "IBLOCK_ID", "PROPERTY_283");
		$arFilter        = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res             = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if ($arFields["PROPERTY_283_VALUE"] == $yes) {
				$negative_values = true;
			}
		}
		//деактивируем пакет
		$PROP      = array();
		$PROP[167] = $no;
		$PROP[296] = $this->get_element_name(77, $no);
		$PROP[163] = 0;
		CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
		//фиксация транзакции
		$transaction_id = $this->add_transaction(34276, false, $units_left, $balance_element, 31232, false, $id, false, "Деактивация неиспользованных заявок пакета");
		CIBlockElement::SetPropertyValuesEx($transaction_id, 87, array(359 => $lose_amount));
		//узнать бонусный баланс мастера
		$bonus_amount = 0;
		$arSelect     = array("ID", "IBLOCK_ID", "PROPERTY_161");
		$arFilter     = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$bonus_element = $arFields["ID"];
			$bonus_amount  = $arFields["PROPERTY_161_VALUE"];
		}
		//возвращаем остаток
		if ($returning_amount) {
			$bonus_amount = $bonus_amount + $returning_amount;
			//изменяем текущую
			if ($bonus_element) {
				$PROP              = array();
				$PROP["SUMMA_RUB"] = $bonus_amount;
				CIBlockElement::SetPropertyValuesEx($bonus_element, 80, $PROP);
			} //создаем новую
			else {
				$bonus_element = $this->add_balance_element(31231, $bonus_amount, false, false, $id, "Бонусный баланс");
			}
			//фиксация транзакции
			$this->add_transaction(31239, $returning_amount, false, $bonus_element, 31231, false, $id, false, "Деактивация неиспользованных заявок пакета");
			$this->add_notification($id, 0, 57360, false, $returning_amount);
		}
		//списываем кешбек
		if ($cashback) {
			$bonus_amount_new = $bonus_amount - $cashback;
			$bonus_delta      = $cashback;
			if ($bonus_amount_new < 0) {
				if ( ! ($negative_values)) {
					$bonus_amount_new = 0;
					$bonus_delta      = $bonus_amount;
				}
			}
			$bonus_amount = $bonus_amount_new;
		}
		if ($cashback and $bonus_delta) {
			//изменяем текущую
			if ($bonus_element) {
				$PROP              = array();
				$PROP["SUMMA_RUB"] = $bonus_amount;
				CIBlockElement::SetPropertyValuesEx($bonus_element, 80, $PROP);
			} //создаем новую
			else {
				$bonus_element = $this->add_balance_element(31231, $bonus_amount, false, false, $id, "Бонусный баланс");
			}
			//фиксация транзакции
			$this->add_transaction(35142, $cashback, false, $bonus_element, 31231, false, $id, false, "Списание кэшбэка за деактивированный пакет");
			$this->add_notification($id, 0, 57376, false, $cashback);
		}

		return $this->getSuccess("");
	}


	//Получение истории транзакций
	function getUserHistory($id, $user_type, $page) {
		$this->user_type_check($user_type);
		global $yes;
		global $no;
		global $setup_element;
		$items        = array();
		$worker_types = array(31235, 31236, 31237, 31238, 31239, 31240, 31241, 31242, 31243, 31244, 34276, 34808, 35139, 35140, 35141, 35142, 1011893, 1011934);
		$client_types = array(31245, 31246);
		$in_types     = array(31235, 31238, 31239, 31242, 31243, 31245, 34808, 35139, 35141, 1011893);
		$out_types    = array(31236, 31237, 31240, 31241, 31244, 34276, 31246, 35140, 35142, 1011934);
		//установка постраничного участка
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_202");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields       = $ob->GetFields();
			$units_per_page = $arFields["PROPERTY_202_VALUE"];
		};
		if ($page and $page != 0) {
			$min_unit = $page * $units_per_page;
			$max_unit = ($page * $units_per_page) + $units_per_page - 1;
		} else {
			$min_unit = 0;
			$max_unit = $units_per_page - 1;
		}
		$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_186", "PROPERTY_187", "PROPERTY_188", "PROPERTY_190", "PROPERTY_251", "PROPERTY_305", "DATE_CREATE");
		$arFilter = array("IBLOCK_ID" => 87, "=PROPERTY_192" => $id);
		$res      = CIBlockElement::GetList(array("id" => "desc"), $arFilter, false, false, $arSelect);
		$unit     = -1;
		while ($ob = $res->GetNextElement()) {
			$arFields     = $ob->GetFields();
			$element_id   = $arFields["ID"];
			$type         = $arFields["PROPERTY_186_VALUE"];
			$balance_type = $arFields["PROPERTY_190_VALUE"];
			if ($user_type == 0 and in_array($type, (array) $client_types)) {
				continue;
			} else if ($user_type == 1 and in_array($type, (array) $worker_types)) {
				continue;
			}
			//проверка страницы
			$unit = $unit + 1;
			if ($unit < $min_unit) {
				continue;
			} else if ($unit > $max_unit) {
				break;
			}
			//собираем ответ
			$item = array();
			//заказ
			if ($arFields["PROPERTY_251_VALUE"]) {
				$item_id = $arFields["PROPERTY_251_VALUE"];
			} else {
				$item_id = 0;
			}
			//направление
			if (in_array($type, (array) $in_types)) {
				$direction      = 0; //поступление
				$direction_text = 'Поступление, ';
			} else {
				$direction      = 1; //списание
				$direction_text = 'Списание, ';
			}
			//тип баланса и количество
			if (in_array($balance_type, array(31230))) {
				$item_type = 0; //деньги
				$amount    = $arFields["PROPERTY_187_VALUE"];
			} else if (in_array($balance_type, array(31231, 31234))) {
				$item_type = 1; //бонусы
				$amount    = $arFields["PROPERTY_187_VALUE"];
			} else if (in_array($balance_type, array(31232))) {
				$item_type = 2; //пакет
				$amount    = $arFields["PROPERTY_188_VALUE"];
			} else {
				continue; //процент возвратов не выводим
			}
			//заголовок
			$title = $direction_text . $this->plural_form($amount, $item_type);
			//описание
			$summary = $arFields["NAME"];
			if ($arFields["PROPERTY_305_VALUE"]) {
				//добавка к описанию
				$summary = $summary . '~BR~' . $arFields["PROPERTY_305_VALUE"];
				$summary = json_encode($summary);
				$summary = str_replace('~BR~', '\r\n', $summary);
				$summary = json_decode($summary);
			}
			//дата
			$date              = date_create($arFields['DATE_CREATE']);
			$correction_period = -2;
			date_add($date, date_interval_create_from_date_string($correction_period . " hours"));
			$date_timestamp = date_timestamp_get($date);
			$item           = array(
				'id'      => (int) $element_id,
				'item_id' => (int) $item_id,
				'type'    => $item_type,
				'date'    => $date_timestamp,
				'title'   => $title,
				'summary' => $summary,
				//'direction' => $direction,
				//'amount' => (int)$amount,
			);
			$items[]        = $item;
		}
		$response = array('list' => $items);

		return $this->getSuccess($response);
	}


	//Нужно ли обновлять заказы
	function checkForOrdersUpdate($id, $user_type, $date, $lat, $lon) {
		$this->master_type_check($user_type);
		global $yes;
		global $no;
		global $setup_element;
		CModule::IncludeModule("crm");
		$orders = array();
		//проверка города
		$user_city = $this->get_contact_city($id);
		if ($lat and $lon) {
			$real_city = $this->city_search($lat, $lon);
		}
		//если другой город - берем координаты из контакта
		if ($user_city != $real_city) {
			$coordinates = $this->get_city_coordinates($user_city);
			$lat         = $coordinates['lat'];
			$lon         = $coordinates['lon'];
		}
		//подготовка координат
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_183");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$radius   = $arFields["PROPERTY_183_VALUE"];
		};
		$lat_min = $lat - ($radius / 111.134);
		$lat_max = $lat + ($radius / 111.134);
		$lon_min = $lon - ($radius / (111.321 * cos(deg2rad($lat))));
		$lon_max = $lon + ($radius / (111.321 * cos(deg2rad($lat))));
		//подготовка даты
		$time_point = date_create();
		date_timestamp_set($time_point, $date);
		//получаем список заказов
		$arFilter                            = array();
		$arFilter["=CATEGORY_ID"]            = '7';
		$arFilter["=STAGE_ID"]               = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
		$arFilter["(int)>=UF_CRM_LATITUDE"]  = $lat_min;
		$arFilter["(int)<=UF_CRM_LATITUDE"]  = $lat_max;
		$arFilter["(int)>=UF_CRM_LONGITUDE"] = $lon_min;
		$arFilter["(int)<=UF_CRM_LONGITUDE"] = $lon_max;
		$arFilter[">UF_CRM_ACTUALTIME"]      = date_format($time_point, 'd.m.Y H:i:s'); //дает погрешность 2 часа в сторону захвата лишнего!
		//запрос по заказам
		$res = CCrmDeal::GetListEx(array(), $arFilter, false, false, array("ID", "UF_CRM_LATITUDE", "UF_CRM_LONGITUDE", "UF_CRM_ACTUALTIME"));
		while ($ob = $res->GetNext()) {
			//проверка времени - отсечение погрешности (таймстамп работает надежно, поля битрикса - нет)
			$actual_date = date_timestamp_get(date_create($ob['UF_CRM_ACTUALTIME']));
			if ($actual_date < $date) {
				continue;
			}
			//проверка координат
			$distance_lat = (abs($ob['UF_CRM_LATITUDE'] - $lat)) * 111.134;
			$distance_lon = (abs($ob['UF_CRM_LONGITUDE'] - $lon)) * 111.321 * cos(deg2rad($lat));
			$distance_sqr = pow($distance_lat, 2) + pow($distance_lon, 2);
			$distance     = sqrt($distance_sqr);
			if ($distance > $radius) {
				continue;
			}
			$orders[] = $ob['ID'];
		}
		if ($orders) {
			$shouldUpdate = true;
		} else {
			$shouldUpdate = false;
		}
		$response = array('shouldUpdate' => $shouldUpdate);

		return $this->getSuccess($response);
	}


	//Получение причин отказов / возвратов
	function getHints($id, $user_type, $type) {
		global $yes;
		$this->user_type_check($user_type);
		$list      = array();
		$user_code = 0;
		if ($type == 0) {
			$user_code = 31169;
			$type_code = 92;
		} else if ($type == 1) {
			$user_code = 31170;
			$type_code = 93;
		} else if ($type == 2) {
			$user_code = 31169;
			$type_code = 94;
		}
		$arSelect = array("ID", "IBLOCK_ID", "NAME");
		$arFilter = array("IBLOCK_ID" => 112, "=PROPERTY_286" => $user_code, "=PROPERTY_306" => $type_code, "=PROPERTY_287" => $yes);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$list[]   = $arFields["NAME"];
		};
		$response = array(
			'list' => $list,
		);

		return $this->getSuccess($response);
	}


	//-----Прочие функции-----

	//поиск ближайшего города по координатам
	function city_search(float $lat, float $lon) {
		$all_cities = array();
		$arSelect   = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_175", "PROPERTY_176");
		$arFilter   = array("IBLOCK_ID" => 84, "CHECK_PERMISSIONS" => 'N');
		$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields               = $ob->GetFields();
			$city_id                = $arFields["ID"];
			$city_lat               = (float) $arFields["PROPERTY_175_VALUE"];
			$city_lon               = (float) $arFields["PROPERTY_176_VALUE"];
			$distance_sqr           = (pow(abs($lat - $city_lat), 2) + pow(abs($lon - $city_lon), 2));
			$distance               = sqrt($distance_sqr);
			$all_cities[ $city_id ] = $distance;
		};
		$city = array_search(min($all_cities), $all_cities);

		return $city;
	}

	//рассчет радиуса по масштабу
	function get_radius($zoom) {
		if ( ! ($zoom)) {
			return false;
		}
		if ($zoom >= 19) {
			$radius = 0.6;
		} else if ($zoom >= 16) {
			$radius = 5;
		} else if ($zoom >= 13) {
			$radius = 50;
		} else if ($zoom >= 10) {
			$radius = 200;
		} else if ($zoom >= 7) {
			$radius = 1500;
		} else {
			$radius = 50000;
		}

		return $radius;
	}

	//получение координат по городу
	function get_city_coordinates($city_id) {
		$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_175", "PROPERTY_176");
		$arFilter = array("IBLOCK_ID" => 84, "=ID" => $city_id, "CHECK_PERMISSIONS" => 'N');
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$city_lat = $arFields["PROPERTY_175_VALUE"];
			$city_lon = $arFields["PROPERTY_176_VALUE"];
		};
		$city_coordinates = array(
			'lat' => (float) $city_lat,
			'lon' => (float) $city_lon,
		);

		return $city_coordinates;
	}

	//получение объекта города по id
	function city_get($city_id) {
		if ( ! ($city_id)) {
			return null;
		}
		$federal_cities = array(34927, 34928, 34926);
		$arSelect       = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_177", "PROPERTY_178", "PROPERTY_LONGITUDE", "PROPERTY_LATITUDE");
		$arFilter       = array("IBLOCK_ID" => 84, "=ID" => $city_id);
		$res            = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$title    = $arFields["NAME"];
			$lat      = (double) $arFields["PROPERTY_LATITUDE_VALUE"] ?? 0;
			$lon      = (double) $arFields["PROPERTY_LONGITUDE_VALUE"] ?? 0;
			$summary  = $arFields["PROPERTY_178_VALUE"] . " " . $arFields["PROPERTY_177_VALUE"];
			if (in_array($arFields["ID"], (array) $federal_cities)) {
				$summary = "";
			}
		};
		$city = array(
			'id'      => (int) $city_id,
			'title'   => $title,
			'lat'     => $lat,
			'lon'     => $lon,
			'summary' => $summary,
		);

		return $city;
	}

	//получение города из контакта
	function get_contact_city($user_id) {
		$arFilter       = array("=ID" => $user_id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("UF_CRM_NEW_CITY");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$city_id = $ob["UF_CRM_NEW_CITY"];
		}

		return $city_id;
	}

	//удаление записи инфоблока
	function element_delete($iblock_id, $element_id) {
		CIBlockElement::Delete($element_id);
	}

	//поиск контакта по номеру телефона
	function contact_search($phone) {
		$dbResult = CCrmFieldMulti::GetList(
			array(),
			array(
				'ENTITY_ID' => 'CONTACT',
				'TYPE_ID'   => 'PHONE',
				'VALUE'     => "%" . $phone,
			)
		);

		$contacts = [];
		while ($ar = $dbResult->Fetch()) {
			$contacts[] = $ar['ELEMENT_ID'];
		}

		return $contacts;
	}

	//обновление поля в контакте
	function update_field_contact($contact_id, $field, $data) {
		$entity = new CCrmContact;
		$fields = array(
			$field => $data,
		);
		$entity->update($contact_id, $fields);
	}

	//обновить дату активности в контакте
	function active_date_contact($contact_id) {
		$entity = new CCrmContact;
		$fields = array(
			'UF_CRM_ACTIVE_DATE' => date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y'),
		);
		$entity->update($contact_id, $fields);
	}

	//обновление поля в сделке
	function update_field_deal($deal_id, $field, $data) {
		$entity = new CCrmDeal;
		$fields = array(
			$field => $data,
		);
		$entity->update($deal_id, $fields);
	}

	//вынужденная очистка обязательного поля в сделке
	function forced_update_field_deal($deal_id, $field) {
		//внимание - для работы с новым полем нужно добавить новую ветку в БП!
		$wfId = CBPDocument::StartWorkflow(
			203,
			array("crm", "CCrmDocumentDeal", "DEAL_" . $deal_id),
			array("target" => "cleanfield_" . $field),
			$arErrorsTmp
		);
	}

	//добавить телефон в контакт
	function add_phone_contact($contact_id, $phone, $phone_type) {
		if (strpos($phone, ' + 7') !== false) {
			$phone = substr($phone, 1);
		} else if (substr($phone, 0, 1) == '8') {
			$phone = substr($phone, 1);
			$phone = '7' . $phone;
		}
		$entity = new CCrmFieldMulti();
		$fields = array(
			'ENTITY_ID'  => 'CONTACT',
			'ELEMENT_ID' => $contact_id,
			'TYPE_ID'    => 'PHONE',
			'VALUE'      => $phone,
			'VALUE_TYPE' => $phone_type,
		);
		$entity->add($fields);
		//дебагнуть контакт
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				236,
				array("crm", "CCrmDocumentContact", "CONTACT_" . $contact_id),
				array_merge(),
				$arErrorsTmp
			);
		}
	}

	//изменить тип телефона в контакте
	function update_phone_type_contact($contact_id, $phone_id, $phone_type) {
		$dbResult = CCrmFieldMulti::GetList(array(), array('ID' => $phone_id));
		while ($ar = $dbResult->Fetch()) {
			$update = array('PHONE' => array($phone_id => $ar));
		}
		$update['PHONE'][ $phone_id ]['COMPLEX_ID'] = 'PHONE_' . $phone_type;
		$update['PHONE'][ $phone_id ]['VALUE_TYPE'] = $phone_type;
		$entity                                     = new CCrmFieldMulti();
		$entity->SetFields('CONTACT', $contact_id, $update);
	}

	//получить телефон из контакта
	function get_phone($contact_id, $return_array) {
		if ($return_array) {
			$phone = array();
		} else {
			$phone = null;
		}
		if ( ! ($contact_id)) {
			return $phone;
		}
		$dbResult = CCrmFieldMulti::GetList(
			array('ID' => 'asc'),
			array(
				'ENTITY_ID'   => 'CONTACT',
				'TYPE_ID'     => 'PHONE',
				'VALUE_TYPE'  => 'MOBILE',
				'=ELEMENT_ID' => $contact_id
			)
		);
		while ($fields = $dbResult->Fetch()) {
			$phone_local = strval($fields['VALUE']);
			if (strpos($phone_local, '+7') === 0) {
				$phone_local = substr($phone_local, 1);
			} else if (strpos($phone_local, '8') === 0) {
				$phone_local = '7' . substr($phone_local, 1);
			}
			if ($return_array and (strlen($phone_local) == 11 or strlen($phone_local) == 12 and strpos($phone_local, '+') === 0)) {
				$phone[] = $phone_local;
			}
			if ( ! ($return_array) and (strlen($phone_local) == 11 or strlen($phone_local) == 12 and strpos($phone_local, '+') === 0)) {
				$phone = $phone_local;
				break;
			}
		}
		if ( ! ($phone)) {
			$dbResult = CCrmFieldMulti::GetList(
				array('ID' => 'asc'),
				array(
					'ENTITY_ID'   => 'CONTACT',
					'TYPE_ID'     => 'PHONE',
					'=ELEMENT_ID' => $contact_id
				)
			);
			while ($fields = $dbResult->Fetch()) {
				$phone_local = strval($fields['VALUE']);
				if (strpos($phone_local, '+7') === 0) {
					$phone_local = substr($phone_local, 1);
				} else if (strpos($phone_local, '8') === 0) {
					$phone_local = '7' . substr($phone_local, 1);
				}
				if ($return_array and (strlen($phone_local) == 11 or strlen($phone_local) == 12 and strpos($phone_local, '+') === 0)) {
					$phone[] = $phone_local;
				}
				if ( ! ($return_array) and (strlen($phone_local) == 11 or strlen($phone_local) == 12 and strpos($phone_local, '+') === 0)) {
					$phone = $phone_local;
					break;
				}
			}
		}

		return $phone;
	}

	//расставление нормальных переносов при чтении из поля html/текст
	function html_decode($text) {
		$text = htmlspecialcharsBack($text); //первичная очистка от мусорных символов
		//убрать ссаные мэйлту
		$href_pos1 = strpos($text, "<a href");
		$href_pos2 = strpos($text, '">');
		if ($href_pos1 or $href_pos1 === 0) {
			$href_pos2 = $href_pos2 + 2;
			$text_del1 = substr($text, $href_pos1, ($href_pos2 - $href_pos1));
			$text_del2 = " </a > ";
			$text      = str_replace($text_del1, '', $text);
			$text      = str_replace($text_del2, '', $text);
		}
		//--------------------
		$text = str_replace('&nbsp;', ' ', $text); //и нбсп свои убери
		$text = str_replace('&quot;', ' ', $text); //и куот не забудь
		//echo addcslashes($text, 'A..z'); //если нужно вывести невидимые символы
		$text = str_replace('<br>', '~BR~', $text); //временно заменяем невидимый код нужных переносов на что-то видимое
		$text = json_encode($text);
		$text = str_replace('\r\n', '', $text); //сносим все переносы (и наши, и случайные)
		$text = str_replace('~BR~ ', '\r\n', $text); //проставляем переносы в нужных местах (с пост-пробелом)
		$text = str_replace('~BR~', '\r\n', $text); //проставляем переносы в нужных местах 2 (без пост-пробела)
		$text = json_decode($text);

		return $text;
	}

	//расставление БИТРИКСОВСКИХ переносов для записи в поле html/текст
	function html_encode($text) {
		//этот перенос нельзя просто создать искусственно, берем из таблицы битрикса, где он уже есть
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_291");
		$arFilter = array("IBLOCK_ID" => 99, "=ID" => 59924);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$field_content = $arFields["~PROPERTY_291_VALUE"]["TEXT"]; //код поля с ~!!! Это важно, из поля без ~ не возьмется, да, там их 2 разных
		};
		$line_break = substr($field_content, 1, 6); //обрезка кода переноса битрикса, длиной в 6!!! неизвестных символов
		//а вот теперь берем нормальную строку
		$text = json_encode($text);
		$text = str_replace('\r\n', '~BR~', $text); //заменяем нормальные переносы видимой херней в нужных местах
		$text = json_decode($text);
		$text = str_replace('~BR~', $line_break, $text); //заменяем видимую херню на битриксовский невидимый код переносов

		return $text; //мы сделали это поздравляю
	}

	//расставление переносов перед выводом в битрикс
	function br_encode($text) {
		$text = json_encode($text);
		$text = str_replace('<*>', '\r\n', $text); //проставляем переносы в нужных местах
		$text = json_decode($text);

		return $text;
	}

	//замена кавычек в строке при передаче в json
	function quot_decode($text) {
		$text = str_replace(" & quot;", "''", $text); //замена двойных кавычек на две одинарные

		return $text;
	}

	//принудительное завершение процесса на сделке
	function stop_deal_process($deal_id) {
		$arFilter       = array("=ID" => $deal_id);
		$arSelectFields = array("UF_CRM_ACTIVE_PROCESS");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$wfId = $ob['UF_CRM_ACTIVE_PROCESS'];
		}
		if ( ! ($wfId)) {
			return;
		}
		if (CModule::IncludeModule("bizproc")) {
			CBPDocument::TerminateWorkflow(
				$wfId,
				array("crm", "CCrmDocumentDeal", "DEAL_" . $deal_id),
				$arErrorsTmp
			);
		}
		$this->update_field_deal($deal_id, "UF_CRM_ACTIVE_PROCESS", "");
	}

	//проверка типа пользователя - где критично
	function user_type_check($user_type) {
		if ($user_type === '0' or $user_type === '1' or $user_type === 0 or $user_type === 1) {
			return true;
		} else {
			echo $this->getFailure("Неверный тип пользователя", false, null);
			$_SESSION = array();
			exit;
		}
	}

	//проверка типа пользователя (только мастер) - где критично
	function master_type_check($user_type) {
		if ($user_type === '0' or $user_type === 0) {
			return true;
		} else {
			echo $this->getFailure("Неверный тип пользователя", false, null);
			$_SESSION = array();
			exit;
		}
	}

	//проверка типа пользователя (только клиент) - где критично
	function client_type_check($user_type) {
		if ($user_type === '1' or $user_type === 1) {
			return true;
		} else {
			echo $this->getFailure("Неверный тип пользователя", false, null);
			$_SESSION = array();
			exit;
		}
	}

	//проверка имени пользователя
	function user_name_check($id) {
		$arFilter       = array("=ID" => $id);
		$arSelectFields = array("NAME", "LAST_NAME");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$name      = $ob["NAME"];
			$last_name = $ob["LAST_NAME"];
		}
		if ( ! ($name) or ! ($last_name) or $name == 'Исполнитель' or $name == 'Заказчик') {
			echo $this->getFailure("Пожалуйста заполните профиль", false, "name_error");
			$_SESSION = array();
			exit;
		} else {
			return true;
		}
	}

	//получение основной категории
	function get_main_category($work_type, $work_id) {
		switch ($work_type) {
			case 1:
				$main_category = $work_id;
				break;
			case 2:
				$arSelect = array("DEPTH_LEVEL", "ID", "IBLOCK_SECTION_ID");
				$arFilter = array("IBLOCK_ID" => 66, "DEPTH_LEVEL" => array(1, 2), "=ID" => $work_id);
				$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
				while ($ar_result = $db_list->GetNext()) {
					$main_category = $ar_result["IBLOCK_SECTION_ID"];
				};
				break;
			case 3:
				$arSelect = array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID");
				$arFilter = array("IBLOCK_ID" => 66, "=ID" => $work_id);
				$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields     = $ob->GetFields();
					$sub_category = $arFields["IBLOCK_SECTION_ID"];
				};
				$arSelect = array("DEPTH_LEVEL", "ID", "IBLOCK_SECTION_ID");
				$arFilter = array("IBLOCK_ID" => 66, "DEPTH_LEVEL" => array(1, 2), "=ID" => $sub_category);
				$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
				while ($ar_result = $db_list->GetNext()) {
					if ($ar_result["IBLOCK_SECTION_ID"]) {
						$main_category = $ar_result["IBLOCK_SECTION_ID"];
					} else {
						$main_category = $sub_category;
					}
				};
				break;
		}

		return $main_category;
	}

	//получение названия категории или работы
	function get_work_title($work_type, $work_id) {
		$title = "Новая заявка";
		switch ($work_type) {
			case 1:
			case 2:
				$arSelect = array("NAME");
				$arFilter = array("IBLOCK_ID" => 66, "=ID" => $work_id);
				$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
				while ($ar_result = $db_list->GetNext()) {
					$title = $ar_result["NAME"];
				};
				break;
			case 3:
				$arSelect = array("NAME");
				$arFilter = array("IBLOCK_ID" => 66, "=ID" => $work_id);
				$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields = $ob->GetFields();
					$title    = $arFields["NAME"];
				};
				break;
		}

		return $title;
	}

	//получение подкатегории для работы
	function get_subcategory($work_id) {
		$arSelect = array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID");
		$arFilter = array("IBLOCK_ID" => 66, "=ID" => $work_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields     = $ob->GetFields();
			$sub_category = $arFields["IBLOCK_SECTION_ID"];
		};
		//проверка, что это подкатегория, а не основная
		$arSelect = array("DEPTH_LEVEL", "ID", "IBLOCK_SECTION_ID");
		$arFilter = array("IBLOCK_ID" => 66, "DEPTH_LEVEL" => array(1, 2), "=ID" => $sub_category);
		$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
		while ($ar_result = $db_list->GetNext()) {
			if ( ! ($ar_result["IBLOCK_SECTION_ID"])) {
				$sub_category = false;
			}
		};

		return $sub_category;
	}

	//получение списка id заказов мастера по активным откликам
	function get_orders_by_responds($id, $active_orders, $active_responds) {
		global $yes;
		global $no;
		//подготовка к отбраковке лишнего
		$false_responds = array();
		if ( ! ($active_orders) and ! ($active_responds)) {
			$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_136" => $id, "=PROPERTY_132" => 31193);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, array("ID", "IBLOCK_ID", "PROPERTY_135"));
			while ($ob = $res->GetNextElement()) {
				$arFields         = $ob->GetFields();
				$false_responds[] = $arFields["PROPERTY_135_VALUE"];
			}
		}
		//выборка
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_135");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_136" => $id);
		if ($active_orders and $active_responds) {
			$arFilter["=PROPERTY_132"] = 31191;
		} else if ($active_orders and ! ($active_responds)) {
			$arFilter["=PROPERTY_132"] = 31212;
		} else if ( ! ($active_orders) and ! ($active_responds)) {
			$arFilter["=PROPERTY_132"] = 31192;
		}
		$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		$extra_orders = array();
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if (in_array($arFields["PROPERTY_135_VALUE"], (array) $false_responds)) {
				continue;
			}
			$extra_orders[] = $arFields["PROPERTY_135_VALUE"];
		}

		return $extra_orders;
	}

	//установка коэффициента принятия
	function set_acception_tax($work_type, $work_id, $city, $price) {
		global $yes;
		global $no;
		global $setup_element;
		if ( ! ($yes)) {
			$globals       = $this->set_globals();
			$yes           = $globals['yes'];
			$no            = $globals['no'];
			$setup_element = $globals['setup_element'];
		}
		//чтение настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_154", "PROPERTY_155", "PROPERTY_303");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element, "CHECK_PERMISSIONS" => 'N');
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields         = $ob->GetFields();
			$base_rate        = $arFields["PROPERTY_154_VALUE"];
			$selection_period = $arFields["PROPERTY_155_VALUE"];
			$free_price       = $arFields["PROPERTY_303_VALUE"];
		};
		if ($city and $work_id) {
			$selection_period = $selection_period * (-1);
			$main_category    = $this->get_main_category($work_type, $work_id);
			if ($work_type == 3) {
				$subcategory = $this->get_subcategory($work_id);
			} else if ($work_type == 2) {
				$subcategory = $work_id;
			}
			//узнать основную категорию и все работы в ней
			$categories = array($main_category);
			$unit_works = array();
			$arSelect   = array("ID");
			$arFilter   = array("=SECTION_ID" => $categories, "GLOBAL_ACTIVE" => "Y", "DEPTH_LEVEL" => 2, "CHECK_PERMISSIONS" => 'N');
			$db_list    = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
			while ($ar_result = $db_list->GetNext()) {
				$categories[] = $ar_result["ID"];
			};
			$arSelect = array("ID");
			$arFilter = array("IBLOCK_ID" => 66, "ACTIVE" => "Y", "=SECTION_ID" => $categories, "CHECK_PERMISSIONS" => 'N');
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields     = $ob->GetFields();
				$unit_works[] = $arFields["ID"];
			};
			array_multisort($unit_works);
			//выборка по заказам
			$time_point = date_create(date('Y-m-d', time()));
			date_add($time_point, date_interval_create_from_date_string($selection_period . " days"));
			$arOrder  = array("id" => "asc");
			$arFilter = array(
				"=CATEGORY_ID"      => '7',
				"=UF_CRM_NEW_CITY"  => $city,
				">=BEGINDATE"       => date_format($time_point, 'd.m.Y'),
				"CHECK_PERMISSIONS" => 'N',
			);

			$arFilter[] = array(
				"LOGIC" => "OR",
				array("=UF_CRM_UNDER_CATEGORY" => $main_category),
				array("=UF_CRM_WORK_TYPE" => $unit_works),
			);

			$arSelectFields = array("ID");
			$res            = CCrmDeal::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);
			$all_deals      = [];
			while ($ob = $res->GetNext()) {
				$all_deals[] = $ob['ID'];
			}
			$count_deals = count($all_deals);
			//echo ("точка времени ".date_format($time_point, 'd.m.Y')." кол - во ".$count_deals);
			//находим поправочный коэф-т
			$corrective_rates = array();
			$arSelect         = array("ID", "IBLOCK_ID", "PROPERTY_157", "PROPERTY_158");
			$arFilter         = array("IBLOCK_ID" => 79, ">=PROPERTY_157" => $count_deals, "=PROPERTY_159" => $yes, "CHECK_PERMISSIONS" => 'N');
			$res              = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields                                            = $ob->GetFields();
				$corrective_rates[ $arFields["PROPERTY_157_VALUE"] ] = $arFields["PROPERTY_158_VALUE"];
			};
			if ($corrective_rates) {
				ksort($corrective_rates);
				reset($corrective_rates);
				$rate = $base_rate * (current($corrective_rates));
			} else {
				$rate = $base_rate;
			}
		} else {
			$rate = $base_rate;
		}
		//проверка бесплатных заказов
		if ($price <= $free_price) {
			$rate = 0;
		}
		$rate = round($rate, 1);

		return $rate;
	}

	//получение счетчиков - новый запуск
	function getBadgeCounts($id, $user_type, $version) {
		$response = array(
			'counts'            => $this->get_counts($id, $user_type, false, false, false, false),
			'should_update_app' => $this->checkIncorrectAppVersion($version)
		);

		return $this->getSuccess($response);
	}

	/**
	 * Проверка на необходимость обновления приложения.
	 * Некорректные версии храняться в инфболоке Глобальные настройки
	 *
	 * @param $currentVersion - текущая версия приложения. Приходит в теле запроса из приложения
	 *
	 * @return bool
	 */
	function checkIncorrectAppVersion($currentVersion) {
		global $setup_element;

		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_INCORRECT_APP_VERSIONS");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

		$result = false;
		while ($ob = $res->GetNextElement()) {
			$arFields            = $ob->GetFields();
			$incorrectAppVesions = $arFields["PROPERTY_INCORRECT_APP_VERSIONS_VALUE"];

			$result = strpos($incorrectAppVesions, $currentVersion) !== false;
		};

		return $result;
	}

	//получение счетчиков событий
	function get_counts($id, $user_type, $type, $coord, $categories, $unit_works) {
		include_once($_SERVER["DOCUMENT_ROOT"] . "/api/extra_utils.php");
		$extra_utils = new ExtraUtils();
		global $yes;
		global $no;
		global $setup_element;
		$waits   = 30956;
		$confirm = 30957;
		//чтение общих настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_230");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields          = $ob->GetFields();
			$max_regular_views = $arFields["PROPERTY_230_VALUE"];
		};
		/*
        //заказы на главной
        if ($user_type == 0) {
            $arFilter = array();
            $arSelectFields = array('ID', 'STAGE_ID', 'UF_CRM_LATITUDE', 'UF_CRM_LONGITUDE', 'UF_CRM_COUNTER_WATCHEDMASTERS', 'UF_CRM_COUNTER_HIDEMASTERS', 'UF_CRM_FEEDBACK_PROCESS', 'UF_CRM_CLOSED_BY_WORKER');
            $arFilter['=CATEGORY_ID'] = '7';
            if ($coord) {
                $arFilter["(int)>=UF_CRM_LATITUDE"] = $coord[ lat_min ];
                $arFilter["(int)<=UF_CRM_LATITUDE"] = $coord[ lat_max ];
                $arFilter["(int)>=UF_CRM_LONGITUDE"] = $coord[ lon_min ];
                $arFilter["(int)<=UF_CRM_LONGITUDE"] = $coord[ lon_max ];
            }
            $arFilter['=STAGE_ID'] = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
            $arFilter[] = array(
                "LOGIC" => " or ",
                array("=UF_CRM_UNDER_CATEGORY" => $categories),
                array("=UF_CRM_WORK_TYPE" => $unit_works),
            );
            $res = CCrmDeal::GetListEx(array(id => desc), $arFilter, false, false, $arSelectFields);
            $total = array();
            while($ob = $res->GetNext()) {
                if (in_array($id, $ob['UF_CRM_COUNTER_HIDEMASTERS'])) {
                    continue;
                }
                $distance_lat = (abs($ob['UF_CRM_LATITUDE']-$coord[ lat ]))*111.134;
                $distance_lon = (abs($ob['UF_CRM_LONGITUDE']-$coord[ lon ]))*111.321*cos(deg2rad($coord[ lat ]));
                $distance_sqr = pow($distance_lat,2)+pow($distance_lon,2);
                $distance = sqrt($distance_sqr);
                if ($distance > $coord[ radius ]) {
                    continue;
                }
                if ($ob['UF_CRM_COUNTER_WATCHEDMASTERS'] and in_array($id, $ob['UF_CRM_COUNTER_WATCHEDMASTERS'])) {
                    continue;
                }
                $total[] = $ob['ID'];
            }
            $main_counter = count($total);
        }
        else {
            $main_counter = 0;
        }
        */
		//мои активные заказы
		$border_stages            = array('C7:3', 'C7:1');
		$arFilter                 = array();
		$arSelectFields           = array('ID', 'STAGE_ID', 'UF_CRM_FEEDBACK_PROCESS', 'UF_CRM_CLOSED_BY_WORKER');
		$user_counter_fields      = array(
			"LOGIC" => "OR",
			array("=UF_CRM_COUNTER_FREEREQUEST" => $waits),
			array("=UF_CRM_COUNTER_ORDERCOMPL" => $waits),
			array("=UF_CRM_COUNTER_NOMASTERS" => $waits),
			array("=UF_CRM_COUNTER_BONUSANSWER" => $waits),
		);
		$worker_counter_fields    = array(
			"LOGIC" => "OR",
			array("=UF_CRM_COUNTER_ORDERCOMPL_WORKER" => $waits),
			array("=UF_CRM_COUNTER_BONUSOFFER" => $waits),
			array("=UF_CRM_COUNTER_BONUSDONE" => $waits),
		);
		$arFilter['=CATEGORY_ID'] = '7';
		if ($user_type == 0) { //мастер
			$arFilter['=STAGE_ID']      = array('C7:FINAL_INVOICE', 'C7:2', 'C7:6', 'C7:1');
			$arFilter['=UF_CRM_MASTER'] = $id;
			$arFilter[]                 = $worker_counter_fields;
		} else if ($user_type == 1) { //заказчик
			$arFilter['=STAGE_ID']    = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING', 'C7:FINAL_INVOICE', 'C7:1', 'C7:2', 'C7:6', 'C7:3');
			$arFilter['!=CONTACT_ID'] = false;
			$arFilter['=CONTACT_ID']  = $id;
			$arFilter[]               = $user_counter_fields;
		}
		$res           = CCrmDeal::GetListEx(array("id" => "desc"), $arFilter, false, false, $arSelectFields);
		$active_orders = array();
		while ($ob = $res->GetNext()) {
			if ($user_type == 1 and in_array($ob['STAGE_ID'], (array) $border_stages) and $ob['UF_CRM_FEEDBACK_PROCESS'] == 30957) {
				continue;
			}
			if ($user_type == 0 and in_array($ob['STAGE_ID'], (array) $border_stages) and $ob['UF_CRM_CLOSED_BY_WORKER'] == 30957) {
				continue;
			}
			$active_orders[] = $ob['ID'];
		}

		//мои закрытые заказы
		if ($user_type == 0) { //мастер
			$arFilter['=STAGE_ID'] = array('C7:1', 'C7:3', 'C7:WON');
		} else if ($user_type == 1) { //заказчик
			$arFilter['=STAGE_ID'] = array('C7:1', 'C7:3', 'C7:WON');
		}
		$res           = CCrmDeal::GetListEx(array("id" => "desc"), $arFilter, false, false, $arSelectFields);
		$closed_orders = array();
		while ($ob = $res->GetNext()) {
			if ($user_type == 1 and in_array($ob['STAGE_ID'], (array) $border_stages) and $ob['UF_CRM_FEEDBACK_PROCESS'] != 30957) {
				continue;
			}
			if ($user_type == 0 and in_array($ob['STAGE_ID'], (array) $border_stages) and $ob['UF_CRM_CLOSED_BY_WORKER'] != 30957) {
				continue;
			}
			$closed_orders[] = $ob['ID'];
		}

		//корректировка по откликам
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_135");
		$arFilter = array("IBLOCK_ID" => 73);
		if ($user_type == 0) { //мастер
			$arFilter["=PROPERTY_136"] = $id;
			$arFilter["=PROPERTY_134"] = $no;
			$arFilter["=PROPERTY_132"] = array(31193, 31212);
		} else if ($user_type == 1) { //заказчик
			$arFilter["=PROPERTY_137"] = $id;
			$arFilter["=PROPERTY_133"] = $no;
			$arFilter["=PROPERTY_132"] = 31191;
		}
		$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		$extra_orders = array();
		while ($ob = $res->GetNextElement()) {
			$arFields       = $ob->GetFields();
			$extra_orders[] = $arFields["PROPERTY_135_VALUE"];
		}
		if ($extra_orders) {
			$real_orders = array();
			CModule::IncludeModule("crm");
			$arFilter       = array("=ID" => $extra_orders);
			$arSelectFields = array("ID");
			$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$real_orders[] = $ob['ID'];
			}
			$extra_orders = $real_orders;
		}

		//корректировка закрытых по откликам
		$extra_orders_closed = array();
		if ($user_type == 0) {
			$arSelect                  = array("ID", "IBLOCK_ID", "PROPERTY_135");
			$arFilter                  = array("IBLOCK_ID" => 73);
			$arFilter["=PROPERTY_136"] = $id;
			$arFilter["=PROPERTY_134"] = $no;
			$arFilter["=PROPERTY_132"] = 31192;
			$res                       = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			$active_stages             = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
			while ($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();
				//проверка попадания неактивных откликов при активных заказах
				$active_order = false;
				$res2         = CCrmDeal::GetListEx(array(), array("=ID" => $arFields["PROPERTY_135_VALUE"]), false, false, array("STAGE_ID"));
				while ($ob2 = $res2->GetNext()) {
					if (in_array($ob2['STAGE_ID'], (array) $active_stages)) {
						$active_order = true;
					}
				}
				if ($active_order) {
					continue;
				}
				$extra_orders_closed[] = $arFields["PROPERTY_135_VALUE"];
			}
			if ($extra_orders_closed) {
				$real_orders = array();
				CModule::IncludeModule("crm");
				$arFilter       = array("=ID" => $extra_orders_closed);
				$arSelectFields = array("ID");
				$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
				while ($ob = $res->GetNext()) {
					$real_orders[] = $ob['ID'];
				}
				$extra_orders_closed = $real_orders;
			}
		}

		//корректировка по сообщениям - В РАЗРАБОТКЕ
		$extra_orders2        = array();
		$extra_orders_closed2 = array();
		//итоговые подсчеты по заказам
		$active_orders = array_values(array_unique(array_merge($active_orders, $extra_orders, $extra_orders2)));
		array_multisort($active_orders);
		$closed_orders = array_values(array_unique(array_merge($closed_orders, $extra_orders_closed, $extra_orders_closed2)));
		array_multisort($closed_orders);
		$active_orders_counter = count($active_orders);
		$closed_orders_counter = count($closed_orders);
		$my_orders_counter     = $active_orders_counter + $closed_orders_counter;
		//непрочитанные уведомления
		/*
        $notifications = array();
        if ($user_type == 0) {
			$user_type_code = 31169;
		}
        else {
			$user_type_code = 31170;
		}
        $arSelect = array("ID", "IBLOCK_ID", "PROPERTY_205", "PROPERTY_229");   //, "PROPERTY_210", , "PROPERTY_380"
        $arFilter = array("IBLOCK_ID"=>90, "=PROPERTY_206"=>$user_type_code, "=PROPERTY_208"=>$yes);
        $arFilter[] = array(
            "LOGIC" => " or ",
            array("=PROPERTY_205"=>34316),
            array("=PROPERTY_205"=>34315, "=PROPERTY_207"=>$id),
        );
        $res = CIBlockElement::GetList(array('ID' => 'desc'), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            //if (in_array($id, $arFields["PROPERTY_380_VALUE"])) {continue;}

            //проверка однотипности старая
            if ($arFields["PROPERTY_205_VALUE"] == 34316 and $arFields["PROPERTY_229_VALUE"]) { //общее по регулярному шаблону
                $regular_views_id = array();
                $arSelect2 = array("ID");
                $arFilter2 = array("IBLOCK_ID"=>90, "=PROPERTY_206"=>$user_type_code, "=PROPERTY_229"=>$arFields["PROPERTY_229_VALUE"], "=PROPERTY_210"=>$id);
                $res2 = CIBlockElement::GetList(array('ID' => 'desc'), $arFilter2, false, false, $arSelect2);
                while($ob2 = $res2->GetNextElement()) {
                    $arFields2 = $ob2->GetFields();
                    $regular_views_id[] = $arFields2["ID"];
                }
                $regular_views = count($regular_views_id);
                if ($regular_views >= $max_regular_views) {continue;}
            }

            //проверка однотипности новая
            if ($arFields["PROPERTY_205_VALUE"] == 34316 and $arFields["PROPERTY_229_VALUE"]) { //общее по регулярному шаблону
                $notification_issue = $extra_utils->template_notification_issue_check($id, $user_type, $arFields["PROPERTY_229_VALUE"], $arFields["ID"], $max_regular_views);
                if (!($notification_issue)) {continue;}
            }
            $notifications[] = $arFields["ID"];
        }
        $notifications_counter = count($notifications);
        $notifications_counter = 0;
        */
		$notifications_counter = 0;
		$arFilter              = array("=ID" => $id);
		$arSelectFields        = array("UF_CRM_NEW_NOTIF_WORKER", "UF_CRM_NEW_NOTIF_CLIENT");
		$res                   = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			if ($user_type == 0) {
				$notifications = $ob["UF_CRM_NEW_NOTIF_WORKER"];
			} else {
				$notifications = $ob["UF_CRM_NEW_NOTIF_CLIENT"];
			}
			$notifications_counter = count($notifications);
		}
		//сообщения в чате поддержки - В РАЗРАБОТКЕ
		$support_counter = 0;
		//события в профиле - В РАЗРАБОТКЕ
		$profile_counter = 0;
		$counts          = array(
			'orders_active' => $active_orders_counter,
			'orders_closed' => $closed_orders_counter,
			'notifications' => $notifications_counter,
			'support'       => $support_counter,
			'profile'       => $profile_counter,
			'my_orders'     => $my_orders_counter,
			//'home' => $main_counter,
		);

		return $counts;
	}

	//получение счетчиков по одному заказу
	function get_order_counts($id, $user_type, $order_id) {
		$waits             = 30956;
		$confirm           = 30957;
		$count             = 0;
		$count_newworkers  = 0;
		$count_newmessages = 0;
		global $yes;
		global $no;
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array(
			"CONTACT_ID",
			"STAGE_ID",
			'UF_CRM_MASTER',
			'UF_CRM_FEEDBACK_PROCESS',
			'UF_CRM_CLOSED_BY_WORKER',
			"UF_CRM_COUNTER_BONUSDONE",
			"UF_CRM_COUNTER_BONUSOFFER",
			"UF_CRM_COUNTER_ORDERCOMPL_WORKER",
			"UF_CRM_COUNTER_BONUSANSWER",
			"UF_CRM_COUNTER_NOMASTERS",
			"UF_CRM_COUNTER_ORDERCOMPL",
			"UF_CRM_COUNTER_FREEREQUEST"
		);
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			if ($user_type == 0) {
				if ($ob['UF_CRM_COUNTER_ORDERCOMPL_WORKER'] == $waits) {
					$count++;
				}
				if ($ob['UF_CRM_COUNTER_BONUSOFFER'] == $waits) {
					$count++;
				}
				if ($ob['UF_CRM_COUNTER_BONUSDONE'] == $waits) {
					$count++;
				}
			} else {
				if ($ob['UF_CRM_COUNTER_FREEREQUEST'] == $waits) {
					$count++;
				}
				if ($ob['UF_CRM_COUNTER_ORDERCOMPL'] == $waits) {
					$count++;
				}
				if ($ob['UF_CRM_COUNTER_NOMASTERS'] == $waits) {
					$count++;
				}
				if ($ob['UF_CRM_COUNTER_BONUSANSWER'] == $waits) {
					$count++;
				}
			}
		}
		//корректировка по откликам
		$arSelect = array("ID", "IBLOCK_ID");
		$arFilter = array("IBLOCK_ID" => 73);
		if ($user_type == 0) { //мастер
			$arFilter["=PROPERTY_136"] = $id;
			$arFilter["=PROPERTY_134"] = $no;
			$arFilter["=PROPERTY_132"] = array(31193, 31212);
			$arFilter["=PROPERTY_135"] = $order_id;
		} else if ($user_type == 1) { //заказчик
			$arFilter["=PROPERTY_137"] = $id;
			$arFilter["=PROPERTY_133"] = $no;
			$arFilter["=PROPERTY_132"] = 31191;
			$arFilter["=PROPERTY_135"] = $order_id;
		}
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$count++;
			if ($user_type == 1) {
				$count_newworkers++;
			}
		}
		$counts = array(
			'events'   => $count,
			'workers'  => $count_newworkers,
			'messages' => $count_newmessages,
		);

		return $counts;
	}

	//начисление бонусов за заявку
	function add_order_bonus($user_id, $order_id) {
		global $yes;
		global $setup_element;
		//проверяем заказ
		if ( ! ($user_id)) {
			return;
		}
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("UF_CRM_BONUS_ADD");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			if ($ob['UF_CRM_BONUS_ADD'] == $yes) {
				return;
			};
		}
		//гасим заказ
		$this->update_field_deal($order_id, 'UF_CRM_BONUS_ADD', $yes);
		//читаем настройки
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_242", "PROPERTY_243");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields     = $ob->GetFields();
			$max_counter  = $arFields["PROPERTY_242_VALUE"];
			$bonus_amount = $arFields["PROPERTY_243_VALUE"];
		};
		//читаем контакт
		$arFilter       = array("=ID" => $user_id);
		$arSelectFields = array("UF_CRM_ORDERS_FOR_BONUS");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$counter = $ob["UF_CRM_ORDERS_FOR_BONUS"];
		}
		//проверяем начисления и обновляем контакт
		$add_bonus = false;
		$counter   = $counter + 1;
		if ($counter >= $max_counter) {
			$add_bonus = true;
			$counter   = $counter - $max_counter;
		}
		$this->update_field_contact($user_id, "UF_CRM_ORDERS_FOR_BONUS", $counter);
		//начисляем бонусы
		if ($add_bonus) {
			//ищем запись по балансу
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $user_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields        = $ob->GetFields();
				$balance_element = $arFields["ID"];
				$balance_amount  = $arFields["PROPERTY_161_VALUE"];
			}
			//изменяем текущую
			if ($balance_element) {
				$PROP              = array();
				$PROP["SUMMA_RUB"] = $balance_amount + $bonus_amount;
				CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
			} //создаем новую
			else {
				$balance_element = $this->add_balance_element(31231, $bonus_amount, false, false, $user_id, "Бонусный баланс");
			}
			//фиксация транзакции
			$this->add_transaction(31239, $bonus_amount, false, $balance_element, 31231, false, $user_id, false, "Начисление бонусов");
			$this->add_notification($user_id, 0, 34492, false, $bonus_amount);
		}
	}

	//начисление бонусов на остаток
	function add_percent_bonus() {
		global $yes;
		global $no;
		global $setup_element;
		if ( ! ($yes)) {
			$globals       = $this->set_globals();
			$yes           = $globals['yes'];
			$no            = $globals['no'];
			$setup_element = $globals['setup_element'];
		}
		$worker_type = 31169;
		//защита от массовых срабатываний
		$control_element = 34510;
		$arSelect        = array("ID", "IBLOCK_ID", "PROPERTY_248");
		$arFilter        = array("IBLOCK_ID" => 99, "=ID" => $control_element);
		$res             = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$last_month = $arFields["PROPERTY_248_VALUE"];
		};
		$time_point            = date_create(date('Y-m-d', time()));
		$last_month_time_point = date_create(date('Y-m-d H:i:s', time()));
		date_add($last_month_time_point, date_interval_create_from_date_string("-1 months"));
		$month_now = date_format($time_point, 'n');
		if ($last_month == $month_now) {
			return;
		}
		$PROP = array(248 => $month_now);
		CIBlockElement::SetPropertyValuesEx($control_element, 99, $PROP);
		//чтение настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_245");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$bonus_percent = $arFields["PROPERTY_245_VALUE"];
		};
		//выборка мастеров
		CModule::IncludeModule("crm");
		$worker_list       = array();
		$worker_bonus_list = array();
		$arFilter          = array("=UF_CRM_NEW_APP_REGISTER" => $worker_type);
		$arSelectFields    = array("ID");
		$res               = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$worker_list[] = $ob["ID"];
		}
		foreach ($worker_list as $key => $worker_id) {
			$balance_element    = false;
			$balance_amount     = 0;
			$balance_last_month = 0;
			$bonus_amount_old   = 0;
			//ищем запись по основному балансу
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31230, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $worker_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields       = $ob->GetFields();
				$balance_amount = $arFields["PROPERTY_161_VALUE"];
			}
			//считаем пополнения за последний месяц
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_187");
			$arFilter = array("IBLOCK_ID" => 87, "=PROPERTY_192" => $worker_id, "=PROPERTY_186" => 31235, ">DATE_CREATE" => date_format($last_month_time_point, 'd.m.Y H:i:s'));
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields           = $ob->GetFields();
				$balance_last_month = $balance_last_month + $arFields["PROPERTY_187_VALUE"];
			}
			//отрезаем лишнее
			if ($balance_last_month < $balance_amount) {
				$balance_amount = $balance_last_month;
			}
			//проверка и расчет
			$bonus_amount = round((($balance_amount * $bonus_percent) / 100), 0);
			if ($balance_amount == 0 or $bonus_amount == 0) {
				continue;
			}
			$worker_bonus_list[] = $worker_id;
			//ищем запись по бонусному балансу
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $worker_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields         = $ob->GetFields();
				$balance_element  = $arFields["ID"];
				$bonus_amount_old = $arFields["PROPERTY_161_VALUE"];
			}
			//изменяем текущую
			if ($balance_element) {
				$PROP              = array();
				$PROP["SUMMA_RUB"] = $bonus_amount_old + $bonus_amount;
				CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
			} //создаем новую
			else {
				$balance_element = $this->add_balance_element(31231, $bonus_amount, false, false, $worker_id, "Бонусный баланс");
			}
			//фиксация транзакции
			$this->add_transaction(31239, $bonus_amount, false, $balance_element, 31231, false, $worker_id, false, "Ежемесячное начисление кэшбека на остаток");
		}
		$this->add_notification($worker_bonus_list, 0, 34511, false, $bonus_percent);
	}

	//начисление бонусов за покупку пакета
	function add_package_bonus($user_id, $package_price, $balance_spend, $package_title) {
		global $yes;
		global $no;
		$rate = $balance_spend / $package_price;
		if ($rate == 1.00) {
			$cashback_rate = 0.10;
		} else if ($rate >= 0.50) {
			$cashback_rate = 0.05;
		} else if ($rate >= 0.01) {
			$cashback_rate = 0.00;
		} else {
			return false;
		}
		$cashback_amount = round(($package_price * $cashback_rate), 0);
		if ($cashback_amount == 0) {
			return false;
		}
		//ищем запись по балансу
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
		$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $user_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$balance_element = $arFields["ID"];
			$balance_amount  = $arFields["PROPERTY_161_VALUE"];
		}
		//изменяем текущую
		if ($balance_element) {
			$PROP              = array();
			$PROP["SUMMA_RUB"] = $balance_amount + $cashback_amount;
			CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
		} //создаем новую
		else {
			$balance_element = $this->add_balance_element(31231, $cashback_amount, false, false, $user_id, "Бонусный баланс");
		}
		//фиксация транзакции
		$this->add_transaction(31239, $cashback_amount, false, $balance_element, 31231, false, $user_id, false, "Кэшбэк за покупку пакета " . $package_title);
		$this->add_notification($user_id, 0, 39502, false, $cashback_amount);

		return $cashback_amount;
	}

	//реализация возврата
	function realize_refund($order_id) {
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		$worker_type = 31169;
		$client_type = 31170;
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array(
			"TITLE",
			"OPPORTUNITY",
			"CONTACT_ID",
			"UF_CRM_MASTER",
			"UF_CRM_ACCEPT_PERCENT",
			"STAGE_ID",
			"UF_CRM_REFUND_DECISION",
			"UF_CRM_REFUND_ELEMENT",
			"UF_CRM_FEEDBACK_PROCESS",
			"UF_CRM_CLOSED_BY_WORKER",
			"UF_CRM_CANCELED_WORKERS"
		);
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$title            = $ob['TITLE'];
			$order_price      = $ob['OPPORTUNITY'];
			$client_id        = $ob['CONTACT_ID'];
			$worker_id        = $ob['UF_CRM_MASTER'];
			$refund           = $ob['UF_CRM_REFUND_DECISION'];
			$refund_element   = $ob['UF_CRM_REFUND_ELEMENT'];
			$stage            = $ob['STAGE_ID'];
			$client_closed    = $ob['UF_CRM_FEEDBACK_PROCESS'];
			$worker_closed    = $ob['UF_CRM_CLOSED_BY_WORKER'];
			$canceled_workers = $ob['UF_CRM_CANCELED_WORKERS'];
		}
		//Получение выбранного отклика данного мастера
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_138", "PROPERTY_249");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_129" => 31188, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $worker_id, "=PROPERTY_132" => 31193);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$respond       = $arFields["ID"];
			$respond_price = $arFields["PROPERTY_138_VALUE"];
			$last_refund   = $arFields["PROPERTY_249_VALUE"];
		}
		if ( ! ($respond) or ! ($respond_price)) {
			CIBlockElement::SetPropertyValuesEx($refund_element, 103, array(255 => 34541));
			$this->timeline_comment($order_id, "ВНИМАНИЕ! Возврат не был проведен, не найден подходящий отклик данного мастера(по тарифу стандарт)");
		} else if ($last_refund == $yes) {
			CIBlockElement::SetPropertyValuesEx($refund_element, 103, array(255 => 34541));
			$this->timeline_comment($order_id, "ВНИМАНИЕ! Возврат не был проведен, поскольку по данному отклику возврат уже проводился ранее");
		} else if ( ! ($refund) or $refund == 34541) {
			CIBlockElement::SetPropertyValuesEx($refund_element, 103, array(255 => 34541));
			$this->timeline_comment($order_id, "Возврат отклонен менеджером, мастеру направлено уведомление");
			$this->add_notification($worker_id, 0, 34544, $order_id, $title);
		} else {
			//проводим возврат
			if ($refund == 34543) { //полный возврат
				$refund_amount = $respond_price;
				$this->timeline_comment($order_id, "Возврат был одобрен в полном объеме, мастеру направлено уведомление");
				$this->add_notification($worker_id, 0, 34546, $order_id, $title);
			} else { //частичный возврат
				if ( ! ($order_price) or $order_price < 5000) {
					$refund_reduction = 50;
				} else if ($order_price < 10000) {
					$refund_reduction = 100;
				} else if ($order_price < 15000) {
					$refund_reduction = 150;
				} else if ($order_price < 20000) {
					$refund_reduction = 200;
				} else if ($order_price < 30000) {
					$refund_reduction = 250;
				} else if ($order_price < 50000) {
					$refund_reduction = 300;
				} else {
					$refund_reduction = 450;
				}
				$refund_amount = $respond_price - $refund_reduction;
				if ($refund_amount <= 0) {
					$refund_amount = 0;
					$this->timeline_comment($order_id, "Возврат был одобрен частично, однако после вычета поправочной суммы возвращать стало нечего, мастеру направлено уведомление(отказ в возврате)");
					$this->add_notification($worker_id, 0, 34544, $order_id, $title);
				} else {
					$this->timeline_comment($order_id, "Возврат был одобрен частично, мастеру направлено уведомление");
					$this->add_notification($worker_id, 0, 34545, $order_id, $title);
				}
			}
			if ($refund_amount > 0) {
				//ищем запись по бонусному балансу
				$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
				$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $worker_id);
				$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields      = $ob->GetFields();
					$bonus_element = $arFields["ID"];
					$bonus_amount  = $arFields["PROPERTY_161_VALUE"];
				}
				//изменяем текущую
				if ($bonus_element) {
					$PROP              = array();
					$PROP["SUMMA_RUB"] = $bonus_amount + $refund_amount;
					CIBlockElement::SetPropertyValuesEx($bonus_element, 80, $PROP);
				} //создаем новую
				else {
					$bonus_element = $this->add_balance_element(31231, $refund_amount, false, false, $worker_id, "Бонусный баланс");
				}
				//фиксация транзакции
				$element_id = $this->add_transaction(31242, $refund_amount, false, $bonus_element, 31231, false, $worker_id, false, "Возврат по заказу " . $title);
				//проставить возврат - был
				$PROP      = array();
				$PROP[249] = $yes;
				CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
			}
			CIBlockElement::SetPropertyValuesEx($refund_element, 103, array(255 => $refund, 256 => $refund_amount, 257 => $element_id));
		}
		//приведение заявки в соответствие стадии
		$new_stages      = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
		$inwork_stages   = array('C7:FINAL_INVOICE');
		$postwork_stages = array('C7:1', 'C7:3');
		$success_stages  = array('C7:WON');
		$lose_stages     = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		$time_point      = date_create(date('Y-m-d H:i:s', time()));
		if (in_array($stage, (array) $new_stages)) {
			//отбраковка отклика
			$PROP            = array();
			$PROP["AKTIVEN"] = 31192;  //неактивен
			CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
			//убрать мастера из сделки, актуализироваь заказ
			$time_point         = date_create(date('Y-m-d H:i:s', time()));
			$canceled_workers[] = $worker_id;
			$entity             = new CCrmDeal;
			$fields             = array(
				"UF_CRM_MASTER"                 => "",
				"UF_CRM_ACTUALTIME"             => date_format($time_point, 'd.m.Y H:i:s'),
				"UF_CRM_COUNTER_WATCHEDMASTERS" => array(),
				"UF_CRM_CANCELED_WORKERS"       => $canceled_workers,
				"UF_CRM_HAND_ORDER_PUSH"        => false,
				"UF_CRM_REPORT_TAX"             => false,
				"UF_CRM_REPORT_TARIF"           => false,
				"UF_CRM_RATING_MAIN"            => false,
				"UF_CRM_RATING_QUALITY"         => false,
				"UF_CRM_RATING_CLEAN"           => false,
				"UF_CRM_RATING_TIME"            => false,
				"UF_CRM_RATING_TEXT"            => false,
				"UF_CRM_FEEDBACK_PROCESS"       => false,
				"UF_CRM_COUNTER_ORDERCOMPL"     => false,
			);
			$entity->update($order_id, $fields);
			//удалить запись в заказах мастеров
			$arSelect = array("ID", "IBLOCK_ID");
			$arFilter = array("IBLOCK_ID" => 82, "=PROPERTY_169" => $order_id, "=PROPERTY_170" => $worker_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields   = $ob->GetFields();
				$element_id = $arFields["ID"];
			};
			$this->element_delete(82, $element_id);
		} else if (in_array($stage, (array) $postwork_stages)) {
			$entity = new CCrmDeal;
			$fields = array(
				"UF_CRM_1607157907"       => $order_price,
				"UF_CRM_WHO_COMPLETE"     => $worker_type,
				"UF_CRM_COMPLETE_TIME"    => date_format($time_point, 'd.m.Y H:i:s'),
				"UF_CRM_CLOSED_BY_WORKER" => 30957,
			);
			if ($client_closed != 30957) {
				$fields["UF_CRM_COUNTER_ORDERCOMPL"] = 30956;
				$this->add_notification($client_id, 1, 34422, $order_id, $title);
			}
			$entity->update($order_id, $fields);
		} else if (in_array($stage, (array) $success_stages)) {
			$entity = new CCrmDeal;
			$fields = array(
				"UF_CRM_1607157907"       => $order_price,
				"UF_CRM_WHO_COMPLETE"     => $worker_type,
				"UF_CRM_COMPLETE_TIME"    => date_format($time_point, 'd.m.Y H:i:s'),
				"UF_CRM_CLOSED_BY_WORKER" => 30957,
			);
			$entity->update($order_id, $fields);
		} else if (in_array($stage, (array) $lose_stages)) {
			$entity = new CCrmDeal;
			$fields = array(
				"UF_CRM_LOSE_REASON"      => 35155,
				"UF_CRM_CLOSED_BY_WORKER" => 30957,
			);
			$entity->update($order_id, $fields);
			//отбраковка отклика
			$PROP            = array();
			$PROP["AKTIVEN"] = 31192;  //неактивен
			CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
		}
		$this->update_field_deal($order_id, 'UF_CRM_REFUND_REQUEST', 30957);
	}

	//проверка перерасчета
	function respond_recount($order_id, $price_fact) {
		//чтение заказа
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("CONTACT_ID", "TITLE", "ASSIGNED_BY_ID", "UF_CRM_MASTER", "OPPORTUNITY", "STAGE_ID", "UF_CRM_MAIN_TYPE", "UF_CRM_1607157907");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$client      = $ob['CONTACT_ID'];
			$title       = $ob['TITLE'];
			$worker_id   = $ob['UF_CRM_MASTER'];
			$price_start = $ob['OPPORTUNITY'];
			$stage       = $ob['STAGE_ID'];
			$main_type   = $ob['UF_CRM_MAIN_TYPE'];
			$assigned_id = $ob['ASSIGNED_BY_ID'];
			$assigned_id = str_replace('user_', '', $assigned_id);
		}
		if ( ! ($price_fact)) {
			$price_fact = $ob['UF_CRM_1607157907'];
		}
		if ( ! ($price_fact)) {
			$price_fact = $price_start;
		}
		if ($main_type != 34604) {
			return false;
		}
		if ($price_start == 0) {
			return false;
		}
		$ratio = round(($price_fact / $price_start), 3);
		//Получение выбранного отклика данного мастера
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_129", "PROPERTY_130", "PROPERTY_131", "PROPERTY_138");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $worker_id, "=PROPERTY_132" => 31193);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$respond       = $arFields["ID"];
			$respond_price = $arFields["PROPERTY_138_VALUE"];
			$respond_type  = $arFields["PROPERTY_129_VALUE"];
			$offer_price   = $arFields["PROPERTY_130_VALUE"];
			$package_id    = $arFields["PROPERTY_131_VALUE"];
		}
		if ( ! ($respond) or ! ($respond_price)) {
			return false;
		}
		$new_respond_price = round(($respond_price * $ratio), 0);
		$respond_price_dif = $respond_price - $new_respond_price;
		if ($respond_price_dif <= 0) {
			return false;
		}
		$text = "НЕОБХОДИМО РАССМОТРЕТЬ ПЕРЕРАСЧЕТ КОМИССИИ ЗА ЗАЯВКУ <*>Информация:<*>";
		$text = $text . "Изначальная цена работ " . $price_start . " руб <*>";
		$text = $text . "Предложенная мастером цена работ " . $offer_price . " руб <*>";
		$text = $text . "Итоговая цена работ " . $price_fact . " руб <*>";
		$text = $text . "Коэффициент изменения стоимости " . $ratio . " <*>";
		if ($respond_type == 31187) {
			$arSelect = array("ID", "IBLOCK_ID", "NAME");
			$arFilter = array("IBLOCK_ID" => 75, "=ID" => $package_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields      = $ob->GetFields();
				$package_title = $arFields["NAME"];
			};
			$text = $text . "Отклик был осуществлен по пакету " . $package_title . " <*>";
		} else {
			$text = $text . "Отклик был осуществлен по проценту от стоимости заказа <*>";
		}
		$text = $text . "Изначальная цена отклика " . $respond_price . " руб <*>";
		$text = $text . "Новая предлагаемая цена отклика " . $new_respond_price . " руб <*>";
		$text = $text . "Мастеру будет возвращено на бонусный счет " . $respond_price_dif . " руб <*>";
		$text = $text . " <*>Переведите сделку в следующую стадию и укажите решение по перерасчету <*>";
		$text = $this->br_encode($text);
		$this->timeline_comment($order_id, $text);

		return true;
	}

	//реализация перерасчета
	function realize_recount($order_id) {
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		CModule::IncludeModule("crm");
		//чтение заказа
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("CONTACT_ID", "TITLE", "ASSIGNED_BY_ID", "UF_CRM_MASTER", "OPPORTUNITY", "STAGE_ID", "UF_CRM_MAIN_TYPE", "UF_CRM_1607157907", "UF_CRM_RECOUNT_DECISION");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$client      = $ob['CONTACT_ID'];
			$title       = $ob['TITLE'];
			$worker_id   = $ob['UF_CRM_MASTER'];
			$price_start = $ob['OPPORTUNITY'];
			$price_fact  = $ob['UF_CRM_1607157907'];
			$stage       = $ob['STAGE_ID'];
			$main_type   = $ob['UF_CRM_MAIN_TYPE'];
			$assigned_id = $ob['ASSIGNED_BY_ID'];
			$assigned_id = str_replace('user_', '', $assigned_id);
			$recount     = $ob['UF_CRM_RECOUNT_DECISION'];
		}
		if ($recount == 34620) {
			$this->timeline_comment($order_id, "Менеджером было принято решение отказать в перерасчете");

			return false;
		}
		$failure_text = "Менеджером было принято решение произвести перерасчет, однако для данной заявки перерасчет невозможен";
		if ( ! ($price_fact)) {
			$price_fact = $price_start;
		}
		if ($main_type != 34604) {
			$this->timeline_comment($order_id, $failure_text);

			return false;
		}
		if ($price_start == 0) {
			$this->timeline_comment($order_id, $failure_text);

			return false;
		}
		$ratio = round(($price_fact / $price_start), 3);
		//Получение выбранного отклика данного мастера
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_129", "PROPERTY_130", "PROPERTY_131", "PROPERTY_138");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $worker_id, "=PROPERTY_132" => 31193);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$respond       = $arFields["ID"];
			$respond_price = $arFields["PROPERTY_138_VALUE"];
			$respond_type  = $arFields["PROPERTY_129_VALUE"];
			$offer_price   = $arFields["PROPERTY_130_VALUE"];
			$package_id    = $arFields["PROPERTY_131_VALUE"];
		}
		if ( ! ($respond) or ! ($respond_price)) {
			$this->timeline_comment($order_id, $failure_text);

			return false;
		}
		//расчет возврата
		$new_respond_price = round(($respond_price * $ratio), 0);
		$refund_amount     = $respond_price - $new_respond_price;
		if ($refund_amount <= 0) {
			$this->timeline_comment($order_id, $failure_text);

			return false;
		}
		//ищем запись по бонусному балансу
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
		$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $worker_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields      = $ob->GetFields();
			$bonus_element = $arFields["ID"];
			$bonus_amount  = $arFields["PROPERTY_161_VALUE"];
		}
		//изменяем текущую
		if ($bonus_element) {
			$PROP              = array();
			$PROP["SUMMA_RUB"] = $bonus_amount + $refund_amount;
			CIBlockElement::SetPropertyValuesEx($bonus_element, 80, $PROP);
		} //создаем новую
		else {
			$bonus_element = $this->add_balance_element(31231, $refund_amount, false, false, $worker_id, "Бонусный баланс");
		}
		//фиксация транзакции
		$this->add_transaction(34808, $refund_amount, false, $bonus_element, 31231, false, $worker_id, false, "Перерасчет комиссии по заказу " . $title);
		//проставить скорректированную цену в отклик
		$PROP      = array();
		$PROP[138] = $new_respond_price;
		CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
		//уведомления и корректировка полей
		$this->add_notification($worker_id, 0, 34622, $order_id, $title);
		$success_text = "Менеджером было принято решение произвести перерасчет по отклику по причине изменения конечной цены заказа . Мастеру зачислено " . $refund_amount . " бонусов";
		$this->timeline_comment($order_id, $success_text);
		$this->update_field_deal($order_id, 'UF_CRM_RECOUNT_REQUEST', 30957);

		return true;
	}

	//отбраковка откликов при списании баланса
	function responds_check($user_id) {
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		CModule::IncludeModule("crm");
		//собираем балансовые записи
		$main_balance_amount    = 0;
		$bonus_balance_amount   = 0;
		$package_balance_amount = array();
		$time_point             = date_create(date('Y-m-d H:i:s', time()));
		$arSelect               = array("ID", "IBLOCK_ID", "PROPERTY_160", "PROPERTY_161", "PROPERTY_162", "PROPERTY_163", "PROPERTY_200_VALUE");
		$arFilter               = array("IBLOCK_ID" => 80, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $user_id);
		$res                    = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if ($arFields["PROPERTY_160_VALUE"] == 31230) {
				$main_balance_amount = $arFields["PROPERTY_161_VALUE"];
			} else if ($arFields["PROPERTY_160_VALUE"] == 31231) {
				$bonus_balance_amount = $arFields["PROPERTY_161_VALUE"];
			} else if ($arFields["PROPERTY_160_VALUE"] == 31232) {
				$package_end = date_create($arFields["PROPERTY_200_VALUE"]);
				if ($package_end <= $time_point) {
					continue;
				}
				$package_balance_amount[ $arFields["PROPERTY_162_VALUE"] ] = $arFields["PROPERTY_163_VALUE"];
			}
		}
		$total_balance_amount = $main_balance_amount + $bonus_balance_amount;
		//проверяем отклики
		$disabling_responds = array();
		$arSelect           = array("ID", "IBLOCK_ID", "PROPERTY_129", "PROPERTY_131", "PROPERTY_135", "PROPERTY_138");
		$arFilter           = array("IBLOCK_ID" => 73, "=PROPERTY_132" => 31191, "=PROPERTY_136" => $user_id);
		$res                = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$respond_id = $arFields["ID"];
			$order_id   = $arFields["PROPERTY_135_VALUE"];
			if ($arFields["PROPERTY_129_VALUE"] == 31188) { //за процент от стоимости
				if ($arFields["PROPERTY_138_VALUE"] > $total_balance_amount) {
					$disabling_responds[] = array('respond_id' => $respond_id, 'order_id' => $order_id);
				}
			} else if ($arFields["PROPERTY_129_VALUE"] == 31187) { //за пакет
				if ( ! ($package_balance_amount[ $arFields["PROPERTY_131_VALUE"] ] and $package_balance_amount[ $arFields["PROPERTY_131_VALUE"] ] > 0)) {
					$disabling_responds[] = array('respond_id' => $respond_id, 'order_id' => $order_id);
				}
			}
		}
		//отбраковка откликов и уведомления
		$PROP = array(132 => 31212);
		foreach ($disabling_responds as $key => $respond) {
			CIBlockElement::SetPropertyValuesEx($respond['respond_id'], 73, $PROP);
			$res = CCrmDeal::GetListEx(array(), array("=ID" => $respond['order_id']), false, false, array("TITLE"));
			while ($ob = $res->GetNext()) {
				$title = $ob['TITLE'];
			}
			$this->add_notification($user_id, 0, 35088, $respond['order_id'], $title);
		}
	}

	//перевод стадии
	function change_stage($deal_id, $stage_id) {
		CModule::IncludeModule("crm");
		$entity = new CCrmDeal;
		$fields = array(
			"STAGE_ID" => $stage_id,
		);
		$entity->update($deal_id, $fields);
	}

	//добавление комментария в карточку сделки
	function timeline_comment($deal_id, $text) {
		$text = json_encode($text);
		$text = str_replace('~BR~', '\r\n', $text);
		$text = json_decode($text);
		\Bitrix\Crm\Timeline\CommentEntry::create(
			array(
				'TEXT'      => $text,
				'SETTINGS'  => array('HAS_FILES' => 'N'),
				'AUTHOR_ID' => 146420,
				'BINDINGS'  => array(array('ENTITY_TYPE_ID' => 2, 'ENTITY_ID' => $deal_id))
			));
	}

	//добавление задания в карточку сделки
	function timeline_activity($deal_id, $text, $user_id) {
		$arFields = array(
			'BINDINGS'       => array(array('OWNER_TYPE_ID' => 2, 'OWNER_ID' => $deal_id)),
			'TYPE_ID'        => 1, //встреча
			'SUBJECT'        => "Рассмотреть перерасчет по заявке",
			'RESPONSIBLE_ID' => $user_id,
			'DESCRIPTION'    => $text,
		);
		$ID       = CCrmActivity::Add($arFields, false, true, array());
	}

	//склонение в истории балансов
	function plural_form($amount, $item_type) {
		switch ($item_type) {
			case 0:
				$string_array = array('рубль', 'рубля', 'рублей');
				break;
			case 1:
				$string_array = array('балл', 'балла', 'баллов');
				break;
			case 2:
				$string_array = array('заказ', 'заказа', 'заказов');
				break;
		}
		$n  = $amount % 100;
		$n1 = $amount % 10;
		if ($n >= 11 and $n <= 19) {
			$form = $string_array[2];
		} else if ($n1 >= 2 and $n1 <= 4) {
			$form = $string_array[1];
		} else if ($n1 == 1) {
			$form = $string_array[0];
		} else {
			$form = $string_array[2];
		}

		return $amount . " " . $form;
	}

	//назначение персонального уведомления
	function add_notification($user_id, $user_type, $sys_notification_id, $object_id, $text_add) {
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		if ($user_type == 0) {
			$user_type_code = 31169;
		} else {
			$user_type_code = 31170;
		}
		//проверка существования и регистрации в приложении
		if ( ! ($user_id)) {
			return;
		}
		if ( ! ($this->is_register($user_id, $user_type))) {
			return;
		}
		//читаем шаблон
		$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_222", "PROPERTY_223", "PROPERTY_224", "PROPERTY_235");
		$arFilter = array("IBLOCK_ID" => 95, "=ID" => $sys_notification_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields    = $ob->GetFields();
			$name        = $arFields["NAME"];
			$text        = $arFields["~PROPERTY_222_VALUE"]["TEXT"];
			$event_type  = $arFields["PROPERTY_223_VALUE"];
			$days_active = $arFields["PROPERTY_224_VALUE"];
			if ($arFields["PROPERTY_235_VALUE"] == $yes) {
				$send_push = $yes;
			} else {
				$send_push = $no;
			}
		}
		//добавляем уведомление в журнал
		if ($text_add) {
			$text = str_replace('(*)', $text_add, $text);
		}
		$time_point = date_create(date('Y-m-d H:i:s', time()));
		date_add($time_point, date_interval_create_from_date_string($days_active . " days"));
		$el   = new CIBlockElement;
		$PROP = array(
			204 => $text,
			205 => 34315,
			206 => $user_type_code,
			218 => $event_type,
			208 => $yes,
			209 => date_format($time_point, 'd.m.Y H:i:s'),
			207 => $user_id,
			237 => $send_push,
		);
		if ($object_id) {
			$PROP[219] = $object_id;
		}
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 90,
			"NAME"            => $name,
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id         = $el->Add($arLoadProductArray);
		//запуск БП отложенной деактивации и отправки пуша
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				202,
				array("lists", "BizprocDocument", $element_id),
				array_merge(),
				$arErrorsTmp
			);
		}
	}

	//отправка пуша - подготовка данных
	function send_push($notification_id) {
		global $yes;
		global $no;
		global $setup_element;
		if ( ! ($yes)) {
			$globals       = $this->set_globals();
			$yes           = $globals['yes'];
			$no            = $globals['no'];
			$setup_element = $globals['setup_element'];
		}
		include_once('extra_utils.php');
		$extra_utils = new ExtraUtils();
		//чтение настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_230");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields          = $ob->GetFields();
			$max_regular_views = $arFields["PROPERTY_230_VALUE"];
		};
		//читаем уведомление
		$arSelect    = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_204", "PROPERTY_205", "PROPERTY_206", "PROPERTY_207", "PROPERTY_218", "PROPERTY_219", "PROPERTY_229");
		$arFilter    = array("IBLOCK_ID" => 90, "=ID" => $notification_id);
		$res         = CIBlockElement::GetList(array('ID' => 'desc'), $arFilter, false, false, $arSelect);
		$worker_type = 31169;
		$client_type = 31170;
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$title    = $arFields["NAME"];
			$text     = $this->html_decode($arFields["~PROPERTY_204_VALUE"]["TEXT"]);
			if ($arFields["PROPERTY_205_VALUE"] == 34316) {
				$type = 'common';
			} else if ($arFields["PROPERTY_205_VALUE"] == 34315) {
				$type = 'private';
			} else {
				return;
			}
			if (in_array($worker_type, (array) $arFields["PROPERTY_206_VALUE"]) and in_array($client_type, (array) $arFields["PROPERTY_206_VALUE"])) {
				$user_type = 'common';
			} else if (in_array($worker_type, (array) $arFields["PROPERTY_206_VALUE"])) {
				$user_type = 'worker';
			} else if (in_array($client_type, (array) $arFields["PROPERTY_206_VALUE"])) {
				$user_type = 'client';
			} else {
				return;
			}
			$event_type = $arFields["PROPERTY_218_VALUE"];
			if ( ! ($event_type)) {
				$event_type = 34355;
			}
			$user_list = $arFields["PROPERTY_207_VALUE"];
			if ($type == 'private' and ! ($user_list)) {
				return;
			}
			$template = $arFields["PROPERTY_229_VALUE"];
			$order_id = $arFields["PROPERTY_219_VALUE"];
		}
		if ( ! ($order_id)) {
			$order_id = null;
		}

		$block_list = array();
		//собираем пользователей, у кого разрешен данный тип
		if ($type == 'common') {
			$user_list = array();
			if ($user_type == 'worker') {
				$arFilter = array("=UF_CRM_NEW_APP_REGISTER" => $worker_type);
				if ($template) {
					$block_list = $extra_utils->template_notification_block_list($template, 0);
				}
			} else if ($user_type == 'client') {
				$arFilter = array("=UF_CRM_NEW_APP_REGISTER" => $client_type);
				if ($template) {
					$block_list = $extra_utils->template_notification_block_list($template, 1);
				}
			} else {
				$arFilter = array("! UF_CRM_NEW_APP_REGISTER" => false);
				if ($template) {
					$block_list  = $extra_utils->template_notification_block_list($template, 0);
					$block_list2 = $extra_utils->template_notification_block_list($template, 1);
					$block_list  = array_values(array_unique(array_merge($block_list, $block_list2)));
					array_multisort($block_list);
				}
			}
			$arFilter["=UF_CRM_PUSH_AVAILABLE"] = $event_type;
			$arFilter["CHECK_PERMISSIONS"]      = 'N';
			$arSelectFields                     = array("ID");
			$res                                = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$user_id = $ob["ID"];
				if (in_array($user_id, (array) $block_list)) {
					continue;
				}
				$user_list[] = $user_id;
			}
		}
		//собираем токены
		$token_associate = array();
		$token_list      = array();
		$arSelect        = array("ID", "IBLOCK_ID", "PROPERTY_238", "PROPERTY_239", "PROPERTY_240");
		$arFilter        = array("IBLOCK_ID" => 97, "=PROPERTY_240" => $user_list);
		if ($user_type == 'worker') {
			$arFilter["=PROPERTY_241"] = $worker_type;
		} else if ($user_type == 'client') {
			$arFilter["=PROPERTY_241"] = $client_type;
		}

		$res = CIBlockElement::GetList(array('ID' => 'DESC'), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$user_id  = $arFields["PROPERTY_240_VALUE"];
			$token    = $arFields["PROPERTY_238_VALUE"];
			if ($token_associate[ $user_id ]) {
				$token_associate[ $user_id ][] = $token;
				if (count($token_associate[ $user_id ]) > 1) {
					continue;
				}
			} else {
				$token_associate[ $user_id ] = array($token);
			}
			$token_list[] = $arFields["PROPERTY_238_VALUE"];
		};
		if ( ! ($user_list) or ! ($token_list)) {
			return false;
		}

		//пишем лог
		$log                = array(
			'order_id'   => $order_id,
			'user_list'  => $user_list,
			'token_list' => $token_list,
		);
		$log                = 'title: ' . $title . ', text: ' . $text . ', ' . json_encode($log);
		$el                 = new CIBlockElement;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 89,
			"NAME"            => "Отправка пушей",
			"PROPERTY_VALUES" => array("ZAPROS" => $log),
		);
		$element_id         = $el->Add($arLoadProductArray);

		//непосредственно отправка пушей
		$this->send_push_final($token_list, $title, $text, $order_id);
	}

	//отправка пуша - отправка по токенам
	function send_push_final($token_list, $title, $text, $order_id) {

		/*
        //В РАЗРАБОТКЕ - заглушка для тестов, потом убрать
        $test_token = 'e_ns_uMz_7Q:APA91bFJ-UxJqH7xiIwFPqOOBBTdk-dc312MbjF8CihnBP1TS5y31bsl_uEdvlyHHGWh97vq7H1LB-EY65NgvJ_wZ71A467DT8zbQ8LaOxOwtbQYjRztCDvss8IaeqfKkuH_m4n_8vPP';
        if (in_array($test_token, $token_list)) {
            $token_list = array($test_token);
        }
        else {
            return;
        }
        */


		include_once 'CPushNotification.php';
		$notification = new CPushNotification();
		$notification->setRecipient($token_list)
		             ->setTitle($title)
		             ->setBody($text)
		             ->setData(array(
			             'order_id' => $order_id,
		             ));
		$response = $notification->send();
	}

	//проверка регистрации в новом приложении
	function is_register($id, $user_type) {
		if ($user_type == 0) {
			$user_code = 31169;
		} else {
			$user_code = 31170;
		}
		$arSelectFields = array("UF_CRM_NEW_APP_REGISTER");
		$res            = CCrmContact::GetListEx(array(), array("=ID" => $id, "CHECK_PERMISSIONS" => 'N'), false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$is_register = $ob["UF_CRM_NEW_APP_REGISTER"];
		}
		if (in_array($user_code, (array) $is_register)) {
			return true;
		} else {
			return false;
		}
	}

	//запуск переразмещений
	function replace_start($price) {
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		$replacing_rates = array();
		$arSelect        = array("ID", "IBLOCK_ID", "PROPERTY_231", "PROPERTY_232");
		$arFilter        = array("IBLOCK_ID" => 96, "<=PROPERTY_231" => $price, "=PROPERTY_233" => $yes);
		$res             = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields                                           = $ob->GetFields();
			$replacing_rates[ $arFields["PROPERTY_231_VALUE"] ] = $arFields["PROPERTY_232_VALUE"];
		};
		if ($replacing_rates) {
			ksort($replacing_rates);
			$replacing_rates = array_reverse($replacing_rates, true);
			reset($replacing_rates);
			$replace_times = current($replacing_rates);
		} else {
			$replace_times = 0;
		}

		return $replace_times;
	}

	//лог кассы
	function payment_log($operation, $request, $user_id, $version = "") {
		if ($operation == 1) {
			$name           = "Успешный платеж";
			$operation_code = 32374;
		} else if ($operation == 2) {
			$name           = "Проверка платежа - запрос приложения";
			$operation_code = 32375;
		} else if ($operation == 5) {
			$name           = "Проверка платежа - параметры гет - запроса";
			$operation_code = 128554;
		} else if ($operation == 3) {
			$name           = "Проверка платежа - ответ кассы";
			$operation_code = 35055;
		} else if ($operation == 4) {
			$name           = "Проверка платежа - ответ в приложение";
			$operation_code = 35056;
		}
		$el                 = new CIBlockElement;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 85,
			"NAME"            => $name,
			"PROPERTY_VALUES" => array(
				184 => $operation_code,
				263 => json_encode($request),
				262 => $user_id,
				412 => $version,
			),
		);
		$element_id         = $el->Add($arLoadProductArray);

		return $element_id;
	}

	//добавление записи по внутрисистемной транзакции
	function add_transaction($type, $value, $package_value, $balance_element, $balance_type, $kassa_element, $user_id, $order_id, $title) {
		$el        = new CIBlockElement;
		$PROP      = array();
		$PROP[186] = $type;
		$PROP[297] = $this->get_element_name(83, $type);
		if ($value) {
			$PROP[187] = round($value, 0);
		}
		if ($package_value) {
			$PROP[188] = $package_value;
		}
		$PROP[189] = $balance_element;
		$PROP[298] = $this->get_element_name(80, $balance_element);
		$PROP[190] = $balance_type;
		$PROP[299] = $this->get_element_name(81, $balance_type);
		if ($kassa_element) {
			$PROP[191] = $kassa_element;
		}
		$PROP[192] = $user_id;
		$PROP[300] = $this->get_contact_name($user_id);
		if ($order_id) {
			$PROP[251] = $order_id;
		}
		$PROP[301]          = date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y');
		$city_id            = $this->get_contact_city($user_id);
		$PROP[311]          = $this->get_element_name(84, $city_id);
		$PROP[355]          = $city_id;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 87,
			"NAME"            => $title,
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id         = $el->Add($arLoadProductArray);

		return $element_id;
	}

	//добавление записи по балансу
	function add_balance_element($type, $amount, $package_id, $package_amount, $user_id, $title) {
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		$el        = new CIBlockElement;
		$PROP      = array();
		$PROP[160] = $type;
		$PROP[292] = $this->get_element_name(81, $type);
		if ($amount) {
			$PROP[161] = $amount;
		}
		if ($package_id) {
			$PROP[162] = $package_id;
			$PROP[294] = $this->get_element_name(75, $package_id);
		}
		if ($package_amount) {
			$PROP[163] = $package_amount;
		}
		$PROP[167]          = $yes;
		$PROP[296]          = $this->get_element_name(77, $yes);
		$PROP[168]          = $user_id;
		$PROP[293]          = $this->get_contact_name($user_id);
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 80,
			"NAME"            => $title,
			"PROPERTY_VALUES" => $PROP,
		);
		$balance_element    = $el->Add($arLoadProductArray);

		return $balance_element;
	}

	//получение названий полей-привязок
	function get_element_name($iblock_id, $element_id) {
		$name = "";
		if ( ! ($element_id)) {
			return $name;
		}
		$arSelect = array("NAME");
		$arFilter = array("IBLOCK_ID" => $iblock_id, "=ID" => $element_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$name     = $arFields["NAME"];
		};

		return $name;
	}

	//получение названий полей-привязок к разделам
	function get_section_name($iblock_id, $section_id) {
		if ( ! ($iblock_id) or ! ($section_id)) {
			return "";
		}
		$name     = "";
		$arSelect = array("NAME");
		$arFilter = array("IBLOCK_ID" => $iblock_id, "GLOBAL_ACTIVE" => "Y", "=ID" => $section_id);
		$db_list  = CIBlockSection::GetList(array($by => $order), $arFilter, true, $arSelect);
		while ($ar_result = $db_list->GetNext()) {
			$name = $ar_result["NAME"];
		};

		return $name;
	}

	//получение названий контактов-привязок
	function get_contact_name($id) {
		$name_1 = '';
		$name_2 = '';
		$name_3 = '';
		CModule::IncludeModule("crm");
		$arOrder          = array();
		$arFilter         = array("=ID" => $id, "CHECK_PERMISSIONS" => 'N');
		$arGroupBy        = false;
		$arNavStartParams = false;
		$arSelectFields   = array("NAME", "LAST_NAME", "SECOND_NAME");
		$res              = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$name_1 = $ob["NAME"];
			$name_2 = $ob["LAST_NAME"];
			$name_3 = $ob["SECOND_NAME"];
		}
		$name = $name_1 . ' ' . $name_2;

		return $name;
	}

	//проставить координаты в контакт
	function set_contact_coordinates($contact_id, $lat, $lon) {
		if ( ! ($lat) or ! ($lon)) {
			$city        = $this->get_contact_city($contact_id);
			$coordinates = $this->get_city_coordinates($city);
			$lat         = $coordinates['lat'];
			$lon         = $coordinates['lon'];
		}
		$entity = new CCrmContact;
		$fields = array(
			'UF_CRM_REPORT_LAT' => $lat,
			'UF_CRM_REPORT_LON' => $lon,
		);
		$entity->update($contact_id, $fields);
	}

	//проверка доступности возврата по отклику
	function respond_refund_check($user_id, $order_id) {
		//получить отклик мастера
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_138");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $user_id, "=PROPERTY_132" => 31193, "=PROPERTY_129" => 31188);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$respond    = $arFields["ID"];
			$worker_tax = $arFields["PROPERTY_138_VALUE"];
		}
		if ($respond and $worker_tax > 0) {
			return true;
		} else {
			return false;
		}
	}

	//получение текста подсказки по статусу
	function get_hint($user_type, $order_id, $state) {
		$hint = "";
		if ($user_type == 0) {
			$hint = "Данный заказ больше не доступен . ";
			switch ($state) {
				case 0:
				case 4:
					$hint = "";
					break;
				case 1:
					$hint = "Вы отозвались на данный заказ . Если Вас выберут исполнителем, появятся контакты заказчика и с Вас автоматически будут списаны средства . ";
					break;
				case 2:
					$hint = "Если Вы все еще готовы выполнить данный заказ, Вам нужно повторно его принять, так как истек срок принятия, а заказчик еще не сделал выбор . ";
					break;
				case 3:
					$hint = "Вы являетесь исполнителем и можете позвонить или написать заказчику . ";
					break;
				case 5:
					$hint = "Вы отзывались на данный заказ, но он был отменен или отдан другому мастеру . ";
					break;
				case 6:
					$hint = "Вы отказались от выполнения данного заказа . ";
					break;
				case 7:
					$hint = "По данному заказу был запрошен возврат . Мы скоро рассмотрим Ваш запрос и сообщим о решении . ";
					break;
			}
		} else if ($user_type == 1) {
			switch ($state) {
				case 0:
					$active_responds = array();
					//получение списка мастеров
					$arSelect2 = array("ID", "IBLOCK_ID");
					$arFilter2 = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_132" => array(31191, 31193));
					$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
					while ($ob2 = $res2->GetNextElement()) {
						$arFields2         = $ob2->GetFields();
						$active_responds[] = $arFields2["ID"];
					}
					if ($active_responds) {
						$hint = "Выберите одного из мастеров в качестве исполнителя, затем Вам будут доступны его контакты и чат с ним . ";
					} else {
						$hint = "Мы отправили Ваш заказ всем доступным мастерам . Как только они начнут отзываться, Вам придет уведомление . ";
					}
					break;
				case 1:
				case 2:
				case 4:
					$hint = "";
					break;
				case 3:
					$hint = "Ваш заказ исполняется . Пожалуйста, завершите его, если исполнитель закончит работу . ";
					break;
				case 5:
					$hint = "Данный заказ был отменен . ";
					break;
				case 6:
					$hint = "Пожалуйста, подтвердите Ваш заказ, если он все еще актуальный . ";
					break;
				case 7:
					$hint = "По данному заказу был запрошен возврат . ";
					break;
			}
		}

		return $hint;
	}

	//событие по обновлению города в профиле мастера
	function event_contact_update_city($id, $city_id) {
		$city_title = $this->get_element_name(84, $city_id);
		//проставить в транзакции, где не заполнено
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_192");
		$arFilter = array("IBLOCK_ID" => 87, "PROPERTY_192" => $id, "PROPERTY_311" => false);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$element_id = $arFields["ID"];
			$PROP       = array(311 => $city_title, 355 => $city_id);
			CIBlockElement::SetPropertyValuesEx($element_id, 87, $PROP);
		}
	}

	//перенос баланса из СП - ручной дебаг
	function forced_carryover($id) {
		//return false;
		include_once('extra_utils.php');
		$extra_utils = new ExtraUtils();
		CModule::IncludeModule("crm");
		$this->update_field_contact($id, "UF_CRM_CARRYOVER_OLDAPP", false);
		$worker_phone = $this->get_phone($id, false);
		$extra_utils->carryover($id, $worker_phone);
	}

	//проверка аккаунта на тестовый
	function test_account_check($id) {
		//проверка среди тестовых
		$arSelect = array("ID");
		$arFilter = array("IBLOCK_ID" => 116, "PROPERTY_312" => $id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$check_id = $arFields["ID"];
		};
		if ($check_id) {
			return true;
		} else {
			return false;
		}
	}

	//формирование тестового аккаунта
	function test_account_update($id, $phone) {
		//проверка среди тестовых
		$arSelect = array("ID");
		$arFilter = array("IBLOCK_ID" => 116, "PROPERTY_309" => $phone, "PROPERTY_312" => false);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$element_id = $arFields["ID"];
			CIBlockElement::SetPropertyValuesEx($element_id, 116, array(312 => $id));
		};
	}

	//отладка запросов
	function debug($text) {
		$el                 = new CIBlockElement;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 89,
			"NAME"            => "Тестовый запрос",
			"PROPERTY_VALUES" => array("ZAPROS" => $text),
		);
		$element_id         = $el->Add($arLoadProductArray);
	}

	//тест
	function test($id) {
		return 'test result';
	}

}

?>