<?php

//require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//$APPLICATION->SetTitle("event_handler");

class EventHandler
{

	function add_message($arFields = false, $arRes = false) {

        //проверка типа
        $chat_entity_id = $arRes["CHAT_ENTITY_ID"];  //"olchat_wa_connector_2|6|79229026280|146488"
        $line_id_pos = strpos($chat_entity_id, "|");
        $line_id_pos++;
        $line_substr = substr($chat_entity_id, $line_id_pos);
        $line_id_pos = strpos($line_substr, "|");
        $line_id = substr($line_substr, 0, $line_id_pos);
        $line_id_pos++;
        $line_substr = substr($line_substr, $line_id_pos);
        $phone_pos = strpos($line_substr, "|");
        $phone = substr($line_substr, 0, $phone_pos);
		$system = $arFields["SYSTEM"];
		if ($system == 'Y') {$is_system = true;}
		else {$is_system = false;}

		//ДЕЙСТВИЯ ДЛЯ ВНУТРЕННИХ ЧАТОВ НП
		if ($line_id == 27 and !($is_system)) {
			//пишем лог
			$el = new CIBlockElement;
			$arLoadProductArray = array(
				"IBLOCK_ID" => 89,
				"NAME" => "Ответ сотрудника в ОЛ",
				"PROPERTY_VALUES" => array("ZAPROS" => 'arFields: ' . json_encode($arFields) . ' arRes: ' . json_encode($arRes) . ' line id ' . $line_id . ' phone ' . $phone),
			);
			$element_id = $el->Add($arLoadProductArray);
		}

        //сценарий по олчату
        if ($line_id == 6 or $line_id == 22) {
			//$this->olchat_binding($arFields, $arRes, $phone, $line_id);
        }

        return;
    }


	function change_status($params = false) {
		$config_id = $params["SESSION"]["CONFIG_ID"];
		$user_code = $params["SESSION"]["USER_CODE"];
		$session_id = $params["SESSION"]["SESSION_ID"];
		//ДЕЙСТВИЯ ДЛЯ ВНУТРЕННИХ ЧАТОВ НП
		if ($config_id == 27) {
			//ищем связанные открытые дела
			CModule::IncludeModule("crm");
			$activity_list = array();
			$res = \CCrmActivity::GetList(
				array('LAST_UPDATED' => 'DESC'),
				array("TYPE_ID" => 6, "COMPLETED" => 'N', "ASSOCIATED_ENTITY_ID" => $session_id),
				false,
				false,
				array(),
				array('QUERY_OPTIONS' => array('LIMIT' => 100, 'OFFSET' => 0))
			);
			while($ob = $res->GetNext()) {
				$activity_id = $ob['ID'];
				$activity_list[] = $activity_id;
				//закрываем дела
				$entity = new CCrmActivity;
				$fields = array(
					"COMPLETED" => "Y",
				);
				$entity->update($activity_id, $fields);
			};
			//пишем лог
			$el = new CIBlockElement;
			$arLoadProductArray = array(
				"IBLOCK_ID" => 89,
				"NAME" => "Смена статуса чата",
				"PROPERTY_VALUES" => array("ZAPROS" => ' params: '.json_encode($params).' activities '.json_encode($activity_list)),
			);
			$element_id = $el->Add($arLoadProductArray);

		}
        return;
    }


    //привязка чата от ватсапа олчата
    function olchat_binding($arFields, $arRes, $phone, $line_id)
    {

        $portal_url = 'https://dombezzabot.net';
        $chat_id = $arRes["CHAT_ID"];
        $chat_entity_data_2 = $arRes["CHAT_ENTITY_DATA_2"];  //"CHAT_ENTITY_DATA_2":"LEAD|619205|COMPANY|0|CONTACT|0|DEAL|0",
        $chat_entity_data_1 = $arRes["CHAT_ENTITY_DATA_1"];  //"CHAT_ENTITY_DATA_1":"Y|LEAD|619205|N|N|257125|1617611303|0|0|",

        $contact_pos = strpos($chat_entity_data_2, "CONTACT|");
        $contact_pos = $contact_pos + 8;
        $deal_pos = strpos($chat_entity_data_2, "|DEAL");
        $contact_id_length = $deal_pos - $contact_pos;
        $contact_id = substr($chat_entity_data_2, $contact_pos, $contact_id_length);

        $session_id_pos = strpos($chat_entity_data_1, "|");
        $session_id_pos++;
        $session_id = substr($chat_entity_data_1, $session_id_pos); //LEAD|619205|N|N|257125|1617611303|0|0|"
        $session_id_pos = strpos($session_id, "|");
        $session_id_pos++;
        $session_id = substr($session_id, $session_id_pos); //619205|N|N|257125|1617611303|0|0|"
        $session_id_pos = strpos($session_id, "|");
        $session_id_pos++;
        $session_id = substr($session_id, $session_id_pos); //N|N|257125|1617611303|0|0|"
        $session_id_pos = strpos($session_id, "|");
        $session_id_pos++;
        $session_id = substr($session_id, $session_id_pos); //N|257125|1617611303|0|0|"
        $session_id_pos = strpos($session_id, "|");
        $session_id_pos++;
        $session_id = substr($session_id, $session_id_pos); //257125|1617611303|0|0|"
        $session_id_pos = strpos($session_id, "|");
        $session_id = substr($session_id, 0, $session_id_pos);

        if ($contact_id == 0) { //$contact_id == 0
            CModule::IncludeModule("imconnector");
            CModule::IncludeModule("im");
            CModule::IncludeModule("crm");
            include_once($_SERVER["DOCUMENT_ROOT"] . "/api/utils.php");
            $utils = new Utils();
            //ищем телефон по базе
            if (strpos($phone, '+') !== false) {
                $phone_substr = substr($phone, 2);
            } else {
                $phone_substr = substr($phone, 1);
            }
            if (strlen($phone_substr) == 10) {
                $phone_format = true;
            }
            $contacts = $utils->contact_search($phone_substr);
            $contact = min($contacts);
            $user = $contact;
            if (!($user)) { //$chat_id != 171063      164666
                return;
            }

            //костыльный запуск модулей
            $time_point = date_create(date('Y-m-d H:i:s', time()));
            $date_timestamp = date_timestamp_get($time_point);
            $user_db = 362600;
            $message_unique_id = $date_timestamp.$user_db;
            $message_data = array(
                'user' => array('id'=>$user_db, 'name'=>'Коннектор приложения'),
                'message' => array('id'=>$message_unique_id, 'date'=>'', 'text'=>'Техническое сообщение'),
                'chat' => array('id'=>$user_db, 'name'=>'chat_'.$user_db, 'url'=>''),
            );
            $res = \Bitrix\ImConnector\CustomConnectors::sendMessages(network, 29, array($message_data));

            //привязка чата к контакту, получение кода привязки
            $chat = new \Bitrix\ImOpenLines\Chat($chat_id);
            $chat->setCrmFlag(array('CONTACT' => $user));
            $chat_data = $chat->getData();
            $chat_entity = $chat_data['ENTITY_ID'];



            //провекра наличия открытого дела в карточке контакта
            $activity_old = 0;
            $activity_old = \CCrmActivity::GetList(
                array('LAST_UPDATED' => 'DESC'),
                array('OWNER_ID' => $user, 'OWNER_TYPE_ID' => 3, "TYPE_ID" => 6, "ASSOCIATED_ENTITY_ID" => $session_id, "COMPLETED" => 'N'),
                false,
                false,
                array('ID'),
                array('QUERY_OPTIONS' => array('LIMIT' => 1, 'OFFSET' => 0))
            )->Fetch();



            //создание дела в карточке контакта
            if (!($activity_old)) {
                $entity = new CCrmActivity;
                $fields = array(
                    "BINDINGS" => array(array(
                        'OWNER_ID' => $user,
                        'OWNER_TYPE_ID' => 3,
                    )),
                    'OWNER_ID' => $user,
                    'OWNER_TYPE_ID' => 3,
                    "SUBJECT" => 'Коннектор олчат',
                    "RESPONSIBLE_ID" => $arFields["AUTHOR_ID"],
                    "ORIGIN_ID" => 'IMOL_'.$session_id,
                    "PROVIDER_ID" => "IMOPENLINES_SESSION",
                    "PROVIDER_TYPE_ID" => 1,
                    "TYPE_ID" => 6,
                    "THREAD_ID" => $user,
                    "ASSOCIATED_ENTITY_ID" => $session_id,
                    "COMPLETED" => 'N',
                    "DESCRIPTION" => 'Обращение в ватсап',
                    "DESCRIPTION_TYPE" => 1,
                    "PROVIDER_PARAMS" => array(
                        "USER_CODE" => $chat_entity,
                    ),
                );
                $activity_id = $entity->add($fields, false, false, array('REGISTER_SONET_EVENT' => true));
            }


            //создание коммуникаций - для прямого открытия чата из дела в карточке
            if($activity_id > 0 and !($activity_old)) {
                $arComms = array(array(
                    'ACTIVITY_ID' => $activity_id,
                    'ENTITY_ID' => $user,
                    'ENTITY_TYPE_ID' => 3,
                    'TYPE' => 'IM',
                    'VALUE' => 'imol|'.$chat_entity,
                    'FORMATTED_VALUE' => 'imol|'.$chat_entity,
                    'TITLE' => 'Чат с мастером',
                    'SHOW_URL' => '/crm/contact/details/'.$user.'/',
                ));
                CCrmActivity::SaveCommunications($activity_id, $arComms, $fields, true, false);
            }


            //запуск автоматизации
            $bindings = array();
            $bindings[] = array(
                'OWNER_ID' => $user,
                'OWNER_TYPE_ID' => 3,
            );
            $trigger_method = new \Bitrix\ImOpenLines\Crm;
            $trigger = $trigger_method->executeAutomationTrigger($bindings, null);

/*
            //СИСТЕМНОЕ СООБЩЕНИЕ
            $user_ct = $arFields["AUTHOR_ID"];
            $user_name = $utils->get_contact_name($user);
            $message_unique_id = $date_timestamp.$user_ct;
            $message_data = array(
                'user' => array('id'=>$user_ct, 'name'=>'Системное сообщение'),
                'message' => array('id'=>$message_unique_id, 'date'=>'', 'text'=>'Чат привязан к контакту: [url='.$portal_url.'/crm/contact/details/'.$user.'/]'.$user_name.'[/url]'),
                'chat' => array('id'=>$chat_id, 'name'=>'chat_'.$user, 'url'=>''),
            );
            $res = \Bitrix\ImConnector\CustomConnectors::sendMessages(olchat_wa_connector_2, $line_id ,array($message_data));
*/

            //ищем сделки
            $good_stages = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING', 'C7:FINAL_INVOICE', 'C7:2', 'C7:3', 'C7:1');
            $deal_id = 0;
            $arFilter = array("=CONTACT_ID" => $user, "=CATEGORY_ID" => 7, "STAGE_ID" => $good_stages);
            $arSelectFields = array("ID", "TITLE");
            $res = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
            while($ob = $res->GetNext()) {
                $deal_id = $ob['ID'];
                $deal_title = $ob['TITLE'];

                //привязка чата к контакту, получение кода привязки
                $chat = new \Bitrix\ImOpenLines\Chat($chat_id);
                $chat->setCrmFlag(array('DEAL' => $deal_id));
                $chat_data = $chat->getData();
                $chat_entity = $chat_data['ENTITY_ID'];

                //создание дела в карточке сделки
                if (!($activity_old)) {
                    $entity = new CCrmActivity;
                    $fields = array(
                        "BINDINGS" => array(array(
                            'OWNER_ID' => $deal_id,
                            'OWNER_TYPE_ID' => 2,
                        )),
                        'OWNER_ID' => $deal_id,
                        'OWNER_TYPE_ID' => 2,
                        "SUBJECT" => 'Коннектор олчат',
                        "RESPONSIBLE_ID" => $arFields["AUTHOR_ID"],
                        "ORIGIN_ID" => 'IMOL_'.$session_id,
                        "PROVIDER_ID" => "IMOPENLINES_SESSION",
                        "PROVIDER_TYPE_ID" => 1,
                        "TYPE_ID" => 6,
                        "THREAD_ID" => $deal_id,
                        "ASSOCIATED_ENTITY_ID" => $session_id,
                        "COMPLETED" => 'N',
                        "DESCRIPTION" => 'Обращение в ватсап',
                        "DESCRIPTION_TYPE" => 1,
                        "PROVIDER_PARAMS" => array(
                            "USER_CODE" => $chat_entity,
                        ),
                    );
                    $activity_id = $entity->add($fields, false, false, array('REGISTER_SONET_EVENT' => true));
                }


                //создание коммуникаций - для прямого открытия чата из дела в карточке
                if($activity_id > 0 and !($activity_old)) {
                    $arComms = array(array(
                        'ACTIVITY_ID' => $activity_id,
                        'ENTITY_ID' => $deal_id,
                        'ENTITY_TYPE_ID' => 2,
                        'TYPE' => 'IM',
                        'VALUE' => 'imol|'.$chat_entity,
                        'FORMATTED_VALUE' => 'imol|'.$chat_entity,
                        'TITLE' => 'Чат с мастером',
                        'SHOW_URL' => '/crm/contact/details/'.$deal_id.'/',
                    ));
                    CCrmActivity::SaveCommunications($activity_id, $arComms, $fields, true, false);
                }


                //запуск автоматизации
                $bindings = array();
                $bindings[] = array(
                    'OWNER_ID' => $deal_id,
                    'OWNER_TYPE_ID' => 2,
                );
                $trigger_method = new \Bitrix\ImOpenLines\Crm;
                $trigger = $trigger_method->executeAutomationTrigger($bindings, null);

/*
                //СИСТЕМНОЕ СООБЩЕНИЕ
                $message_unique_id = $date_timestamp.$user_ct.$deal_id;
                $message_data = array(
                    'user' => array('id'=>$user_ct, 'name'=>'Системное сообщение'),
                    'message' => array('id'=>$message_unique_id, 'date'=>'', 'text'=>'Чат привязан к сделке: [url='.$portal_url.'/crm/deal/details/'.$deal_id.'/]'.$deal_title.'[/url]'),
                    'chat' => array('id'=>$chat_id, 'name'=>'chat_'.$user, 'url'=>''),
                );
                $res = \Bitrix\ImConnector\CustomConnectors::sendMessages(olchat_wa_connector_2, $line_id ,array($message_data));
*/
            }


            //пишем лог
            $el = new CIBlockElement;
            $arLoadProductArray = array(
                "IBLOCK_ID" => 89,
                "NAME" => "Ответ сотрудника в ОЛ 2",
                "PROPERTY_VALUES" => array("ZAPROS" => 'deal '.$deal_id.' old ', $activity_old . ' new ' . $activity_id . ' ses ' . $session_id),
            );
            $element_id = $el->Add($arLoadProductArray);


        }
    }



}




