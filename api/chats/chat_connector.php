<?php
include_once "../utils.php";

class ChatConnector extends Utils
{

    //входящее сообщение в канал техподдержки
    function support_message_in($user, $user_type, $message_text) {
        if (!($user)) {return false;}

        //подключение прав, модулей и классов
        CModule::IncludeModule("imconnector");
        CModule::IncludeModule("im");
        CModule::IncludeModule("crm");

        //подготовка данных
        $time_point = date_create(date('Y-m-d H:i:s', time()));
        $date_timestamp = date_timestamp_get($time_point);
        if ($user_type == 0) {
            $user_index = 1000;
            $activity_title = 'Чат поддержки НП - мастер';
            $user_type_title = 'МАСТЕР';
        }
        else {
            $user_index = 1001;
            $activity_title = 'Чат поддержки НП - заказчик';
            $user_type_title = 'ЗАКАЗЧИК';
        }
        $user_name = $this->get_contact_name($user);
        $message_unique_id = $date_timestamp.$user.$user_index;

        //подготавливаем массив сообщения
        $message_data = array(
            'user' => array('id'=>$user.$user_index, 'name'=>$user_name),
            'message' => array('id'=>$message_unique_id, 'date'=>'', 'text'=>$message_text),
            'chat' => array('id'=>$user.$user_index, 'name'=>'chat_'.$user.$user_index, 'url'=>''),
        );

        //отправка сообщения
        $res = \Bitrix\ImConnector\CustomConnectors::sendMessages(network, 27,array($message_data));
        $fields = $res->getData();
        $sender_id = $fields['RESULT'][0]['user']; //получим ид пользователя(внешнего) отправившего сообщение.
        $message_id = CIMMessageParam::GetMessageIdByParam("CONNECTOR_MID", $message_unique_id)[0]; //получим идентификатор сообщения
        $message_res = new CIMMessage($sender_id);
        $message = $message_res->GetMessage($message_id); //получим информацию о сообщении
        $chat_id = $message['CHAT_ID'];

        //получение данных о сессии
        $session_res = Bitrix\ImOpenLines\Model\SessionTable::getList(array(
            'select' => Array('ID', 'CHAT_ID'),
            'filter' => array(
                'CHAT_ID' => $chat_id,
            )
        ));
        $session = $session_res->fetch(); //идентификатор лежит в $session['ID']
        $session_id = $session['ID'];

        //привязка чата к контакту, получение кода привязки
        $chat = new \Bitrix\ImOpenLines\Chat($chat_id);
        $chat->setCrmFlag(array('CONTACT' => $user));
        $chat_data = $chat->getData();
        $chat_entity = $chat_data['ENTITY_ID'];

        //echo $chat_entity;

        //проставить чат в поле контакта
        $arFilter = array("=ID" => $user);
        $arSelectFields = array("UF_CRM_CHAT_ID_WORKER", "UF_CRM_CHAT_ID_CLIENT");
        $res = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
        while($ob = $res->GetNext()) {
            $chat_id_worker = $ob["UF_CRM_CHAT_ID_WORKER"];
            $chat_id_client = $ob["UF_CRM_CHAT_ID_CLIENT"];
        }
        if ($user_type == 0 and !($chat_id_worker)) {
            $entity = new CCrmContact;
            $fields = array("UF_CRM_CHAT_ID_WORKER" => $chat_id);
            $entity->update($user, $fields);
        }
        else if ($user_type == 1 and !($chat_id_client)) {
            $entity = new CCrmContact;
            $fields = array("UF_CRM_CHAT_ID_CLIENT" => $chat_id);
            $entity->update($user, $fields);
        }

        //проверка наличия такой линии мессенджера в контакте
        $multy_old = 0;
        $dbResult = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array(
                'ENTITY_ID' => 'CONTACT',
                'TYPE_ID' => 'IM',
                'ELEMENT_ID' => $user,
            )
        );
        while ($fields_db = $dbResult->Fetch()) {
            if ($fields_db['VALUE'] == 'imol|'.$chat_entity) {
                $multy_old = $fields_db['ID'];
            }
        }

        //создание линии мессенджера в контакте
        if (!($multy_old)) {
            $multi = new CCrmFieldMulti();
            $CrmFieldMultiUpdate=array(
                "ENTITY_ID"=>'CONTACT',
                "ELEMENT_ID"=> $user,
                "TYPE_ID"=>"IM",
                "VALUE_TYPE"=>"IMOL",
                "COMPLEX_ID"=>"IM_IMOL",
                "VALUE"=>'imol|'.$chat_entity,
            );
            $multi->add($CrmFieldMultiUpdate);
        }

        //провекра наличия открытого дела в карточке контакта
        $activity_old = 0;
        $activity_old = \CCrmActivity::GetList(
            array('LAST_UPDATED' => 'DESC'),
            array('OWNER_ID' => $user, 'OWNER_TYPE_ID' => 3, "TYPE_ID" => 6, "ASSOCIATED_ENTITY_ID" => $session_id, "COMPLETED" => 'N'),
            false,
            false,
            array(),
            array('QUERY_OPTIONS' => array('LIMIT' => 1, 'OFFSET' => 0))
        )->Fetch();

        //тест
        //$activity =\CCrmActivity::GetList($activity_old)->Fetch();
        //$this->debug(json_encode($activity_old));

        //создание дела в карточке контакта
        if (!($activity_old)) {
            $entity = new CCrmActivity;
            $fields = array(
                "BINDINGS" => array(array(
                    'OWNER_ID' => $user,
                    'OWNER_TYPE_ID' => 3,
                )),
                'OWNER_ID' => $user,
                'OWNER_TYPE_ID' => 3,
                "SUBJECT" => $activity_title,
                "RESPONSIBLE_ID" => 146420,
                "ORIGIN_ID" => 'IMOL_'.$session_id,
                "PROVIDER_ID" => "IMOPENLINES_SESSION",
                "PROVIDER_TYPE_ID" => 1,
                "TYPE_ID" => 6,
                "THREAD_ID" => $user,
                "ASSOCIATED_ENTITY_ID" => $session_id,
                "COMPLETED" => 'N',
                "DESCRIPTION" => 'Обращение пользователя в техническую поддержку',
                "DESCRIPTION_TYPE" => 1,
                "PROVIDER_PARAMS" => array(
                    "USER_CODE" => $chat_entity,
                ),
            );
            $activity_id = $entity->add($fields, false, false, array('REGISTER_SONET_EVENT' => true));
        }

        //создание коммуникаций - для прямого открытия чата из дела в карточке
        if($activity_id > 0 and !($activity_old)) {
            $arComms = array(array(
                'ACTIVITY_ID' => $activity_id,
                'ENTITY_ID' => $user,
                'ENTITY_TYPE_ID' => 3,
                'TYPE' => 'IM',
                'VALUE' => 'imol|'.$chat_entity,
                'FORMATTED_VALUE' => 'imol|'.$chat_entity,
                'TITLE' => 'Чат с мастером',
                'SHOW_URL' => '/crm/contact/details/'.$user.'/',
            ));
            CCrmActivity::SaveCommunications($activity_id, $arComms, $fields, true, false);
        }


        //прописать в чате ссылку на контакт
        if (!($activity_old)) {
            $ar = Array(
                "TO_CHAT_ID" => $chat_id, // ID чата
                "FROM_USER_ID" => 0,
                "SYSTEM" => 'Y',
                "MESSAGE"  => "ЧАТ ОБРАЩЕНИЯ ".$user_type_title."А В ТЕХНИЧЕСКУЮ ПОДДЕРЖКУ",
            );
            CIMChat::AddMessage($ar);
            $ar = Array(
                "TO_CHAT_ID" => $chat_id, // ID чата
                "FROM_USER_ID" => 0,
                "SYSTEM" => 'Y',
                "MESSAGE"  => "Чат прикреплен к контакту [url=https://dombezzabot.net/crm/contact/details/".$user."/]".$user_name."[/url]",
            );
            CIMChat::AddMessage($ar);
        }

        //прописать в чате ссылки на связанные сделки
        if (!($activity_old)) {
            //собираем активные сделки
            $deals_text ='';
            $arFilter = array("STAGE_ID" => array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING', 'C7:FINAL_INVOICE', 'C7:2', 'C7:6', 'C7:3', 'C7:1'));
            if ($user_type == 0) {$arFilter["=UF_CRM_MASTER"] = $user;}
            else {$arFilter["=CONTACT_ID"] = $user;}
            $arSelectFields = array("ID", "TITLE");
            $res = CCrmDeal::GetListEx(array(id=>asc), $arFilter, false, false, $arSelectFields);
            while($ob = $res->GetNext()) {
                $local_deal_id = $ob['ID'];
                $local_deal_title = $ob['TITLE'];
                if (!($deals_text)) {$text_add = "";}
                else {$text_add = ",   ";}
                $deals_text = $deals_text.$text_add."[url=https://dombezzabot.net/crm/deal/details/".$local_deal_id."/]".$local_deal_title."[/url]";
                $last_deal = $local_deal_id;
                $this->chat_deal_attach($user, $local_deal_id, $session_id, $chat_entity, $activity_title);
            }
            if ($deals_text) {
                $ar = Array(
                    "TO_CHAT_ID" => $chat_id, // ID чата
                    "FROM_USER_ID" => 0,
                    "SYSTEM" => 'Y',
                    "MESSAGE"  => "Активные сделки: ".$deals_text,
                );
                CIMChat::AddMessage($ar);
            }
            $chat->setCrmFlag(array('DEAL' => $last_deal));
            //собираем закрытые сделки
            $close_deals_text ='';
            $time_point_closedeals = date_create(date('Y-m-d', time()));
            date_add($time_point_closedeals, date_interval_create_from_date_string("-15 days"));
            $arFilter = array("STAGE_ID" => array('C7:WON', 'C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11'), ">=CLOSEDATE" => date_format($time_point_closedeals, 'd.m.Y'));
            if ($user_type == 0) {$arFilter["=UF_CRM_MASTER"] = $user;}
            else {$arFilter["=CONTACT_ID"] = $user;}
            $arSelectFields = array("ID", "TITLE");
            $res = CCrmDeal::GetListEx(array(id=>asc), $arFilter, false, false, $arSelectFields);
            while($ob = $res->GetNext()) {
                $local_deal_id = $ob['ID'];
                $local_deal_title = $ob['TITLE'];
                if (!($close_deals_text)) {$text_add = "";}
                else {$text_add = ",   ";}
                $close_deals_text = $close_deals_text.$text_add."[url=https://dombezzabot.net/crm/deal/details/".$local_deal_id."/]".$local_deal_title."[/url]";
            }
            if ($close_deals_text) {
                $ar = Array(
                    "TO_CHAT_ID" => $chat_id, // ID чата
                    "FROM_USER_ID" => 0,
                    "SYSTEM" => 'Y',
                    "MESSAGE"  => "Недавно закрытые сделки: ".$close_deals_text,
                );
                CIMChat::AddMessage($ar);
            }
        }

        //запуск автоматизации
        $bindings = array();
        $bindings[] = array(
            'OWNER_ID' => $user,
            'OWNER_TYPE_ID' => 3,
        );
        $trigger_method = new \Bitrix\ImOpenLines\Crm;
        $trigger = $trigger_method->executeAutomationTrigger($bindings, null);

        //силовая перезарядка Push-and-Pull
        if (\Bitrix\Main\Loader::includeModule('pull')) {
            \Bitrix\Pull\Event::send();
        }

        return $this->getSuccess("");

    }

    //прикрепелние дела чата к сделке
    function chat_deal_attach($user, $deal_id, $session_id, $chat_entity, $activity_title) {
        //провекра наличия открытого дела в карточке сделки
        $activity_old = 0;
        $activity_old = \CCrmActivity::GetList(
            array('LAST_UPDATED' => 'DESC'),
            array('OWNER_ID' => $deal_id, 'OWNER_TYPE_ID' => 2, "TYPE_ID" => 6, "ASSOCIATED_ENTITY_ID" => $session_id, "COMPLETED" => 'N'),
            false,
            false,
            array('ID'),
            array('QUERY_OPTIONS' => array('LIMIT' => 1, 'OFFSET' => 0))
        )->Fetch();

        //создание дела в карточке сделки
        if (!($activity_old)) {
            $entity = new CCrmActivity;
            $fields = array(
                "BINDINGS" => array(array(
                    'OWNER_ID' => $deal_id,
                    'OWNER_TYPE_ID' => 2,
                )),
                'OWNER_ID' => $deal_id,
                'OWNER_TYPE_ID' => 2,
                "SUBJECT" => $activity_title,
                "RESPONSIBLE_ID" => 146420,
                "ORIGIN_ID" => 'IMOL_'.$session_id,
                "PROVIDER_ID" => "IMOPENLINES_SESSION",
                "PROVIDER_TYPE_ID" => 1,
                "TYPE_ID" => 6,
                "THREAD_ID" => $user,
                "ASSOCIATED_ENTITY_ID" => $session_id,
                "COMPLETED" => 'N',
                "DESCRIPTION" => 'Обращение пользователя в техническую поддержку',
                "DESCRIPTION_TYPE" => 1,
                "PROVIDER_PARAMS" => array(
                    "USER_CODE" => $chat_entity,
                ),
            );
            $activity_id = $entity->add($fields, false, false, array('REGISTER_SONET_EVENT' => true));
        }

        //создание коммуникаций - для прямого открытия чата из дела в карточке
        if($activity_id > 0 and !($activity_old)) {
            $arComms = array(array(
                'ACTIVITY_ID' => $activity_id,
                'ENTITY_ID' => $deal_id,
                'ENTITY_TYPE_ID' => 2,
                'TYPE' => 'IM',
                'VALUE' => 'imol|'.$chat_entity,
                'FORMATTED_VALUE' => 'imol|'.$chat_entity,
                'TITLE' => 'Чат с мастером',
                'SHOW_URL' => '/crm/deal/details/'.$deal_id.'/',
            ));
            CCrmActivity::SaveCommunications($activity_id, $arComms, $fields, true, false);
        }

        //запуск автоматизации
        $bindings = array();
        $bindings[] = array(
            'OWNER_ID' => $deal_id,
            'OWNER_TYPE_ID' => 2,
        );
        $trigger_method = new \Bitrix\ImOpenLines\Crm;
        $trigger = $trigger_method->executeAutomationTrigger($bindings, null);
    }

    //получение списка всех сообщений
    function get_messages($id, $user_type, $order_id) {
        $messages = array();
        if (!($messages)) {$messages = null;}
        //тестовая врезка
        if ($id == 187982) {
            if (!($id)) {return false;}
            CModule::IncludeModule("imconnector");
            CModule::IncludeModule("im");
            CModule::IncludeModule("crm");
            if ($order_id == 0) { //чат с поддержкой
                $arFilter = array("=ID" => $id);
                $arSelectFields = array("UF_CRM_CHAT_ID_WORKER", "UF_CRM_CHAT_ID_CLIENT");
                $res = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
                while($ob = $res->GetNext()) {
                    if ($user_type == 0) {$chat_id = $ob["UF_CRM_CHAT_ID_WORKER"];}
                    else if ($user_type == 1) {$chat_id = $ob["UF_CRM_CHAT_ID_CLIENT"];}
                }
                if (!($chat_id)) {
                    $response=array('list'=>$messages);
                    return $this->getSuccess($response);
                }
                $chat = new CIMChat();
                $message_list = $chat->GetLastMessageLimit($chat_id, 0, 999999999999999999, true);
                foreach ($message_list["users"] as $key => $user) {
                    if ($user['connector']) {$client_id = $user['id'];}
                }
                foreach ($message_list["message"] as $key => $message_data) {
                    if (!($message_data['senderId'])) {continue;}
                    $message_id = $message_data['id'];
                    if ($message_data['senderId'] == $client_id) {$message_type = 1;}
                    else {$message_type = 0;}
                    $text = $message_data['text'];
                    $name = $message_list['users'][$message_data['senderId']]['name'];
                    $message = array(
                        'id' => $message_id,
                        'type' => $message_type,
                        'text' => $text,
                        'name' => $name,
                    );
                    $messages[] = $message;
                    //echo 'id '.$id.' text '.$text.' type '.$type.' name '.$name.'<br>';
                }
                $response=array('list'=>$messages);
                return $this->getSuccess($response);
            }
            else { //чат по заказу
                $response=array('list'=>$messages);
                return $this->getFailure("", false, null);
            }
        }
        else {
            $response=array('list'=>$messages);
            return $this->getFailure("", false, null);
            //return $this->getSuccess($response);
        }
    }


}
