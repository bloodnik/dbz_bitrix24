<?php

use lib\contact\CDbzContact;

class ExtraUtils extends Utils {

	//-----Функции обработки запросов-----

	//Обноление одной работы пользователя
	function UpdateUserWork($id, $user_type, $work, $is_active) {
		$this->master_type_check($user_type);
		if ($work['type'] == 1 or $work['type'] == 2) {
			$category_id = $work['id'];
		} else if ($work['type'] == 3) {
			//проверяем в работах
			$check               = false;
			$checking_element_id = $work['id'];
			$arSelect            = array("ID");
			$arFilter            = array("IBLOCK_ID" => 66, "=ID" => $checking_element_id);
			$res                 = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$check   = true;
				$work_id = $checking_element_id;
			};
			//проверяем в поисковых запросах
			if ( ! ($check)) {
				$work_id = $this->search_requests_work($checking_element_id);
			}
			if ($work_id and $is_active) { //при добавлении работы также добавляем основную категорию
				$category_id = $this->get_main_category($work['type'], $work_id);
			}
		}
		//чтение контакта
		$arFilter       = array("=ID" => $id);
		$arSelectFields = array("UF_CRM_UNDER_CATEGORY", "UF_CRM_WORK_TYPE");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$contact_categories = $ob["UF_CRM_UNDER_CATEGORY"];
			$contact_works      = $ob["UF_CRM_WORK_TYPE"];
		}
		//подготовка полей
		$fields = array();
		if ($is_active) { //добавление
			if ($category_id and ! (in_array($category_id, (array) $contact_categories))) {
				$contact_categories[]            = $category_id;
				$fields["UF_CRM_UNDER_CATEGORY"] = $contact_categories;
			}
			if ($work_id and ! (in_array($work_id, (array) $contact_works))) {
				$contact_works[]            = $work_id;
				$fields["UF_CRM_WORK_TYPE"] = $contact_works;
			}
		} else { //удаление
			if ($category_id and (in_array($category_id, (array) $contact_categories))) {
				unset($contact_categories[ array_search($category_id, $contact_categories) ]);
				$contact_categories              = array_values($contact_categories);
				$fields["UF_CRM_UNDER_CATEGORY"] = $contact_categories;
			}
			if ($work_id and (in_array($work_id, (array) $contact_works))) {
				unset($contact_works[ array_search($work_id, $contact_works) ]);
				$contact_works              = array_values($contact_works);
				$fields["UF_CRM_WORK_TYPE"] = $contact_works;
			}
		}
		//изменение контакта
		if ($fields) {
			$entity = new CCrmContact;
			$entity->update($id, $fields);
		}
		//обновление таблицы регистраций
		$this->register_add($id);

		return $this->getSuccess("");
	}



	//-----Прочие функции-----

	//перенос баланса из СП в НП
	function carryover($id, $phone) {

		//return false; //ОТКЛЮЧАЮ - СТАРОЕ ПРИЛОЖЕНИЕ ЛЕГЛО

		/*
        //ЗАТЫЧКА ДО ВВОДА В РАБОТУ
        $test_ids = array(325108, 50178, 482);
        if (!(in_array($id, $test_ids))) {
            return;
        }
        */

		//провекра на повторный перенос
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("UF_CRM_CARRYOVER_OLDAPP");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$check = $ob["UF_CRM_CARRYOVER_OLDAPP"];
		}
		if ($check or ! ($id) or ! ($phone)) {
			return;
		}
		//реализация переноса
		$globals = $this->set_globals();
		$yes     = $globals['yes'];
		$no      = $globals['no'];
		//запрос баланса
		$site_url     = "dom-bez-zabot.online";
		$site_root    = "/youdo/api/migrateToNP.php";
		$token        = 'migrateToNewApp2021';
		$type         = 'deactivateUser';
		$get_params   = "token=" . $token . "&type=" . $type . "&ph=" . $phone;
		$strQueryText = QueryGetData($site_url, 80, $site_root, $get_params, $error_number, $error_text, "GET");
		$get_response = json_decode($strQueryText, true);
		$this->carryover_log(1, $get_params, $id);
		$this->carryover_log(2, $get_response, $id);
		$extra_balance_amount = round($get_response["BUDGET"], 0);
		$extra_bonus_amount   = round($get_response["BONUS"], 0);
		$extra_package_list   = array();
		if ($get_response["TARIFF"]) {
			$extra_package_list[] = $get_response["TARIFF"];
		}
		$name        = $get_response["NAME"];
		$second_name = $get_response["SECOND_NAME"];
		$last_name   = $get_response["LAST_NAME"];
		$this->carryover_name($id, $name, $second_name, $last_name);

		if ($get_response and $extra_balance_amount) {
			//ищем запись по балансу
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31230, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields        = $ob->GetFields();
				$balance_element = $arFields["ID"];
				$balance_amount  = $arFields["PROPERTY_161_VALUE"];
			}
			//изменяем текущую
			if ($balance_element) {
				$PROP              = array();
				$PROP["SUMMA_RUB"] = $balance_amount + $extra_balance_amount;
				CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
			} //создаем новую
			else {
				$balance_element = $this->add_balance_element(31230, $extra_balance_amount, false, false, $id, "Основной баланс");
			}
			//фиксация транзакции
			$tra_element_id = $this->add_transaction(31235, $extra_balance_amount, false, $balance_element, 31230, false, $id, false, "Перенос основного баланса");
			CIBlockElement::SetPropertyValuesEx($tra_element_id, 87, array(305 => 'Перенос баланса из старого приложения'));
		}
		if ($get_response and $extra_bonus_amount) {
			//ищем запись по балансу
			$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161");
			$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31231, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields      = $ob->GetFields();
				$bonus_element = $arFields["ID"];
				$bonus_amount  = $arFields["PROPERTY_161_VALUE"];
			}
			//изменяем текущую
			if ($bonus_element) {
				$PROP              = array();
				$PROP["SUMMA_RUB"] = $bonus_amount + $extra_bonus_amount;
				CIBlockElement::SetPropertyValuesEx($bonus_element, 80, $PROP);
			} //создаем новую
			else {
				$bonus_element = $this->add_balance_element(31231, $extra_bonus_amount, false, false, $id, "Бонусный баланс");
			}
			//фиксация транзакции
			$tra_element_id = $this->add_transaction(31239, $extra_bonus_amount, false, $bonus_element, 31231, false, $id, false, "Перенос бонусов");
			CIBlockElement::SetPropertyValuesEx($tra_element_id, 87, array(305 => 'Перенос баланса из старого приложения'));
		}
		if ($get_response and $extra_package_list) {
			foreach ($extra_package_list as $key => $package) {
				$package_title     = $package["NAME"];
				$package_amount    = $package["TASK_COUNT"];
				$package_active_to = $package["ACTIVE_TO"];

				$package_title = str_replace('"', '', $package_title);
				$package_title = str_replace('«', '', $package_title);
				$package_title = str_replace('»', '', $package_title);
				$package_id    = $this->search_package($package_title);
				if ( ! ($package_id)) {
					continue;
				}
				$package_active_to = date_create($package_active_to);

				//ищем активную запись по данному пакету
				$time_point = date_create(date('Y-m-d H:i:s', time()));
				$arSelect   = array("ID", "IBLOCK_ID", "PROPERTY_163", "PROPERTY_200");
				$arFilter   = array("IBLOCK_ID" => 80, "=PROPERTY_160" => 31232, "=PROPERTY_162" => $package_id, "!=PROPERTY_163" => 0, ">PROPERTY_200" => date_format($time_point, 'Y-m-d H:i:s'), "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
				$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields                = $ob->GetFields();
					$package_balance_element = $arFields["ID"];
					$package_amount_old      = $arFields["PROPERTY_163_VALUE"];
					$package_active_to_old   = date_create($arFields["PROPERTY_200_VALUE"]);
				}
				if ($package_balance_element) {
					//такой пакет есть - объединяем
					$PROP      = array();
					$PROP[163] = $package_amount + $package_amount_old;
					if ($package_active_to > $package_active_to_old) {
						$PROP[200] = date_format($package_active_to, 'd.m.Y H:i:s');
					}
					CIBlockElement::SetPropertyValuesEx($package_balance_element, 80, $PROP);
				} else {
					//пакета нет - создаем
					//чтение данных о пакете
					$action_price = false;
					$arSelect     = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_141", "PROPERTY_142", "PROPERTY_145", "PROPERTY_146", "PROPERTY_152");
					$arFilter     = array("IBLOCK_ID" => 75, "=ID" => $package_id);
					$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
					while ($ob = $res->GetNextElement()) {
						$arFields = $ob->GetFields();
						if ($arFields["PROPERTY_146_VALUE"]) {
							$package_price = $arFields["PROPERTY_146_VALUE"];
						} else {
							$package_price = $arFields["PROPERTY_145_VALUE"];
						}
						$package_nominal_amount = $arFields["PROPERTY_141_VALUE"];
					};
					$package_balance_element = $this->add_balance_element(31232, false, $package_id, $package_amount, $id, "Пакет " . $package_title);
					//дополняем запись по пакету
					$PROP             = array();
					$time_start_point = date_create(date('Y-m-d H:i:s', time()));
					$PROP[200]        = date_format($package_active_to, 'd.m.Y H:i:s');
					$PROP[295]        = date_format($package_active_to, 'd.m.Y');
					$PROP[280]        = date_format($time_start_point, 'd.m.Y H:i:s');
					if ($package_amount != 0) {
						$PROP[250] = round(($package_price / $package_nominal_amount), 0);
					} else {
						$PROP[250] = 0;
					}
					CIBlockElement::SetPropertyValuesEx($package_balance_element, 80, $PROP);
					//запуск процесса деактивации пакета
					if (CModule::IncludeModule("bizproc")) {
						$wfId = CBPDocument::StartWorkflow(
							200,
							array("lists", "BizprocDocument", $package_balance_element),
							array_merge(),
							$arErrorsTmp
						);
					}
				}
				//фиксация транзакции - покупка пакета
				$tra_element_id = $this->add_transaction(31243, false, $package_amount, $package_balance_element, 31232, false, $id, false, "Перенос пакета " . $package_title);
				CIBlockElement::SetPropertyValuesEx($tra_element_id, 87, array(305 => 'Перенос баланса из старого приложения'));
			}
		}
		$this->update_field_contact($id, "UF_CRM_CARRYOVER_OLDAPP", 'перенесено');
	}

	//лог переноса
	function carryover_log($operation, $request, $id) {
		if ($operation == 1) {
			$name = "Параметры запроса в СП - перенос баланса";
		} else if ($operation == 2) {
			$name = "Ответ от СП - перенос баланса";
		}
		$el                 = new CIBlockElement;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 115,
			"NAME"            => $name,
			"PROPERTY_VALUES" => array(
				307 => json_encode($request),
				308 => $id,
			),
		);
		$element_id         = $el->Add($arLoadProductArray);

		return $element_id;
	}

	//поиск пакета по названию
	function search_package($title) {
		$arSelect = array("ID");
		$arFilter = array("IBLOCK_ID" => 75, "NAME" => $title);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$package_id = $arFields["ID"];
		};
		if ($title == 'Стандарт') {
			return false;
		} else if ($package_id) {
			return $package_id;
		} else {
			$this->write_error_log('Не найден пакет при переносе баланса из СП', $title, json_encode($ob));

			return false;
		}
	}

	//логгирование ошибок
	function write_error_log($title, $request_element, $request_full) {
		$el                 = new CIBlockElement;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 111,
			"NAME"            => $title,
			"PROPERTY_VALUES" => array(278 => $request_element, 279 => $request_full),
		);
		$element_id         = $el->Add($arLoadProductArray);
	}

	//перенос ФИО
	function carryover_name($id, $name, $second_name, $last_name) {
		$arFilter       = array("=ID" => $id);
		$arSelectFields = array("NAME", "LAST_NAME", "SECOND_NAME");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$old_name = $ob["NAME"];
		}
		if ($old_name == 'Исполнитель') {
			$entity = new CCrmContact;
			$fields = array(
				"NAME"        => $name,
				"SECOND_NAME" => $second_name,
				"LAST_NAME"   => $last_name,
			);
			$entity->update($id, $fields);
		}
		$this->register_update($id);
	}

	//идентификация контакта при ручном вводе заявки
	function identification_contact($deal_id) {
		global $portal_url;
		if ( ! ($portal_url)) {
			$globals    = $this->set_globals();
			$portal_url = $globals['portal_url'];
		}
		//чтение сделки
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $deal_id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("CONTACT_ID", "UF_CRM_CLIENT_PHONE", "UF_CRM_CLIENT_NAME", "UF_CRM_CLIENT_LASTNAME", "UF_CRM_NEW_CITY", "UF_CRM_LATITUDE", "UF_CRM_LONGITUDE");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$client_phone    = $ob['UF_CRM_CLIENT_PHONE'];
			$client_name     = $ob['UF_CRM_CLIENT_NAME'];
			$client_lastname = $ob['UF_CRM_CLIENT_LASTNAME'];
			$client_city     = $ob['UF_CRM_NEW_CITY'];
			$client_lat      = $ob['UF_CRM_LATITUDE'];
			$client_lon      = $ob['UF_CRM_LONGITUDE'];
		}
		//сопоставляем контакт
		$client_phone = str_replace(' ', '', $client_phone);
		$client_phone = str_replace('-', '', $client_phone);
		$client_phone = str_replace('(', '', $client_phone);
		$client_phone = str_replace(')', '', $client_phone);
		$contact      = null;
		if (strpos($client_phone, '+') !== false) {
			$phone_substr = substr($client_phone, 2);
		} else {
			$phone_substr = substr($client_phone, 1);
		}
		if (strlen($phone_substr) == 10) {
			$phone_format = true;
		} else {
			$phone_format = false;
		}
		//ищем телефон по базе
		$contacts = $this->contact_search($phone_substr);
		$contact  = min($contacts);
		if ($contact) {  //нашли контакт по телефону
			//формируем телефоны в контакте
			$contact_mobile_phones = array();
			$found_this_phone      = false;
			$dbResult              = CCrmFieldMulti::GetList(
				array(),
				array(
					'ENTITY_ID'  => 'CONTACT',
					'TYPE_ID'    => 'PHONE',
					'ELEMENT_ID' => $contact,
					'VALUE_TYPE' => 'MOBILE',
				)
			);
			while ($ar = $dbResult->Fetch()) {
				$contact_mobile_phones[ $ar['ID'] ] = $ar['VALUE'];
			}
			foreach ($contact_mobile_phones as $phone_id => $phone_value) {
				if (stripos($phone_substr, $phone_value) === false) {
					$this->update_phone_type_contact($contact, $phone_id, 'WORK');
				} else {
					$found_this_phone = true;
				}
			}
			if ( ! ($found_this_phone)) {
				$dbResult = CCrmFieldMulti::GetList(
					array(),
					array(
						'ENTITY_ID'  => 'CONTACT',
						'ELEMENT_ID' => $contact,
						'TYPE_ID'    => 'PHONE',
						'VALUE'      => "%" . $phone_substr,
					)
				);
				while ($ar = $dbResult->Fetch()) {
					$this->update_phone_type_contact($contact, $ar['ID'], 'MOBILE');
				}
			}
			//проверка контакта
			$city_now       = false;
			$arFilter       = array("=ID" => $contact, "CHECK_PERMISSIONS" => 'N');
			$arSelectFields = array("UF_CRM_NEW_CITY");
			$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$city_now = $ob["UF_CRM_NEW_CITY"];
			}
			//обновить имя и город
			$entity = new CCrmContact;
			$fields = array(
				"NAME"      => $client_name,
				"LAST_NAME" => $client_lastname,
			);
			if ($client_city and ! ($city_now)) {
				$fields["UF_CRM_NEW_CITY"] = $client_city;
			}
			$entity->update($contact, $fields);
			$this->register_update($contact);
		} else {  //создаем новый
			$entity  = new CCrmContact;
			$fields  = array(
				'NAME'            => $client_name,
				"LAST_NAME"       => $client_lastname,
				"UF_CRM_NEW_CITY" => $client_city,
				'TYPE_ID'         => "CLIENT",
			);
			$contact = $entity->add($fields);
			$this->add_phone_contact($contact, $client_phone, 'MOBILE');
		}
		$this->active_date_contact($contact);
		if ($client_lat and $client_lon) {
			$this->set_contact_coordinates($contact, $client_lat, $client_lon);
		}
		$this->update_field_deal($deal_id, 'CONTACT_ID', $contact);
		$client_title = $this->get_contact_name($contact);
		$this->timeline_comment($deal_id, 'Создана заявка, заказчик: [url=' . $portal_url . '/crm/contact/details/' . $contact . '/]' . $client_title . '[/url]~BR~Телефон: ' . $client_phone);
	}

	//создание записей в таблице регистраций, провекра по категориям
	function register_add($id) {
		CModule::IncludeModule("crm");
		$categories = array();
		//чтение контакта
		$arFilter       = array("=ID" => $id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("NAME", "LAST_NAME", "SECOND_NAME", "UF_CRM_NEW_CITY", "UF_CRM_UNDER_CATEGORY", "UF_CRM_REGISTER_DATE");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$name        = $ob["NAME"];
			$last_name   = $ob["LAST_NAME"];
			$second_name = $ob["SECOND_NAME"];
			$city        = $ob["UF_CRM_NEW_CITY"];
			$categories  = $ob["UF_CRM_UNDER_CATEGORY"];
			$full_name   = $ob["NAME"] . ' ' . $last_name;
			$reg_date    = date_create($ob['UF_CRM_REGISTER_DATE']);
		}
		//получение списка текущих записей по категориям
		$categories_now = array();
		$arSelect       = array("ID", "IBLOCK_ID", "PROPERTY_331");
		$arFilter       = array("IBLOCK_ID" => 123, "=PROPERTY_330" => $id);
		$res            = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields                          = $ob->GetFields();
			$categories_now[ $arFields["ID"] ] = $arFields["PROPERTY_331_VALUE"];
		};
		//удаление старого
		foreach ($categories_now as $element_id => $category_now) {
			if ( ! (in_array($category_now, (array) $categories))) {
				$this->element_delete(123, $element_id);
			}
		}
		//добавление нового
		foreach ($categories as $key => $category) {
			if ( ! (in_array($category, (array) $categories_now))) {
				$el                 = new CIBlockElement;
				$PROP               = array(
					330 => $id,
					331 => $category,
					332 => $city,
					327 => $full_name,
					328 => $this->get_element_name(84, $city),
					329 => $this->get_section_name(66, $category),
					333 => date_format($reg_date, 'd.m.Y'),
				);
				$arLoadProductArray = array(
					"IBLOCK_ID"       => 123,
					"NAME"            => "Запись регистрации",
					"PROPERTY_VALUES" => $PROP,
				);
				$new_element_id     = $el->Add($arLoadProductArray);
			}
		}
	}

	//обновление записей в таблице регистраций, при возможном изменении полей
	function register_update($id) {
		CModule::IncludeModule("crm");
		//чтение контакта
		$arFilter       = array("=ID" => $id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("NAME", "LAST_NAME", "SECOND_NAME", "UF_CRM_NEW_CITY", "UF_CRM_REGISTER_DATE");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$name        = $ob["NAME"];
			$last_name   = $ob["LAST_NAME"];
			$second_name = $ob["SECOND_NAME"];
			$city        = $ob["UF_CRM_NEW_CITY"];
			$full_name   = $ob["NAME"] . ' ' . $last_name;
			$reg_date    = date_create($ob['UF_CRM_REGISTER_DATE']);
		}
		//получение списка текущих записей и обновление
		$categories_now = array();
		$arSelect       = array("ID", "IBLOCK_ID");
		$arFilter       = array("IBLOCK_ID" => 123, "=PROPERTY_330" => $id);
		$res            = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$PROP     = array(
				327 => $full_name,
				332 => $city,
				328 => $this->get_element_name(84, $city),
				333 => date_format($reg_date, 'd.m.Y'),
			);
			CIBlockElement::SetPropertyValuesEx($arFields["ID"], 123, $PROP);
		};
	}

	//разовый пересчет записей в таблице регистраций
	function register_total_count() {
		CModule::IncludeModule("crm");
		//очищаем таблицу
		$arSelect = array("ID", "IBLOCK_ID");
		$arFilter = array("IBLOCK_ID" => 124);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$element_id = $arFields["ID"];
			$this->element_delete(124, $element_id);
		};
		//собираем категории
		$categories      = array();
		$category_titles = array();
		$arSelect        = array();
		$arFilter        = array('IBLOCK_ID' => 66, 'GLOBAL_ACTIVE' => 'Y', 'DEPTH_LEVEL' => 1);
		$db_list         = CIBlockSection::GetList(array("SORT" => "ASC"), $arFilter, true, $arSelect);
		while ($ar_result = $db_list->GetNext()) {
			$id                     = $ar_result['ID'];
			$name                   = $ar_result['NAME'];
			$categories[]           = $id;
			$category_titles[ $id ] = $name;
		};
		foreach ($categories as $key => $category) {
			//поиск в контактах
			$category_count = array();
			$arFilter       = array("=UF_CRM_UNDER_CATEGORY" => $category, "CHECK_PERMISSIONS" => 'N');
			$arSelectFields = array("UF_CRM_NEW_CITY");
			$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$city_id = $ob["UF_CRM_NEW_CITY"];
				if ($category_count[ $city_id ]) {
					$category_count[ $city_id ]++;
				} else {
					$category_count[ $city_id ] = 1;
				}
			}
			ksort($category_count);
			//создание записей по данной категории
			foreach ($category_count as $city => $workers_amount) {
				$el                 = new CIBlockElement;
				$PROP               = array(
					338 => $category,
					337 => $city,
					336 => $workers_amount,
					335 => $this->get_section_name(66, $category),
					334 => $this->get_element_name(84, $city),
				);
				$arLoadProductArray = array(
					"IBLOCK_ID"       => 124,
					"NAME"            => "Запись регистрации",
					"PROPERTY_VALUES" => $PROP,
				);
				$new_element_id     = $el->Add($arLoadProductArray);
			}
		}
	}

	//разовый расчет отчета пополнений по городам
	function cities_cash_count($report_element_id) {
		if ( ! ($report_element_id)) {
			return;
		}
		CModule::IncludeModule("crm");
		//очищаем таблицу
		$arSelect = array("ID", "IBLOCK_ID");
		$arFilter = array("IBLOCK_ID" => 126);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$element_id = $arFields["ID"];
			$this->element_delete(126, $element_id);
		};
		//чтение дат из записи
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_353", "PROPERTY_354");
		$arFilter = array("IBLOCK_ID" => 127, "=ID" => $report_element_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields  = $ob->GetFields();
			$date_from = date_create($arFields["PROPERTY_353_VALUE"]);
			$date_to   = date_create($arFields["PROPERTY_354_VALUE"]);
		};
		date_add($date_to, date_interval_create_from_date_string("1 days"));
		//собираем все города
		$cities   = array();
		$arFilter = array("IBLOCK_ID" => 84);
		$arSelect = array("ID");
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$city     = $arFields["ID"];
			$cities[] = $city;
		}
		foreach ($cities as $key => $city_id) {
			//количество пользователей
			$total_workers  = array();
			$active_workers = array();
			$total_orders   = array();
			$orders_accept  = array();
			$orders_cancel  = array();
			$orders_active  = array();
			$pays_total     = 0;
			$spends_total   = 0;
			$spends_package = 0;
			$arFilter       = array("=UF_CRM_NEW_CITY" => $city_id, "=UF_CRM_NEW_APP_REGISTER" => 31169, "CHECK_PERMISSIONS" => 'N');
			$arSelectFields = array("ID");
			$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$total_workers[] = $ob['ID'];
			}
			$count_total_workers = count($total_workers);
			//количество заявок
			$arFilter       = array("=CATEGORY_ID" => 7, "=UF_CRM_NEW_CITY" => $city_id, ">UF_CRM_BEGINTIME" => date_format($date_from, 'd.m.Y H:i:s'), "<UF_CRM_BEGINTIME" => date_format($date_to, 'd.m.Y H:i:s'), "CHECK_PERMISSIONS" => 'N');
			$arSelectFields = array("ID");
			$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$order_id       = $ob['ID'];
				$total_orders[] = $order_id;
			}
			$count_total_orders = count($total_orders);
			if ($count_total_workers or $count_total_orders) {
				//сумма пополнений / списаний, активные пользователи
				$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_186", "PROPERTY_187", "PROPERTY_188", "PROPERTY_192");
				$arFilter = array("IBLOCK_ID" => 87, "=PROPERTY_355" => $city_id, ">DATE_CREATE" => date_format($date_from, 'd.m.Y H:i:s'), "<DATE_CREATE" => date_format($date_to, 'd.m.Y H:i:s'));
				$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields            = $ob->GetFields();
					$name                = $arFields["NAME"];
					$type                = $arFields["PROPERTY_186_VALUE"];
					$amount              = $arFields["PROPERTY_187_VALUE"];
					$package_amount      = $arFields["PROPERTY_188_VALUE"];
					$transaction_user_id = $arFields["PROPERTY_192_VALUE"];
					if ( ! (in_array($transaction_user_id, (array) $active_workers))) {
						$active_workers[] = $transaction_user_id;
					}
					if ($type == 31235 and $name != 'Перенос основного баланса') { //пополнение основного баланса
						$pays_total = $pays_total + $amount;
					}
					if ($type == 31236) { //списание за заявку
						$spends_total = $spends_total + $amount;
					}
					if ($type == 31237) { //списание за пакет
						$spends_total   = $spends_total + $amount;
						$spends_package = $spends_package + $amount;
					}
				};
				$count_active_workers = count($active_workers);
				//принятые заявки
				$accept_stages  = array('C7:FINAL_INVOICE', 'C7:3', 'C7:1', 'C7:WON');
				$arFilter       = array(
					"=CATEGORY_ID"        => 7,
					"=UF_CRM_NEW_CITY"    => $city_id,
					"STAGE_ID"            => $accept_stages,
					">UF_CRM_INWORK_TIME" => date_format($date_from, 'd.m.Y H:i:s'),
					"<UF_CRM_INWORK_TIME" => date_format($date_to, 'd.m.Y H:i:s'),
					"CHECK_PERMISSIONS"   => 'N'
				);
				$arSelectFields = array("ID");
				$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
				while ($ob = $res->GetNext()) {
					$order_id        = $ob['ID'];
					$orders_accept[] = $order_id;
				}
				$count_orders_accept = count($orders_accept);
				//непринятые заявки
				$cancel_stages  = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
				$arFilter       = array("=CATEGORY_ID" => 7, "=UF_CRM_NEW_CITY" => $city_id, "STAGE_ID" => $cancel_stages, ">=CLOSEDATE" => date_format($date_from, 'd.m.Y'), "<CLOSEDATE" => date_format($date_to, 'd.m.Y'), "CHECK_PERMISSIONS" => 'N');
				$arSelectFields = array("ID");
				$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
				while ($ob = $res->GetNext()) {
					$order_id        = $ob['ID'];
					$orders_cancel[] = $order_id;
				}
				$count_orders_cancel = count($orders_cancel);
				//активные заявки - считается на сейчас вне зависимости от участка времени
				$active_stages  = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
				$arFilter       = array("=CATEGORY_ID" => 7, "=UF_CRM_NEW_CITY" => $city_id, "STAGE_ID" => $active_stages, "CHECK_PERMISSIONS" => 'N');
				$arSelectFields = array("ID");
				$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
				while ($ob = $res->GetNext()) {
					$order_id        = $ob['ID'];
					$orders_active[] = $order_id;
				}
				$count_orders_active = count($orders_active);
				//создание записей в таблице
				$el                 = new CIBlockElement;
				$PROP               = array(
					342 => $count_total_workers,
					343 => $count_active_workers,
					344 => $pays_total,
					345 => $spends_total,
					346 => $spends_package,
					350 => $count_total_orders,
					351 => $city_id,
					357 => $count_orders_accept,
					358 => $count_orders_cancel,
					356 => $count_orders_active,
				);
				$arLoadProductArray = array(
					"IBLOCK_ID"       => 126,
					"NAME"            => $this->get_element_name(84, $city_id),
					"PROPERTY_VALUES" => $PROP,
				);
				$new_element_id     = $el->Add($arLoadProductArray);
			}
		}
		if (CModule::IncludeModule("bizproc")) {
			$wfId = CBPDocument::StartWorkflow(
				240,
				array("lists", "BizprocDocument", $report_element_id),
				array_merge(),
				$arErrorsTmp
			);
		}
	}

	//очистка таблицы логов
	function log_clean() {
		//выборка записей
		$selection_period = -7;
		$time_point       = date_create(date('Y-m-d H:i:s', time()));
		date_add($time_point, date_interval_create_from_date_string($selection_period . " days"));
		$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_203", "DATE_CREATE");
		$arFilter = array("IBLOCK_ID" => 89, "<DATE_CREATE" => date_format($time_point, 'd.m.Y H:i:s'));
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$element_id = $arFields["ID"];
			$title      = $arFields["NAME"];
			$request    = $arFields["~PROPERTY_203_VALUE"]["TEXT"];
			$date       = date_create($arFields["DATE_CREATE"]);
			//пишем лог
			if (is_object(json_decode($request))) {
				$request = json_decode($request);
			}
			$logBody = array(
				'title'   => $title,
				'request' => $request,
			);
			$log     = date_format($date, 'Y-m-d H:i:s') . ' - log - ' . print_r($logBody, true);
			file_put_contents(__DIR__ . '/main_log.txt', $log . PHP_EOL, FILE_APPEND);
			$this->element_delete(89, $element_id);
			echo $element_id . '<br>';
		};


	}

	//получение статуса заказа - мастер
	function get_order_status_worker($id, $order_id, $stage_id, $worker_id, $closed_flag, $canceled_workers) {
		$preparation_stages = array('C7:NEW', 'C7:PREPARATION');
		$active_stages      = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
		$inwork_stages      = array('C7:FINAL_INVOICE', 'C7:3', 'C7:1');
		$refund_stages      = array('C7:2', 'C7:6');
		$success_stages     = array('C7:WON');
		$lose_stages        = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		$state_color        = 0;

		//проверка откликов
		$active_responds   = array();
		$expired_responds  = array();
		$inactive_responds = array();
		if ($id) {
			$arSelect_resp = array("ID", "IBLOCK_ID", "PROPERTY_132");
			$arFilter_resp = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_136" => $id, "=PROPERTY_132" => array(31191, 31193, 31212, 31192));
			$res_resp      = CIBlockElement::GetList(array(), $arFilter_resp, false, false, $arSelect_resp);
			while ($ob_resp = $res_resp->GetNextElement()) {
				$arFields_resp = $ob_resp->GetFields();
				if ($arFields_resp["PROPERTY_132_VALUE"] == 31212) {
					$expired_responds[] = $arFields_resp["ID"];
				} else if ($arFields_resp["PROPERTY_132_VALUE"] == 31191 or $arFields_resp["PROPERTY_132_VALUE"] == 31193) {
					$active_responds[] = $arFields_resp["ID"];
				} else if ($arFields_resp["PROPERTY_132_VALUE"] == 31192) {
					$inactive_responds[] = $arFields_resp["ID"];
				}
			};
		}

		//---------РАСЧЕТ СТАТУСОВ---------
		//статусы в подготовке (защита от случайного попадания)
		if (in_array($stage_id, (array) $preparation_stages)) {
			$state       = -1;
			$state_color = 1;
			$state_title = "Заказ недоступен";
		} //статусы с выбранным текущим мастером
		else if ($id and $worker_id == $id and in_array($stage_id, (array) $refund_stages)) {
			$state       = 7;
			$state_title = "По заказу был запрошен возврат";
		} else if ($id and $worker_id == $id and in_array($stage_id, (array) $inwork_stages) and $closed_flag == 30957) {
			$state       = 4;
			$state_title = "Я являюсь исполнителем заказа, заказ завершен";
		} else if ($id and $worker_id == $id and in_array($stage_id, (array) $inwork_stages)) {
			$state       = 3;
			$state_title = "Я являюсь исполнителем заказа, заказ активен";
		} else if ($id and $worker_id == $id and in_array($stage_id, (array) $success_stages)) {
			$state       = 4;
			$state_title = "Я являюсь исполнителем заказа, заказ завершен";
		} else if ($id and $worker_id == $id and in_array($stage_id, (array) $lose_stages)) {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ был отменен';
		} //статусы в свободных заказах
		else if (in_array($stage_id, (array) $active_stages) and $id and in_array($id, (array) $canceled_workers)) {
			$state_color = 1;
			$state       = 6;
			$state_title = 'Вы уже отказались от этого заказа';
		} else if (in_array($stage_id, (array) $active_stages) and $active_responds) {
			$state       = 1;
			$state_title = "Заказ принят";
		} else if (in_array($stage_id, (array) $active_stages) and $expired_responds) {
			$state       = 2;
			$state_title = "Время принятия заказа истекло";
		} else if (in_array($stage_id, (array) $active_stages)) {
			$state       = 0;
			$state_title = "Заказ не принят";
		} //прочие статусы, где выбран другой мастер
		else if ($inactive_responds) {
			$state       = 5;
			$state_color = 1;
			$state_title = "Я не являюсь исполнителем заказа: заказ отдан другому или отменен";
		} else if (in_array($stage_id, (array) $success_stages)) {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ уже выполнен';
		} else if (in_array($stage_id, (array) $lose_stages)) {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ был отменен';
		} else {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ уже взят в работу';
		}

		//возвращаем ответ
		return array(
			'state'       => $state,
			'state_color' => $state_color,
			'state_title' => $state_title,
		);
	}

	//получение статуса заказа - заказчик
	function get_order_status_client($id, $order_id, $stage_id, $closed_flag, $confirmation_flag) {
		global $yes;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
		}
		$new_stages     = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
		$inwork_stages  = array('C7:FINAL_INVOICE', 'C7:2', 'C7:6');
		$border_stages  = array('C7:1', 'C7:3');
		$success_stages = array('C7:WON');
		$lose_stages    = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		$refund_stages  = array('C7:2', 'C7:6');

		$state_color = 0;
		if (in_array($stage_id, (array) $lose_stages)) {
			$state       = 5;
			$state_color = 1;
			$state_title = "Заказ отменен";
		} else if (in_array($stage_id, (array) $success_stages)) {
			$state       = 4;
			$state_title = "Заказ завершен";
		} else if (in_array($stage_id, (array) $refund_stages)) {
			$state       = 7;
			$state_title = "По заказу был запрошен возврат";
		} else if (in_array($stage_id, (array) $border_stages) and $closed_flag == 30957) {
			$state       = 4;
			$state_title = "Заказ завершен";
		} else if (in_array($stage_id, (array) $border_stages)) {
			$state       = 3;
			$state_title = "Заказ ожидает оценки";
		} else if (in_array($stage_id, (array) $inwork_stages)) {
			$state       = 3;
			$state_title = "Заказ выполняется";
		} else if ($confirmation_flag == $yes) {
			$state       = 6;
			$state_title = "Заказ доступен для принятия, но размещен давно: требуется актуализация";
		} else {
			$state       = 0;
			$state_title = "Заказ доступен для принятия";
		}

		//возвращаем ответ
		return array(
			'state'       => $state,
			'state_color' => $state_color,
			'state_title' => $state_title,
		);
	}

	//проведение ручной транзакции
	function hand_transaction($target, $balance_type, $package_id, $value, $info, $yes, $id, $time_end_point = false) {
		$globals = $this->set_globals();
		$yes     = $globals['yes'];
		$no      = $globals['no'];
		//предустановки
		$result = 0;
		if ($balance_type == 31230) {
			$title        = "Основной баланс";
			$action_title = "Изменение основного баланса менеджером";
			if ($target == 34822) { //списание
				$action_type = 35140;
			} else {
				$action_type = 35139;
			}
		} else if ($balance_type == 31231) {
			$title        = "Бонусный баланс";
			$action_title = "Изменение бонусного баланса менеджером";
			if ($target == 34822) {
				$action_type = 35142;
			} else {
				$action_type = 35141;
			}
		} else if ($balance_type == 31234) {
			$title        = "Бонусный баланс заказчика";
			$action_title = "Изменение бонусного баланса заказчика менеджером";
			if ($target == 34822) {
				$action_type = 31246;
			} else {
				$action_type = 31245;
			}
		} else if ($balance_type == 31232) {
			$package_title = $this->get_element_name(75, $package_id);
			$title         = "Пакет";
			$action_title  = "Изменение пакета " . $package_title . " менеджером";
			if ($target == 34822) {
				$action_type = 1011934;
			} else {
				$action_type = 1011893;
			}
		}
		//ищем запись по балансу
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_161", "PROPERTY_163");
		$arFilter = array("IBLOCK_ID" => 80, "=PROPERTY_160" => $balance_type, "=PROPERTY_167" => $yes, "=PROPERTY_168" => $id);
		if ($balance_type == 31232 and $package_id) {
			$arFilter["=PROPERTY_162"] = $package_id;
		}
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields        = $ob->GetFields();
			$balance_element = $arFields["ID"];
			if ($balance_type == 31232) { //пакет
				$balance_amount = $arFields["PROPERTY_163_VALUE"];
			} else { //прочее
				$balance_amount = $arFields["PROPERTY_161_VALUE"];
			}
			if ( ! ($balance_amount)) {
				$balance_amount = 0;
			}
		}
		//изменяем текущую
		if ($balance_element) {
			$PROP = array();
			if ($target == 34822) {
				if ($value > $balance_amount) {
					$value  = $balance_amount;
					$result = "VALUE_REDUCED";
					if ($value == 0) {
						$result = "VALUE_CANCELED";
					}
				}
				$new_amount = $balance_amount - $value;
			} else {
				$new_amount = $balance_amount + $value;
			}
			//перезапись элемента
			if ($balance_type == 31232) {
				$PROP[163] = $new_amount;
			} else {
				$PROP[161] = $new_amount;
			}
			CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
			//пакет закончился в ноль
			if ($balance_type == 31232 and $new_amount <= 0) {
				CIBlockElement::SetPropertyValuesEx($balance_element, 80, array(167 => $no, 296 => 'Нет'));
			}
		} else { //создаем новую
			if ($target == 34822) { //списание
				$value  = 0;
				$result = "VALUE_CANCELED";
			} else if ($balance_type == 31232) { //создание нового пакета
				if ( ! ($time_end_point)) { //не задано время
					return array(
						'result' => "VALUE_CANCELED_PACK_TIME",
						'amount' => 0,
					);
				}
				//чтение данных о пакете
				$action_price = false;
				$arSelect     = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_141", "PROPERTY_142", "PROPERTY_145", "PROPERTY_146", "PROPERTY_152", "PROPERTY_360");
				$arFilter     = array("IBLOCK_ID" => 75, "=ID" => $package_id);
				$res          = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields        = $ob->GetFields();
					$max_activations = $arFields["PROPERTY_360_VALUE"];
					$standard_value  = $arFields["PROPERTY_141_VALUE"];
					if ($arFields["PROPERTY_152_VALUE"] == $no) {
						$value  = 0;
						$result = "VALUE_CANCELED";
					}
					if ($arFields["PROPERTY_146_VALUE"]) {
						$package_price = $arFields["PROPERTY_146_VALUE"];
						$package_title = $arFields["NAME"] . " (акция)";
						$action_price  = true;
					} else {
						$package_price = $arFields["PROPERTY_145_VALUE"];
						$package_title = $arFields["NAME"];
					}
				};
				//создаем запись баланса по пакету
				$balance_element = $this->add_balance_element($balance_type, false, $package_id, $value, $id, "Пакет " . $package_title);
				//дополняем запись по пакету
				$time_start_point = date_create(date('Y-m-d H:i:s', time()));
				//очистка даты от мусора
				$pos1 = strpos($time_end_point, "[");
				$pos2 = strpos($time_end_point, "]");
				if ($pos1) {
					$pos1           = $pos1 - 1;
					$pos2           = $pos2 + 1;
					$text_del1      = substr($time_end_point, $pos1, ($pos2 - $pos1));
					$time_end_point = str_replace($text_del1, '', $time_end_point);
				}
				$time_end_point = date_create($time_end_point);
				$PROP           = array();
				$PROP[200]      = date_format($time_end_point, 'd.m.Y H:i:s');
				$PROP[295]      = date_format($time_end_point, 'd.m.Y');
				$PROP[280]      = date_format($time_start_point, 'd.m.Y H:i:s');
				if ($standard_value != 0) {
					$PROP[250] = round(($package_price / $standard_value), 0);
				} else {
					$PROP[250] = 0;
				}
				CIBlockElement::SetPropertyValuesEx($balance_element, 80, $PROP);
				//запуск процесса деактивации пакета
				if (CModule::IncludeModule("bizproc")) {
					$wfId = CBPDocument::StartWorkflow(
						200,
						array("lists", "BizprocDocument", $balance_element),
						array_merge(),
						$arErrorsTmp
					);
				}
				$action_title = "Активация пакета " . $package_title . " менеджером";
			} else { //создание нового обычного баланса
				$balance_element = $this->add_balance_element($balance_type, $value, false, false, $id, $title);
			}
		}
		//фиксация транзакции
		if ($value != 0) {
			if ($balance_type == 31232) { //пакет
				$transaction = $this->add_transaction($action_type, false, $value, $balance_element, $balance_type, false, $id, false, $action_title);
			} else { //прочее
				$transaction = $this->add_transaction($action_type, $value, false, $balance_element, $balance_type, false, $id, false, $action_title);
			}
			CIBlockElement::SetPropertyValuesEx($transaction, 87, array(264 => $yes, 305 => $info));
		}
		if ( ! ($result)) {
			$result = "SUCCESS";
		}

		return array(
			'result' => $result,
			'amount' => $value,
		);
	}

	//очистка заявки при возврате на старт
	function order_restart($order_id) {
		if ( ! ($order_id)) {
			return false;
		}
		global $yes;
		global $no;
		if ( ! ($yes)) {
			$globals = $this->set_globals();
			$yes     = $globals['yes'];
			$no      = $globals['no'];
		}
		CModule::IncludeModule("crm");
		//остановить старый процесс
		$this->stop_deal_process($order_id);
		//чтение сделки
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("UF_CRM_MASTER", "UF_CRM_CANCELED_WORKERS");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$worker_id        = $ob['UF_CRM_MASTER'];
			$canceled_workers = $ob['UF_CRM_CANCELED_WORKERS'];
		}
		$canceled_workers[] = $worker_id;
		//Получение всех откликов по заказу для отмены
		$i        = 0;
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_138", "PROPERTY_249");
		$arFilter = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $order_id, "=PROPERTY_132" => array(31191, 31212, 31193));
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$respond  = $arFields["ID"];
			$i++;
			if ($i > 20) {
				continue;
			} //защита от массового пробоя фильра, на всякий случай
			//отбраковка отклика
			$PROP            = array();
			$PROP["AKTIVEN"] = 31192;  //неактивен
			CIBlockElement::SetPropertyValuesEx($respond, 73, $PROP);
		}
		//убрать мастера из сделки, актуализировать заказ
		$time_point = date_create(date('Y-m-d H:i:s', time()));
		$entity     = new CCrmDeal;
		$fields     = array(
			"UF_CRM_MASTER"                 => false,
			"UF_CRM_ACTUALTIME"             => date_format($time_point, 'd.m.Y H:i:s'),
			"UF_CRM_COUNTER_WATCHEDMASTERS" => array(),
			"UF_CRM_CANCELED_WORKERS"       => $canceled_workers,
			"UF_CRM_REPORT_TAX"             => false,
			"UF_CRM_REPORT_TARIF"           => false,
			"UF_CRM_RATING_MAIN"            => false,
			"UF_CRM_RATING_QUALITY"         => false,
			"UF_CRM_RATING_CLEAN"           => false,
			"UF_CRM_RATING_TIME"            => false,
			"UF_CRM_RATING_TEXT"            => false,
			"UF_CRM_FEEDBACK_PROCESS"       => false,
			"UF_CRM_COUNTER_ORDERCOMPL"     => false,
		);
		$entity->update($order_id, $fields);
		//изменить запись в заказах мастеров
		if ($worker_id) {
			$arSelect = array("ID", "IBLOCK_ID");
			$arFilter = array("IBLOCK_ID" => 82, "=PROPERTY_169" => $order_id, "=PROPERTY_170" => $worker_id);
			$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields   = $ob->GetFields();
				$element_id = $arFields["ID"];
			};
			CIBlockElement::SetPropertyValuesEx($element_id, 82, array(339 => 95));
		}
	}

	//пуш по ручным заявкам
	function hand_orders_push($order_id) {
		//чтение сделки
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $order_id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array(CDbzContact::CATEGORIES_USER_FIELD_CODE, CDbzContact::WORKS_USER_FIELD_CODE, "UF_CRM_NEW_CITY", "TITLE");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$main_category = $ob[ CDbzContact::CATEGORIES_USER_FIELD_CODE ];
			$workType      = $ob[ CDbzContact::WORKS_USER_FIELD_CODE ];
			$city          = $ob['UF_CRM_NEW_CITY'];
			$title         = $ob['TITLE'];

			if ( (! $workType && ! $main_category) || ! $city) {
				return;
			}

			//проверка наличия мастеров - уведомления
			$right_workers                = array();
			$arFilter                     = array("CHECK_PERMISSIONS" => 'N');
			$arFilter["=UF_CRM_NEW_CITY"] = $city;

			$arFilter[] = array(
				"LOGIC" => "OR",
				array(CDbzContact::CATEGORIES_USER_FIELD_CODE => $main_category),
				array(CDbzContact::WORKS_USER_FIELD_CODE => $workType),
			);

			$arSelectFields = array("ID");
			$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$right_workers[] = $ob['ID'];
			}

			$count_workers = count($right_workers);
			if ($count_workers !== 0) {
				$this->add_notification($right_workers, 0, 34417, $order_id, $title); //мастерам - новая заявка
			}
		}
	}

	//подсчет запросов не из приложения
	function hand_request_count($order_id) {
		//чтение сделки
		CModule::IncludeModule("crm");
		$arFilter       = array("=ID" => $order_id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("UF_CRM_UNDER_CATEGORY", "UF_CRM_NEW_CITY", "TITLE");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$main_category = $ob['UF_CRM_UNDER_CATEGORY'];
			$city          = $ob['UF_CRM_NEW_CITY'];
		}
		if ( ! ($main_category) or ! ($city)) {
			return;
		}
		//просчет количества масетров
		$right_workers                        = array();
		$arFilter                             = array("=UF_CRM_UNDER_CATEGORY" => $main_category, "CHECK_PERMISSIONS" => 'N');
		$arFilter["=UF_CRM_NEW_APP_REGISTER"] = 31169;
		$arFilter["=UF_CRM_NEW_CITY"]         = $city;
		$arSelectFields                       = array("ID");
		$res                                  = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$right_workers[] = $ob['ID'];
		}
		$count_workers = count($right_workers);
		//запись в таблицу запросов
		if ($city and $main_category) {
			//ищем запись
			$time_point                = date_create(date('Y-m-d H:i:s', time()));
			$arSelect                  = array("ID", "IBLOCK_ID", "PROPERTY_269");
			$arFilter                  = array("IBLOCK_ID" => 109, "=PROPERTY_272" => $city, "=PROPERTY_267" => $main_category);
			$arFilter["=PROPERTY_273"] = false;
			$res                       = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			while ($ob = $res->GetNextElement()) {
				$arFields   = $ob->GetFields();
				$element_id = $arFields["ID"];
				$workers    = $arFields["PROPERTY_269_VALUE"];
			};
			//изменяем текущую
			if ($element_id) {
				$PROP      = array();
				$PROP[269] = $workers + 1;
				$PROP[270] = date_format($time_point, 'd.m.Y H:i:s');
				$PROP[274] = (int) $count_workers;
				CIBlockElement::SetPropertyValuesEx($element_id, 109, $PROP);
			} //создаем новую
			else {
				$el                 = new CIBlockElement;
				$PROP               = array();
				$PROP[272]          = $city;
				$PROP[267]          = $main_category;
				$PROP[269]          = 1;
				$PROP[270]          = date_format($time_point, 'd.m.Y H:i:s');
				$PROP[274]          = (int) $count_workers;
				$arLoadProductArray = array(
					"IBLOCK_ID"       => 109,
					"NAME"            => "Запись счетчика запросов",
					"PROPERTY_VALUES" => $PROP,
				);
				$element_id         = $el->Add($arLoadProductArray);
			}
			if ($count_workers === 0) {
				if (CModule::IncludeModule("bizproc")) {
					$wfId = CBPDocument::StartWorkflow(
						212,
						array("lists", "BizprocDocument", $element_id),
						array_merge(),
						$arErrorsTmp
					);
				}
			}
		}
	}

	//поиск по таблице поисковых запросов - по тексту
	function search_requests_find($text) {
		$works = array();
		//собираем доп. запросы
		$arSelect = array("ID", "NAME");
		$arFilter = array("IBLOCK_ID" => 125, "ACTIVE" => "Y", $this->search_wide($text));
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$work     = array("id" => (int) $arFields["ID"], "title" => $arFields["NAME"], "type" => 3);
			$works[]  = $work;
		};

		return $works;
	}

	//поиск работы по таблице поисковых запросов
	function search_requests_work($element_id) {
		$work     = false;
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_340");
		$arFilter = array("IBLOCK_ID" => 125, "ID" => $element_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$work     = $arFields["PROPERTY_340_VALUE"];
		};

		return $work;
	}

	//расширение поиска с допуском на ошибки
	function search_wide($text) {
		$one_mistake    = 4;
		$two_mistakes   = 6;
		$split          = str_split($text, 2);
		$len            = count($split);
		$search_array   = array("LOGIC" => "OR");
		$search_array[] = array("NAME" => "%" . $text . "%");
		if ($len >= $one_mistake) {
			foreach ($split as $i => $letter) {
				$split_local = $split;
				if ($i == 0) {
					continue;
				}
				$split_local[ $i ] = '_';
				$name              = implode("", $split_local);
				$search_array[]    = array("NAME" => "%" . $name . "%");
				if ($len >= $two_mistakes) {
					foreach ($split as $i2 => $letter2) {
						$split_local2 = $split_local;
						if ($i != $i2) {
							$split_local2[ $i2 ] = '_';
							$name                = implode("", $split_local2);
							$search_array[]      = array("NAME" => "%" . $name . "%");
						}
					}
				}
			}
		}

		return $search_array;
	}

	//расширение поиска с допуском на ошибки - категории
	function search_wide_category($text) {
		$one_mistake    = 4;
		$two_mistakes   = 6;
		$split          = str_split($text, 2);
		$len            = count($split);
		$search_array[] = array("NAME" => "%" . $text . "%");
		if ($len >= $one_mistake and $len <= 15) {
			foreach ($split as $i => $letter) {
				$split_local = $split;
				if ($i == 0) {
					continue;
				}
				$split_local[ $i ] = '_';
				$name              = implode("", $split_local);
				$search_array[]    = array("NAME" => "%" . $name . "%");
				if ($len >= $two_mistakes) {
					foreach ($split as $i2 => $letter2) {
						$split_local2 = $split_local;
						if ($i != $i2) {
							$split_local2[ $i2 ] = '%';
							$name                = implode("", $split_local2);
							$search_array[]      = array("NAME" => "%" . $name . "%");
						}
					}
				}
			}
		}

		return $search_array;
	}

	//сортировка поисковых запросов и отсев неподходящих
	function search_sort($works, $text) {
		$max_levenshtein = 60;
		$min_winkler     = 0.45;
		$new_works       = array();
		$weight          = array();
		foreach ($works as $key => $work) {
			$work_name     = $work['title'];
			$distance      = levenshtein(mb_convert_case($text, MB_CASE_LOWER, "UTF-8"), mb_convert_case($work_name, MB_CASE_LOWER, "UTF-8"), 1, 10, 10);
			$distance_rate = $this->JaroWinkler(mb_convert_case($text, MB_CASE_LOWER, "UTF-8"), mb_convert_case($work_name, MB_CASE_LOWER, "UTF-8"));
			if ($work['type'] == 1) {
				$distance = $distance - 5; //приоритет выдачи для категорий
			}
			if ($distance > $max_levenshtein or $distance_rate < $min_winkler) {
				unset($works[ $key ]);
				continue;
			}
			$new_works[] = $work;
			$weight[]    = $distance;
		}
		array_multisort($weight, SORT_ASC, $new_works);

		return $new_works;
	}

	function getCommonCharacters($string1, $string2, $allowedDistance) {

		$str1_len     = strlen($string1);
		$str2_len     = strlen($string2);
		$temp_string2 = $string2;

		$commonCharacters = '';

		for ($i = 0; $i < $str1_len; $i++) {

			$noMatch = true;

			// compare if char does match inside given allowedDistance
			// and if it does add it to commonCharacters
			for ($j = max(0, $i - $allowedDistance); $noMatch && $j < min($i + $allowedDistance + 1, $str2_len); $j++) {
				if ($temp_string2[ $j ] == $string1[ $i ]) {
					$noMatch = false;

					$commonCharacters .= $string1[ $i ];

					if (is_array($temp_string2)) {
						$temp_string2[ $j ] = '';
					}

				}
			}
		}

		return $commonCharacters;
	}

	function Jaro($string1, $string2) {

		$str1_len = strlen($string1);
		$str2_len = strlen($string2);

		// theoretical distance
		$distance = (int) floor(min($str1_len, $str2_len) / 2.0);

		// get common characters
		$commons1 = $this->getCommonCharacters($string1, $string2, $distance);
		$commons2 = $this->getCommonCharacters($string2, $string1, $distance);

		if (($commons1_len = strlen($commons1)) == 0) {
			return 0;
		}
		if (($commons2_len = strlen($commons2)) == 0) {
			return 0;
		}

		// calculate transpositions
		$transpositions = 0;
		$upperBound     = min($commons1_len, $commons2_len);
		for ($i = 0; $i < $upperBound; $i++) {
			if ($commons1[ $i ] != $commons2[ $i ]) {
				$transpositions++;
			}
		}
		$transpositions /= 2.0;

		// return the Jaro distance
		return ($commons1_len / ($str1_len) + $commons2_len / ($str2_len) + ($commons1_len - $transpositions) / ($commons1_len)) / 3.0;

	}

	function getPrefixLength($string1, $string2, $MINPREFIXLENGTH = 4) {

		$n = min(array($MINPREFIXLENGTH, strlen($string1), strlen($string2)));

		for ($i = 0; $i < $n; $i++) {
			if ($string1[ $i ] != $string2[ $i ]) {
				// return index of first occurrence of different characters
				return $i;
			}
		}

		// first n characters are the same
		return $n;
	}

	function JaroWinkler($string1, $string2, $PREFIXSCALE = 0.1) {

		$JaroDistance = $this->Jaro($string1, $string2);

		$prefixLength = $this->getPrefixLength($string1, $string2);

		return $JaroDistance + $prefixLength * $PREFIXSCALE * (1.0 - $JaroDistance);
	}

	//Добавление отзыва
	function addFeedback($id, $user_type, $order_id, $ratings, $text) {
		$globals = $this->set_globals();
		$yes     = $globals['yes'];
		$no      = $globals['no'];
		$this->client_type_check($user_type);
		CModule::IncludeModule("crm");
		//чтение заказа
		$arFilter       = array("=ID" => $order_id);
		$arSelectFields = array("CONTACT_ID", "TITLE", "UF_CRM_RATING_MAIN", "STAGE_ID", "UF_CRM_CLOSED_BY_WORKER", "UF_CRM_MASTER");
		$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$client_id        = $ob["CONTACT_ID"];
			$rating_old       = $ob["UF_CRM_RATING_MAIN"];
			$stage_id         = $ob["STAGE_ID"];
			$closed_by_worker = $ob["UF_CRM_CLOSED_BY_WORKER"];
			$title            = $ob["TITLE"];
			$worker_id        = $ob["UF_CRM_MASTER"];
		}
		if ($id != $client_id or $rating_old or $rating_old === '0') {
			return $this->getFailure("Уже невозможно оставить отзыв до данному заказу", false, null);
		}
		//дешифровка оценок
		$rating_main    = 0;
		$rating_quality = false;
		$rating_clean   = false;
		$rating_time    = false;
		foreach ($ratings as $key => $rating) {
			if ($rating['type'] == 0 and $rating['value'] != '') {
				$rating_main = $rating['value'];
			} else if ($rating['type'] == 1) {
				if ($rating['value'] > 0) {
					$rating_quality = $yes;
				} else {
					$rating_quality = $no;
				}
			} else if ($rating['type'] == 2) {
				if ($rating['value'] > 0) {
					$rating_clean = $yes;
				} else {
					$rating_clean = $no;
				}
			} else if ($rating['type'] == 3) {
				if ($rating['value'] > 0) {
					$rating_time = $yes;
				} else {
					$rating_time = $no;
				}
			}
		}
		//проставить отзыв в сделку
		$entity = new CCrmDeal;
		$fields = array(
			"UF_CRM_RATING_MAIN"        => $rating_main,
			"UF_CRM_RATING_QUALITY"     => $rating_quality,
			"UF_CRM_RATING_CLEAN"       => $rating_clean,
			"UF_CRM_RATING_TIME"        => $rating_time,
			"UF_CRM_RATING_TEXT"        => $text,
			"UF_CRM_FEEDBACK_PROCESS"   => 30957,
			"UF_CRM_COUNTER_ORDERCOMPL" => 30957,
		);
		//проставить отзыв в таблицу
		$el                 = new CIBlockElement;
		$PROP               = array(
			362 => $rating_main,
			363 => $rating_quality,
			364 => $rating_clean,
			365 => $rating_time,
			366 => $worker_id,
			367 => $order_id,
			368 => $text,
			369 => $id,
		);
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 128,
			"NAME"            => "Отзыв по заказу " . $title,
			"PROPERTY_VALUES" => $PROP,
		);
		$element_id         = $el->Add($arLoadProductArray);
		//в успех, если остальные условия выполнены
		if ($stage_id == 'C7:1' and $closed_by_worker == 30957) {
			$this->stop_deal_process($order_id);
			$fields["STAGE_ID"] = 'C7:WON';
		}
		$entity->update($order_id, $fields);
		//комментарий в карточку
		$this->timeline_comment($order_id, "Заказчиком была оценена работа мастера~BR~Общая оценка: " . $rating_main . "~BR~Оценка качества: " . $this->get_element_name(77, $rating_quality) . "~BR~Оценка чистоты: " . $this->get_element_name(77, $rating_clean) . "~BR~Оценка скорости: " . $this->get_element_name(77, $rating_time) . "~BR~Текст отзыва:~BR~" . $text);
		//расчет рейтинга мастера
		$this->update_contact_rating($worker_id);

		return $this->getSuccess("");
	}

	//Получение списка отзывов и рейтинга
	function getFeedbacks($id, $user_type, $page) {
		$globals       = $this->set_globals();
		$yes           = $globals['yes'];
		$no            = $globals['no'];
		$setup_element = $globals['setup_element'];
		$this->user_type_check($user_type);
		CModule::IncludeModule("crm");
		//установка постраничного участка
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_371");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields       = $ob->GetFields();
			$units_per_page = $arFields["PROPERTY_371_VALUE"];
		};
		if ($page and $page != 0) {
			$min_unit = $page * $units_per_page;
			$max_unit = ($page * $units_per_page) + $units_per_page - 1;
		} else {
			$min_unit = 0;
			$max_unit = $units_per_page - 1;
		}
		//собираем общий рейтинг
		if ($user_type == 0) {
			$this->update_contact_rating($id);
			//чтение рейтинга из контакта
			$rating_main    = null;
			$rating_quality = null;
			$rating_clean   = null;
			$rating_time    = null;
			$arFilter       = array("=ID" => $id);
			$arSelectFields = array("UF_CRM_RATING_MAIN", "UF_CRM_RATING_QUALITY", "UF_CRM_RATING_CLEAN", "UF_CRM_RATING_TIME");
			$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				if ($ob["UF_CRM_RATING_MAIN"]) {
					$rating_main = (float) $ob["UF_CRM_RATING_MAIN"];
				}
				if ($ob["UF_CRM_RATING_QUALITY"]) {
					$rating_quality = (float) $ob["UF_CRM_RATING_QUALITY"];
				}
				if ($ob["UF_CRM_RATING_CLEAN"]) {
					$rating_clean = (float) $ob["UF_CRM_RATING_CLEAN"];
				}
				if ($ob["UF_CRM_RATING_TIME"]) {
					$rating_time = (float) $ob["UF_CRM_RATING_TIME"];
				}
			}
			$ratings = array(
				array('type' => 0, 'value' => $rating_main),
				array('type' => 1, 'value' => $rating_quality),
				array('type' => 2, 'value' => $rating_clean),
				array('type' => 3, 'value' => $rating_time),
			);
		} else {
			$ratings = null;
		}
		//собираем массив отзывов
		$feedbacks = array();
		$unit      = -1;
		if ($user_type == 0) {
			$master_id   = $id;
			$master_name = $this->get_contact_name($master_id);
		}
		$arSelect = array("ID", "NAME", "IBLOCK_ID", "PROPERTY_362", "PROPERTY_363", "PROPERTY_364", "PROPERTY_365", "PROPERTY_366", "PROPERTY_367", "PROPERTY_368");
		$arFilter = array("IBLOCK_ID" => 128);
		if ($user_type == 0) {
			$arFilter["=PROPERTY_366"] = $id;
		} else {
			$arFilter["=PROPERTY_369"] = $id;
		}
		$res = CIBlockElement::GetList(array('ID' => 'desc'), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			//проверка страницы
			$unit = $unit + 1;
			if ($unit < $min_unit) {
				continue;
			} else if ($unit > $max_unit) {
				break;
			}
			//собираем данные по отзыву
			$rating_main    = null;
			$rating_quality = null;
			$rating_clean   = null;
			$rating_time    = null;
			if ($arFields["PROPERTY_362_VALUE"] or $arFields["PROPERTY_362_VALUE"] === '0') {
				$rating_main = (float) $arFields["PROPERTY_362_VALUE"];
			}
			if ($arFields["PROPERTY_363_VALUE"]) {
				if ($arFields["PROPERTY_363_VALUE"] == $yes) {
					$rating_quality = 1;
				} else {
					$rating_quality = 0;
				}
			}
			if ($arFields["PROPERTY_364_VALUE"]) {
				if ($arFields["PROPERTY_364_VALUE"] == $yes) {
					$rating_clean = 1;
				} else {
					$rating_clean = 0;
				}
			}
			if ($arFields["PROPERTY_365_VALUE"]) {
				if ($arFields["PROPERTY_365_VALUE"] == $yes) {
					$rating_time = 1;
				} else {
					$rating_time = 0;
				}
			}
			$id            = $arFields["ID"];
			$ratings_local = array(
				array('type' => 0, 'value' => $rating_main),
				array('type' => 1, 'value' => $rating_quality),
				array('type' => 2, 'value' => $rating_clean),
				array('type' => 3, 'value' => $rating_time),
			);
			$title         = $arFields["NAME"];
			$title         = str_replace('Отзыв по заказу ', '', $title);
			$text          = $arFields["PROPERTY_368_VALUE"];
			if ($user_type != 0) {
				$master_id   = $arFields["PROPERTY_366_VALUE"];
				$master_name = $this->get_contact_name($master_id);
			}
			//собираем массив отзыва
			$feedback    = array(
				'id'          => (int) $id,
				'ratings'     => $ratings_local,
				'title'       => $title,
				'text'        => $text,
				'master_name' => $master_name,
				'master_id'   => (int) $master_id,
			);
			$feedbacks[] = $feedback;
		};
		$response = array(
			'ratings'   => $ratings,
			'feedbacks' => $feedbacks,
		);

		return $this->getSuccess($response);
	}

	//Обновление рейтинга в карточке мастера
	function update_contact_rating($id) {
		$globals       = $this->set_globals();
		$yes           = $globals['yes'];
		$no            = $globals['no'];
		$setup_element = $globals['setup_element'];
		CModule::IncludeModule("crm");
		//чтение настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_370");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields          = $ob->GetFields();
			$elements_main_min = $arFields["PROPERTY_370_VALUE"];
		};
		//расчет рейтинга по таблице
		$rating_main          = false;
		$rating_quality       = false;
		$rating_clean         = false;
		$rating_time          = false;
		$rating_main_total    = 0;
		$rating_quality_total = 0;
		$rating_clean_total   = 0;
		$rating_time_total    = 0;
		$elements_main        = 0;
		$elements_quality     = 0;
		$elements_clean       = 0;
		$elements_time        = 0;
		$arSelect             = array("ID", "IBLOCK_ID", "PROPERTY_362", "PROPERTY_363", "PROPERTY_364", "PROPERTY_365");
		$arFilter             = array("IBLOCK_ID" => 128, "=PROPERTY_366" => $id);
		$res                  = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields          = $ob->GetFields();
			$rating_main_total = $rating_main_total + $arFields["PROPERTY_362_VALUE"];
			$elements_main++;
			if ($arFields["PROPERTY_363_VALUE"]) {
				if ($arFields["PROPERTY_363_VALUE"] == $yes) {
					$rating_quality_total = $rating_quality_total + 5;
				}
				$elements_quality++;
			}
			if ($arFields["PROPERTY_364_VALUE"]) {
				if ($arFields["PROPERTY_364_VALUE"] == $yes) {
					$rating_clean_total = $rating_clean_total + 5;
				}
				$elements_clean++;
			}
			if ($arFields["PROPERTY_365_VALUE"]) {
				if ($arFields["PROPERTY_365_VALUE"] == $yes) {
					$rating_time_total = $rating_time_total + 5;
				}
				$elements_time++;
			}
		};
		if ($elements_main >= $elements_main_min and $elements_main != 0) {
			$rating_main = round($rating_main_total / $elements_main, 2);
			if ($elements_quality != 0) {
				$rating_quality = round($rating_quality_total / $elements_quality, 2);
			}
			if ($elements_clean != 0) {
				$rating_clean = round($rating_clean_total / $elements_clean, 2);
			}
			if ($elements_time != 0) {
				$rating_time = round($rating_time_total / $elements_time, 2);
			}
		}
		//проставить рейтинг в контакт
		$entity = new CCrmContact;
		$fields = array(
			"UF_CRM_RATING_MAIN"    => $rating_main,
			"UF_CRM_RATING_QUALITY" => $rating_quality,
			"UF_CRM_RATING_CLEAN"   => $rating_clean,
			"UF_CRM_RATING_TIME"    => $rating_time,
		);
		$entity->update($id, $fields);
	}

	//Проверка выдачи и просмотр уведомления по шаблону
	function template_notification_issue_check($user_id, $user_type, $template_id, $notification_id, $max_notifications) {
		if ($user_type == 0) {
			$user_code = 31169;
		} else {
			$user_code = 31170;
		}
		$element_id       = false;
		$notification_ids = array();
		$arSelect         = array("ID", "PROPERTY_378");
		$arFilter         = array("IBLOCK_ID" => 131, "=PROPERTY_375" => $user_id, "=PROPERTY_376" => $user_code, "=PROPERTY_377" => $template_id);
		$res              = CIBlockElement::GetList(array('ID' => 'desc'), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields              = $ob->GetFields();
			$element_id            = $arFields["ID"];
			$notification_ids      = $arFields["PROPERTY_378_VALUE"];
			$notifications_counter = count($notification_ids);
		}
		if ( ! ($element_id)) {
			$el                 = new CIBlockElement;
			$PROP               = array(
				375 => $user_id,
				376 => $user_code,
				377 => $template_id,
				378 => array($notification_id),
			);
			$arLoadProductArray = array(
				"IBLOCK_ID"       => 131,
				"NAME"            => "Запись счетчика",
				"PROPERTY_VALUES" => $PROP,
			);
			$element_id         = $el->Add($arLoadProductArray);

			return true;
		} else if (in_array($notification_id, (array) $notification_ids)) {
			return true;
		} else if ($notifications_counter < $max_notifications) {
			$notification_ids[] = $notification_id;
			$PROP               = array(378 => $notification_ids);
			CIBlockElement::SetPropertyValuesEx($element_id, 131, $PROP);

			return true;
		} else {
			return false;
		}
	}

	//Получение списка юзеров запрещения для новых уведомлений по шаблону
	function template_notification_block_list($template_id, $user_type) {
		$globals       = $this->set_globals();
		$setup_element = $globals['setup_element'];
		//чтение настроек
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_230");
		$arFilter = array("IBLOCK_ID" => 78, "=ID" => $setup_element);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields          = $ob->GetFields();
			$max_notifications = $arFields["PROPERTY_230_VALUE"];
		};
		//установка списка запрещения
		if ($user_type == 0) {
			$user_code = 31169;
		} else {
			$user_code = 31170;
		}
		$block_users_list = array();
		$notification_ids = array();
		$arSelect         = array("ID", "PROPERTY_375", "PROPERTY_378");
		$arFilter         = array("IBLOCK_ID" => 131, "=PROPERTY_376" => $user_code, "=PROPERTY_377" => $template_id);
		$res              = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields              = $ob->GetFields();
			$notification_ids      = $arFields["PROPERTY_378_VALUE"];
			$notifications_counter = count($notification_ids);
			if ($notifications_counter >= $max_notifications) {
				$block_users_list[] = $arFields["PROPERTY_375_VALUE"];
			}
		}

		return $block_users_list;
	}

	//Проверка дубликата при создании контакта, объединение
	function contact_add_search_duplicates($id) {
		CModule::IncludeModule("crm");
		//получить телефон контакта
		$phone = $this->get_phone($id, false);
		if (strpos($phone, '7') === 0) {
			$phone = substr($phone, 1);
		}

		//пишем лог
		$el                 = new CIBlockElement;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 89,
			"NAME"            => "тут",
			"PROPERTY_VALUES" => array("ZAPROS" => 'Контакт ' . $id . ' телефон ' . $phone),
		);
		$element_id         = $el->Add($arLoadProductArray);


		if ( ! ($phone) or ! ($id)) {
			return false;
		}
		//найти дубликаты
		$duplicates = array();
		$dbResult   = CCrmFieldMulti::GetList(
			array(),
			array(
				'ENTITY_ID' => 'CONTACT',
				'VALUE'     => "%" . $phone,
			)
		);
		while ($ar = $dbResult->Fetch()) {
			if ($ar['ELEMENT_ID'] != $id) {
				$duplicates[] = $ar['ELEMENT_ID'];
			}
		}
		if ($duplicates) {
			$main_contact = min($duplicates);
			$all_deals    = array();
			//поиск связанных сделок
			$arFilter       = array("=CONTACT_ID" => $id, "CHECK_PERMISSIONS" => 'N');
			$arSelectFields = array("ID");
			$res            = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$deal_id     = $ob['ID'];
				$all_deals[] = $deal_id;
				//перебивка сделок на основной контакт
				$entity = new CCrmDeal;
				$fields = array(
					"CONTACT_ID" => $main_contact,
				);
				$entity->update($deal_id, $fields);
				if (CModule::IncludeModule("bizproc")) {
					$wfId = CBPDocument::StartWorkflow(
						252,
						array("crm", "CCrmDocumentDeal", "DEAL_" . $deal_id),
						array("contact_id" => $main_contact),
						$arErrorsTmp
					);
				}
			}
			//удаление текущего контакта
			$entity = new CCrmContact;
			$entity->delete($id);
			//пишем лог
			$el                 = new CIBlockElement;
			$arLoadProductArray = array(
				"IBLOCK_ID"       => 89,
				"NAME"            => "Автопроверка дубликатов при создании",
				"PROPERTY_VALUES" => array("ZAPROS" => 'Контакт ' . $id . ' удален как дубликат, объединен с контактом ' . $main_contact . ' по телефону ' . $phone . ', перебиты сделки: ' . json_encode($all_deals)),
			);
			$element_id         = $el->Add($arLoadProductArray);
		}

		return true;
	}

	//добавление уведомления в список непросмотренных
	function notification_view_add($notification_id) {
		if ( ! ($notification_id)) {
			return false;
		}
		$globals    = $this->set_globals();
		$portal_url = $globals['portal_url'];
		CModule::IncludeModule("crm");
		$worker_type = 31169;
		$client_type = 31170;
		//$max_notif = 50;
		$max_pack  = 400;
		$user_list = array();
		$arSelect  = array("ID", "IBLOCK_ID", "PROPERTY_205", "PROPERTY_206", "PROPERTY_207");
		$arFilter  = array("IBLOCK_ID" => 90, "=ID" => $notification_id);
		$res       = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			if ($arFields["PROPERTY_205_VALUE"] == 34316) {
				$type = 'common';
			} else if ($arFields["PROPERTY_205_VALUE"] == 34315) {
				$type = 'private';
			} else {
				return;
			}
			if (in_array($worker_type, (array) $arFields["PROPERTY_206_VALUE"]) and in_array($client_type, (array) $arFields["PROPERTY_206_VALUE"])) {
				$user_type = 'common';
			} else if (in_array($worker_type, (array) $arFields["PROPERTY_206_VALUE"])) {
				$user_type = 'worker';
			} else if (in_array($client_type, (array) $arFields["PROPERTY_206_VALUE"])) {
				$user_type = 'client';
			} else {
				return;
			}
			$user_list = $arFields["PROPERTY_207_VALUE"];
		}
		//собираем пользователей
		if ($type == 'common') {
			if ($user_type == 'worker') {
				$arFilter = array("=UF_CRM_NEW_APP_REGISTER" => $worker_type);
			} else if ($user_type == 'client') {
				$arFilter = array("=UF_CRM_NEW_APP_REGISTER" => $client_type);
			} else {
				$arFilter = array("!UF_CRM_NEW_APP_REGISTER" => false);
			}
			$arFilter["CHECK_PERMISSIONS"] = 'N';
			$arSelectFields                = array("ID");
			$res                           = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$user_id     = $ob["ID"];
				$user_list[] = $user_id;
			}
		}
		//демонизация
		$notif_amount = count($user_list);
		if ($notif_amount > $max_pack) {
			$demon = true;
		} else {
			$demon = false;
		}
		if ($demon) {
			//echo count($user_list).'<br>';
			$pack         = array();
			$pack_counter = 0;
			foreach ($user_list as $key => $user_id) {
				$pack_counter++;
				if ($pack_counter > $max_pack) {
					//echo $pack_counter.'<br>';
					//тут действие с паком
					$user_list_json = json_encode($pack);
					//добавление записи в таблице
					$el                 = new CIBlockElement;
					$PROP               = array(391 => $user_list_json);
					$arLoadProductArray = array(
						"IBLOCK_ID"       => 133,
						"NAME"            => "Запуск",
						"PROPERTY_VALUES" => $PROP,
					);
					$element_id         = $el->Add($arLoadProductArray);
					$hook_body          = $portal_url . '/api/other/local_start.php?action=nv_add_demontale&user_type=' . $user_type . '&element_id=' . $element_id . '&notification_id=' . $notification_id;
					//запуск бп
					if (CModule::IncludeModule("bizproc")) {
						$wfId = CBPDocument::StartWorkflow(
							257,
							array("lists", "BizprocDocument", $element_id),
							array("hook_body" => $hook_body),
							$arErrorsTmp
						);
					}
					//обнуление пака
					$pack         = array();
					$pack_counter = 1;
				}
				$pack[] = $user_id;
			}
			$user_list = $pack;
			//echo count($user_list).'<br>';
		}
		//проставляем уведомления
		$this->nv_add_final($user_type, $user_list, $notification_id, $demon);
	}

	//ветка демонизации добавления уведомлений
	function nv_add_demontale($user_type, $element_id, $notification_id) {
		//чтение основной записи
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_391");
		$arFilter = array("IBLOCK_ID" => 133, "=ID" => $element_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields  = $ob->GetFields();
			$user_list = $arFields['~PROPERTY_391_VALUE'];
		};
		CIBlockElement::Delete($element_id);
		$user_list = json_decode($user_list);
		//проставление счетчиков уведомлений
		$this->nv_add_final($user_type, $user_list, $notification_id, true);
	}

	//конечное внесение уведомлений в контакты
	function nv_add_final($user_type, $user_list, $notification_id, $demonized = false) {
		CModule::IncludeModule("crm");
		$max_notif   = 50;
		$worker_type = 31169;
		$client_type = 31170;
		//чтение шаблона уведомления
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_229");
		$arFilter = array("IBLOCK_ID" => 90, "=ID" => $notification_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$template = $arFields["PROPERTY_229_VALUE"];
		}
		//получение списка запрещения
		$block_list = array();
		if ($template) {
			$block_list_worker = $this->template_notification_block_list($template, 0);
			$block_list_client = $this->template_notification_block_list($template, 1);
			if ($user_type == 'worker') {
				$block_list = $block_list_worker;
			} else if ($user_type == 'client') {
				$block_list = $block_list_client;
			} else {
				$block_list = array_values(array_unique(array_merge($block_list_worker, $block_list_client)));
				array_multisort($block_list);
			}
		}
		foreach ($user_list as $key => $user_id) {
			if (in_array($user_id, (array) $block_list)) {
				continue;
			}
			$entity         = new CCrmContact;
			$fields         = array();
			$arFilter       = array("=ID" => $user_id, "CHECK_PERMISSIONS" => 'N');
			$arSelectFields = array("UF_CRM_NEW_NOTIF_WORKER", "UF_CRM_NEW_NOTIF_CLIENT", "UF_CRM_NEW_APP_REGISTER");
			$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$notifications_worker = $ob["UF_CRM_NEW_NOTIF_WORKER"];
				$notifications_client = $ob["UF_CRM_NEW_NOTIF_CLIENT"];
				$register             = $ob["UF_CRM_NEW_APP_REGISTER"];
			}
			if ($user_type == 'worker') {
				$notifications_worker[] = $notification_id;
				if (count($notifications_worker) <= $max_notif) {
					$fields["UF_CRM_NEW_NOTIF_WORKER"] = $notifications_worker;
				}
			} else if ($user_type == 'client') {
				$notifications_client[] = $notification_id;
				if (count($notifications_client) <= $max_notif) {
					$fields["UF_CRM_NEW_NOTIF_CLIENT"] = $notifications_client;
				}
			} else {
				$notifications_worker[] = $notification_id;
				$notifications_client[] = $notification_id;
				if (count($notifications_worker) <= $max_notif and in_array($worker_type, (array) $register)) {
					$fields["UF_CRM_NEW_NOTIF_WORKER"] = $notifications_worker;
				}
				if (count($notifications_client) <= $max_notif and in_array($client_type, (array) $register)) {
					$fields["UF_CRM_NEW_NOTIF_CLIENT"] = $notifications_client;
				}
			}
			if ($fields) {
				$entity->update($user_id, $fields);
			}
		}
		//итоговое логгирование
		if ($demonized) {
			$el                 = new CIBlockElement;
			$PROP               = array(391 => json_encode($user_list));
			$arLoadProductArray = array(
				"IBLOCK_ID"       => 133,
				"NAME"            => "Уведомление " . $notification_id . " тип " . $user_type,
				"PROPERTY_VALUES" => $PROP,
			);
			$element_id         = $el->Add($arLoadProductArray);
			//CIBlockElement::Delete($element_id);
		}
	}

	//исключение уведомления из списка непросмотренных (при деактуализации)
	function notification_view_delete($notification_id) {
		$globals    = $this->set_globals();
		$portal_url = $globals['portal_url'];
		CModule::IncludeModule("crm");
		$max_pack = 1000;
		//собираем контакты
		$user_list      = array();
		$arFilter       = array("=UF_CRM_NEW_NOTIF_WORKER" => $notification_id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("ID");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$user_list[] = $ob["ID"];
		}
		$arFilter       = array("=UF_CRM_NEW_NOTIF_CLIENT" => $notification_id, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("ID");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$user_list[] = $ob["ID"];
		}
		$user_list = array_values(array_unique($user_list));
		array_multisort($user_list);
		//проверка демонизации
		$notif_amount = count($user_list);
		if ($notif_amount > $max_pack) {
			$demon = true;
		} else {
			$demon = false;
		}
		if ($demon) {
			//echo count($user_list).'<br>';
			$pack         = array();
			$pack_counter = 0;
			foreach ($user_list as $key => $user_id) {
				$pack_counter++;
				if ($pack_counter > $max_pack) {
					//echo $pack_counter.'<br>';
					//тут действие с паком
					$user_list_json = json_encode($pack);
					//добавление записи в таблице
					$el                 = new CIBlockElement;
					$PROP               = array(391 => $user_list_json);
					$arLoadProductArray = array(
						"IBLOCK_ID"       => 133,
						"NAME"            => "Запуск (очистка)",
						"PROPERTY_VALUES" => $PROP,
					);
					$element_id         = $el->Add($arLoadProductArray);
					$hook_body          = $portal_url . '/api/other/local_start.php?action=nv_delete_demontale&element_id=' . $element_id . '&notification_id=' . $notification_id;
					//запуск бп
					if (CModule::IncludeModule("bizproc")) {
						$wfId = CBPDocument::StartWorkflow(
							257,
							array("lists", "BizprocDocument", $element_id),
							array("hook_body" => $hook_body),
							$arErrorsTmp
						);
					}
					//обнуление пака
					$pack         = array();
					$pack_counter = 1;
				}
				$pack[] = $user_id;
			}
			$user_list = $pack;
			//echo count($user_list).'<br>';
		}
		//проставляем уведомления
		$this->nv_delete_final($user_list, $notification_id, $demon);
	}

	//ветка демонизации исключения уведомлений
	function nv_delete_demontale($element_id, $notification_id) {
		//чтение основной записи
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_391");
		$arFilter = array("IBLOCK_ID" => 133, "=ID" => $element_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields  = $ob->GetFields();
			$user_list = $arFields['~PROPERTY_391_VALUE'];
		};
		CIBlockElement::Delete($element_id);
		$user_list = json_decode($user_list);
		//проставление счетчиков уведомлений
		$this->nv_delete_final($user_list, $notification_id, true);
	}

	//конечное исключение уведомлений из контактов
	function nv_delete_final($user_list, $notification_id, $demonized = false) {
		CModule::IncludeModule("crm");
		foreach ($user_list as $key => $user_id) {
			$entity         = new CCrmContact;
			$fields         = array();
			$arFilter       = array("=ID" => $user_id, "CHECK_PERMISSIONS" => 'N');
			$arSelectFields = array("UF_CRM_NEW_NOTIF_WORKER", "UF_CRM_NEW_NOTIF_CLIENT");
			$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
			while ($ob = $res->GetNext()) {
				$notifications_worker = $ob["UF_CRM_NEW_NOTIF_WORKER"];
				$notifications_client = $ob["UF_CRM_NEW_NOTIF_CLIENT"];
			}
			if (in_array($notification_id, (array) $notifications_worker)) {
				unset($notifications_worker[ array_search($notification_id, $notifications_worker) ]);
				$fields["UF_CRM_NEW_NOTIF_WORKER"] = $notifications_worker;
			}
			if (in_array($notification_id, (array) $notifications_client)) {
				unset($notifications_client[ array_search($notification_id, $notifications_client) ]);
				$fields["UF_CRM_NEW_NOTIF_CLIENT"] = $notifications_client;
			}
			if ($fields) {
				$entity->update($user_id, $fields);
			}
		}
		//итоговое логгирование
		if ($demonized) {
			$el                 = new CIBlockElement;
			$PROP               = array(391 => json_encode($user_list));
			$arLoadProductArray = array(
				"IBLOCK_ID"       => 133,
				"NAME"            => "ОЧИСТКА Уведомление " . $notification_id,
				"PROPERTY_VALUES" => $PROP,
			);
			$element_id         = $el->Add($arLoadProductArray);
			//CIBlockElement::Delete($element_id);
		}
	}

}


?>