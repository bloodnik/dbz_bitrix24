<?php
//Включение логов ошибок
use Bitrix\Main\ArgumentException;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use lib\feedbacks\CDbzFeedbacks;
use lib\helpers\CApiHelpers;
use lib\helpers\CDadataHelpers;
use lib\helpers\CDbzFilesHelper;
use lib\location\CDbzLocationResponse;
use lib\main\CDbzMain;
use lib\order\CDbzOrder;
use lib\order\CDbzOrders;
use lib\userdata\CUserData;
use lib\work\CDbzWork;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(-1);

//Общие переменные
$portal_url    = 'https://dombezzabot.net';
$setup_element = 31213;
$yes           = 31194;
$no            = 31195;

//Подключение модулей для отработки функций, но чтобы возвращало чистую строку без кода страницы
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

//Подключение класса Utils
include_once('utils.php');
include_once('extra_utils.php');

//Содержит входные параметры
$request = $_POST['request'];
if (is_object(json_decode($request))) {
	$request = json_decode($request, true);  //декодируем, если пришло в json
} else {
	$test_direct_request = true; //тестовый запрос запускаю без кодировки в json
}

if ( ! ($request) and $_GET) {
	$request = $_GET;
}


//отладка запросов
$el                 = new CIBlockElement;
$arLoadProductArray = array(
	"IBLOCK_ID"       => 89,
	"NAME"            => "Входящий запрос",
	"PROPERTY_VALUES" => array("ZAPROS" => json_encode($request), "CONTACT" => $request['id']),
);
$element_id         = $el->Add($arLoadProductArray);

global $USER;

$arNonAuthorizeMethods = [
	"GetMain",
	"GetCategories",
	"GetWorksSuggestions",
	"GetOrders_v2",
	"GetOrder_v2",
	"GetWorkSearchResults",
	"GetOrder",
	"GetCitySearchResults",
	"GetStartContent"
];

//входим за юзера - коннектор приложения (после проверки кода либо токена)
$action_check = $request['action'];
if ($action_check == 'LogInWithPhone') {
	if (code_check($request['data']['phone'], $request['data']['code'])) {
		$USER->Authorize(146420);
	} else {
		exit;
	}
} else if ($action_check == 'AddPercentBonus' or $action_check == 'CheckRespond' or $action_check == 'ChangeStage' or $action_check == 'CitiesCashCount' or $action_check == 'ContactAddDuplicatesSearch') {
	if (local_token_check($request['element_id'], $request['token'])) {
		$USER->Authorize(146420);
	} else {
		exit;
	}
} else if ($action_check == 'SendLogInCode') {
	$no_checks = true;
} else if (($action_check == 'GetOrders' && $request['type'] == 0 and $request['data']['type'] == 0) || in_array($action_check, $arNonAuthorizeMethods, true)) {
	$USER->Authorize(146420);
} else {
	if (token_check($request['id'], $request['token'], $action_check)) {
		$USER->Authorize(146420);
	} else {
		exit;
	}
}

//Вызов фунции, этот код выполняется при обращении к файлу
$main = new Main();
$main->init($request);

//выходим из юзера - очищаем данные сессии
$_SESSION = array();

class Main {

	private $db;
	private $utils;

	public function __construct() {
	}

	function init($request) {
		$utils = new Utils();

		if (isset($request)) {

			//Тип действия
			$action = $request['action'];

			//ID, тип и токен пользователя
			$id      = $request['id'];
			$type    = $request['type'];
			$token   = $request['token'];
			$version = $request['version'] ?? "";

			//массив данных полученного файла
			$file = $_FILES['file'];

			//Содержит входные параметры для конкретной функции
			$data = $request['data'];

			switch ($action) {
				case "SendLogInCode":
					echo($result = $utils->sendLogInCode($data['phone']));
					break;
				case "LogInWithPhone":
					echo($result = $utils->logInWithPhone($type, $data['phone'], $data['code']));
					break;
				case "AddPushToken":
					echo($result = $utils->addPushToken($id, $type, $data['token']));
					break;
				case "SetUserData":
					echo($result = $utils->setUserData($id, $data['name_1'], $data['name_2'], $data['name_3']));
					break;
				case "GetUserData":
					echo($result = $utils->getUserData($id));
					break;
				case "GetUserDataNew":
					$userData   = new CUserData($id, $type);
					$arResponse = $userData->getUserData();

					if (in_array($version, ["0.51a", "0.52a"])) {
						echo(CApiHelpers::getSuccess(["list" => $arResponse]));
					} else {
						echo(CApiHelpers::getSuccess([
							"list" => [
								[
									"items" => $arResponse
								]
							]
						]));
					}
					break;
				case "SetUserDataNew":
					$userData = new CUserData($id, $type);
					try {
						$userData->setUserData($data["list"]);
						echo CApiHelpers::getSuccess("");
					} catch (LoaderException | SystemException | JsonException $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetUserInfo":
					echo($result = $utils->getUserInfo($id, $type));
					break;
				case "GetCityByLocation":
					echo($result = $utils->getCityByLocation($data['lat'], $data['lon']));
					break;
				case "GetCitySearchResults":
					echo($result = $utils->getCitySearchResults($data['text']));
					break;
				case "SetUserCity":
					echo($result = $utils->setUserCity($id, $type, $data['id']));
					break;
				case "GetWorkSearchResults":
					echo($result = $utils->getWorkSearchResults($data['text']));
					break;
				case "SetUserWorks":
					echo($result = $utils->setUserWorks($id, $type, $data['items']));
					break;
				case "GetUserWorks":
					echo($result = $utils->getUserWorks($id, $type));
					break;
				case "UpdateUserWork":
					$extra_utils = new ExtraUtils();
					echo($result = $extra_utils->UpdateUserWork($id, $type, $data['item'], $data['is_active']));
					break;
				case "GetBadgeCounts":
					echo($result = $utils->getBadgeCounts($id, $type, $version));
					break;
				case "GetWorkers":
					echo($result = $utils->getWorkers($id, $type, $data['order_id']));
					break;
				case "GetMedia":
					echo($result = $utils->getMedia($id, $type, $data['order_id']));
					break;
				case "SetUserNotificationChannels":
					echo($result = $utils->setUserNotificationChannels($id, $type, $data['items']));
					break;
				case "GetUserNotificationChannels":
					echo($result = $utils->getUserNotificationChannels($id, $type));
					break;
				case "GetOrders":
					echo($result = $utils->getOrders($id, $type, $data['type'], $request['page'], $data['lat'], $data['lon'], $data['zoom']));
					break;
				case "GetOrders_v2":
					$obOrders = new CDbzOrders($id);
					$obOrders->setLat($data['lat']);
					$obOrders->setLon($data['lon']);
					$obOrders->setUserType($type);
					$obOrders->setZoom($data['zoom']);
					$obOrders->setPage($request['page']);
					$obOrders->setOrdersListType($data['type']);
					try {
						$arOrders = $obOrders->getOrders();
						echo CApiHelpers::getSuccess(["list" => $arOrders]);
					} catch (Exception $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					};
					break;
				case "GetOrder_v2":
					$obOrder = new CDbzOrder($data['id']);
					$obOrder->setUserId($id);
					$obOrder->setUserType($type);
					try {
						$arOrder = $obOrder->getOrder();
						echo CApiHelpers::getSuccess($arOrder);
					} catch (Exception $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					};
					break;
				case "AddOrder":
					$utils->user_name_check($id);
					echo($result = $utils->addOrder($id, $data['work_id'], $data['work_type'], false, $data['summary'], $data['price'], $file, $data['lat'], $data['lon'], false, $data['location']));
					break;
				case "EditOrder":
					try {
						$obOrder = new CDbzOrder();
						$obOrder->setUserId($id);
						$obOrder->setUserType($type);
						$orderId = $obOrder->editOrder($data);
						echo CApiHelpers::getSuccess(["id" => $orderId]);
					} catch (ObjectPropertyException | ArgumentException | LoaderException | SystemException $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetWorkWorkersCount":
					echo($result = $utils->getWorkWorkersCount($id, $data['work_id'], $data['work_type'], $data['lat'], $data['lon']));
					break;
				case "GetOrder":
					echo($result = $utils->getOrderDetails($id, $type, $data['id']));
					break;
				case "AcceptOrder":
					echo($result = $utils->acceptOrder($id, $type, $data['order_id'], $data['package_id'], $data['price']));
					break;
				case "AcceptWorker":
					$utils->timeline_comment($data['order_id'], 'Мастер был выбран заказчиком через новое приложение');
					echo($result = $utils->acceptWorker($id, $type, $data['order_id'], $data['worker_id']));
					break;
				case "CancelOrder":
					echo($result = $utils->cancelOrder($id, $type, $data['order_id'], $data['reason'], false, false, false));
					break;
				case "CompleteOrder":
					echo($result = $utils->completeOrder($id, $type, $data['order_id'], $data['price']));
					break;
				case "ConfirmOrder":
					echo($result = $utils->confirmOrder($id, $type, $data['order_id']));
					break;
				case "RefundOrder":
					echo($result = $utils->refundOrder($id, $type, $data['order_id'], $data['reason']));
					break;
				case "GetNotifications":
					if ($data['type'] and $data['type'] == 1) {
						echo($result = $utils->getUserHistory($id, $type, $request['page']));
					} else {
						echo($result = $utils->getNotifications($id, $type, $request['page']));
					}
					break;
				case "GetMessages":
					include_once('chats/chat_connector.php');
					$chat_connector = new ChatConnector();
					echo($result = $chat_connector->get_messages($id, $type, $data['chat_id']));
					break;
				case "AddMessage":
					include_once('chats/chat_connector.php');
					$chat_connector = new ChatConnector();
					if ($data['chat_id'] == 0) { //чат с поддержкой
						echo($result = $chat_connector->support_message_in($id, $type, $data['text']));
					}
					break;
				case "GetPackages":
					echo($result = $utils->getPackages($id, $type, $data['order_id'], $data['accepted_price']));
					break;
				case "GetDefaultPackage":
					echo($result = $utils->getDefaultPackage($id, $type, $data['order_id'], $data['accepted_price']));
					break;
				case "ConfirmPayment":
					include_once('email_sender.php');
					$email_sender = new EmailSender();
					$email_sender->add_email($id, $data['email']);
					$log_element_id = $utils->payment_log(2, $request, $id);
					echo($result = $utils->confirmPayment($id, $type, $version, $data['token'], $data['amount'], $data['type']));
					break;
				case "BuyPackage":
					echo($result = $utils->buyPackage($id, $type, $data['id']));
					break;
				case "RefundPackage":
					echo($result = $utils->deactivatePackage($id, $type, $data['package_id']));
					break;
				case "GetUserHistory":
					echo($result = $utils->getUserHistory($id, $type, $request['page']));
					break;
				case "CheckForOrdersUpdate":
					echo($result = $utils->checkForOrdersUpdate($id, $type, $data['date'], $data['lat'], $data['lon']));
					break;
				case "GetHints":
					echo($result = $utils->getHints($id, $type, $data['type']));
					break;
				case "SendRefundEmail":
					include_once('email_sender.php');
					$email_sender = new EmailSender();
					$email_sender->add_email($id, $data['email']);
					$email_sender->funds_withdrawal($id);
					echo($utils->getSuccess(""));
					break;
				case "AddPercentBonus":
					echo($result = $utils->add_percent_bonus());
					break;
				case "CheckRespond":
					$utils->responds_check($request['worker_id']);
					break;
				case "ChangeStage":
					$utils->change_stage($request['deal_id'], $request['stage_id']);
					break;
				case "CitiesCashCount":
					$extra_utils = new ExtraUtils();
					$extra_utils->cities_cash_count($request['report_element_id']);
					break;
				case "ContactAddDuplicatesSearch":
					$extra_utils = new ExtraUtils();
					$extra_utils->contact_add_search_duplicates($request['contact_id']);
					break;
				case "AddFeedback_old":
					$extra_utils = new ExtraUtils();
					echo($result = $extra_utils->addFeedback($id, $type, $data['order_id'], $data['ratings'], $data['text']));
					break;
				case "AddFeedback":
					try {
						$obFeedback = new CDbzFeedbacks($id, $type);
						$response   = $obFeedback->addFeedback($data);
						echo CApiHelpers::getSuccess($response);
					} catch (SystemException | LoaderException $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetFeedbacks":
					$extra_utils = new ExtraUtils();
					echo($result = $extra_utils->getFeedbacks($id, $type, $request['page']));
					break;
				case "GetFeedback_v2":
					try {
						$obFeedback = new CDbzFeedbacks($id, $type);
						$obFeedback->setOrderId($data['order_id']);
						$response = $obFeedback->getUserOrderFeedback();
						echo CApiHelpers::getSuccess($response);
					} catch (SystemException | LoaderException | JsonException $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetRatings":
					try {
						$obFeedback = new CDbzFeedbacks($id, $type);
						$response   = $obFeedback->getRatingsFields();
						echo CApiHelpers::getSuccess(["list" => $response]);
					} catch (SystemException | LoaderException | JsonException $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetStreetSuggestions":
					$dadataHelper    = new CDadataHelpers();
					$suggestionsList = $dadataHelper->getAddressSuggestions($data["text"]);
					$response        = CApiHelpers::getSuccess(array("list" => $suggestionsList));
					echo $response;
					break;
				case "Test": //В РАЗРАБОТКЕ
					echo($result = $utils->test($data['id']));
					break;
				case "UploadFile":
					try {
						$fileHelpers = new CDbzFilesHelper($request);
						$fileHelpers->setUserType($type);
						$response = $fileHelpers->uploadFile();
						echo CApiHelpers::getSuccess($response);
					} catch (SystemException $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetCategories":
					try {
						$obLocationResponse = new CDbzLocationResponse($data["location"]);
						$obWorks            = new CDbzWork($id);
						$obWorks->setUserType($type);
						$arCategories = $obWorks->getCategoriesResponse($obLocationResponse);

						echo CApiHelpers::getSuccess(["list" => $arCategories]);
					} catch (Exception $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					} catch (TypeError $e) {
						echo CApiHelpers::getFailure("Ошибка TypeError");
					}
					break;
				case "GetWorksSuggestions":
					try {
						$obLocationResponse = new CDbzLocationResponse($data["location"]);
						$obWorks            = new CDbzWork($id);
						$obWorks->setUserType($type);
						$arWorks = $obWorks->getWorkSuggestions($data["text"], $data["category"], $obLocationResponse);

						echo CApiHelpers::getSuccess(["list" => $arWorks]);
					} catch (Exception $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					} catch (TypeError $e) {
						echo CApiHelpers::getFailure("Ошибка TypeError");
					}
					break;
				case "UpdateUserWorks":
					try {
						$obWorks = new CDbzWork($id);
						$obWorks->updateUserWorks($data["id"], $data["type"], $data["is_selected"]);
						echo CApiHelpers::getSuccess("");
					} catch (Exception $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					} catch (TypeError $e) {
						echo CApiHelpers::getFailure("Ошибка TypeError");
					}
					break;
				case "SetOrderFavorite":
					try {
						$obOrderFavorite = new \lib\order\CDbzOrderFavorites($id, $type);
						$obOrderFavorite->setOrderId($data["order_id"]);
						$obOrderFavorite->setOrderFavorite($data["value"]);
						echo CApiHelpers::getSuccess("");
					} catch (Exception | TypeError $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetMain":
					try {
						$obMain = new CDbzMain($id, $type);
						$obMain->setAppVersion($version);
						echo CApiHelpers::getSuccess($obMain->getMain());
					} catch (Exception | TypeError $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
				case "GetStartContent":
					try {
						$obMain = new CDbzMain($id, $type);
						echo CApiHelpers::getSuccess(["list" => $obMain->getStartContent(new CDbzLocationResponse($data["location"]))]);
					} catch (Exception | TypeError $e) {
						echo CApiHelpers::getFailure($e->getMessage());
					}
					break;
			};

		};

		return;
	}

}

;


//проверка, является ли строка Json
function isJson($string) {
	json_decode($string);

	return (json_last_error() == JSON_ERROR_NONE);
}

//проверка кода авторизации
function code_check($phone, $code) {
	if ( ! ($phone) or ! ($code)) {
		echo(getFailure("Код авторизации неверный, либо истек срок его действия"));

		return false;
	}
	//проверка среди тестовых
	$arSelect = array("ID");
	$arFilter = array("IBLOCK_ID" => 116, "PROPERTY_309" => $phone, "=PROPERTY_310" => $code);
	$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFields   = $ob->GetFields();
		$element_id = $arFields["ID"];
	};
	if ($element_id) {
		return true;
	}
	//проверка основная
	$arSelect = array("ID");
	$arFilter = array("IBLOCK_ID" => 69, "PROPERTY_123" => $phone, "=PROPERTY_124" => $code, "ACTIVE" => "Y");
	$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFields   = $ob->GetFields();
		$element_id = $arFields["ID"];
	};
	if ($element_id) {
		$check = true;
	} else {
		echo(getFailure("Код авторизации неверный, либо истек срок его действия"));
		$check = false;
	}

	return $check;
}

//проверка токена
function token_check($id, $token, $action) {
	if ($id and $token) {
		$arSelect = array("ID");
		$arFilter = array("IBLOCK_ID" => 71, "=PROPERTY_125" => $id, "=PROPERTY_126" => $token, "ACTIVE" => "Y");
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields   = $ob->GetFields();
			$element_id = $arFields["ID"];
		};
	}
	if ($element_id and $id and $token) {
		$check = true;
	} else {
		if ($action == 'GetOrders') {
			$counts   = array('orders_active' => 0, 'orders_closed' => 0, 'notifications' => 0, 'support' => 0, 'profile' => 0, 'my_orders' => 0, 'home' => 0);
			$response = array('list' => array());
			echo(getSuccess($response));
		} else if ($action == 'GetNotifications') {
			$response = array('list' => array());
			echo(getSuccess($response));
		} else if ($action == 'GetUserWorks') {
			$response = array('list' => array());
			echo(getSuccess($response));
		} else {
			echo(getFailure("Авторизуйтесь в приложении"));
		}
		$check = false;
	}

	return $check;
}

//проверка внутреннего токена
function local_token_check($element_id, $token) {
	if ( ! ($element_id) or ! ($token)) {
		return false;
	}
	$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_260");
	$arFilter = array("IBLOCK_ID" => 107, "=ID" => $element_id);
	$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	while ($ob = $res->GetNextElement()) {
		$arFields      = $ob->GetFields();
		$element_token = $arFields["PROPERTY_260_VALUE"];
	};
	if ($token == $element_token) {
		return true;
	} else {
		return false;
	}
}

//кодирование успешного ответа
function getSuccess($response) {
	return json_encode(['success' => true, 'message' => null, 'response' => json_encode($response)]);
}

//кодирование неудачного ответа
function getFailure($message) {
	return json_encode(['success' => false, 'message' => $message, 'response' => "", 'money_amount' => null, 'should_select_city' => null, 'should_enter_name' => null]);
}

//require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>