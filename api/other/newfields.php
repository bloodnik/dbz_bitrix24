<?

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("restapi_test");


exit; //затычка от случайного запуска


$iblock_id = 87;
$field1 = "PROPERTY_186";
$field1_iblock_id = 83;
$field2 = "PROPERTY_297";
$type = 0; //тип перебивки: 0-привязка к таблице, 1-контакт, 2-конвертация дат
$main_field1 = false; //если первое поле - стандартное битрикса


$USER->Authorize(146420);
$field2_code = str_replace('PROPERTY_', '', $field2);
$arSelect = array("ID", "IBLOCK_ID", $field1);
$arFilter = array("IBLOCK_ID"=>$iblock_id, "ID"=>"");
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $element_id = $arFields["ID"];
	if ($main_field1) {
		$field1_value = $arFields[$field1];
	}
	else {
		$field1_value = $arFields[$field1."_VALUE"];
	}
	if ($type == 0) {
		$name = get_element_name($field1_iblock_id, $field1_value);
	}
	else if ($type == 1) {
		$name = get_contact_name($field1_value);
	}
	else if ($type == 2) {
		$name = date_convert($field1_value);
	}
	$PROP = array($field2_code => $name);
	CIBlockElement::SetPropertyValuesEx($element_id, $iblock_id, $PROP);
};
$_SESSION = array();
echo "Работа завершена";


function get_element_name($iblock_id, $element_id) {
	if (!($iblock_id) or !($element_id)) {return '';}
	$name = "";
	$arSelect = array("NAME");
	$arFilter = array("IBLOCK_ID"=>$iblock_id, "=ID"=>$element_id);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$name = $arFields["NAME"];
	};
	return $name;
}

function get_contact_name($id) {
	if (!($id)) {return '';}
	CModule::IncludeModule("crm");
	$arOrder =array();
	$arFilter = array("=ID"=>$id);
	$arGroupBy = false;
	$arNavStartParams = false;
	$arSelectFields = array("NAME", "LAST_NAME", "SECOND_NAME");
	$res = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
	while($ob = $res->GetNext()) {
		$name_1 = $ob["NAME"];
		$name_2 = $ob["LAST_NAME"];
		$name_3 = $ob["SECOND_NAME"];
	}
	$name = $name_1.' '.$name_2;
	return $name;
}

function date_convert($data_time) {
	if (!($data_time)) {return '';}
	$time_point = date_create($data_time);
	$date = date_format($time_point, 'd.m.Y');
	return $date;
}


?>