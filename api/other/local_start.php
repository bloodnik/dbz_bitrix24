<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

include_once($_SERVER["DOCUMENT_ROOT"]."/api/utils.php");
include_once($_SERVER["DOCUMENT_ROOT"]."/api/extra_utils.php");
$utils = new Utils();
$extra_utils = new ExtraUtils();

$request = $_GET;
$action = $request['action'];

if ($action == 'notification_view_add') {
    $USER->Authorize(146420);
	if ($request['push'] == 1) {
		$utils->send_push($request['id']);
	}
	if ($request['view'] == 'stop') {
		$no_view_add = true;
	}
	else {
		$extra_utils->notification_view_add($request['id']);
	}
}


else if ($action == 'nv_add_demontale') {
    $USER->Authorize(146420);
	$extra_utils->nv_add_demontale($request['user_type'], $request['element_id'], $request['notification_id']);
}


else if ($action == 'notification_view_delete') {
    $USER->Authorize(146420);
	$extra_utils->notification_view_delete($request['id']);
}


else if ($action == 'nv_delete_demontale') {
    $USER->Authorize(146420);
	$extra_utils->nv_delete_demontale($request['element_id'], $request['notification_id']);
}



$_SESSION = array();

?>
