<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$test = 1;  //Раскомментить, если только вывести данные, но не записывать в таблицу

if ($test) {
	echo 'ТЕСТОВЫЙ ЗАПУСК - БЕЗ ЗАПИСИ В ТАБЛИЦУ<br><br>';
}
else {
	echo 'БОЕВОЙ ЗАПУСК - ПРОИЗВОДИТСЯ ЗАПИСЬ В ТАБЛИЦУ<br><br>';
}

//читаем csv в массив и json
$file = 'price_upload_test.csv';
$csv= file_get_contents($file);
$array = array_map("str_getcsv", explode("\n", $csv));
$json = json_encode($array);

//собираем массив значений
$values = array();
foreach ($array as $key => $line) {
	$title = $line[0];
	$summary = $line[1];
	if ($title == 'Ключевое слово' or (!($title) and !($summary))) {continue;}
	if ($title) {
		$actual_title = $title;
		$values[$title] = $summary;
	}
	else {
		$values[$actual_title] = $values[$actual_title].'<br>'.$summary;
	}
}

//записываем значения в таблицу
foreach ($values as $value => $price) {
	//формируем текст в формат битрикса
	$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_291");
	$arFilter = array("IBLOCK_ID"=>99, "=ID"=>59924);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$field_content = $arFields["~PROPERTY_291_VALUE"]["TEXT"]; //код поля с ~!!! Это важно, из поля без ~ не возьмется, да, там их 2 разных
	};
	$line_break = substr($field_content, 1, 6); //обрезка кода переноса битрикса, длиной в 6!!! неизвестных символов
	$price = str_replace('<br>', $line_break, $price); //заменяем видимую херню на битриксовский невидимый код переносов
	//поиск соответствий в таблице работ
	$element_id = false;
	$line_title = 'ВНИМАНИЕ! Строка с таким названием не найдена в таблице работ';
	$arSelect = array("ID", "IBLOCK_ID");
	$arFilter = array("IBLOCK_ID"=>66, "=NAME"=>$value);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$element_id = $arFields["ID"];
		$line_title = 'Строка в таблице работ для записи: id '.$element_id;
	};
	//вывод текста
	echo 'В работу '.$value.' записываем данные по прайсу:<br>'.$price.'<br>'.$line_title;
	//запись прайса в таблицу
	if (!($test) and $element_id) {
		$PROP = array(379 => $price);
		CIBlockElement::SetPropertyValuesEx($element_id, 66, $PROP);
		echo '<br>ЗАПИСЬ ПРОИЗВЕДЕНА';
	}
	echo '<br>'.'<br>';

}



?>
