<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("restapi_test2");

$USER->Authorize(146420);

//поиск по таблице
$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_186", "PROPERTY_188", "PROPERTY_189", "PROPERTY_305");
$arFilter = array("IBLOCK_ID"=>87, "=PROPERTY_186"=>array(31243, 31244, 34276), "PROPERTY_359"=>false, ">ID"=>0, "<=ID"=>5000000);
$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $title = $arFields["NAME"];
    $type = $arFields["PROPERTY_186_VALUE"];
    $package_amount = $arFields["PROPERTY_188_VALUE"];
    $balance_element_id = $arFields["PROPERTY_189_VALUE"];
    $info = $arFields["PROPERTY_305_VALUE"];
    if ($info == 'Перенос баланса из старого приложения') {continue;}
    if ($type == 34276 and $title == 'Деактивация неиспользованных заявок пакета') {
        $rate = 1 - 0.5;
    }
    else {
        $rate = 1;
    }
    //узнать стоимость заявки
    $arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_250");
    $arFilter2 = array("IBLOCK_ID"=>80, "=ID"=>$balance_element_id);
    $res2 = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
    while($ob2 = $res2->GetNextElement()) {
        $arFields2 = $ob2->GetFields();
        $package_unit_cost = $arFields2["PROPERTY_250_VALUE"];
    }
    //общий расчет и запись в поле
    $total_price = $package_amount * $package_unit_cost * $rate;
    CIBlockElement::SetPropertyValuesEx($arFields["ID"], 87, array(359 => $total_price));
	echo 'Прошли id '.$arFields["ID"].'<br>';
};



$_SESSION = array();
exit;


?>