<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("city");

exit;

$file = file_get_contents('city_table.json');
$taskList = json_decode($file,TRUE);

foreach ($taskList as $key => $city) {
	$index = $city["Индекс"];
	$region_type = $city["Тип региона"];
	$region = $city["Регион"];
	$name = $city["Город"];
	$center = $city["Признак центра района или региона"];
	$time = $city["Часовой пояс"];
	$latitude = $city["Широта"];
	$longitude = $city["Долгота"];
	$round = $city["Федеральный округ"];
	$population = $city["Население"];

	echo ($name."<br>");

	$el = new CIBlockElement;
	$PROP = array();
	$PROP["INDEX"] = $index;
	$PROP["LATITUDE"] = $latitude;
	$PROP["LONGITUDE"] = $longitude;
	$PROP["REGION_TYPE"] = $region_type;
	$PROP["REGION"] = $region;
	$PROP["CENTER"] = $center;
	$PROP["TIME_ZONE"] = $time;
	$PROP["ROUND"] = $round;
	$PROP["POPULATION"] = $population;
	$arLoadProductArray = array(
		"IBLOCK_ID" => 84,
		"NAME" => $name,
		"PROPERTY_VALUES"=> $PROP,
	);
	$element_id = $el->Add($arLoadProductArray);

}













?>