<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Для тестов");

\Bitrix\Main\Loader::includeModule('iblock');


// Города
$arCities = [];
$resCity  = CIBlockElement::GetList(array('ID' => 'DESC'), ["IBLOCK_ID" => 84, "ACTIVE" => "Y"], false, false, ["ID", "NAME"]);
while ($arFields = $resCity->GetNext()) {
	$arCities[ $arFields["ID"] ] = $arFields["NAME"];
}

// Разделы услуг
$serviceTypes = [];
$resTypes     = CIBlockSection::GetList(array($by => $order), ["IBLOCK_ID" => 134, "ACTIVE" => "Y"], false, ["ID", "NAME"]);
while ($arFields = $resTypes->GetNext()) {
	$serviceTypes[ $arFields["ID"] ] = $arFields["NAME"];
}

// Справочник услуг
$arSubTypes  = [];
$resSubTypes = CIBlockElement::GetList(array('ID' => 'DESC'), ["IBLOCK_ID" => 134, "ACTIVE" => "Y"], false, false, ["ID", "NAME", "IBLOCK_SECTION_ID"]);
while ($arFields = $resSubTypes->GetNext()) {
	$arFields["SECTION_NAME"]      = $serviceTypes[ $arFields["IBLOCK_SECTION_ID"] ];
	$arSubTypes[ $arFields["ID"] ] = ["SECTION" => $arFields["SECTION_NAME"], "NAME" => $arFields["NAME"]];
}


$entity_data_class = getHighloadEntityByName("DbzAvitoSchedule");
$rsData            = $entity_data_class::getList(array(
	"select" => array("*"),
	"filter" => [
		"UF_DATE" => date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time())
	],
));

while ($arData = $rsData->Fetch()) {
	// Объявления
	$arSelect = array("*", "PROPERTY_*");
	$arFilter = array("IBLOCK_ID" => 135, "ACTIVE" => "Y", "PROPERTY_SERVICE_SUBTYPE" => $arData["UF_SUBTYPE"], "PROPERTY_UF_CITY" => $arData["UF_CITY"]);
	$res      = CIBlockElement::GetList(array('ID' => 'DESC'), $arFilter, false, false, $arSelect);

	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$arProps  = $ob->GetProperties();

		if ( ! empty($arData["UF_END_DATE"])) {
			$el   = new CIBlockElement;
			$res2 = $el->Update($arFields["ID"], ["DATE_ACTIVE_TO" => $arData["UF_END_DATE"]->toString()]);
		}
		CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array("AD_STATUS" => $arData["UF_AD_STATUS"]));
		CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array("CONTACT_PHONE" => $arData["UF_PHONE"]));

	};
}

?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>