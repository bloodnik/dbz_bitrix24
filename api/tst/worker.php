<?

use Bitrix\Iblock\Iblock;
use Bitrix\Main\Loader;
use lib\contact\CDbzContact;
use lib\helpers\CDbzConstants;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Для тестов");
CModule::IncludeModule("iblock");


$arFilter       = ["=UF_CRM_NEW_APP_REGISTER" => CDbzContact::NEW_APP_REGISTER_MASTER_ID];
$arSelectFields = ["ID", "NAME", "LAST_NAME", CDbzContact::CATEGORIES_USER_FIELD_CODE, CDbzContact::WORKS_USER_FIELD_CODE];
$res2           = CCrmContact::GetListEx(array(), $arFilter, false, ["nPageSize" => 5, "iNumPage" => 1], $arSelectFields);
$i              = 0;
$arMasters      = [];
while ($ob2 = $res2->GetNext()) {

	$i++;

	$arMasters[] = [
		"ID"         => $ob2["ID"],
		"CATEGORIES" => $ob2[ CDbzContact::CATEGORIES_USER_FIELD_CODE ],
	];
}

foreach ($arMasters as $arMaster) {

	$iblock = Iblock::wakeUp(CDbzConstants::DBZ_SPECIALIZATIONS_IBLOCK_ID)->getEntityDataClass();

	$arSelect = ["ID", "NAME", "SECTION" => "IBLOCK_SECTION_ID"];
	$arFilter = ["OLD_CATEGORY_MAPPING.VALUE" => $arMaster["CATEGORIES"]];
	$elements = $iblock::getList([
		'select' => $arSelect,
		'filter' => $arFilter,
	])->fetchAll();

	$newWorks = [];
	foreach ($elements as $element) {
		$newWorks[] = $element["ID"];
	}

	$obContact = new CDbzContact($arMaster["ID"]);
	try {
		$obContact->updateContactField("UF_CRM_WORK_TYPE_V2", $newWorks);
		PR("Обновлено " . $arMaster["ID"]);
	} catch (\Bitrix\Main\LoaderException $e) {
		PR($e->getMessage());
	}

}


PR($i);
PR($arMasters);


?>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>