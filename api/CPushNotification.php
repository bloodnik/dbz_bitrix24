<?php
/**
 * Класс для отправки пуш уведомления через FirebaseCloudMessaging
 *
 *
 * User: SIRIL
 * Date: 18.01.2021
 * Time: 19:13
 * Project: dombezzabot.net
 */

use Bitrix\Main\Web\HttpClient;

class CPushNotification {

	const SERVER_KEY = "AAAA6GqNGzI:APA91bEalzlNCI1xoY8VVzNqe7YJc30p9Rqq8uqyY3UWmqrQepIi0Cr-HL_ZLGwIG_tv9vNfzf38FhkJ_j2DUOpoEau7oKAFF-i9OtuHKW9InkdOiVrCvZBnbnL8IKRiDRlBuCVI1dtb";
	const FCM_SEND_ENDPOINT = "https://fcm.googleapis.com/fcm/send";


	/**
	 * @var string[] Заголовки запроса
	 */
	private $headers = array();

	/**
	 * @var array Массив токенов устройств для отправки уведолмения
	 */
	private $recipient = array();

	/**
	 * @var string Заголовок уведомления
	 */
	private $title = "";

	/**
	 * @var string Содержимое уведомления
	 */
	private $body = "";

	/**
	 * @var array Доп параметры
	 */
	private $data = array();

	/**
	 * CPushNotification constructor.
	 */
	public function __construct() {
		$this->headers = array(
			'Authorization:key=' . self::SERVER_KEY,
			'Content-Type: application/json'
		);
	}


	public function send() {
		if (empty($this->recipient)) {
			return false;
		}

		$response = [];

		// Разбиваем массив на массивы по 500 токенов. Ограничение FCM
		$registrationIdsChunked = array_chunk($this->recipient, 300);

		foreach ($registrationIdsChunked as $registrationIds) {
			$payload = array(
				'registration_ids' => $registrationIds,
				'priority'         => 'high',
				"notification"     => array(
					"title" => $this->getTitle(),
					"body"  => $this->getBody(),
					"sound" => "default"
				),
				'data'             => $this->data
			);

			$httpClient = new HttpClient();
			$httpClient->setHeader('Authorization', 'key=' . self::SERVER_KEY, true);
			$httpClient->setHeader('Content-Type', 'application/json', true);

			$response = $httpClient->post(self::FCM_SEND_ENDPOINT, json_encode($payload));
		}

		return $response;

	}


	/**
	 * @return array
	 */
	public function getHeaders() {
		return $this->headers;
	}

	/**
	 * @param array $headers
	 */
	public function setHeaders($headers) {
		$this->headers = $headers;
	}

	/**
	 * @return array
	 */
	public function getRecipient() {
		return $this->recipient;
	}

	/**
	 * @param array $recipient
	 */
	public function setRecipient($recipient) {
		if (is_array($recipient)) {
			$this->recipient = $recipient;
		} else {
			$this->recipient = array($recipient);
		}

		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * @param string $body
	 */
	public function setBody($body) {
		$this->body = $body;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * @param array $data
	 */
	public function setData($data) {
		$this->data = $data;

		return $this;
	}


}