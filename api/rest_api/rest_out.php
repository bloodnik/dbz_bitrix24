<?

class RestSender {

    //перемещение заявки в облаке в отказную стадию
    function cancelOrder($deal_id, $stage_id) {
        // формируем URL в переменной $queryUrl
        $queryUrl = 'https://dombezzabot.bitrix24.ru/rest/3672/ly4egqhabm9zdoi8/crm.deal.update';

        // формируем параметры в переменной $queryData
        $queryData = http_build_query(array(
            'id' => $deal_id,
            'fields' => array(
                'STAGE_ID' => $stage_id,
            ),
            'params' => array("REGISTER_SONET_EVENT" => "Y")
        ));

        // обращаемся к Битрикс24 при помощи функции curl_exec
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;

    }

    //проброс в облачную сделку ссылку на сделку в коробке и флага копирования
    function sendLink($deal_id, $box_deal_id) {
        // формируем URL в переменной $queryUrl
        $queryUrl = 'https://dombezzabot.bitrix24.ru/rest/3672/ly4egqhabm9zdoi8/crm.deal.update';

        $box_url = 'https://dombezzabot.net/crm/deal/details/'.$box_deal_id.'/';

        // формируем параметры в переменной $queryData
        $queryData = http_build_query(array(
            'id' => $deal_id,
            'fields' => array(
                'UF_CRM_BOX_LINK' => $box_url,
                'UF_CRM_NEWAPP_ADD' => 'Y',
            ),
            'params' => array("REGISTER_SONET_EVENT" => "Y")
        ));

        // обращаемся к Битрикс24 при помощи функции curl_exec
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;

    }

    //проверка возможности принятия заявки (в облаке - в свободных)
    function checkOrder($deal_id) {
        // формируем URL в переменной $queryUrl
        $queryUrl = 'https://dombezzabot.bitrix24.ru/rest/3672/ly4egqhabm9zdoi8/crm.deal.get';

        // формируем параметры в переменной $queryData
        $queryData = http_build_query(array(
            'id' => $deal_id,
        ));

        // обращаемся к Битрикс24 при помощи функции curl_exec
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $queryUrl,
            CURLOPT_POSTFIELDS => $queryData,
        ));
        $result = curl_exec($curl);
        curl_close($curl);

        $result = json_decode($result, 1);
        $stage_id = $result['result']['STAGE_ID'];

        //проверяем стадию
        $true_stages = array('7', '12');
        if (in_array($stage_id, $true_stages)) {
            return true;
        }
        else {
            return false;
        }
    }



}


?>


