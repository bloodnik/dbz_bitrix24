<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("rest_in");

//получаем входящие параметры
$request = $_POST;
if (is_object(json_decode($request))) {
    $request = json_decode($request, true);  //декодируем, если пришло в json
}
$request_get = $_GET;
$token = $request_get['token'];
if (!($token)) {$token = $request['token'];}

//пишем лог запроса
$el = new CIBlockElement;
$arLoadProductArray = array(
    "IBLOCK_ID" => 89,
    "NAME" => "Входящий вебхук",
    "PROPERTY_VALUES" => array("ZAPROS"=>json_encode($request).json_encode($request_get)),
);
$element_id = $el->Add($arLoadProductArray);


//запуск обработчика при верном токене
if ($token == 'fc1416079a3653dc7a4dc1a99c5bfe') {
    include_once('../utils.php');
    include_once('../extra_utils.php');
    $USER->Authorize(146420);
    $restHandler = new RestHandler();
    $restHandler->init($request, $request_get);
    $_SESSION = array();
}



class RestHandler {

    //разбираем по типам запросов
    function init($request, $request_get) {
        $unit = $request['document_id'][2];
        $action = $request_get['action'];
        if (!($action)) {$action = $request['action'];}
        switch ($action) {
            case "AddOrder":
                $this->addOrder($unit, $request_get);
                break;
            case "CancelOrder":
                $this->cancelOrder($unit, $request_get);
                break;
            case "CheckOrder":
                echo json_encode($this->checkOrder($request));
                break;
            case "AddWorker":
                $this->addWorker($unit, $request_get);
                break;
        }
    }

    //создание заказа
    function addOrder($unit, $request_get) {
        $cloud_deal_id = str_replace('DEAL_', '', $unit);
        $title = $request_get['title'];
        $summary = $request_get['summary'];
        //$city_name = $request_get['city'];
        $price = $request_get['price'];
        $category_decode = json_decode($request_get['category'], 1);
        $category_id_old = $category_decode['sectionId'][0];
        $city_id = $category_decode['city'];
        $client_name = $request_get['client_name'];
        $client_surname = $request_get['client_surname'];
        $client_phone = $request_get['client_phone'];
        $address = $request_get['address'];
        $address_cloud = $request_get['address'];


        //новая передача id
        $intitle_id_pos = strpos($title, "§");
        if ($intitle_id_pos) {
            $cloud_deal_id = substr($title, 0, $intitle_id_pos);
            $intitle_id_pos = $intitle_id_pos + 2;
            $title = substr($title, $intitle_id_pos);
        }


        $utils = new Utils();
        $extra_utils = new ExtraUtils();
        CModule::IncludeModule("crm");
        //проверка на дубль
        if ($cloud_deal_id) {
            $arFilter = array("=UF_CRM_CLOUD_DEAL_ID" => $cloud_deal_id);
            $arSelectFields = array("ID");
            $res = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
            while($ob = $res->GetNext()) {
                if ($ob['ID']) {
                    $this->write_error_log("Заказ является дублем заказа ".$ob['ID'], $cloud_deal_id, "entity: ".$unit." request ".json_encode($request_get));
                    return;
                }
            }
            //проверка перехвата
            $int_element = false;
            $arSelect = array("ID", "IBLOCK_ID", "PROPERTY_288");
            $arFilter = array("IBLOCK_ID"=>113, "=PROPERTY_288"=>$cloud_deal_id);
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $int_element = $arFields["ID"];
            };
            if ($int_element) {
                $this->write_error_log("Заявка была перехвачена системой ПРО", $cloud_deal_id, "entity: ".$unit." request ".json_encode($request_get));
                return;
            }
        }
        //сопоставляем категорию
        $category_id = $this->getCategoryIdNew($category_id_old);
        if (!($category_id)) {
            $this->write_error_log("Не найдена категория", $category_id_old, "entity: ".$unit." request ".json_encode($request_get));
            return;
        }
        //сопоставляем город
        $address_pos = strpos($address, "|");
        if ($address and $address_pos) {
            //пришел гугл-адрес с координатами
            $address_pos = $address_pos + 1;
            $address = substr($address, $address_pos);
            $lon_pos = strpos($address, ";");
            $lat = substr($address, 0, $lon_pos);
            $lon = substr($address, ($lon_pos+1));
            $city = $utils->city_search($lat, $lon);
            $arFilter = array("IBLOCK_ID"=>84, "=ID"=>$city);
            $arSelect = array("ID", "IBLOCK_ID", "NAME");
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $city_name = $arFields["NAME"];
            };
            if (!($city) or !($city_name) or !($lat) or !($lon)) {
                $this->write_error_log("Не найден город", $address, "entity: ".$unit." request ".json_encode($request_get));
                return;
            }
        }
        else {
            //берем по id города в базе сайта
            $city_name = $this->get_city_by_id($city_id);
            $city_name = str_replace('Оспаривается', '', $city_name);
            $arFilter = array("IBLOCK_ID"=>84, "NAME"=>$city_name);
            $arSelect = array("ID", "IBLOCK_ID", "NAME");
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $city = $arFields["ID"];
            };
            if (!($city) or !($city_name)) {
                $this->write_error_log("Не найден город", $city_name, "entity: ".$unit." request ".json_encode($request_get));
                return;
            }
            $coordinates = $utils->get_city_coordinates($city);
            $lat = $coordinates['lat'];
            $lon = $coordinates['lon'];
        }
        //сопоставляем контакт
        $contact = null;
        if (strpos($client_phone, ",") and strlen($client_phone)>12) {
            $phones = explode(", ", $client_phone);
        }
        else {
            $phones = array($client_phone);
        }
        $true_phones = array();
        foreach ($phones as $key => $phone_element) {
            if (strpos($phone_element, '+') !== false) {$phone_substr = substr($phone_element, 2);}
            else {$phone_substr = substr($phone_element, 1);}
            if (strlen($phone_substr)==10) {$phone_format = true;}
            else {$phone_format = false;}
            //ищем телефон по базе
            if ($phone_format == true) {
                $true_phones[] = $phone_element;
                if (!($contact)) {
                    $contacts = $utils->contact_search($phone_substr);
                    $contact = min($contacts);
                }
            }
        }
        if (!($true_phones)) {
            $this->write_error_log("Неверный формат номера заказчика", $client_phone, "entity: ".$unit." request ".json_encode($request_get));
            return;
        }
        if ($contact) {  //нашли контакт по телефону
            $contact_phones = array();
            $contact_phones_substr = array();
            //формируем телефоны в контакте
            $dbResult = CCrmFieldMulti::GetList(
                array(),
                array(
                    'ENTITY_ID' => 'CONTACT',
                    'TYPE_ID' => 'PHONE',
                    'ELEMENT_ID' => $contact,
                )
            );
            while ($ar = $dbResult->Fetch()) {
                if (strpos($ar['VALUE'], '+') !== false) {$value_substr = substr($ar['VALUE'], 2);}
                else {$value_substr = substr($ar['VALUE'], 1);}
                $contact_phones[$ar['ID']] = $ar['VALUE'];
                $contact_phones_substr[$ar['ID']] = $value_substr;
            }
            foreach ($true_phones as $key => $true_phone_element) {
                if (strpos($true_phone_element, '+') !== false) {$true_phone_substr = substr($true_phone_element, 2);}
                else {$true_phone_substr = substr($true_phone_element, 1);}
                if (in_array($true_phone_substr, $contact_phones_substr)) {
                    $phone_id = array_search($true_phone_substr, $contact_phones_substr);
                    $utils->update_phone_type_contact($contact, $phone_id, 'MOBILE');
                }
                else {
                    $utils->add_phone_contact($contact, $true_phone_element, 'MOBILE');
                }
            }
            //обновить имя и город
            $entity = new CCrmContact;
            $fields = array(
                "NAME" => $client_name,
                "LAST_NAME" => $client_surname,
                "UF_CRM_NEW_CITY" => $city,
            );
            $entity->update($contact, $fields);
        }
        else {  //создаем новый
            $entity = new CCrmContact;
            $fields = array(
                'NAME' => $client_name,
                "LAST_NAME" => $client_surname,
                "UF_CRM_NEW_CITY" => $city,
                'TYPE_ID' => "CLIENT",
            );
            $contact = $entity->add($fields);
            foreach ($true_phones as $key => $true_phone_element) {
                $utils->add_phone_contact($contact, $true_phone_element, 'MOBILE');
            }
        }
        $utils->active_date_contact($contact);
        $utils->set_contact_coordinates($contact, $lat, $lon);
        //добавляем заказ
        $title = $this->summary_decode($title);
        $summary = $this->summary_decode($summary);
        $result = $utils->addOrder($contact, $category_id, 1, $title, $summary, $price, false, $lat, $lon, true);
        $result = json_decode($result, 1);
        $response = json_decode($result['response'], 1);
        //обновить заказ
        $entity = new CCrmDeal;
        $fields = array(
            "UF_CRM_MAIN_TYPE" => 40626,
            "UF_CRM_1548513476" => $address_cloud,
            "UF_CRM_DEAL_ADDRESS" => $address_cloud,
            "STAGE_ID" => 'C7:PREPAYMENT_INVOICE',
        );
        if ($cloud_deal_id) {
            $fields["UF_CRM_CLOUD_DEAL_ID"] = $cloud_deal_id;
            $fields["UF_CRM_CLOUD_DEAL_URL"] = 'https://dombezzabot.bitrix24.ru/crm/deal/details/'.$cloud_deal_id.'/';
        }
        $entity->update($response["id"], $fields);
        //подсчет в таблице запросов
        $extra_utils->hand_request_count($response["id"]);
        //----------------пробросить ссылку в сделку в облаке---------------
        include_once('rest_out.php');
        $rest_sender = new RestSender();
        $result = $rest_sender->sendLink($cloud_deal_id, $response["id"]);
        //------------------------------------------------------------------
    }

    //отмена заказа
    function cancelOrder($unit, $request_get) {
        $cloud_deal_id = str_replace('DEAL_', '', $unit);

        $utils = new Utils();
        CModule::IncludeModule("crm");
        //ищем сделку
        if (!($cloud_deal_id)) {
            $this->write_error_log("Не передана сделка для отмены", $cloud_deal_id, "entity: ".$unit." request ".json_encode($request_get));
            return;
        }
        $arFilter = array("=UF_CRM_CLOUD_DEAL_ID" => $cloud_deal_id);
        $arSelectFields = array("ID", "STAGE_ID", "CONTACT_ID");
        $res = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
        while($ob = $res->GetNext()) {
            $deal_id = $ob['ID'];
            $stage_id = $ob['STAGE_ID'];
            $client_id = $ob['CONTACT_ID'];
            $cloud_status = $ob['UR_CRM_CLOUD_STATUS'];
        }
        if (!($deal_id)) {
            $this->write_error_log("Не найдена сделка для отмены", $cloud_deal_id, "entity: ".$unit." request ".json_encode($request_get));
            return;
        }
        $active_stages = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
        $lose_stages = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
        if (!(in_array($stage_id, $active_stages))) {
            if (!($stage_id == 'C7:5')) {
                $this->write_error_log("Сделка ".$deal_id." на неверной стадии ".$stage_id, $cloud_deal_id, "entity: ".$unit." request ".json_encode($request_get));
            }
            return;
        }
        if (in_array($stage_id, $lose_stages)) { //и так уже в отказе - защита от зацикливания
            return;
        }
        //реализация отмены
        $result = $utils->cancelOrder($client_id, 1, $deal_id, "", false, false, true);
        if (!($cloud_status)) {
            $utils->update_field_deal($deal_id, 'UF_CRM_CLOUD_STATUS', 22124);
        }
        $utils->update_field_deal($deal_id, 'UF_CRM_COMPLETE_TIME', date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s'));
    }

    //проверка заказа из СП, принятие в СП
    function checkOrder($request) {
        $cloud_deal_id = $request['id'];
        $utils = new Utils();

        CModule::IncludeModule("crm");
        $true_stages = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
        $arFilter = array("=UF_CRM_CLOUD_DEAL_ID" => $cloud_deal_id);
        $arSelectFields = array("ID", "STAGE_ID", "CONTACT_ID");
        $res = CCrmDeal::GetListEx(array(), $arFilter, false, false, $arSelectFields);
        while($ob = $res->GetNext()) {
            $deal_id = $ob['ID'];
            $stage_id = $ob['STAGE_ID'];
            $client_id = $ob['CONTACT_ID'];
        }
        //проверка стадии заказа
        if (!($deal_id)) { //не нашли
            $is_available = true;
            //запись в таблицу перехвата
            $el = new CIBlockElement;
            $PROP = array();
            $PROP[288] = $cloud_deal_id;
            $arLoadProductArray = array(
                "IBLOCK_ID" => 113,
                "NAME" => "Запрещение на прием",
                "PROPERTY_VALUES"=> $PROP,
            );
            $element_id = $el->Add($arLoadProductArray);
        }
        else if (in_array($stage_id, $true_stages)) { //доступен для принятия
            $is_available = true;
            //реализация отмены
            $result = $utils->cancelOrder($client_id, 1, $deal_id, "", false, false, true);
        }
        else { //недоступен для принятия
            $is_available = false;
        }
        $utils->update_field_deal($deal_id, 'UF_CRM_CLOUD_STATUS', 22125);
        $utils->update_field_deal($deal_id, 'UF_CRM_INWORK_TIME', date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s'));
        $utils->update_field_deal($deal_id, 'UF_CRM_COMPLETE_TIME', date_format(date_create(date('Y-m-d H:i:s', time())), 'd.m.Y H:i:s'));
        $response = array('is_available' => $is_available);
        return $response;
    }

    //создание контакта мастера
    function addWorker($unit, $request_get) {
        $name = $request_get['name'];
        $second_name = $request_get['second_name'];
        $last_name = $request_get['last_name'];
        $worker_phone = $request_get['phone'];
        $city_name = $request_get['city'];

        $utils = new Utils();
        CModule::IncludeModule("crm");


        //ищем город
        $city_name = str_replace('Оспаривается', '', $city_name);
        if ($city_name) {
            $arFilter = array("IBLOCK_ID"=>84, "NAME"=>$city_name);
            $arSelect = array("ID", "IBLOCK_ID", "NAME");
            $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $city = $arFields["ID"];
            }
            $coordinates = $utils->get_city_coordinates($city);
            $lat = $coordinates['lat'];
            $lon = $coordinates['lon'];
        }
        else {
            $city = false;
            $lat = false;
            $lon = false;
        }
        //сопоставляем контакт
        $contact = null;
        if (strpos($worker_phone, ",") and strlen($worker_phone)>12) {
            $phones = explode(", ", $worker_phone);
        }
        else {
            $phones = array($worker_phone);
        }
        $true_phones = array();
        foreach ($phones as $key => $phone_element) {
            if (strpos($phone_element, '+') !== false) {$phone_substr = substr($phone_element, 2);}
            else {$phone_substr = substr($phone_element, 1);}
            if (strlen($phone_substr)==10) {$phone_format = true;}
            else {$phone_format = false;}
            //ищем телефон по базе
            if ($phone_format == true) {
                $true_phones[] = $phone_element;
                if (!($contact)) {
                    $contacts = $utils->contact_search($phone_substr);
                    $contact = min($contacts);
                }
            }
        }
        if (!($true_phones)) {
            $this->write_error_log("Неверный формат номера мастера", $worker_phone, "entity: ".$unit." request ".json_encode($request_get));
            return;
        }
        if ($contact) {  //нашли контакт по телефону
            $contact_phones = array();
            $contact_phones_substr = array();
            //формируем телефоны в контакте
            $dbResult = CCrmFieldMulti::GetList(
                array(),
                array(
                    'ENTITY_ID' => 'CONTACT',
                    'TYPE_ID' => 'PHONE',
                    'ELEMENT_ID' => $contact,
                )
            );
            while ($ar = $dbResult->Fetch()) {
                if (strpos($ar['VALUE'], '+') !== false) {$value_substr = substr($ar['VALUE'], 2);}
                else {$value_substr = substr($ar['VALUE'], 1);}
                $contact_phones[$ar['ID']] = $ar['VALUE'];
                $contact_phones_substr[$ar['ID']] = $value_substr;
            }
            foreach ($true_phones as $key => $true_phone_element) {
                if (strpos($true_phone_element, '+') !== false) {$true_phone_substr = substr($true_phone_element, 2);}
                else {$true_phone_substr = substr($true_phone_element, 1);}
                if (in_array($true_phone_substr, $contact_phones_substr)) {
                    $phone_id = array_search($true_phone_substr, $contact_phones_substr);
                    $utils->update_phone_type_contact($contact, $phone_id, 'MOBILE');
                }
                else {
                    $utils->add_phone_contact($contact, $true_phone_element, 'MOBILE');
                }
            }
            //обновить
            $entity = new CCrmContact;
            $fields = array(
                "NAME" => $name,
                "SECOND_NAME" => $second_name,
                "LAST_NAME" => $last_name,
                "TYPE_ID" => "SUPPLIER",
                "UF_CRM_NEW_CITY" => $city,
            );
            $entity->update($contact, $fields);
        }
        else {  //создаем новый
            $entity = new CCrmContact;
            $fields = array(
                "NAME" => $name,
                "SECOND_NAME" => $second_name,
                "LAST_NAME" => $last_name,
                "TYPE_ID" => "SUPPLIER",
                "UF_CRM_NEW_CITY" => $city,
            );
            $contact = $entity->add($fields);
            foreach ($true_phones as $key => $true_phone_element) {
                $utils->add_phone_contact($contact, $true_phone_element, 'MOBILE');
            }
        }
        $utils->active_date_contact($contact);
        $utils->set_contact_coordinates($contact, $lat, $lon);
    }

    //сопоставление категорий
    function getCategoryIdNew($category_id_old) {
        $categories = array(
            '484' => 154,
            '480' => 153,
            '523' => 153,
            '524' => 181,
            '519' => 177,
            '340' => 160,
            '341' => 155,
            '482' => 176,
            '520' => 175,
            '380' => 179,
            '333' => 157,
            '330' => 157,
            '373' => 157,
            '374' => 157,
            '375' => 157,
            '376' => 157,
            '498' => 180,
            '500' => 180,
            '349' => 187,
            '489' => 211,
            '491' => 211,
            '490' => 211,
            '501' => 211,
            '492' => 211,
            '506' => 211,
            '345' => 163,
            '342' => 211,
            '334' => 216,
            '377' => 216,
            '502' => 216,
            '504' => 216,
            '503' => 216,
            '517' => 216,
            '512' => 216,
            '344' => 206,
            '522' => 191,
            '521' => 163,
            '383' => 217,
            '347' => 155,
            '507' => 155,
            '509' => 155,
            '516' => 155,
            '505' => 155,
            '508' => 155,
            '515' => 155,
            '385' => 155,
            '486' => 190,
            '381' => 158,
            '395' => 218,
            '394' => 219,
            '396' => 160,
            '481' => 197,
        );
        $category_id = $categories[$category_id_old];
        return $category_id;
    }

    //получение города по id
    function get_city_by_id($city_id) {
        return false; //ОТКЛЮЧАЮ - САЙТ ДБЗ.ОНЛАЙН ЛЕГ
        $site_url = "dom-bez-zabot.online";
        $site_root = "/youdo/api/get_data.php";
        $get_params = "type=cities";
        $strQueryText = QueryGetData($site_url, 80, $site_root, $get_params, $error_number, $error_text, "GET");
        $city_list = json_decode($strQueryText, 1);
        foreach ($city_list as $key => $value) {
            if ($value["ID"] == $city_id) {
                $city_name = $value["NAME"];
            }
        }
        if (!($city_name)) {return false;}
        return $city_name;
    }

    //дешифровка текста описания
    function summary_decode($text) {
        $text = str_replace('_', '~BR~', $text); //временно заменяем код нужных переносов на что-то видимое
        $text = json_encode($text);
        $text = str_replace('~BR~', '\r\n', $text); //проставляем переносы в нужных местах
        $text = json_decode($text);
        $text = str_replace('"', '', $text);
        $text = str_replace('&quot;', '', $text);
        return $text;
    }

    //логгирование ошибок
    function write_error_log($title, $request_element, $request_full) {
        $el = new CIBlockElement;
        $arLoadProductArray = array(
            "IBLOCK_ID" => 111,
            "NAME" => $title,
            "PROPERTY_VALUES" => array(278=>$request_element, 279=>$request_full),
        );
        $element_id = $el->Add($arLoadProductArray);
    }

    //отладка запросов
    function debug($text) {
        $el = new CIBlockElement;
        $arLoadProductArray = array(
            "IBLOCK_ID" => 89,
            "NAME" => "Промежуточный лог входящего вебхука",
            "PROPERTY_VALUES" => array("ZAPROS"=>$text),
        );
        $element_id = $el->Add($arLoadProductArray);
    }


}






?>