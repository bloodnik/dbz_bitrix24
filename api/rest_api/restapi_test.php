<?

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("restapi_test");


//получаем входящие параметры
$request = $_POST;
$request_get = $_GET;


//пишем лог запроса
/*
$el = new CIBlockElement;
$arLoadProductArray = array(
    "IBLOCK_ID" => 89,
    "NAME" => "Тестовый входящий запрос",
    "PROPERTY_VALUES" => array("ZAPROS" => json_encode($request) . json_encode($request_get)),
);
$element_id = $el->Add($arLoadProductArray);
*/

if ($request['data']['PARAMS']['FROM_USER_ID'] == 737) {
    $message = $request['data']['PARAMS']['MESSAGE'];
    $index_start = strpos($message, "-");
    $index_finish = strpos($message, "!");
    $index_delta = $index_finish - ($index_start + 1);
    $index = substr($message, ($index_start + 1), $index_delta);
    $text = substr($message, ($index_finish + 2));
    if ($index_start !== false and $index_finish !== false and $index and $text) {
        send($index, $text);
    }
}
else {
    $message = $request['data']['PARAMS']['MESSAGE'];
	$dialog_id = $request['data']['PARAMS']['DIALOG_ID'];
	$index_chat = strpos($dialog_id, "hat");
    $from_user = $request['data']['PARAMS']['FROM_USER_ID'];
    $from_user_name = $request['data']['USER']['NAME'];
    $text = $from_user_name.' ('.$from_user.') пишет мне: '.$message;
    if ($from_user and $message and !($index_chat)) {
        send(737, $text);
    }

}


function send($index, $text) {
    $queryUrl = 'https://businessmechanism.bitrix24.ru/rest/737/q8ec9ju4avrxfzwo/imbot.message.add';

    // формируем параметры в переменной $queryData
    $queryData = http_build_query(array(
        'BOT_ID' => 1923,
        'CLIENT_ID' => 'eujej823ncjnsklnkes24',
        'DIALOG_ID' => $index,
        'MESSAGE' => $text,
    ));

    // обращаемся к Битрикс24 при помощи функции curl_exec
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $queryUrl,
        CURLOPT_POSTFIELDS => $queryData,
    ));
    $result = curl_exec($curl);
    curl_close($curl);

    return $result;
}


?>
