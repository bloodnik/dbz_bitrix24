<?
class ManagersFee {

    //перенос данных по звонкам из csv
    function fee_calculation() {
        CModule::IncludeModule("crm");

        //читаем csv в массив и json
        $file = 'managers_fee_data.csv';
        $csv= file_get_contents($file);
        $array = array_map("str_getcsv", explode("\n", $csv));
        $json = json_encode($array);

        //ограничители
        $min = 0;
        $max = 5000;

        //собираем массив значений
        $arManagers = array();
        foreach ($array as $key => $line) {
            //if ($key < $min or $key > $max) {continue;}
            if ($key == 0) {continue;}
            $date = date_create($line[0]);
            $name = $line[1];
            $phone = $line[5];
            $phone = str_replace(' ', '', $phone);
            $phone = str_replace('+', '', $phone);
            $phone = str_replace('-', '', $phone);
            if (strpos($name, "ургинян") !== false) {continue;}
            if (strpos($name, "ургнян") !== false) {continue;}
            if (strpos($name, "Курги") !== false) {continue;}
            if (strpos($name, "Куринян") !== false) {continue;}
            if (strpos($name, "Зарипов") !== false) {continue;}
            if (strpos($name, "Шаймарданов") !== false) {continue;}
            if (strpos($name, "Кузнецов") !== false) {continue;}
            if (strpos($name, "Никитин") !== false) {continue;}
            if (strpos($name, "Смирнова") !== false) {continue;}
            if (strpos($name, "Панкратова Анаст") !== false) {$name = "Панкратова Анастасия Андреевна";}
            if (strpos($name, "Панкратова  Анаст") !== false) {$name = "Панкратова Анастасия Андреевна";}
            if (strpos($name, "Развозжаева Ольга") !== false) {$name = "Развозжаева Ольга Андреевна";}
            if (!($date) or !($name) or !($phone)) {continue;}
            //echo ' date '.date_format($date, 'd.m.Y H:i:s').' name '.$name.' phone '.$phone.'<br>';
            $arManagers[$name][] = array('phone' => $phone, 'date' => date_format($date, 'd.m.Y H:i:s'));

        }

        $min = 11;
        $max = 100;
        $test = 0;
        $i = -1;

        foreach ($arManagers as $name => $data) {
            $i++;
            if ($i < 2) {continue;}
            if (strpos($name, "Панкратова") !== false) {$manager_id = 136008;}
            if (strpos($name, "Развозжаева") !== false) {$manager_id = 146427;}
            if (strpos($name, "Прокофьева") !== false) {$manager_id = 146442;}
            echo 'manager '.$name.' id '.$manager_id.' data '.json_encode($data).'<br><br>';
            foreach ($data as $key => $local_data) {
                if ($key < $min or $key > $max) {continue;}
                $phone_local = $local_data['phone'];
                $date_local = $local_data['date'];
                if (strpos($phone_local, '+7') === 0) {$phone_local = substr($phone_local, 1);}
                else if (strpos($phone_local, '8') === 0) {$phone_local = '7'.substr($phone_local, 1);}
                $phone_local = substr($phone_local, 1);
                //ищем контакт
                $true_contact = false;
                $unregistered_contact = false;
                $contacts = array();
                $dbResult = CCrmFieldMulti::GetList(
                    array(),
                    array(
                        'ENTITY_ID' => 'CONTACT',
                        'VALUE' => "%".$phone_local,
                    )
                );
                while($ar = $dbResult->Fetch()) {
                    $contacts[] = $ar['ELEMENT_ID'];
                    //чтение контакта
                    $arFilter = array("=ID" => $ar['ELEMENT_ID']);
                    $arSelectFields = array("UF_CRM_NEW_APP_REGISTER");
                    $res = CCrmContact::GetListEx(array(id => asc), $arFilter, false, false, $arSelectFields);
                    while($ob = $res->GetNext()) {
                        if (in_array(31169, $ob["UF_CRM_NEW_APP_REGISTER"])) {
                            $true_contact = $ar['ELEMENT_ID'];
                        }
                        $unregistered_contact = $ar['ELEMENT_ID'];
                    }
                }
                if ($true_contact) {
                    $contact_result = ' true contact '.$true_contact;
                }
                else if ($unregistered_contact) {
                    $contact_result = ' NOT A true contact '.$unregistered_contact;
                    $true_contact = $unregistered_contact;
                }
                else {
                    $contact_result = ' CONTACT NOT FOUND!!!';
                }
                echo 'phone '.$phone_local.' date '.$date_local.$contact_result.'<br>';
                if ($true_contact and !($test)) {
                    $entity = new CCrmContact;
                    $fields = array(
                        "UF_CRM_FIRST_CALL_DATE" => $date_local,
                        "ASSIGNED_BY_ID" => $manager_id,
                        "UF_CRM_MARK" => 'звонок',
                    );
                    $entity->update($true_contact, $fields);
                }
            }

            exit;
        }






    }

    //расчет таблицы вознаграждений менеджеров
    function fee_final_calculation($month, $year) {
        CModule::IncludeModule("crm");
        include_once($_SERVER["DOCUMENT_ROOT"]."/api/utils.php");
        $utils = new Utils();
        //подготовка дат
        $month = $month - 2;
        if ($month == 0) {
            $month = 12;
            $year = $year - 1;
        }
        else if ($month == -1) {
            $month = 11;
            $year = $year - 1;
        }
        $month_codes = array(
            1 => 102,
            2 => 103,
            3 => 104,
            4 => 105,
            5 => 106,
            6 => 107,
            8 => 108,
            9 => 110,
            10 => 99,
            11 => 100,
            12 => 101,
        );
        $month_code = $month_codes[$month];
        $date_start = date_create($year.'-'.$month.'-01 00:00:00');
        $days_in_month = date_format($date_start, 't');
        $date_finish = date_create($year.'-'.$month.'-'.$days_in_month.' 23:59:59');

        echo date_format($date_start, 'd.m.Y H:i:s');
        echo '<br>';
        echo date_format($date_finish, 'd.m.Y H:i:s');
        echo '<br><br>';

        //чтение общих настроек
        $globals = $utils->set_globals();
        $setup_element = $globals['$setup_element'];
        $arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_389");
        $arFilter2 = array("IBLOCK_ID"=>78, "=ID"=>$setup_element);
        $res2 = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
        while($ob2 = $res2->GetNextElement()) {
            $arFields2 = $ob2->GetFields();
            $tax_ratio = $arFields2["PROPERTY_389_VALUE"];
        };

        //чтение группы контактов
        $manager_tax = array();
        $arFilter = array(">=UF_CRM_FIRST_CALL_DATE" => date_format($date_start, 'd.m.Y H:i:s'), "<=UF_CRM_FIRST_CALL_DATE" => date_format($date_finish, 'd.m.Y H:i:s'), "=UF_CRM_NEW_APP_REGISTER" => 31169, "CHECK_PERMISSIONS"=>'N');
        $arSelectFields = array("ID", "UF_CRM_FIRST_CALL_DATE", "ASSIGNED_BY_ID");
        $res = CCrmContact::GetListEx(array('UF_CRM_FIRST_CALL_DATE' => 'asc'), $arFilter, false, false, $arSelectFields);
        while($ob = $res->GetNext()) {
            $contact_id = $ob["ID"];
            $manager_id = $ob["ASSIGNED_BY_ID"];
            $call_date = date_create($ob['UF_CRM_FIRST_CALL_DATE']);
            $post_call_date = date_create(date_format($call_date, 'd.m.Y H:i:s'));
            date_add($post_call_date, date_interval_create_from_date_string("30 days"));

            //считаем пополнения следующие зо дней
            $transactions = array();
            $balance_for_month = 0;
            $arSelect2 = array("ID", "IBLOCK_ID", "PROPERTY_187");
            $arFilter2 = array("IBLOCK_ID"=>87, "=PROPERTY_192"=>$contact_id, "=PROPERTY_186"=>31235, ">=DATE_CREATE"=>date_format($call_date, 'd.m.Y H:i:s'), "<=DATE_CREATE"=>date_format($post_call_date, 'd.m.Y H:i:s'), "!=PROPERTY_305"=>'Перенос баланса из старого приложения');
            $res2 = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
            while($ob2 = $res2->GetNextElement()) {
                $arFields2 = $ob2->GetFields();
                $transactions[] = $arFields2["ID"];
                $balance_for_month = round($balance_for_month + $arFields2["PROPERTY_187_VALUE"], 0);
            }
            if ($balance_for_month) {
                if ($manager_tax[$manager_id]) {
                    $manager_tax[$manager_id] = $manager_tax[$manager_id] + $balance_for_month;
                }
                else {
                    $manager_tax[$manager_id] = $balance_for_month;
                }
            }
            echo 'contact '.$contact_id.' manager '.$manager_id.' transactions '.json_encode($transactions).' amount '.$balance_for_month.'<br>';

        }
        echo '<br>'.json_encode($manager_tax);
        foreach ($manager_tax as $user_id => $balance) {
            $tax_amount = $balance * $tax_ratio * 0.01;
            //добавление записей в таблицу
            $el = new CIBlockElement;
            $PROP = array(
                384 => $user_id,
                386 => $month_code,
                390 => $year,
                387 => $balance,
                388 => round($tax_amount, 0),
            );
            $arLoadProductArray = array(
                "IBLOCK_ID" => 132,
                "NAME" => "Запись процента менеджера",
                "PROPERTY_VALUES"=> $PROP,
            );
            $element_id = $el->Add($arLoadProductArray);
        }

    }

}


?>