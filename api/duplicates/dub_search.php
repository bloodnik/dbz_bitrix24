<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
$APPLICATION->SetTitle("dub_search");

//ЗАТЫЧКА ОТ СЛУЧАЙНОГО ЗАПУСКА
exit;

$USER->Authorize(146420);

CModule::IncludeModule("crm");
$dupl_count = 0;
$arOrder =Array();
$arFilter = Array(
    "=TYPE_ID"=>"SUPPLIER",
	"!=UF_CRM_VIEWED"=>"да",
    ">=ID"=>"337564",
    "<=ID"=>"500000"
);
$arGroupBy = false;
$arNavStartParams = false;
$arSelectFields = Array("ID");
$res = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
while($ob = $res->GetNext()) {
	$total[] = $ob['ID'];

	$dbResult = CCrmFieldMulti::GetList(
		array('ID' => 'asc'),
		array(
			'ENTITY_ID' => 'CONTACT',
			'TYPE_ID' => 'PHONE',
			'ELEMENT_ID' => $ob['ID']
		)
	);
	$fields = $dbResult->Fetch();
	$phone = strval($fields['VALUE']);

	if (strpos($phone, '+') !== false) {
		$phone_substr = substr($phone, 2);
	} else {
		$phone_substr = substr($phone, 1);
	};

	$duplicates = search_duplicates($phone_substr, $ob['ID']);
	if (empty($phone_substr)) {
		$dupl_count = $dupl_count + 1;
		$dupl_all[] = $ob['ID'];
		update_fields_empty($ob['ID']);
	} elseif (count($duplicates)!=0) {
		$dupl_count = $dupl_count + 1;
		$dupl_all[] = $ob['ID'];
		update_fields_dupl($ob['ID'], $duplicates);
	} else {
		update_fields_viewonly($ob['ID']);
	};
	echo ("Для контакта ".$ob['ID']." дубликаты: ".json_encode($duplicates)." искали по номеру ".$phone_substr."<br>");
};
echo ("Всего проверили контактов ".count($total)."<br>");
echo ("Список ID проверенных контактов ".json_encode($total)."<br>");
echo ("Всего нашли дубликатов ".$dupl_count."<br>");
echo ("Список ID контактов с дубликтами ".json_encode($dupl_all)."<br>");

$_SESSION = array();




function update_fields_empty ($id) {
	$entity = new CCrmContact;
	$fields = array( 
		'UF_CRM_DOUBLE' => 'пустой контакт',
		'UF_CRM_VIEWED' => 'да',
	); 
	$entity->update($id, $fields);
}

function update_fields_dupl ($id, $dupl_list) {
	$entity = new CCrmContact;
	$fields = array( 
		'UF_CRM_DOUBLE' => 'да',
		'UF_CRM_DOUBLE_LIST' => $dupl_list,
		'UF_CRM_VIEWED' => 'да',
	); 
	$entity->update($id, $fields);
}

function update_fields_viewonly ($id) {
	$entity = new CCrmContact;
	$fields = array( 
		'UF_CRM_VIEWED' => 'да'
	); 
	$entity->update($id, $fields);
}



function search_duplicates ($phone, $id) {
	$dbResult = CCrmFieldMulti::GetList(
		array(),
		array(
			'ENTITY_ID' => 'CONTACT',
			'VALUE' => "%".$phone,
		)
	);
	while($ar = $dbResult->Fetch()) {
		if ($ar['ELEMENT_ID'] != $id) {
			$total[] = $ar['ELEMENT_ID'];
		}
	}
	return $total;
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>