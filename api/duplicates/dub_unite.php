<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("dub_unite");

//ЗАТЫЧКА ОТ СЛУЧАЙНОГО ЗАПУСКА
exit;


CModule::IncludeModule("crm"); //ищем контакты, помеченные как дубликаты
$arOrder =Array();
$arFilter = Array(
    "!=TYPE_ID"=>"SUPPLIER",
    "UF_CRM_DOUBLE"=>"да",
    "!=UF_CRM_VIEWED"=>"помечен",
    ">=ID"=>"0",
	"<=ID"=>"500000" //здесь можно постепенно увеличивать выборку, чтобы перегонять по частям
);
$arGroupBy = false;
$arNavStartParams = false;
$arSelectFields = Array("ID", "UF_CRM_DOUBLE_LIST");
$res = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
while($ob = $res->GetNext()) {     //читаем каждый найденный контакт
	$id = Array();    //задаем массивы данных для группы контактов - дубликатов
	$name = Array();
	$last_name = Array();
	$second_name = Array();
	$city = Array();
	$action_type = Array();
	$post = Array();
	$about_me = Array();
	$dopusk = Array();
	$propusk = Array();
	$master = Array();
	$sert = Array();
	$pasp_foto = Array();
	$pasp_number = Array();
	$fail_reason = Array();
	$comments = Array();
	$type_id = Array();
	$birthdate = Array();
	$source = Array();
	$source_descr = Array();
	$analitics = Array();
	$request_price = Array();
	$request_descr = Array();
	$request_locate = Array();
	$leads = Array();
	$deals = Array();
	$phones = Array();

	$total[] = $ob['ID'];
	$local_id = $ob['ID'];
	$local_idlist = $ob['UF_CRM_DOUBLE_LIST'];

	$local_idlist[] = $local_id;
	sort($local_idlist);   //теперь имеем полный список ид дубликатов + свой ид

	//проверка слишком большого числа дубликатов
	if (count($local_idlist) >= 25) {
		echo ("<br>Для контакта ".$local_id." обнаружено слишком много дубликатов: ".count($local_idlist).", не объединяем<br>");
		too_many($local_id);
		continue;
	}

	//проверка уже просмотренного контакта
	$mark_check = CCrmContact::GetListEx(array(), array('=ID'=>$local_id), false, false, array("UF_CRM_VIEWED"))->GetNext();
	if ($mark_check['UF_CRM_VIEWED'] == 'помечен') {
		echo ("<br>Контакт ".$local_id." уже помечен, пропускаем<br>");
		continue;
	}

	foreach ($local_idlist as $key => $dupl_id) {   //перебираем группу дубликатов
		$arOrder =Array();
		$arFilter = Array(
			"ID"=>$dupl_id
		);
		$arGroupBy = false;
		$arNavStartParams = false;
		$arSelectFields = Array("ID", "NAME", "LAST_NAME", "SECOND_NAME", "UF_CRM_5C70359B5EB25", "UF_CRM_1551197597", "POST", "UF_CRM_1566455517", "UF_CRM_1566455943", "UF_CRM_1566455903", "UF_CRM_1566455857", "UF_CRM_1566455583", "UF_CRM_1566455780", "UF_CRM_1566455749", "UF_CRM_1566455982", "COMMENTS", "TYPE_ID", "BIRTHDATE", "SOURCE_ID", "SOURCE_DESCRIPTION", "TRACKING_SOURCE_ID", "UF_CRM_5C6953A779BAB", "UF_CRM_5C6953A77FD66", "UF_CRM_5C6953A7862B6");
		$res_in = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		while($ob_in = $res_in->GetNext()) {   //читаем данный дубликат
			$id[] = $dupl_id;
			$name[] = $ob_in['NAME'];
			$last_name[] = $ob_in['LAST_NAME']; //фамилия
			$second_name[] = $ob_in['SECOND_NAME']; //отчество
			$city[] = $ob_in['UF_CRM_5C70359B5EB25']; //город
			$action_type[] = $ob_in['UF_CRM_1551197597']; //виды деятельности
			$post[] = $ob_in['POST']; //должность
			$about_me[] = $ob_in['UF_CRM_1566455517']; //о себе
			$dopusk[] = $ob_in['UF_CRM_1566455943']; //допущен
			$propusk[] = $ob_in['UF_CRM_1566455903']; //промодерирован
			$master[] = $ob_in['UF_CRM_1566455857']; //мастер
			$sert[] = $ob_in['UF_CRM_1566455583']; //сертификаты
			$pasp_foto[] = $ob_in['UF_CRM_1566455780']; //фото паспорта
			$pasp_number[] = $ob_in['UF_CRM_1566455749']; //номер паспорта
			$fail_reason[] = $ob_in['UF_CRM_1566455982']; //причина отказа
			$comments[] = $ob_in['COMMENTS'];
			$type_id[] = $ob_in['TYPE_ID'];
			$birthdate[] = $ob_in['BIRTHDATE'];
			$source[] = $ob_in['SOURCE_ID'];
			$source_descr[] = $ob_in['SOURCE_DESCRIPTION'];
			$analitics[] = $ob_in['TRACKING_SOURCE_ID']; //сквозная аналитика
			$request_price[] = $ob_in['UF_CRM_5C6953A779BAB']; //стоимость заявки
			$request_descr[] = $ob_in['UF_CRM_5C6953A77FD66']; //описание заявки
			$request_locate[] = $ob_in['UF_CRM_5C6953A7862B6']; //адрес заявки
			//собираем лиды
			$leads_array = array();
			$lead_list = CCrmLead::GetListEx(array(), array('=CONTACT_ID'=>$dupl_id), false, false, array("ID"));
			while($lead = $lead_list->GetNext()) {
				$leads_array[] = $lead['ID'];
			}
			$leads[] = $leads_array;
			//собираем сделки
			$deals_array = array();
			$deal_list = CCrmDeal::GetListEx(array(), array('=CONTACT_ID'=>$dupl_id), false, false, array("ID"));
			while($deal = $deal_list->GetNext()) {
				$deals_array[] = $deal['ID'];
			}
			$deals[] = $deals_array;
			//собираем все телефоны
			$phones_array = array();
			$dbResult = CCrmFieldMulti::GetList(array('ID' => 'asc'), array('ENTITY_ID'=>'CONTACT', 'TYPE_ID'=>'PHONE', 'ELEMENT_ID'=>$dupl_id));
			while($number = $dbResult->Fetch()) {
				$phones_array[] = strval($number['VALUE']);
			}
			$phones[] = $phones_array;
		}; //собрали массивы полей группы контактов-дубликатов
	};
	echo ("<br><br>Для контакта ".$local_id." выводим массивы: "."<br>ID дубликатов - ".json_encode($id)."<br>Источник - ".json_encode($source)."<br>Лиды - ".json_encode($leads)."<br>Сделки - ".json_encode($deals)."<br>Телефоны - ".json_encode($phones)."<br>");

	//выбрать приоритетный контакт
	$prior = false;
	$prior_flag = false;
	foreach ($id as $i => $this_id) {
		if (!$prior and !(stripos($name[$i], "ез имени") or stripos($name[$i], "лиент") or stripos($name[$i], "тест") or stripos($name[$i], "тест")===0 or stripos($name[$i], "Тест")===0)) {
			$prior = $this_id;
			$prior_pos = $i;
			$prior_flag = true;
		}
	}
	if (!$prior) {
		$prior = min($id);
		$prior_pos = array_search($prior, $id);
	}
	echo ("Приоритетным выбран контакт в позиции ".$prior_pos." с ID ".$prior." флаг нахождения = ".$prior_flag."<br>");

	//объединить в приоритетный
	foreach ($id as $i => $this_id) {
		if ($this_id == $prior) {
			continue;
		}
		echo ("Трясем ".$this_id."<br>");
		//перебить все поля
		if (!($last_name[$prior_pos]) and ($last_name[$i])) {update_field($prior, 'LAST_NAME', $last_name[$i]); $last_name[$prior_pos]=$last_name[$i];}
		if (!($second_name[$prior_pos]) and ($second_name[$i])) {update_field($prior, 'SECOND_NAME', $second_name[$i]); $second_name[$prior_pos]=$second_name[$i];}
		if (!($city[$prior_pos]) and ($city[$i])) {update_field($prior, 'UF_CRM_5C70359B5EB25', $city[$i]); $city[$prior_pos]=$city[$i];}
		if (!($action_type[$prior_pos]) and ($action_type[$i])) {update_field($prior, 'UF_CRM_1551197597', $action_type[$i]); $action_type[$prior_pos]=$action_type[$i];}
		if (!($post[$prior_pos]) and ($post[$i])) {update_field($prior, 'POST', $post[$i]); $post[$prior_pos]=$post[$i];}
		if (!($about_me[$prior_pos]) and ($about_me[$i])) {update_field($prior, 'UF_CRM_1566455517', $about_me[$i]); $about_me[$prior_pos]=$about_me[$i];}
		if (!($dopusk[$prior_pos]) and ($dopusk[$i])) {update_field($prior, 'UF_CRM_1566455943', $dopusk[$i]); $dopusk[$prior_pos]=$dopusk[$i];}
		if (!($propusk[$prior_pos]) and ($propusk[$i])) {update_field($prior, 'UF_CRM_1566455903', $propusk[$i]); $propusk[$prior_pos]=$propusk[$i];}
		if (!($master[$prior_pos]) and ($master[$i])) {update_field($prior, 'UF_CRM_1566455857', $master[$i]); $master[$prior_pos]=$master[$i];}
		if (!($sert[$prior_pos]) and ($sert[$i])) {update_field($prior, 'UF_CRM_1566455583', $sert[$i]); $sert[$prior_pos]=$sert[$i];}
		if (!($pasp_foto[$prior_pos]) and ($pasp_foto[$i])) {update_field($prior, 'UF_CRM_1566455780', $pasp_foto[$i]); $pasp_foto[$prior_pos]=$pasp_foto[$i];}
		if (!($pasp_number[$prior_pos]) and ($pasp_number[$i])) {update_field($prior, 'UF_CRM_1566455749', $pasp_number[$i]); $pasp_number[$prior_pos]=$pasp_number[$i];}
		if (!($fail_reason[$prior_pos]) and ($fail_reason[$i])) {update_field($prior, 'UF_CRM_1566455982', $fail_reason[$i]); $fail_reason[$prior_pos]=$fail_reason[$i];}
		if (!($comments[$prior_pos]) and ($comments[$i])) {update_field($prior, 'COMMENTS', $comments[$i]); $comments[$prior_pos]=$comments[$i];}
		if (!($type_id[$prior_pos]) and ($type_id[$i])) {update_field($prior, 'TYPE_ID', $type_id[$i]); $type_id[$prior_pos]=$type_id[$i];}
		if (!($birthdate[$prior_pos]) and ($birthdate[$i])) {update_field($prior, 'BIRTHDATE', $birthdate[$i]); $birthdate[$prior_pos]=$birthdate[$i];}
		if (!($source[$prior_pos]) and ($source[$i])) {update_field($prior, 'SOURCE_ID', $source[$i]); $source[$prior_pos]=$source[$i];}
		if (!($source_descr[$prior_pos]) and ($source_descr[$i])) {update_field($prior, 'SOURCE_DESCRIPTION', $source_descr[$i]); $source_descr[$prior_pos]=$source_descr[$i];}
		if (!($analitics[$prior_pos]) and ($analitics[$i])) {update_field($prior, 'TRACKING_SOURCE_ID', $analitics[$i]); $analitics[$prior_pos]=$analitics[$i];}
		if (!($request_price[$prior_pos]) and ($request_price[$i])) {update_field($prior, 'UF_CRM_5C6953A779BAB', $request_price[$i]); $request_price[$prior_pos]=$request_price[$i];}
		if (!($request_descr[$prior_pos]) and ($request_descr[$i])) {update_field($prior, 'UF_CRM_5C6953A77FD66', $request_descr[$i]); $request_descr[$prior_pos]=$request_descr[$i];}
		if (!($request_locate[$prior_pos]) and ($request_locate[$i])) {update_field($prior, 'UF_CRM_5C6953A7862B6', $request_locate[$i]); $request_locate[$prior_pos]=$request_locate[$i];}
		//перебить все лиды и сделки
		if ($leads[$i]) {rebind_leads($prior, $leads[$i]);}
		if ($deals[$i]) {rebind_deals($prior, $deals[$i]);}
		//добавить вторые телефоны
		if ($phones[$i]) {
			foreach ($phones[$i] as $n => $phone) {
				if (strpos($phone, '+') !== false) {
					$phone_substr = substr($phone, 2);
				} else {
					$phone_substr = substr($phone, 1);
				};
				$phone_include = false;
				foreach ($phones[$prior_pos] as $n_prior => $phone_prior) {
					if (stripos($phone_prior, $phone_substr)) {$phone_include = true;}
				}
				if ($phone_substr and ($phone_include == false)) {
					echo("Добавляем в контакт ".$prior." телефон ".$phone."<br>");
					add_phone($prior, $phone);
				}
			}
		}
		update_field($this_id, 'UF_CRM_VIEWED', 'помечен');
		update_field($this_id, 'UF_CRM_MARK', 'удалить');
		update_field($this_id, 'UF_CRM_UNITE_TO', $prior);
	}
	update_field($prior, 'UF_CRM_VIEWED', 'помечен');
	update_field($prior, 'UF_CRM_MARK', 'сохранить');
	if (!($prior_flag)) {
		update_field($prior, 'NAME', 'Заказчик');
	}
	echo ("Объединение завершено<br>");
};


//отдельные функции
function too_many ($id) {
	$entity = new CCrmContact;
	$fields = array( 
		'UF_CRM_VIEWED' => 'слишком много дубликатов',
	); 
	$entity->update($id, $fields);
}

function update_field ($contact_id, $field, $data) {
	$entity = new CCrmContact;
	$fields = array( 
		$field => $data,
	); 
	$entity->update($contact_id, $fields);
}

function rebind_leads ($contact_id, $leads) {
	foreach ($leads as $i => $lead) {
		$entity = new CCrmLead;
		$fields = array( 
			'CONTACT_ID' => $contact_id,
		); 
		$entity->update($lead, $fields);
	}
}

function rebind_deals ($contact_id, $deals) {
	foreach ($deals as $i => $deal) {
		$entity = new CCrmDeal;
		$fields = array( 
			'CONTACT_ID' => $contact_id,
		); 
		$entity->update($deal, $fields);
	}
}

function add_phone ($contact_id, $phone) {
	$entity = new CCrmFieldMulti();
	$fields = array( 
		'ENTITY_ID' => 'CONTACT',
		'ELEMENT_ID' => $contact_id,
		'TYPE_ID' => 'PHONE',
		'VALUE' => $phone,
		'VALUE_TYPE' => 'WORK',
	); 
	$entity->add($fields);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>