<?

class EmailSender {

    //письмо на вывод средств
    function funds_withdrawal ($id) {
        if(CModule::IncludeModule("bizproc")) {
            $wfId = CBPDocument::StartWorkflow(
                216,
                array("crm","CCrmDocumentContact", "CONTACT_".$id),
                array("type" => 1),
                $arErrorsTmp
            );
        }
    }

    //добавить почту в контакт
    function add_email($id, $email) {
        //сносим все старые почты
        $old_multy = array();
        $dbResult = CCrmFieldMulti::GetList(
            array("id" => "desc"),
            array(
                'ENTITY_ID' => 'CONTACT',
                'ELEMENT_ID' => $id,
                'TYPE_ID' => 'EMAIL',
            )
        );
        while($ar = $dbResult->Fetch()) {
            $old_multy[] = $ar['ID'];
        }
        if (count($old_multy) < 10) {
            foreach ($old_multy as $key => $mail_id) {
                $entity = new CCrmFieldMulti;
                $entity->delete($mail_id);
            }
        }
        //добавляем актуальную
        $entity = new CCrmFieldMulti();
        $fields = array(
            'ENTITY_ID' => 'CONTACT',
            'ELEMENT_ID' => $id,
            'TYPE_ID' => 'EMAIL',
            'VALUE' => $email,
            'VALUE_TYPE' => 'WORK',
        );
        $entity->add($fields);
    }

    //получить почту из контакта
    function get_email($contact_id, $return_array) {
        if ($return_array) {
            $email = array();
        }
        else {
            $email = "";
        }
        if (!($contact_id)) {return $email;}
        $dbResult = CCrmFieldMulti::GetList(
            array('ID' => 'asc'),
            array(
                'ENTITY_ID' => 'CONTACT',
                'TYPE_ID' => 'EMAIL',
                '=ELEMENT_ID' => $contact_id
            )
        );
        while ($fields = $dbResult->Fetch()) {
            if ($return_array) {
                $email[] = $fields['VALUE'];
            }
            else {
                $email = $fields['VALUE'];
            }
        }
        return $email;
    }

}

?>