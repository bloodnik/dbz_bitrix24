<?php
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

/**
 * ЮКасса считает, что вебхук обработал успешно, если страница возвращает статус 200
 * Если возвращается иной статус, то хук будет срабатывать в течение суток с разной периодичностью
 *
 * Подробнее тут https://yookassa.ru/developers/using-api/webhooks
 * https://github.com/yoomoney/yookassa-github-docs/blob/master/checkout-api/031-01%20url%20%D0%B4%D0%BB%D1%8F%20%D1%83%D0%B2%D0%B5%D0%B4%D0%BE%D0%BC%D0%BB%D0%B5%D0%BD%D0%B8%D0%B9.md
 *
 */

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// Объекты класса уведомлений в зависимости от события
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Model\PaymentStatus;
use Bitrix\Main\HttpRequest;

// Получаем тело запроса POST JSON
$source      = HttpRequest::getInput();
$requestBody = json_decode($source, true);

$log = date('Y-m-d H:i:s') . ' - payment - ' . print_r($requestBody, true);
file_put_contents(__DIR__ . '/payment_log.txt', $log . PHP_EOL, FILE_APPEND);

try {
	// Создаем экземпляр обеъкта подтвержденного платежа из полученного запроса
	$notification = new NotificationSucceeded($requestBody);
} catch (Exception $e) {
	throw new Exception("Ошибка - " . $e->getMessage());
}

try {
	// Получаем объект платежа
	$payment = $notification->getObject();
} catch (Exception $e) {
	throw new Exception("Ошибка - " . $e->getMessage());
}


if ($payment->getStatus() === PaymentStatus::SUCCEEDED) {

	// Тут должна происходить обработка успешного платежа. Т.е. деньги успешно дошли до счета

	// Метаданные, которые были переданы при создании платежа
	$metadata  = $payment->getMetadata()->toArray();
	$paymentId = $payment->getId();
	$amount    = $payment->getAmount()->getValue();
	$user_id   = $metadata["user_id"];
	$version   = $metadata["version"];

	//Начисляем средства в системе
	$user_type = 0;
	include_once($_SERVER["DOCUMENT_ROOT"]."/api/utils.php");
	$utils = new Utils();
	$log_element_id = $utils->payment_log(1, $requestBody, $user_id, $version);
	$result = $utils->addMoney($user_id, $user_type, $amount, $log_element_id);

}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>