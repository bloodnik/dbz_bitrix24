<?php
/**
 * User: SIRIL
 * Date: 20.01.2021
 * Time: 19:54
 * Project: dombezzabot.net
 */

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
use YooKassa\Client;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Common\Exceptions\BadApiRequestException;
use YooKassa\Common\Exceptions\ForbiddenException;
use YooKassa\Common\Exceptions\InternalServerError;
use YooKassa\Common\Exceptions\NotFoundException;
use YooKassa\Common\Exceptions\ResponseProcessingException;
use YooKassa\Common\Exceptions\TooManyRequestsException;
use YooKassa\Common\Exceptions\UnauthorizedException;

define("SHOP_ID_TEST", '757543');
define("API_KEY_TEST", 'test_DGXWnZ1RGTOUzV5UyojPkjJdfu3OZAnWRlk94ke05K0');

define("SHOP_ID", '780728');
define("API_KEY", 'live_yoxOcVsvB72oMDG75dufoa-u2bzcO2N0-9q2XWgevHE');

// Структура json ответа
$response = [
	"status"  => false,
	"message" => "",
	"result"  => null
];

$request = Application::getInstance()->getContext()->getRequest();

//Тип запроса
$type = $request->get("type");

// Создание платежа.
if (strlen($type) > 0 && $type == "createPayment") {

	$payment_token = $request->get("payment_token");
	$userId        = $request->get("user_id");
	$fullName      = $request->get("full_name");
	$phone         = $request->get("phone");
	$email         = $request->get("email");
	$amount        = (float) $request->get("amount");
	$method        = $request->get("method");
	$version       = $request->get("version");


	if ( ! empty($userId) && ! empty($amount)) {
		$client = new Client();
		$client->setAuth(SHOP_ID, API_KEY);

		$paymentData = array(
			'payment_token' => $payment_token,
			'amount'        => array(
				'value'    => $amount,
				'currency' => 'RUB',
			),
			'capture'       => true,
			'confirmation'  => array(
				'type'       => 'redirect',
				'return_url' => '#',
			),
			"metadata"      => array(
				"user_id" => $userId,
				"version" => $version
			),
			'description'   => $userId,
			"receipt"       => array(
				"customer" => array(
					"full_name" => $fullName,
					"phone"     => $phone,
					"email"     => $email
				),
				"items"    => array(
					array(
						"description"     => "Информационные услуги",
						"quantity"        => "1.00",
						"amount"          => array(
							"value"    => $amount,
							"currency" => "RUB"
						),
						"vat_code"        => "2",
						"payment_mode"    => "full_prepayment",
						"payment_subject" => "payment"
					)
				)
			)
		);


		if (strtoupper($method) == 'SBERBANK') {
			$paymentData["confirmation"] = array("type" => "external");
		} elseif (strtoupper($method) == 'EMBEDDED') {
			$paymentData["confirmation"]      = array("type" => "embedded");
		}

		try {
			$payment = $client->createPayment(
				$paymentData,
				uniqid('', true)
			);

			$response["status"]  = true;
			$response["message"] = "Успешно";
			$response["result"]  = $payment;

		} catch (BadApiRequestException $e) {
			$response["message"] = 'BadApiRequestException - ' . $e->getMessage();
		} catch (ForbiddenException $e) {
			$response["message"] = 'ForbiddenException - ' . $e->getMessage();
		} catch (InternalServerError $e) {
			$response["message"] = 'InternalServerError - ' . $e->getMessage();
		} catch (NotFoundException $e) {
			$response["message"] = 'NotFoundException - ' . $e->getMessage();
		} catch (ResponseProcessingException $e) {
			$response["message"] = 'ResponseProcessingException - ' . $e->getMessage();
		} catch (TooManyRequestsException $e) {
			$response["message"] = 'TooManyRequestsException - ' . $e->getMessage();
		} catch (UnauthorizedException $e) {
			$response["message"] = 'UnauthorizedException - ' . $e->getMessage();
		} catch (ApiException $e) {
			$response["message"] = 'ApiException - ' . $e->getMessage();
		}
	} else {
		$response["message"] = "Некорретные параметры";
	}

	echo json_encode($response);

	return;
}

// Проверка статуса платежа по его идетификатору
if (strlen($type) > 0 && $type == "checkPaymentStatus") {

	// Идентификатор платежа. Создается после создания платежа createPayment
	$paymentId = $request->get("paymentId");

	if ( ! empty($paymentId)) {
		$client = new Client();
		$client->setAuth(SHOP_ID, API_KEY);

		try {
			$result = $client->getPaymentInfo($paymentId);

			$response["status"] = true;
			$response["result"] = $result;

		} catch (BadApiRequestException $e) {
			$response["message"] = 'BadApiRequestException - ' . $e->getMessage();
		} catch (ForbiddenException $e) {
			$response["message"] = 'ForbiddenException - ' . $e->getMessage();
		} catch (InternalServerError $e) {
			$response["message"] = 'InternalServerError - ' . $e->getMessage();
		} catch (NotFoundException $e) {
			$response["message"] = 'NotFoundException - ' . $e->getMessage();
		} catch (ResponseProcessingException $e) {
			$response["message"] = 'ResponseProcessingException - ' . $e->getMessage();
		} catch (TooManyRequestsException $e) {
			$response["message"] = 'TooManyRequestsException - ' . $e->getMessage();
		} catch (UnauthorizedException $e) {
			$response["message"] = 'UnauthorizedException - ' . $e->getMessage();
		} catch (ApiException $e) {
			$response["message"] = 'ApiException - ' . $e->getMessage();
		} catch (ExtensionNotFoundException $e) {
			$response["message"] = 'ExtensionNotFoundException - ' . $e->getMessage();
		}

		echo json_encode($response);

		return;
	}

}

// Подтверждение платежа. Сюда yookassа автоматически перенаправит при успешной оплате (и если что-то пойдет не так)
if (strlen($type) > 0 && $type == "paymentConfirm") {

}

