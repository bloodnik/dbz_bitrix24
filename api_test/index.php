<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
global $APPLICATION, $USER;
$APPLICATION->SetTitle("Тестирование API");

if ( ! $USER->IsAdmin()) {
	ShowError("Доступ запрещен");
	die();
}

\Bitrix\Main\UI\Extension::load("ui.vue");

?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"
	        referrerpolicy="no-referrer"></script>
	<div id="vue-application"></div>
	<script>
		BX.Vue.component('ApiTestForm',
			{
				data() {
					return {
						apiTestUrl: "/api/main_test.php",
						apiMainUrl: "/api/main.php",
						form_request: "",
						from_main: true,
						result: "",
						errorMessage: "",
					}
				},
				computed: {
					prettyJson() {
						return JSON.stringify(JSON.parse(this.result), null, 2);
					}
				},
				methods: {
					async onFormSubmit() {
						const fd = new FormData();
						fd.append("request", this.form_request);

						this.errorMessage = "";
						this.result = "";
						try {
							let {data} = await axios.post(this.from_main ? this.apiMainUrl : this.apiTestUrl, fd, {
								headers: {
									'Content-Type': 'multipart/form-data'
								}
							});

							if (data.success) {
								this.result = data.response;
							} else {
								alert(data.message)
							}
						} catch (e) {
							e instanceof Error;
							this.errorMessage = e
							console.log(e.message);
						}

					}
				},
				template: `
				<div>
					<form name="api_test_form" @submit.prevent="onFormSubmit()">
						<textarea v-model="form_request" rows="10" cols="100"></textarea>
						<input type="checkbox" v-model="from_main"></input>
						<br>
						<button type="submit"> SEND REQUEST </button>
					</form>
					<pre v-if="errorMessage">{{errorMessage}}</pre>
					<pre v-if="result">{{prettyJson}}</pre>
				</div>
				`
			});

		BX.Vue.create({
			el: '#vue-application',
			template: '<api-test-form/>'
		});
	</script>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>