<?php
/**
 * Created by SIRIL.
 * User: SIRIL
 * Date: 05.07.2021
 * Time: 22:53
 * Project: dombezzabot.net
 */


$confirmation_token = $_GET["confirmation_token"];

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Форма оплаты</title>
</head>
<body>
<style>
    .info-text {
        text-align: center;
        font-family: "Factor IO", Helvetica, Arial, sans-serif;
        margin: 10px;
        padding: 10px 10px;
        line-height: 1.5;
        -webkit-box-shadow: 0px 0px 16px -2px rgb(34 60 80 / 20%);
        -moz-box-shadow: 0px 0px 16px -2px rgba(34, 60, 80, 0.2);
        box-shadow: 0px 0px 16px -2px rgb(34 60 80 / 20%);
    }
</style>

<!--Подключение библиотеки-->
<script src="https://yookassa.ru/checkout-widget/v1/checkout-widget.js"></script>

<!--HTML-элемент, в котором будет отображаться платежная форма-->
<div id="payment-form"></div>

<div class="info-text">
	<p>
		Информация о карте передается вами в процессинговый центр в зашифрованном виде по защищенному протоколу HTTPS/SSL. <br>
		Дом без забот не имеет доступа к вводимой информации.
	</p>
	<p>
		Пополняя баланс Вы соглашаетесь с условием <a href="https://dombezzabot.info/legal/contract/" target="_blank">оферты</a>
	</p>
</div>


<script>
	//Инициализация виджета. Все параметры обязательные.
	const checkout = new window.YooMoneyCheckoutWidget({
		confirmation_token: "<?=$confirmation_token?>", //Токен, который перед проведением оплаты нужно получить от ЮKassa
		return_url: 'https://dombezzabot.net/yoopayment/success.php', //Ссылка на страницу завершения оплаты
		error_callback: function (error) {
			//Обработка ошибок инициализации
		}
	});

	//Отображение платежной формы в контейнере
	checkout.render('payment-form')
		//После отображения платежной формы метод render возвращает Promise (можно не использовать).
		.then(() => {
			//Код, который нужно выполнить после отображения платежной формы.
		});
</script>
</body>
</html>
