$(document).ready(function () {
	initUserField();
});

function initUserField() {
	setTimeout(function () {
		const addressSuggestionSearchBtn = $(".addressSuggestionSearchBtn");

		addressSuggestionSearchBtn.on('click', function (event) {
			const fieldName = $(event.target).data('field-name');

			const searchValue = $("input[name="+ fieldName +"]").val();
			const addressDropdown = $(".address-dropdown."+ fieldName);
			const addressDropdownList = $(".address-dropdown."+ fieldName + " > ul");

			$.getJSON("/local/php_interface/lib/api/", {type: "GetStreetSuggestions", address: searchValue}).then(function (data) {
				console.log(data);
				if (data.success) {
					const response = JSON.parse(data.response);

					if (response && response.length > 0) {
						addressDropdownList.find('li').remove();
						response.forEach(function (item) {
							addressDropdownList.append(`<li><a href='javascript:void(0)' data-value="${item.title}" data-lat="${item.lat ? item.lat : ""}" data-lon="${item.lon ? item.lon : ""}" >${item.title}</a></li>`)
						})

						const addressDropdownListItems = $(".address-dropdown > ul > li");
						addressDropdownListItems.on("click", function (event) {
							setSagestionsClickHandlers(event);
						})


						addressDropdown.addClass("show")
					} else {
						addressDropdown.removeClass("show")
					}
				}
			});
		})
	}, 500);
}

function setSagestionsClickHandlers(event) {
	const target = event.target;
	const dropdownInputWrap = $(target).parents(".dropdown-input")
	const addressInput = $(dropdownInputWrap).find("input");

	addressInput.val(`${$(target).data("value")}|${$(target).data("lat")};${$(target).data("lon")}`)
	$(".address-dropdown").removeClass("show");
	console.log("setSageestionsClickHandlers");
}



