<?php

function generateAvitoXml() {
	\Bitrix\Main\Loader::includeModule('iblock');

	global $DB;

	// Города
	$arCities = [];
	$resCity  = CIBlockElement::GetList(array('ID' => 'DESC'), ["IBLOCK_ID" => 84, "ACTIVE" => "Y"], false, false, ["ID", "NAME"]);
	while ($arFields = $resCity->GetNext()) {
		$arCities[ $arFields["ID"] ] = $arFields["NAME"];
	}

	// Разделы услуг
	$serviceTypes = [];
	$resTypes     = CIBlockSection::GetList(array($by => $order), ["IBLOCK_ID" => 134, "ACTIVE" => "Y"], false, ["ID", "NAME"]);
	while ($arFields = $resTypes->GetNext()) {
		$serviceTypes[ $arFields["ID"] ] = $arFields["NAME"];
	}

	// Справочник услуг
	$arSubTypes  = [];
	$resSubTypes = CIBlockElement::GetList(array('ID' => 'DESC'), ["IBLOCK_ID" => 134, "ACTIVE" => "Y"], false, false, ["ID", "NAME", "IBLOCK_SECTION_ID"]);
	while ($arFields = $resSubTypes->GetNext()) {
		$arFields["SECTION_NAME"]      = $serviceTypes[ $arFields["IBLOCK_SECTION_ID"] ];
		$arSubTypes[ $arFields["ID"] ] = ["SECTION" => $arFields["SECTION_NAME"], "NAME" => $arFields["NAME"]];
	}

	//Справочник платных статусов
	$entity_data_class = getHighloadEntityByName("Dbzavitoadstatus");
	$rsData            = $entity_data_class::getList(array(
		"select" => array("*"),
	));
	$arAdStatusList    = [];
	while ($arData = $rsData->Fetch()) {
		$arAdStatusList[ $arData["ID"] ] = $arData["UF_NAME"];
	}

	// ====== Групповое обновлене объявлений по расписанию ====== //
	$entity_data_class = getHighloadEntityByName("DbzAvitoSchedule");
	$rsData            = $entity_data_class::getList(array(
		"select" => array("*"),
		"filter" => [
			"UF_DATE" => date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), time())
		],
	));

	while ($arData = $rsData->Fetch()) {
		// Объявления
		$arSelect = array("*", "PROPERTY_*");
		$arFilter = array("IBLOCK_ID" => 135, "ACTIVE" => "Y", "PROPERTY_SERVICE_SUBTYPE" => $arData["UF_SUBTYPE"], "PROPERTY_ADDRESS" => $arData["UF_CITY"]);
		$res      = CIBlockElement::GetList(array('ID' => 'DESC'), $arFilter, false, false, $arSelect);

		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$arProps  = $ob->GetProperties();

			if ( ! empty($arData["UF_END_DATE"])) {
				$el   = new CIBlockElement;
				$res2 = $el->Update($arFields["ID"], ["DATE_ACTIVE_TO" => $arData["UF_END_DATE"]->toString()]);
			}
			CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array("AD_STATUS" => $arData["UF_AD_STATUS"]));
			CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array("CONTACT_PHONE" => $arData["UF_PHONE"]));
			CIBlockElement::SetPropertyValuesEx($arFields["ID"], false, array("PRICE" => $arData["UF_PRICE"]));

		};
	}

	// ====== Формируем XML ====== //
	// Объявления
	$arSelect = array("*", "PROPERTY_*");
	$arFilter = array("IBLOCK_ID" => 135, "ACTIVE" => "Y");
	$res      = CIBlockElement::GetList(array('ID' => 'DESC'), $arFilter, false, false, $arSelect);
	$document = new FluentDOM\DOM\Document();
	$feed     = $document->appendElement('Ads', ["formatVersion" => "3", "target" => "Avito.ru"]);

	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$arProps  = $ob->GetProperties();

		$entry = $feed->appendElement('Ad');
		$entry->appendElement('Id', $arFields["ID"]);
		$entry->appendElement('AvitoId', $arProps["AVITO_ID"]["VALUE"]);
		$entry->appendElement('DateEnd', ConvertDateTime($arFields["DATE_ACTIVE_TO"], "YYYY-MM-DD", "ru"));
		$entry->appendElement('Address', $arCities[ $arProps["ADDRESS"]["VALUE"] ]);
		$entry->appendElement('Category', 'Предложение услуг');

		if ($arProps["SERVICE_SUBTYPE"]["VALUE"]) {
			$entry->appendElement('ServiceType', $arSubTypes[ $arProps["SERVICE_SUBTYPE"]["VALUE"] ]["CATEGORY"]);
			$entry->appendElement('ServiceSubtype', $arSubTypes[ $arProps["SERVICE_SUBTYPE"]["VALUE"] ]["NAME"]);
		}
		if ($arProps["AD_STATUS"]["VALUE"]) {
			$entry->appendElement('AdStatus', $arAdStatusList[ $arProps["AD_STATUS"]["VALUE"] ]);
		}

		$entry->appendElement('Title', $arProps["AVITO_TITLE"]["VALUE"]);
		$entry->appendElement('Description', $arFields["PREVIEW_TEXT"]);

	};
	$document->formatOutput = true;
	echo $document->save('/home/bitrix/www/avitoAdsDbz.xml');



	//	Формируем xml для Blizko Только для уфы
	// ====== Формируем XML ====== //
	// Объявления
	$arSelect = array("*", "PROPERTY_*");
	$arFilter = array("IBLOCK_ID" => 135, "ACTIVE" => "Y", "PROPERTY_ADDRESS" => 31314);
	$res      = CIBlockElement::GetList(array('ID' => 'DESC'), $arFilter, false, false, $arSelect);
	$document = new FluentDOM\DOM\Document();

	$feed = $document->appendElement('yml_catalog', ["date" => ConvertDateTime(date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()), 'YYYY-MM-DD HH:MI:SS')]);
	$shop = $feed->appendElement('shop');
	$shop->appendElement("name", "Дом Без Забот");
	$shop->appendElement("company", "ИП Чистаков В. Ю.");
	$shop->appendElement("url", "https://dombezzabot.info/");
	$currencies = $shop->appendElement("currencies");
	$currencies->appendElement("currency", ["id" => "RUR", "rate" => "1"]);

	$categories = $shop->appendElement("categories");
	foreach ($serviceTypes as $key => $serviceType) {
		$categories->appendElement("category", $serviceType, ["id" => strval($key)]);
	}
	foreach ($arSubTypes as $key => $subType) {
		if ($subType["SECTION"]) {
			$categories->appendElement("category", $subType["NAME"], ["id" => strval($key), "parentId" => strval($subType["SECTION"])]);
		} else {
			$categories->appendElement("category", $subType["NAME"], ["id" => strval($key)]);
		}
	}

	$offers = $shop->appendElement("offers");
	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$arProps  = $ob->GetProperties();


		$offer = $offers->appendElement("offer", ["id" => strval($arFields["ID"])]);
		$offer->appendElement("name", $arProps["AVITO_TITLE"]["VALUE"]);
		$offer->appendElement("url", "https://dombezzabot.info");
		$offer->appendElement("price", $arProps["PRICE"]["VALUE"]);
		$offer->appendElement("currencyId", "RUR");
		$offer->appendElement("categoryId", strval($arProps["SERVICE_SUBTYPE"]["VALUE"]));
		$offer->appendElement("delivery", "false");

	};
	$document->formatOutput = true;
	echo $document->save('/home/bitrix/www/blizkoAdsDbz.xml');

	return 'generateAvitoXml();';

}