<?

/**
 * @param $hlbl - ID нашего highloadblock блока к которому будет делать запросы.
 */
function getHighloadEntity($hlbl) {

	CModule::IncludeModule('highloadblock');

	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($hlbl)->fetch();

	$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);

	return $entity->getDataClass();
}

function getHighloadEntityByName($hlbName): \Bitrix\Main\ORM\Data\DataManager {

	CModule::IncludeModule('highloadblock');

	$result = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter' => array('=NAME' => $hlbName)));
	if ($row = $result->fetch()) {
		$HLBLOCK_ID = $row["ID"];
	}

	$hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HLBLOCK_ID)->fetch();
	$entity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlblock);
	return $entity->getDataClass();
}