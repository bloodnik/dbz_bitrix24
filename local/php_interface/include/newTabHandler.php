<?php
/**
 * User: SIRIL
 * Date: 08.02.2021
 * Time: 16:52
 * Project: dombezzabot.net
 */


//обработчик событий для чатов ОЛ
//добавление сообщения
\Bitrix\Main\EventManager::getInstance()->addEventHandler('im', 'OnBeforeChatMessageAdd', 'ReceivedMessage');
function ReceivedMessage($arFields = false, $arRes = false) {
    //$eventParams = $event->getParameters();
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/api/chats/event_handler.php")) {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/api/chats/event_handler.php");
    }
    $event_handler = new EventHandler();
    $event_handler->add_message($arFields, $arRes);
    return;
}

//смена статуса чата
\Bitrix\Main\EventManager::getInstance()->addEventHandler('imopenlines', 'OnSessionFinish', 'ChangeStatus');
function ChangeStatus(\Bitrix\Main\Event $event) {
    $eventParams = $event->getParameters();
    if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/api/chats/event_handler.php")) {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/api/chats/event_handler.php");
    }
    $event_handler = new EventHandler();
    $event_handler->change_status($eventParams);
    return;
}
