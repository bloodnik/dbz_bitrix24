<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 22.08.2021
 * Time: 12:51
 * Project: dombezzabot.net
 */

namespace lib\location;

use lib\helpers\CDbzResponse;

class CDbzLocationResponse extends CDbzResponse {

	protected int $id = 0;
	protected string $title = "";
	protected float $lat = 0;
	protected float $lon = 0;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * @return float|int
	 */
	public function getLat(): float|int {
		return $this->lat;
	}

	/**
	 * @param float|int $lat
	 */
	public function setLat(float|int $lat): void {
		$this->lat = $lat;
	}

	/**
	 * @return float|int
	 */
	public function getLon(): float|int {
		return $this->lon;
	}

	/**
	 * @param float|int $lon
	 */
	public function setLon(float|int $lon): void {
		$this->lon = $lon;
	}

}