<?php


namespace lib\work;

use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\IblockTable;
use Bitrix\Iblock\ORM\ElementEntity;
use Bitrix\Iblock\SectionTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\ORM\Query\Query;
use Bitrix\Main\SystemException;
use CCrmContact;
use CFile;
use lib\contact\CDbzContact;
use lib\helpers\CDbzBase;
use lib\helpers\CDbzConstants;
use lib\location\CDbzLocationResponse;
use Mihanentalpo\FastFuzzySearch\FastFuzzySearch;

/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 21.08.2021
 * Time: 14:51
 * Project: dombezzabot.net
 */
class CDbzWork extends CDbzBase {


	/**
	 * Список всех категорий для приложения
	 *
	 * @param CDbzLocationResponse $location
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getCategoriesResponse(CDbzLocationResponse $location): array {
		$arAllSections = self::getAllCategories();

		$arSections = [];
		foreach ($arAllSections as $arSection) {
			$obCategoryResponse = new CDbzCategoryResponse();
			$obCategoryResponse->setId($arSection["ID"]);
			$obCategoryResponse->setTitle($arSection["title"]);
			$obCategoryResponse->setCover($arSection["cover"] ? CDbzConstants::DBZ_PORTAL_URL . CFile::GetPath($arSection["cover"]) : null);

			if ($this->getUserType() === CDbzConstants::DBZ_CLIENT_TYPE) {
				$obCategoryResponse->setWorkersCount(self::getCategoryWorkersCountByCity($location->getId(), $arSection["ID"]));
			}

			if ($this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE) {
				$obCategoryResponse->setSelectedCount(self::getSelectedWorksCount($this->getUserId(), $arSection["ID"]));
				$obCategoryResponse->setIsSelected(self::isUserSubscribeToCategory($this->getUserId(), $arSection["ID"]));
			}

			$arSections[] = $obCategoryResponse->toArray();
		}

		// Сортируем по подпискам пользователя
		usort($arSections, static fn($a, $b) => $b['selected_count'] <=> $a['selected_count']);

		return [
			[
				"id"         => 1,
				"categories" => $arSections
			]
		];
	}

	/**
	 * Поиск работ
	 *
	 * @param string|null $searchText
	 * @param int|null $categoryId
	 * @param CDbzLocationResponse $location
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getWorkSuggestions(string|null $searchText, int|null $categoryId, CDbzLocationResponse $location): array {
		$arWorks = self::getAllWorks($this->getUserType(), $categoryId);


		// Фильтруем работы по категории, если она указана
		if ($categoryId) {
			$arWorks = array_filter($arWorks, static fn($category) => (int) $category["SECTION"] === $categoryId);
		}

		// Нечеткий поиск по работам
		// Выбираем похожие по близости названия категорий, если указна поисковая строка
		$arSearchedWorks = [];
		if ($searchText !== '') {
			$arWorkNames = array_map(static fn($work) => $work["NAME"], $arWorks);

			$ffz            = new FastFuzzySearch($arWorkNames);
			$arSimilarWords = $ffz->find($searchText, 15);
			foreach ($arSimilarWords as $arSimilarWork) {
				$foundWork         = array_filter($arWorks, static fn($work) => $work["NAME"] === $arSimilarWork["word"]);
				$work              = current($foundWork);
				$work["WEIGHT"]    = $arSimilarWork["percent"];
				$arSearchedWorks[] = $work;
			}
		}

		// Сортируем работы по "весу" соответсвия поисковой строке
		usort($arSearchedWorks, static fn($a, $b) => $b['WEIGHT'] <=> $a['WEIGHT']);


		if (empty($arSearchedWorks)) {
			$arSearchedWorks = $categoryId ? $arWorks : array_slice($arWorks, 0, 20);
		}

		$arResponse = [];
		foreach ($arSearchedWorks as $arSearchedWork) {
			["ID" => $id, "NAME" => $name, "PRICE" => $price, "UNIT" => $unit] = $arSearchedWork;

			$arWorksResponse = new CDbzWorkResponse();
			$arWorksResponse->setId($id);
			$arWorksResponse->setTitle($name);

			$priceString = $price ? "от " . (int) $price . " ₽/$unit" : null;

			$arWorksResponse->setPrice($priceString);

			if ($this->getUserType() === CDbzConstants::DBZ_CLIENT_TYPE) {
				$arWorksResponse->setWorkersCount(self::getWorkWorkersCountByCity($location->getId(), $arSearchedWork["ID"]));
			}

			if ($this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE) {
				$arWorksResponse->setIsSelected(self::isUserSubscribe($this->getUserId(), $arSearchedWork["ID"]));
			}
			$arResponse[] = $arWorksResponse->toArray();
		}

		// Сортируем по подписке пользователя, если не передана поисковая строка
		if ( ! $searchText) {
			usort($arResponse, static fn($a, $b) => $b['is_selected'] <=> $a['is_selected']);
		}

		return $arResponse;
	}


	/**
	 * Список всех категорий
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getAllCategories(): array {
		Loader::includeModule('iblock');

		$rsSection = SectionTable::getList(array(
			'order'  => array('LEFT_MARGIN' => 'ASC'),
			'filter' => array(
				'IBLOCK_ID'     => CDbzConstants::DBZ_WORKS_IBLOCK_ID,
				'DEPTH_LEVEL'   => 1,
				'ACTIVE'        => 'Y',
				'GLOBAL_ACTIVE' => 'Y',
			),
			'select' => array(
				'id',
				'title' => 'NAME',
				"cover" => 'PICTURE'
			),
		));

		$arSections = [];
		while ($arSection = $rsSection->fetch()) {
			$arSections[] = $arSection;
		}

		return $arSections;
	}


	/**
	 * Список всех работ
	 *
	 * @param int $userType
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getAllWorks(int $userType = CDbzConstants::DBZ_WORKER_TYPE, ?int $categoryId = null): array {
		Loader::includeModule('iblock');

		$shouldWorkHashtagsResult = $userType === CDbzConstants::DBZ_CLIENT_TYPE;
		$worksIblockEntity        = self::prepareWorkIblockEntity($userType);

		$arSelect = ["ID", "NAME", "IBLOCK_SECTION_ID"];
		if ($shouldWorkHashtagsResult) {
			$arSelect = [
				...$arSelect,
				"WORK_TAGS_PROP_" => "WORK_TAGS_PROP",
				"TAG_NAME"        => "WORK_TAGS_PROP.WORK_TAGS.NAME",
				"TAG_PRICE_"      => "WORK_TAGS_PROP.WORK_TAGS.PRICE",
				"TAG_UNIT_"       => "WORK_TAGS_PROP.WORK_TAGS.UNIT"
			];
		}

		$arFilter = [];
		if ($categoryId) {
			$arFilter["=IBLOCK_SECTION_ID"] = $categoryId;
		}

		$qRes = (new Query($worksIblockEntity))->setSelect($arSelect)->setFilter($arFilter)->exec();

		$arWorks = [];
		while ($arrWork = $qRes->fetch()) {
			$arWorks[] = [
				"ID"      => $arrWork["ID"],
				"NAME"    => $shouldWorkHashtagsResult ? $arrWork["TAG_NAME"] : $arrWork["NAME"],
				"SECTION" => $arrWork["IBLOCK_SECTION_ID"],
				"PRICE"   => $arrWork["TAG_PRICE_VALUE"],
				"UNIT"    => $arrWork["TAG_UNIT_VALUE"]
			];
		}

		usort($arWorks, static fn($a, $b) => $b['ID'] <=> $a['ID']);

		return $arWorks;
	}


	/**
	 * Подготовка сущности для получения списка работ.
	 * Если для закачика, то джойним еще таблицу с хэштегами работ через его свойство WORK
	 *
	 * @param int $userType
	 *
	 * @return \Bitrix\Iblock\ORM\ElementEntity
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	private static function prepareWorkIblockEntity(int $userType = CDbzConstants::DBZ_WORKER_TYPE): ElementEntity {
		$workElementEntity = IblockTable::compileEntity("works");

		if ($userType === CDbzConstants::DBZ_CLIENT_TYPE) {
			$tagsElementEntity = IblockTable::compileEntity("specializationTags");
			$tagWorkField      = $tagsElementEntity->getField("WORK");
			$tagPropEntity     = $tagWorkField->getRefEntity();

			// Джойнимся к табклице со свойствами инфоблока "Теги специализаций"
			$workElementEntity->addField((
			new Reference("WORK_TAGS_PROP",
				$tagPropEntity,
				Join::on('this.ID', 'ref.VALUE')
			))->configureJoinType(Join::TYPE_LEFT));


			// Джойнимся таблицей свойств к инфоблоку  "Теги специализаций"
			$tagPropEntity->addField((
			new Reference("WORK_TAGS",
				$tagsElementEntity,
				Join::on('this.IBLOCK_ELEMENT_ID', 'ref.ID')
			))->configureJoinType(Join::TYPE_LEFT));
		}

		return $workElementEntity;
	}


	/**
	 * Возвтращет категорию работ по по ID
	 *
	 * @param $categoryId
	 * @param int|null $userId
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getWorkCategoryById($categoryId, int|null $userId = null): array {
		Loader::includeModule('iblock');

		$sections = SectionTable::getList(array(
			'filter' => array(
				'IBLOCK_ID' => CDbzConstants::DBZ_WORKS_IBLOCK_ID,
				'=ID'       => $categoryId,
			),
			'select' => array('ID', 'NAME', "DEPTH_LEVEL"),
		))->fetchAll();

		$obCategoryResponse = new CDbzCategoryResponse();
		foreach ($sections as $section) {
			$obCategoryResponse->setId($section["ID"]);
			$obCategoryResponse->setTitle($section["NAME"]);
		}
		$obCategoryResponse->setSelectedCount($userId ? self::getSelectedWorksCount($userId, $categoryId) : null);
		$obCategoryResponse->setIsSelected($userId && self::isUserSubscribeToCategory($userId, $categoryId));


		return $obCategoryResponse->toArray();
	}


	/**
	 * Возвращает работу по идентификатору
	 *
	 * @param $workId
	 * @param int|null $userId
	 *
	 * @return array
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function getWorkById($workId, int|null $userId = null): array {
		Loader::includeModule('iblock');
		$iblock = Iblock::wakeUp(CDbzConstants::DBZ_WORKS_IBLOCK_ID)->getEntityDataClass();

		$elements = $iblock::getList([
			'select' => ["ID", "NAME", "IBLOCK_SECTION_ID"],
			'filter' => ["=ID" => $workId]
		])->fetchAll();

		$obWorkResponse = new CDbzWorkResponse();
		foreach ($elements as $element) {
			$obWorkResponse->setId($element["ID"]);
			$obWorkResponse->setTitle($element["NAME"]);
			$obWorkResponse->setCategoryId($element["IBLOCK_SECTION_ID"]);
		}

		$obWorkResponse->setIsSelected($userId && self::isUserSubscribe($userId, $workId));

		return $obWorkResponse->toArray();
	}


	/**
	 * Возвращает работы по ID категории
	 *
	 * @param int $categoryId
	 *
	 * @return array
	 * @throws ArgumentException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	public static function getWorksByCategoryId(int $categoryId): array {
		Loader::includeModule('iblock');
		$iblock = Iblock::wakeUp(CDbzConstants::DBZ_WORKS_IBLOCK_ID)->getEntityDataClass();

		$elements = $iblock::getList([
			'select' => ["ID", "NAME"],
			'filter' => ["IBLOCK_SECTION_ID" => $categoryId]
		])->fetchAll();

		$arResult = [];
		foreach ($elements as $element) {
			$obWorkResponse = new CDbzWorkResponse();
			$obWorkResponse->setId($element["ID"]);
			$obWorkResponse->setTitle($element["NAME"]);
			$arResult[] = $obWorkResponse->toArray();
		}

		return $arResult;
	}


	/**
	 * Количество мастеров, подписки на работы которых, входят категорию по указанному городу
	 *
	 * @param int $cityId
	 * @param int $categoryId
	 *
	 * @return int
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	private static function getCategoryWorkersCountByCity(int $cityId, int $categoryId): int {
		$allWorks = self::getAllWorks();

		// Фильтруем работы по категории
		$arFilteredWorks = array_filter($allWorks, static fn($work) => (int) $work["SECTION"] === $categoryId);

		// Генерируем массив с ID работ
		$arCategoryWorksId = array_map(static fn($work) => $work["ID"], $arFilteredWorks);

		Loader::includeModule("crm");
		$arOrder                                        = array("id" => "asc");
		$arSelectFields                                 = ["ID"];
		$arFilter[ CDbzContact::WORKS_USER_FIELD_CODE ] = $arCategoryWorksId;
		$arFilter['UF_CRM_NEW_CITY']                    = $cityId;
		$arFilter['UF_CRM_NEW_APP_REGISTER']            = CDbzContact::NEW_APP_REGISTER_MASTER_ID;
		$obContact                                      = CCrmContact::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);

		return $obContact->AffectedRowsCount();
	}


	/**
	 * Количество работ в выбранной категории у мастера
	 *
	 * @param int $userId
	 * @param int $categoryId
	 *
	 * @return int
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	private static function getSelectedWorksCount(int $userId, int $categoryId): int {
		$allWorks = self::getAllWorks();

		// Фильтруем работы по категории
		$arFilteredWorks = array_filter($allWorks, static fn($work) => (int) $work["SECTION"] === $categoryId);

		// Генерируем массив с ID работ
		$arCategoryWorksId = array_map(static fn($work) => $work["ID"], $arFilteredWorks);

		Loader::includeModule("crm");
		$arOrder                                        = array("id" => "asc");
		$arSelectFields                                 = ["ID", CDbzContact::WORKS_USER_FIELD_CODE];
		$arFilter[ CDbzContact::WORKS_USER_FIELD_CODE ] = $arCategoryWorksId;
		$arFilter['ID']                                 = $userId;
		$arFilter['UF_CRM_NEW_APP_REGISTER']            = CDbzContact::NEW_APP_REGISTER_MASTER_ID;
		$obContact                                      = CCrmContact::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);

		$selectedCount = 0;
		if ($arContact = $obContact->Fetch()) {
			$selectedCount = count(array_intersect((array) $arContact[ CDbzContact::WORKS_USER_FIELD_CODE ], $arCategoryWorksId));
		}

		return $selectedCount;
	}


	/**
	 * Количество мастеров которые подписаны на работу по указанному городу
	 *
	 * @param $cityId
	 * @param $workId
	 *
	 * @return int
	 * @throws LoaderException
	 */
	private static function getWorkWorkersCountByCity($cityId, $workId): int {
		Loader::includeModule("crm");
		$arOrder                                        = array("id" => "asc");
		$arSelectFields                                 = ["ID"];
		$arFilter[ CDbzContact::WORKS_USER_FIELD_CODE ] = $workId;
		$arFilter['UF_CRM_NEW_CITY']                    = $cityId;
		$arFilter['UF_CRM_NEW_APP_REGISTER']            = CDbzContact::NEW_APP_REGISTER_MASTER_ID;
		$obContact                                      = CCrmContact::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);

		return $obContact->AffectedRowsCount();
	}


	/**
	 * Подписан ли мастер на работу
	 *
	 * @throws LoaderException
	 */
	private static function isUserSubscribe(int $userId, $workId): bool {
		Loader::includeModule("crm");
		$arOrder                                        = array("id" => "asc");
		$arSelectFields                                 = ["ID"];
		$arFilter[ CDbzContact::WORKS_USER_FIELD_CODE ] = $workId;
		$arFilter['ID']                                 = $userId;
		$arFilter['UF_CRM_NEW_APP_REGISTER']            = CDbzContact::NEW_APP_REGISTER_MASTER_ID;
		$obContact                                      = CCrmContact::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);

		return (bool) $obContact->AffectedRowsCount();
	}


	/**
	 * Подписан ли мастер на категорию
	 *
	 * @throws LoaderException
	 */
	private static function isUserSubscribeToCategory(int $userId, $categoryId): bool {
		Loader::includeModule("crm");
		$arOrder                                             = array("id" => "asc");
		$arSelectFields                                      = ["ID"];
		$arFilter[ CDbzContact::CATEGORIES_USER_FIELD_CODE ] = $categoryId;
		$arFilter['ID']                                      = $userId;
		$arFilter['UF_CRM_NEW_APP_REGISTER']                 = CDbzContact::NEW_APP_REGISTER_MASTER_ID;
		$obContact                                           = CCrmContact::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);

		return (bool) $obContact->AffectedRowsCount();
	}


	/**
	 * Подписка или удаление работ
	 *
	 * Если $isSelected == false && $elementType == null, то удаляем все работы в категории вообще
	 *
	 * @param int|null $elementId - id категории или работы.
	 * @param int|null $elementType - 0 - категория, 1 - работа. Если категория, то подписать или отписаться на все работы в этой категории
	 * @param bool $isSelected - true если надо подписать подписать
	 *
	 * @throws \Exception
	 */
	public function updateUserWorks(int|null $elementId, int|null $elementType, bool $isSelected): void {
		$obContact                = new CDbzContact($this->getUserId());
		$arUserWorksAndCategories = CDbzContact::getContactWorkCategories($this->getUserId());
		$arCurrentUserWorks       = (array) $arUserWorksAndCategories["UNIT_WORKS"];
		$arCurrentUserCategories  = (array) $arUserWorksAndCategories["WORK_CATEGORIES"];

		// Отписываем от всех работ в категории
		if ( ! $isSelected && is_null($elementType)) {
			$obContact->updateContactField(CDbzContact::CATEGORIES_USER_FIELD_CODE, []);
			$obContact->updateContactField(CDbzContact::WORKS_USER_FIELD_CODE, []);
		}

		// Подписываем/отписываем мастера на/от все работы категории
		if ($elementId && $elementType === CDbzConstants::DBZ_WORK_CATEGORY) {
			$arWorks    = self::getWorksByCategoryId($elementId);
			$arWorksIds = array_map(static fn($work): int => $work["id"], $arWorks);

			switch ($isSelected) {
				case true:
					$newUserWorks = array_unique(array_merge($arWorksIds, $arCurrentUserWorks));
					$obContact->updateContactField(CDbzContact::WORKS_USER_FIELD_CODE, $newUserWorks);
					$arCurrentUserCategories[] = $elementId;
					$newUserCategories         = array_unique($arCurrentUserCategories);
					$obContact->updateContactField(CDbzContact::CATEGORIES_USER_FIELD_CODE, $newUserCategories);
					break;
				case false:
					$newUserWorks = array_diff($arCurrentUserWorks, $arWorksIds);
					$obContact->updateContactField(CDbzContact::WORKS_USER_FIELD_CODE, $newUserWorks);
					$newUserCategories = array_diff($arCurrentUserCategories, [$elementId]);
					$obContact->updateContactField(CDbzContact::CATEGORIES_USER_FIELD_CODE, $newUserCategories);
					break;
			}
		}

		// Подписывем или отписываем от работы
		if ($elementId && $elementType === CDbzConstants::DBZ_UNIT_WORK) {
			switch ($isSelected) {
				case true:
					$arCurrentUserWorks[] = $elementId;
					$newUserWorks         = array_unique($arCurrentUserWorks);
					$obContact->updateContactField(CDbzContact::WORKS_USER_FIELD_CODE, $newUserWorks);
					break;
				case false:
					$newUserWorks = array_diff($arCurrentUserWorks, [$elementId]);
					$obContact->updateContactField(CDbzContact::WORKS_USER_FIELD_CODE, $newUserWorks);
					break;
			}
		}
	}


}