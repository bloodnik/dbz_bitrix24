<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 21.08.2021
 * Time: 15:05
 * Project: dombezzabot.net
 */

namespace lib\work;


use lib\helpers\CDbzConstants;
use lib\helpers\CDbzResponse;

class CDbzWorkResponse extends CDbzResponse {
	protected int $id;

	protected int $type = CDbzConstants::DBZ_UNIT_WORK;
	protected string $title;
	protected ?string $price;
	protected ?int $category_id;
	protected ?int $workers_count = null;
	protected ?bool $is_selected = null;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * @return ?string
	 */
	public function getPrice(): ?string {
		return $this->price;
	}

	/**
	 * @param ?string $price
	 */
	public function setPrice(?string $price): void {
		$this->price = $price;
	}

	/**
	 * @return int|null
	 */
	public function getWorkersCount(): ?int {
		return $this->workers_count;
	}

	/**
	 * @param int|null $workers_count
	 */
	public function setWorkersCount(?int $workers_count): void {
		$this->workers_count = $workers_count;
	}

	/**
	 * @return bool
	 */
	public function isIsSelected(): bool {
		return $this->is_selected;
	}

	/**
	 * @param bool $is_selected
	 */
	public function setIsSelected(bool $is_selected): void {
		$this->is_selected = $is_selected;
	}

	/**
	 * @return int|null
	 */
	public function getCategoryId(): ?int {
		return $this->category_id;
	}

	/**
	 * @param int|null $category_id
	 */
	public function setCategoryId(?int $category_id): void {
		$this->category_id = $category_id;
	}

	/**
	 * @return int
	 */
	public function getType(): int {
		return $this->type;
	}


}