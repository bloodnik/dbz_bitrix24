<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 21.08.2021
 * Time: 15:05
 * Project: dombezzabot.net
 */

namespace lib\work;

use lib\helpers\CDbzConstants;
use lib\helpers\CDbzResponse;

class CDbzCategoryResponse extends CDbzResponse {
	protected int $id;

	protected int $type = CDbzConstants::DBZ_WORK_CATEGORY;

	/**
	 * Заголовок категории
	 * @var string
	 */
	protected string $title;

	/**
	 * url обложки категории
	 * @var string|null
	 */
	protected string|null $cover = null;

	/**
	 * Подписан ли мастер на категорию
	 *
	 * @var bool
	 */
	protected bool $is_selected = false;

	/**
	 * Число матеров в городе заказчика
	 *
	 * @var int
	 */
	protected int|null $workers_count = null;

	/**
	 * Число выбранных специализаций
	 * @var int
	 */
	protected int|null $selected_count = null;

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * @return string|null
	 */
	public function getCover(): ?string {
		return $this->cover;
	}

	/**
	 * @param string|null $cover
	 */
	public function setCover(?string $cover): void {
		$this->cover = $cover;
	}

	/**
	 * @return int
	 */
	public function getWorkersCount(): int {
		return $this->workers_count;
	}

	/**
	 * @param int $workers_count
	 */
	public function setWorkersCount(int $workers_count): void {
		$this->workers_count = $workers_count;
	}

	/**
	 * @return int
	 */
	public function getSelectedCount(): int {
		return $this->selected_count;
	}

	/**
	 * @param int|null $selected_count
	 */
	public function setSelectedCount(int|null $selected_count): void {
		$this->selected_count = $selected_count;
	}

	/**
	 * @return bool
	 */
	public function isIsSelected(): bool {
		return $this->is_selected;
	}

	/**
	 * @param bool $is_selected
	 */
	public function setIsSelected(bool $is_selected): void {
		$this->is_selected = $is_selected;
	}

	/**
	 * @return int
	 */
	public function getType(): int {
		return $this->type;
	}




}