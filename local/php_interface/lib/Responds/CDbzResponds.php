<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 16.08.2021
 * Time: 19:20
 * Project: dombezzabot.net
 */

namespace lib\responds;

use Bitrix\Main\Loader;
use CIBlockElement;

/**
 * Class CDbzResponds
 *
 * Класс для работы с откликами на заявки
 */
class CDbzResponds {

	const ACTIVE_RESPOND_ID = 31191; // ID актвного отклика
	const INACTIVE_RESPOND_ID = 31192; // ID неактвного отклика
	const TIMEOUT_RESPOND_ID = 31212; // ID отклика Время принаятия истекло
	const CONFIRM_BY_CLIENT_RESPOND_ID = 31193; // ID отклика Выбран заказчиком
	const CANCEL_BY_MASTER_RESPOND_ID = 31193; // ID отклика Отменен мастером

	const PACKAGE_RESPOND_TYPE_ID = 31187; // Тип отклика Пакет
	const PERCENT_RESPOND_TYPE_ID = 31188; // Тип отклика Процент от стоимости

	/**
	 * @param $orderId
	 *
	 * Возвращает список активных откликов по заявке
	 *
	 * @return array
	 */
	public static function getActiveRespondsByOrderId($orderId) {
		Loader::includeModule("iblock");

		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_*");
		$arFilter = array(
			"IBLOCK_ID"         => \lib\helpers\CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID,
			"=PROPERTY_ZAKAZ"   => $orderId,
			"=PROPERTY_AKTIVEN" => self::ACTIVE_RESPOND_ID,
		);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		$arResult = [];
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetProperties();
			$arProps  = [];
			foreach ($arFields as $arField) {
				$arProps[ $arField["CODE"] ] = $arField["VALUE"];
			}
			$arResult[] = $arProps;
		}

		return $arResult;
	}


	/**
	 * @param $orderId
	 *
	 * Возвращает активный отклик мастера
	 *
	 * @return array
	 */
	public static function getMasterActiveRespondByOrderId($orderId, $masterId) {
		Loader::includeModule("iblock");

		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_*");
		$arFilter = array(
			"IBLOCK_ID"         => \lib\helpers\CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID,
			"=PROPERTY_ZAKAZ"   => $orderId,
			"=PROPERTY_AKTIVEN" => self::ACTIVE_RESPOND_ID,
			"=PROPERTY_MASTER"  => $masterId,
		);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		$arFields = [];
		while ($ob = $res->GetNextElement()) {
			$arFields[] = $ob->GetProperties();
			$arFields   = $ob->GetProperties();
			$arProps    = [];
			foreach ($arFields as $arField) {
				$arProps[ $arField["CODE"] ] = $arField["VALUE"];
			}
		}

		return $arProps;
	}

	/**
	 * @param $orderId
	 *
	 * Возвращает активный отклик мастеров по заказу клиента
	 *
	 * @return array
	 */
	public static function getClientOrderResponds($orderId, $clientId) {
		Loader::includeModule("iblock");

		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_*");
		$arFilter = array(
			"IBLOCK_ID"           => \lib\helpers\CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID,
			"=PROPERTY_ZAKAZ"     => $orderId,
			"=PROPERTY_AKTIVEN"   => self::ACTIVE_RESPOND_ID,
			"=PROPERTY_ZAKAZCHIK" => $clientId,
		);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		$arFields = [];
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetProperties();
			$arProps  = [];
			foreach ($arFields as $arField) {
				$arProps[ $arField["CODE"] ] = $arField["VALUE"];
			}
			$arResult[] = $arProps;
		}

		return $arResult;
	}

}