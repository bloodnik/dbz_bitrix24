<?php
$MESS ['DBZ_UT_CHECKBOX_NUM_DESCR'] = 'Список с поиском ДБЗ';
$MESS ['DBZ_UT_CHECKBOX_NUM_VALUE_EMPTY'] = 'любое';
$MESS ['DBZ_UT_CHECKBOX_NUM_VALUE_ABSENT'] = "пусто";
$MESS ['DBZ_UT_CHECKBOX_NUM_VALUE_N'] = 'Нет';
$MESS ['DBZ_UT_CHECKBOX_NUM_VALUE_Y'] = 'Да';
$MESS ['DBZ_UT_CHECKBOX_NUM_SETTING_TITLE'] = 'Настройки показа';
$MESS ['DBZ_UT_CHECKBOX_NUM_SETTING_VALUE_N'] = 'Текст для пустого чекбокса';
$MESS ['DBZ_UT_CHECKBOX_NUM_SETTING_VALUE_Y'] = 'Текст для нажатого чекбокса';
$MESS ['DBZ_UT_CHECKBOX_NUM_VALUE_EMPTY_NEW_STYLE'] = 'Не указан';