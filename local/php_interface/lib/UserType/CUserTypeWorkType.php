<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 05.06.2021
 * Time: 18:17
 * Project: dombezzabot.net
 */

namespace lib\UserType;

use Bitrix\Main\Page\Asset;
use lib\work\CDbzWork;


class CUserTypeWorkType {

	/**
	 * Метод возвращает массив описания собственного типа свойств
	 * @return array
	 */
	public static function GetUserTypeDescription(): array {
		return array(
			"USER_TYPE_ID"  => "work_type_autocomplete", //Уникальный идентификатор типа свойств
			"DESCRIPTION"   => "Вид работ с поиском",
			"BASE_TYPE"     => \CUserTypeManager::BASE_TYPE_INT,
			"CLASS_NAME"    => __CLASS__,
			'EDIT_CALLBACK' => array(__CLASS__, "GetPublicEdit"),
			'VIEW_CALLBACK' => array(__CLASS__, "GetPublicView"),
		);
	}


	public static function GetPublicEdit($arUserFiled, $arAdditionalParameters = []): string {
		\CJSCore::Init(["jquery2"]);
		\Bitrix\Main\UI\Extension::load("ui.forms");

		Asset::getInstance()->addJs(APP_MEDIA_FOLDER . "js/workUnitUserType.js");
		Asset::getInstance()->addJs(APP_MEDIA_FOLDER . "css/workUnitUserType.css");

		$options = self::getWorksSelectList($arUserFiled['VALUE']);

		return '
			<div class="ui-ctl ui-ctl-wa">
			    <input type="text" id="search-' . $arUserFiled["FIELD_NAME"] . '" oninput="onWorkTypeInput(\'' . $arUserFiled["FIELD_NAME"] . '\')"  placeholder="Введите название работы для поиска" class="ui-ctl-element">
			</div>
			<span></span>
			<div class="ui-ctl ui-ctl-multiple-select ui-ctl-wa ">
			    <select class="ui-ctl-element" name="' . $arUserFiled["FIELD_NAME"] . '"  size="3">
					' . $options . '
		        </select>
			</div>
		';
	}

	public static function GetPublicView($arUserField, $arAdditionalParameters = []) {
		$selectedWork = CDbzWork::getWorkById($arUserField["VALUE"]);

		return $selectedWork["title"] ?? "Вид работ не выбран";
	}


	/**
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	private static function getWorksSelectList($selectedValue): string {
		$arWorks = CDbzWork::getAllWorks();

		$options = "";
		foreach ($arWorks as $arWork) {
			$selected = $selectedValue === $arWork["ID"] ? "selected" : "";
			$options  .= "<option {$selected} value='{$arWork['ID']}'>{$arWork['NAME']}</option>";
		}

		return $options;

	}


	/**
	 * Обязательный метод для определения типа поля таблицы в БД при создании свойства
	 *
	 * @param $arUserField
	 *
	 * @return string
	 */
	public static function getDbColumnType($arUserField): string {
		global $DB;
		switch (strtolower($DB->type)) {
			case "mysql":
				return "integer";
		}

		return "integer";
	}

}
