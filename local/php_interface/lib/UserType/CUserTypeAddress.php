<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 05.06.2021
 * Time: 18:17
 * Project: dombezzabot.net
 */

namespace lib\usertype;

use \Bitrix\Main,
	\Bitrix\Main\Localization\Loc,
	\Bitrix\Main\UserField;
use Bitrix\Main\Page\Asset;

class CUserTypeAddress {

	/**
	 * Метод возвращает массив описания собственного типа свойств
	 * @return array
	 */
	public static function GetUserTypeDescription() {
		return array(
			"USER_TYPE_ID"  => "address_autocomplete", //Уникальный идентификатор типа свойств
			"DESCRIPTION"   => "Автозаполнение адреса",
			"BASE_TYPE"     => \CUserTypeManager::BASE_TYPE_STRING,
			"CLASS_NAME"    => __CLASS__,
			'EDIT_CALLBACK' => array(__CLASS__, "GetPublicEdit"),
			'VIEW_CALLBACK' => array(__CLASS__, "GetPublicView"),
		);
	}

	public static function GetPublicEdit($arUserFiled, $arAdditionalParametrs = []) {
		\CJSCore::Init(["jquery2"]);
		\Bitrix\Main\UI\Extension::load("ui.forms");

		Asset::getInstance()->addJs(APP_MEDIA_FOLDER . "js/dadataUserType.js");
		Asset::getInstance()->addJs(APP_MEDIA_FOLDER . "css/dadataUserType.css");

		return '
			<script >initUserField();</script>
			<div class="ui-ctl ui-ctl-textbox ui-ctl-after-icon  ui-ctl-wa dropdown-input"> <!-- 1. Основной контейнер -->
			    <button data-field-name="' . $arUserFiled["FIELD_NAME"] . '" class="ui-ctl-after ui-ctl-icon-search addressSuggestionSearchBtn"></button> <!-- 3. Дополнительный элемент (опционально)-->
			    <input type="text" name="' . $arUserFiled["FIELD_NAME"] . '" value="' . $arUserFiled['VALUE'] . '" class="ui-ctl-element"> <!-- 2. Основное поле -->
			    <div class="address-dropdown ' . $arUserFiled["FIELD_NAME"] . '">
			        <ul>
					</ul>
				</div>
			</div>
		';
	}

	public static function GetPublicView($arUserField, $arAdditionalParametrs = []) {

		return $arUserField["VALUE"];
	}

	/**
	 * Обязательный метод для определения типа поля таблицы в БД при создании свойства
	 *
	 * @param $arUserField
	 *
	 * @return string
	 */
	public static function GetDBColumnType($arUserField) {
		global $DB;
		switch (strtolower($DB->type)) {
			case "mysql":
				return "text(1000)";
			case "oracle":
				return "varchar2(1000)";
			case "mssql":
				return "varchar(1000)";
		}

		return "text(1000)";
	}

}
