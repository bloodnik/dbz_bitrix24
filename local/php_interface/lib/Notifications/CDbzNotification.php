<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 30.08.2021
 * Time: 22:00
 * Project: dombezzabot.net
 */

namespace lib\notifications;

use Bitrix\Iblock\Iblock;
use Bitrix\Main\Type\DateTime;
use CBPDocument;
use CIBlockElement;
use CModule;
use lib\contact\CDbzContact;
use lib\helpers\CDbzConstants;
use lib\helpers\CDbzResponse;

class CDbzNotification {

	private array $recipients = [];
	private int $userId;
	private int $userType;

	/**
	 * Тип уведомлений. Кому
	 * @var array|int[]
	 */
	private array $arNotificationsTypeForWho = [
		"GENERAL"  => 34316,
		"PERSONAL" => 34315,
	];


	/**
	 * CDbzNotification constructor.
	 *
	 * @param int $userId
	 * @param int $userType
	 */
	public function __construct(int $userId, int $userType) {
		$this->userId   = $userId;
		$this->userType = $userType;
	}


	/**
	 * назначение персонального уведомления
	 *
	 * @param $notificationTemplateId - шаблон уведомления
	 * @param $entityId - id объекта (сдпелка, контакт)
	 * @param $additionalText - подменный текст для шаблона. Меняет символ (*)
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function addNotification($notificationTemplateId, $entityId, $additionalText): void {

		$obContact = new CDbzContact($this->getUserId());

		if ( ! $obContact->isContactRegistered()) {
			return;
		}

		//читаем шаблон
		$iblock   = Iblock::wakeUp(CDbzConstants::DBZ_NOTIF_TEMPLATES_IBLCOK_ID)->getEntityDataClass();
		$elements = $iblock::getList([
			'select' => ["ID", "NAME", "TEXT_" => "TEXT", "EVENT_TYPE_" => "EVENT_TYPE", "DAYS_ACTIVE_" => "DAYS_ACTIVE", "SHOULD_SEND_PUSH_" => "SHOULD_SEND_PUSH"],
			'filter' => ["=ID" => $notificationTemplateId]
		])->fetchAll();

		foreach ($elements as $element) {
			$name           = $element["NAME"];
			$arText         = unserialize($element["TEXT_VALUE"], ['allowed_classes' => false]);
			$eventType      = (int) $element["EVENT_TYPE_VALUE"];
			$daysActive     = (int) $element["DAYS_ACTIVE_VALUE"];
			$shouldSendPush = (int) $element["SHOULD_SEND_PUSH_VALUE"];

			$text = $arText["TEXT"];
			if ($additionalText) {
				$text = str_replace('(*)', $additionalText, $text);
			}

			$notificationDate = DateTime::createFromPhp(new \DateTime())->add($daysActive . " days");

			$el   = new CIBlockElement;
			$PROP = array(
				"TEKST_UVEDOMLENIYA"  => $text,
				"TIP"                 => $this->arNotificationsTypeForWho["PERSONAL"],
				"TIP_POLZOVATELYA"    => CDbzConstants::DBZ_WORKER_TYPE ? CDbzContact::NEW_APP_REGISTER_CLIENT_ID : CDbzContact::NEW_APP_REGISTER_MASTER_ID,
				"TIP_SOBYTIYA"        => $eventType,
				"AKTIVNOST"           => CDbzConstants::DBZ_YES_VALUE,
				"AKTIVNO_DO"          => $notificationDate->format('d.m.Y H:i:s'),
				"KOMU_PREDNAZNACHENO" => $this->getRecipients(),
				"PUSH"                => $shouldSendPush,
			);

			if ($entityId) {
				$PROP["ID_OBEKTA"] = $entityId;
			}

			$arLoadProductArray = array(
				"IBLOCK_ID"       => CDbzConstants::DBZ_NOTIFICATIONS_IBLCOK_ID,
				"NAME"            => $name,
				"PROPERTY_VALUES" => $PROP,
			);

			$element_id = $el->Add($arLoadProductArray);
			//запуск БП отложенной деактивации и отправки пуша
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					202,
					array("lists", "BizprocDocument", $element_id),
					array_merge(),
					$arErrorsTmp
				);
			}
		}
	}


	/**
	 * @return int
	 */
	public function getUserId(): int {
		return $this->userId;
	}

	/**
	 * @param int $userId
	 */
	public function setUserId(int $userId): void {
		$this->userId = $userId;
	}

	/**
	 * @return int
	 */
	public function getUserType(): int {
		return $this->userType;
	}

	/**
	 * @param int $userType
	 */
	public function setUserType(int $userType): void {
		$this->userType = $userType;
	}

	/**
	 * @return array
	 */
	public function getRecipients(): array {
		return $this->recipients;
	}

	/**
	 * @param array $recipients
	 */
	public function setRecipients(array $recipients): void {
		$this->recipients = $recipients;
	}

}