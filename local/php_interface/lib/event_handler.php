<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 05.06.2021
 * Time: 18:28
 * Project: dombezzabot.net
 */

use Bitrix\Main;
$eventManager = Main\EventManager::getInstance();

//Вешаем обработчик на событие создания списка пользовательских свойств OnUserTypeBuildList
$eventManager->addEventHandler("main", "OnUserTypeBuildList", ["lib\UserType\CUserTypeAddress", "GetUserTypeDescription"]);
$eventManager->addEventHandler("main", "OnUserTypeBuildList", ["lib\UserType\CUserTypeWorkType", "GetUserTypeDescription"]);
$eventManager->addEventHandler("iblock", "OnIBlockPropertyBuildList", ["lib\UserType\CUserTypeCheckbox", "GetUserTypeDescription"]);
