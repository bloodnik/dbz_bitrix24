<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 16.06.2021
 * Time: 14:32
 * Project: dombezzabot.net
 */

use Bitrix\Main\Application;

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


$request = Application::getInstance()->getContext()->getRequest();

//Тип запроса
$type = $request->get("type");

if (strlen($type) > 0) {

	switch ($type) {
		case "GetStreetSuggestions" :
			$searchString    = $request->get("address");
			$dadataHelper    = new \lib\helpers\CDadataHelpers();
			$sugesstionsList = $dadataHelper->getAddressSuggestions($searchString);
			$response        = \lib\helpers\CApiHelpers::getSuccess($sugesstionsList);
			echo $response;
			break;
	}
}