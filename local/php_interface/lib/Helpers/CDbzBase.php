<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 02.09.2021
 * Time: 20:00
 * Project: dombezzabot.net
 */

namespace lib\helpers;

/**
 * Базовый класс
 *
 * Class CDbzBase
 * @package lib\helpers
 */
class CDbzBase {
	private ?int $userId;
	private ?int $userType;

	/**
	 * CDbzBase constructor.
	 *
	 * @param int|null $userId
	 * @param int|null $userType
	 */
	public function __construct(?int $userId, ?int $userType = CDbzConstants::DBZ_WORKER_TYPE) {
		$this->userId   = $userId;
		$this->userType = $userType;
	}


	/**
	 * @return int|null
	 */
	public function getUserId(): ?int {
		return $this->userId;
	}

	/**
	 * @param int|null $userId
	 */
	public function setUserId(?int $userId): void {
		$this->userId = $userId;
	}

	/**
	 * @return int|null
	 */
	public function getUserType(): ?int {
		return $this->userType;
	}

	/**
	 * @param int|null $userType
	 */
	public function setUserType(?int $userType): void {
		$this->userType = $userType;
	}


}