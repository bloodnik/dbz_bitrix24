<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 09.06.2021
 * Time: 20:22
 * Project: dombezzabot.net
 */

namespace lib\helpers;

use Bitrix\Main\Context;
use Bitrix\Main\SystemException;
use CFile;
use CIBlockElement;
use lib\order\CDbzOrder;
use lib\userdata\CUserData;

class CDbzFilesHelper {
	const TMP_FOLDER = 'tmp';

	private array $arFile;
	private array $data;
	private int $userType;

	/**
	 * CDbzFilesHelper constructor.
	 *
	 * @param $data
	 */
	public function __construct($data) {
		$this->data = $data;
	}

	/**
	 * Загрузка файла
	 * Проверяет файл и загружает его на сервер
	 *
	 * @return array
	 * @throws \Bitrix\Main\SystemException
	 */
	public function uploadFile(): array {
		$request = Context::getCurrent()->getRequest();
		$files   = $request->getFile("file");

		$el                 = new CIBlockElement;
		$arLoadProductArray = array(
			"IBLOCK_ID"       => 89,
			"NAME"            => "Входящий запрос. Файл",
			"PROPERTY_VALUES" => array("ZAPROS" => json_encode($request->getFileList()->toArray())),
		);
		$el->Add($arLoadProductArray);

		//	Проверка файла
		$arFileCheckRes = self::checkFile($files);
		if ($arFileCheckRes['hasError']) {
			throw new SystemException($arFileCheckRes["msg"]);
		}

		// upload file and update user photo
		if (0 < $files['error']) {
			throw new SystemException($files['error']);
		}

		move_uploaded_file($files['tmp_name'], $_SERVER["DOCUMENT_ROOT"] . '/upload/' . self::TMP_FOLDER . '/' . $files['name']);
		if (file_exists($_SERVER["DOCUMENT_ROOT"] . '/upload/' . self::TMP_FOLDER . '/' . $files['name'])) {

			try {
				$this->arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"] . '/upload/' . self::TMP_FOLDER . '/' . $files['name']);
				$result       = $this->saveFileToEntity();
			} catch (\Exception $exception) {
				$result = ["hasError" => true, "msg" => $exception->getMessage()];
			}


			unlink($_SERVER["DOCUMENT_ROOT"] . '/upload/' . self::TMP_FOLDER . '/' . $files['name']);
		} else {
			return ["hasError" => true, "msg" => "Ошибка копировния"];
		}
		move_uploaded_file(
			$files['tmp_name'],
			'upload/' . self::TMP_FOLDER . '/' . $files['name']
		);

		return $result;
	}


	/**
	 * Фунция сохранения файла в сущность
	 * Сущность определяется по свойству id
	 * @return array
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function saveFileToEntity(): array {
		$type = $this->data["data"]["type"];
		$id   = $this->data["data"]["id"];

		switch ($type) {
			case "order":
				// Файл для заказа
				$dbzOrder = new CDbzOrder($id);
				$result   = $dbzOrder->updateOrderPicture($this->arFile);
				break;
			case "user_data":
				// Файл для анкеты
				$userData = new CUserData($this->data["id"], $this->getUserType());
				$fileData = array(
					"id"    => $id,
					"type"  => CUserData::IMAGE_TYPE,
					"value" => $this->arFile
				);
				$userData->setUserData([$fileData]);
				$result = $userData->getLastUserTypeFile($id);
				break;
			default:
				$result = ["hasError" => true, "msg" => "Неподдерживаемый тип"];
				break;

		}

		return $result;
	}


	private static function checkFile($file): array {

		$arResult = array(
			'hasError' => true,
			'msg'      => "Ошибка отправки сообщения",
		);

		$fileSizeLimit = 1024 * 1024 * 5;
		$fileExts      = array('gif', 'png', 'jpg', 'jpeg');

		if ($file["size"] > $fileSizeLimit) {
			$arResult['msg'] = "Превышен допустимый размер файла. Разрешены файлы не более 5Mb.";

			return $arResult;
		}

		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		if ( ! in_array($ext, $fileExts, true)) {
			$arResult['msg'] = "Прикрепить можно только файлы изображений (jpg, jpeg, png, gif)";

			return $arResult;
		}

		return array(
			'hasError' => false,
			'msg'      => "Файлы корректны",
		);
	}

	/**
	 * @return int
	 */
	public function getUserType(): int {
		return $this->userType;
	}

	/**
	 * @param int $userType
	 */
	public function setUserType(int $userType): void {
		$this->userType = $userType;
	}


}