<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 09.06.2021
 * Time: 20:22
 * Project: dombezzabot.net
 */

namespace lib\helpers;

use Bitrix\Main\Loader;
use CCrmContact;
use CIBlockElement;

class CApiHelpers {

	//-----Возвращение ответа-----


	/**
	 * $response - json-объект
	 * $message - код сообщения
	 * кодирование успешного ответа
	 * @throws \JsonException
	 */
	public static function getSuccess($response): bool|string {
		return json_encode(['success' => true, 'message' => null, 'response' => json_encode($response, JSON_THROW_ON_ERROR)], JSON_THROW_ON_ERROR);
	}


	/**
	 * response неудачного ответа
	 *
	 * @throws \JsonException
	 */
	public static function getFailure($message, $money_amount = false, $special_error = false): bool|string {
		if ( ! ($money_amount)) {
			$money_amount = null;
		}
		$shouldSelectCity = null;
		$shouldEnterName  = null;
		if ($special_error === "city_error") {
			$shouldSelectCity = true;
		}
		if ($special_error === "name_error") {
			$shouldEnterName = true;
		}

		return json_encode(['success' => false, 'message' => $message, 'response' => "", 'money_amount' => $money_amount, 'should_select_city' => $shouldSelectCity, 'should_enter_name' => $shouldEnterName], JSON_THROW_ON_ERROR);
	}


	/**
	 * Возвращает структуру Location по идентификатору города
	 *
	 * @param $cityId
	 *
	 * @return array
	 */
	public static function getLocationByCityId($cityId): array {
		if ( ! $cityId) {
			return [];
		}

		$arSelect = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_REGION_TYPE", "PROPERTY_REGION", "PROPERTY_LONGITUDE", "PROPERTY_LATITUDE");
		$arFilter = array("IBLOCK_ID" => CDbzConstants::DBZ_CITIES_IBLOCK_ID, "=ID" => $cityId);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$title    = $arFields["NAME"];
			$lat      = (float) $arFields["PROPERTY_LATITUDE_VALUE"];
			$lon      = (float) $arFields["PROPERTY_LONGITUDE_VALUE"];
		};

		return array(
			'id'    => (int) $cityId,
			'title' => $title ?? "",
			'lat'   => $lat ?? 0.0,
			'lon'   => $lon ?? 0.0,
		);
	}


	//поиск ближайшего города по координатам
	public static function getCityByCoordinates($lat, $lon): int {
		$all_cities = array();
		$arSelect   = array("ID", "IBLOCK_ID", "NAME", "PROPERTY_LATITUDE", "PROPERTY_LONGITUDE");
		$arFilter   = array("IBLOCK_ID" => CDbzConstants::DBZ_CITIES_IBLOCK_ID, "CHECK_PERMISSIONS" => 'N');
		$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields               = $ob->GetFields();
			$city_id                = $arFields["ID"];
			$city_lat               = $arFields["PROPERTY_LATITUDE_VALUE"];
			$city_lon               = $arFields["PROPERTY_LONGITUDE_VALUE"];
			$distance_sqr           = (pow(abs($lat - $city_lat), 2) + pow(abs($lon - $city_lon), 2));
			$distance               = sqrt($distance_sqr);
			$all_cities[ $city_id ] = $distance;
		};

		return (int) array_search(min($all_cities), $all_cities, true);
	}


	//замена кавычек в строке при передаче в json
	public static function quotDecode($text): array|string {
		//замена двойных кавычек на две одинарные
		return str_replace("&quot;", "''", $text);
	}

	// Возвращает массив глобальных настроек
	public static function getGlobalSettings(): array {
		Loader::includeModule("iblock");

		$arSelect = array("ID", "IBLOCK_ID");
		$arFilter = array("IBLOCK_ID" => CDbzConstants::DBZ_SETTINGS_IBLOCK_ID, "=ID" => CDbzConstants::DBZ_MAIN_CONFIG_ID);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		if ($ob = $res->GetNextElement()) {
			$arProps = $ob->GetProperties();

			$arResult = [];
			foreach ($arProps as $arProp) {
				$arResult[ $arProp["CODE"] ] = $arProp["VALUE"];
			}

			return $arResult;
		}

		return [];
	}


	public static function addProfileUpdateLog($contactId) {
		Loader::includeModule("crm");
		Loader::includeModule("iblock");

		//чтение контакта
		$arFilter       = array("=ID" => $contactId, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("NAME", "LAST_NAME", "SECOND_NAME", "UF_CRM_NEW_CITY", "UF_CRM_REGISTER_DATE");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob = $res->GetNext()) {
			$name        = $ob["NAME"];
			$last_name   = $ob["LAST_NAME"];
			$second_name = $ob["SECOND_NAME"];
			$city        = $ob["UF_CRM_NEW_CITY"];
			$full_name   = $ob["NAME"] . ' ' . $last_name;
			$reg_date    = date_create($ob['UF_CRM_REGISTER_DATE']);
		}
		//получение списка текущих записей и обновление
		$categories_now = array();
		$arSelect       = array("ID", "IBLOCK_ID");
		$arFilter       = array("IBLOCK_ID" => 123, "=PROPERTY_330" => $contactId);
		$res            = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$PROP     = array(
				327 => $full_name,
				332 => $city,
				328 => self::get_element_name(84, $city),
				333 => date_format($reg_date, 'd.m.Y'),
			);
			CIBlockElement::SetPropertyValuesEx($arFields["ID"], 123, $PROP);
		};
	}

	//получение названий полей-привязок
	public static function get_element_name($iblock_id, $element_id) {
		$name = "";
		if ( ! ($element_id)) {
			return $name;
		}
		$arSelect = array("NAME");
		$arFilter = array("IBLOCK_ID" => $iblock_id, "=ID" => $element_id);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$name     = $arFields["NAME"];
		};

		return $name;
	}


	// Возвращает массив всех свойств инфоблока
	public static function getIblockPropertyList($iblockId): array {
		$arProps = \Bitrix\Iblock\PropertyTable::getList(array(
			'select' => array('*'),
			'filter' => array('IBLOCK_ID' => $iblockId)
		))->fetchAll();

		$arResult = [];
		foreach ($arProps as $key => $arProp) {
			$arResult[ $arProp["CODE"] ] = $arProp;
		}

		return $arResult;
	}


	// Форматирование номера телефона под формат 71234567889
	public static function formatPhoneNumber($phoneNumber): array|string|null {
		return preg_replace("/[^\d]+/", '', $phoneNumber);
	}


	/**
	 * Проверка на необходимость обновления приложения.
	 * Некорректные версии храняться в инфболоке Глобальные настройки
	 *
	 * @param $currentVersion - текущая версия приложения. Приходит в теле запроса из приложения
	 *
	 * @return bool
	 */
	public static function checkIncorrectAppVersion($currentVersion): bool {
		$incorrectAppVersions = DBZ_GLOBAL_SETTINGS["INCORRECT_APP_VERSIONS"];

		return str_contains($incorrectAppVersions, $currentVersion);
	}

}