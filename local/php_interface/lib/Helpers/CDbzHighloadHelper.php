<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 02.09.2021
 * Time: 20:05
 * Project: dombezzabot.net
 */

namespace lib\helpers;


use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ORM\Data\DataManager;
use CModule;

class CDbzHighloadHelper {

	/**
	 * @param $hlbl - ID нашего highloadblock блока к которому будет делать запросы.
	 *
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getHighloadEntity($hlbl): DataManager|string {

		\CModule::IncludeModule('highloadblock');

		$hlblock = HighloadBlockTable::getById($hlbl)->fetch();

		$entity = HighloadBlockTable::compileEntity($hlblock);

		return $entity->getDataClass();
	}


	/**
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function getHighloadEntityByName($hlbName): DataManager|string {

		CModule::IncludeModule('highloadblock');

		$result     = HighloadBlockTable::getList(array('filter' => array('=NAME' => $hlbName)));
		$HLBLOCK_ID = null;
		if ($row = $result->fetch()) {
			$HLBLOCK_ID = $row["ID"];
		}

		$hlblock = HighloadBlockTable::getById($HLBLOCK_ID)->fetch();
		$entity  = HighloadBlockTable::compileEntity($hlblock);

		return $entity->getDataClass();
	}

}