<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 09.06.2021
 * Time: 20:45
 * Project: dombezzabot.net
 */

namespace lib\helpers;

use Dadata\DadataClient;

class CDadataHelpers {
	private $token = "2986b16592cf8ea1ee5aac57e02b59767e905b5d";
	private $secret = "9c9cd057d85b0e7ad57ffb893f9877fcb1d95810";
	private $dadata;

	/**
	 * CDadataHelpers constructor.
	 *
	 * @param string $token
	 * @param string $secret
	 */
	public function __construct() {
		$this->dadata = new DadataClient($this->token, $this->secret);
	}

	/**
	 * @param $address - вариант адреса произвольной строкой
	 *
	 * @return array - массив предложенных вариантов адресов
	 */
	public function getAddressSuggestions($address) {
		$response = $this->dadata->suggest("address", $address);

		$arAddresses = [];
		foreach ($response as $address) {
			$item["id"] = (int) $address["geoname_id"];
			$item["title"] = $address["value"];
			$item["lat"]   = (double) $address["data"]["geo_lat"] ?? 0;
			$item["lon"]   = (double) $address["data"]["geo_lon"] ?? 0;

			$arAddresses[] = $item;
		}

		return $arAddresses;
	}


}