<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 08.08.2021
 * Time: 19:59
 * Project: dombezzabot.net
 */

namespace lib\helpers;


class CDbzResponse {

	public function __construct(array|null $properties = array()) {
		foreach ($properties as $key => $value) {
			$this->{$key} = $value;
		}
	}

	// Конвертация объекта в массив
	public function toArray(): array {
		$arObject   = get_object_vars($this);
		$arResponse = [];

		foreach ($arObject as $key => $value) {
//			if (isset($value)) {
			$arResponse[ $key ] = $value;
//			}
		}

		return $arResponse;
	}

}