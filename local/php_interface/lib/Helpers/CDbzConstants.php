<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 06.08.2021
 * Time: 20:06
 * Project: dombezzabot.net
 */

namespace lib\helpers;

abstract class CDbzConstants {

	// Константы API

	/**
	 * URL портала
	 */
	public const DBZ_PORTAL_URL = "https://dombezzabot.net";

	/**
	 * Id элемента инфоблока Глобальные настройки
	 */
	public const DBZ_MAIN_CONFIG_ID = 31213;

	/**
	 * Id элемента инфоблока "ДА"
	 */
	public const DBZ_YES_VALUE = 31194;

	/**
	 * Id элемента инфоблока "НЕТ"
	 */
	public const DBZ_NO_VALUE = 31195;

	/**
	 *  Тип мастер
	 */
	public const DBZ_WORKER_TYPE = 0;

	/**
	 * Тип клиент (заказчик)
	 */
	public const DBZ_CLIENT_TYPE = 1;

	/**
	 * Тип "Категория работ"
	 */
	public const DBZ_WORK_CATEGORY = 0;

	/**
	 * Тип "Вид работы"
	 */
	public const DBZ_UNIT_WORK = 1;


	// ID Инфоблоков

	/**
	 *  ID инфоблока Глобальные настройки
	 */
	public const DBZ_SETTINGS_IBLOCK_ID = 78;

	/**
	 * ID инфоблока Автоуведомления. Содержит шаблоны уведолмений
	 */
	public const DBZ_NOTIF_TEMPLATES_IBLCOK_ID = 95;

	/**
	 * ID инфоблока Уведомоления.
	 */
	public const DBZ_NOTIFICATIONS_IBLCOK_ID = 90;

	/**
	 * ID инфоблока Баланс. Содержит информацию об основном и бонусном балансах, о подключенных пакетах, о возвратах
	 */
	public const DBZ_BALANCE_IBLOCK_ID = 80;

	/**
	 * ID инфоблока Отклики на заявки.
	 */
	public const DBZ_ORDER_RESPONDS_IBLOCK_ID = 73;

	/**
	 * ID инфоблока Города
	 */
	public const DBZ_CITIES_IBLOCK_ID = 84;

	/**
	 *  ID инфоблока Причина отказа
	 */
	public const DBZ_CANCEL_TYPES_IBLOCK_ID = 112;

	/**
	 * ID инфоблока Отзывы
	 */
	public const DBZ_FEEDBACKS_IBLOCK_ID = 128;

	/**
	 * ID инфоблока Отзывы мастера
	 */
	public const DBZ_WORKER_FEEDBACKS_IBLOCK_ID = 141;

	/**
	 * ID инфоблока Работы и Категории
	 */
	public const DBZ_WORKS_IBLOCK_ID = 66;

	/**
	 * ID инфоблока Специализации
	 */
	public const DBZ_SPECIALIZATIONS_IBLOCK_ID = 143;

	/**
	 * ID инфоблока Новости
	 */
	public const DBZ_NEWS_IBLOCK_ID = 145;

	/**
	 * ID инфоблока Токены устройств
	 */
	public const DBZ_PUSH_TOKENS_IBLOCK_ID = 97;

}