<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 18.08.2021
 * Time: 21:41
 * Project: dombezzabot.net
 */

namespace lib\feedbacks;


use lib\helpers\CDbzResponse;

class CDbzFeedbackResponse extends CDbzResponse {

	protected int $id;
	protected float $value; // общая оценка
	protected array $ratings; // массив подбровных оценок
	protected string $title; // название категории в которй выполнялся заказ
	protected string $text; // Текст оставленный пользователем

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @return float
	 */
	public function getValue(): float {
		return $this->value;
	}

	/**
	 * @return array
	 */
	public function getRatings(): array {
		return $this->ratings;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getText(): string {
		return $this->text;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}

	/**
	 * @param float $value
	 */
	public function setValue(float $value): void {
		$this->value = $value;
	}

	/**
	 * @param array $ratings
	 */
	public function setRatings(array $ratings): void {
		$this->ratings = $ratings;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * @param string $text
	 */
	public function setText(string $text): void {
		$this->text = $text;
	}


}