<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 18.08.2021
 * Time: 20:45
 * Project: dombezzabot.net
 */

namespace lib\feedbacks;

use Bitrix\Iblock\Iblock;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use JetBrains\PhpStorm\Pure;
use lib\contact\CDbzContact;
use lib\helpers\CApiHelpers;
use lib\helpers\CDbzBase;
use lib\helpers\CDbzConstants;
use lib\order\CDbzOrder;

class CDbzFeedbacks extends CDbzBase {

	/**
	 * Массив свойств подробных оценок для оценки мастера клиентом
	 */
	public const CLIENT_RATING_FIELDS = ["RATING_QUALITY_" => "RATING_QUALITY", "RATING_CLEANNES_" => "RATING_CLEANNES", "RATING_COMMUNICATION_" => "RATING_COMMUNICATION"];
	public const MASTER_RATING_FIELDS = ["RATING_DEADLINES_" => "RATING_DEADLINES", "RATING_COMMUNICATION_" => "RATING_COMMUNICATION"];

	protected int $orderId = 0;

	/**
	 * Возвращет отзыв мастера или клиента по заказу
	 *
	 * @return array|null
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getUserOrderFeedback(): ?array {
		$this->validateUserId();
		$this->validateOrderId();

		$iblock = $this->getIblockEntity();

		$elements = $iblock::getList([
			'select' => $this->setSelectFields(),
			'filter' => $this->setFilter()
		])->fetchAll();

		$feedbackResponse = new CDbzFeedbackResponse();
		foreach ($elements as $element) {
			$feedbackResponse->setId($element["ID"]);
			$feedbackResponse->setTitle($element["NAME"]);
			$feedbackResponse->setValue($element["MAIN_RATING_VALUE"]);
			$feedbackResponse->setText($element["TEXT_VALUE"]);

			$arRatings          = [];
			$arIblockProperties = CApiHelpers::getIblockPropertyList($this->getIblockId());

			$arRatingsFields = $this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? self::MASTER_RATING_FIELDS : self::CLIENT_RATING_FIELDS;
			foreach ($arRatingsFields as $ratingFields) {
				$arRatings[] = [
					"id"    => $ratingFields,
					"title" => $arIblockProperties[ $ratingFields ]["NAME"],
					"value" => (int) $element[ $ratingFields . "_VALUE" ]
				];
			}
			$feedbackResponse->setRatings($arRatings);
		}

		$result = $feedbackResponse->toArray();

		return ! empty($result) ? $result : null;
	}


	/**
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public function getRatingsFields(): array {

		if ($this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE) {
			$arCodes = array_values(self::MASTER_RATING_FIELDS);
		} else {
			$arCodes = array_values(self::CLIENT_RATING_FIELDS);
		}

		$rsProperty = PropertyTable::getList(array(
			'filter' => array(
				'IBLOCK_ID' => $this->getIblockId(),
				'ACTIVE'    => 'Y',
				'CODE'      => $arCodes
			),
			'select' => array(
				'ID',
				'CODE',
				'NAME',
			),
		));

		$arRatings = [];
		while ($arProperty = $rsProperty->fetch()) {
			$arRatings[] = [
				"id"    => $arProperty["CODE"],
				"title" => $arProperty["NAME"],
				"value" => null
			];
		}


		return $arRatings;
	}


	#[Pure] private function setSelectFields(): array {
		$arSelect = ["ID", "NAME", "MASTER_" => "MASTER", "CLIENT_" => "CLIENT", "TEXT_" => "TEXT", "MAIN_RATING_" => "MAIN_RATING"];

		if ($this->getUserType() === CDbzConstants::DBZ_CLIENT_TYPE) {
			$arContactRatingFields = self::CLIENT_RATING_FIELDS;
		} else {
			$arContactRatingFields = self::MASTER_RATING_FIELDS;
		}

		return array_merge($arSelect, $arContactRatingFields);
	}


	#[Pure] private function setFilter(): array {
		$arFilter = array();

		if ($this->getOrderId()) {
			$arFilter["=ORDER.VALUE"] = $this->getOrderId();
		}

		if ($this->getUserId()) {
			$contactFilter              = $this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? 'MASTER.VALUE' : 'CLIENT.VALUE';
			$arFilter[ $contactFilter ] = $this->getUserId();
		}

		$arFilter["FROM_MASTER.VALUE"] = $this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? 1 : 0;

		return $arFilter;
	}


	/**
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function addFeedback($feedbackData): int {

		$this->setOrderId($feedbackData["order_id"]);
		$this->validateUserId();
		$this->validateOrderId();

		if ($this->isFeedbackExist()) {
			throw new SystemException("Отзыв по заказу уже существует");
		}

		$userTypeString = $this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? 'мастера' : 'клиента';

		$obOrder     = new CDbzOrder($this->getOrderId());
		$arOrder     = $obOrder->getOrderData();
		$newFeedback = $this->getIblockEntity()::createObject();
		if (isset($newFeedback)) {
			$newFeedback->setName("Отзыв {$userTypeString} по заказу {$arOrder["ID"]}");
			$newFeedback->setMaster($arOrder["UF_CRM_MASTER"]);
			$newFeedback->setClient($arOrder["CONTACT_ID"]);
			$newFeedback->setOrder($this->getOrderId());
			$newFeedback->setFromMaster($this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? 1 : 0);

			$newFeedback->setText($feedbackData["text"]);
			$newFeedback->setMainRating($feedbackData["value"]);

			foreach ($feedbackData["ratings"] as $rating) {
				$newFeedback->set($rating["id"], (int) $rating["value"]);
			}

			$newFeedback->save();

			self::updateMainRatingField($this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? $arOrder["CONTACT_ID"] : $arOrder["UF_CRM_MASTER"], $this->getUserType());

			return $newFeedback->getId();
		}

		return 0;
	}


	/**
	 * Количество отзывов у пользователя
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function getUserFeedbacksCount(): int {
		$iblock = $this->getIblockEntity();

		$arFilter = [];
		if ($this->getUserId()) {
			$contactFilter              = $this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? 'MASTER.VALUE' : 'CLIENT.VALUE';
			$arFilter[ $contactFilter ] = $this->getUserId();
		}

		return $iblock::getList([
			'filter'      => $arFilter,
			'count_total' => true,
		])->getCount();
	}


	/**
	 * Обновление среднего рейтинга пользователя
	 * Если отзыв добавляет заказчик, то обновляем рейтинг мастера и наоборот
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	public static function updateMainRatingField(int $userId = 0, ?int $userType = CDbzConstants::DBZ_WORKER_TYPE): bool {
		Loader::includeModule('iblock');
		$iblock = Iblock::wakeUp(CDbzConstants::DBZ_FEEDBACKS_IBLOCK_ID)->getEntityDataClass();

		if ( ! $userId) {
			return false;
		}

		$contactFilter              = $userType === CDbzConstants::DBZ_WORKER_TYPE ? 'CLIENT.VALUE' : 'MASTER.VALUE';
		$arFilter[ $contactFilter ] = $userId;

		$obRes = $iblock::getList([
			'select'  => ["RATING_AVG"],
			'filter'  => $arFilter,
			'runtime' => [
				"RATING_AVG" => [
					'data_type'  => "float",
					'expression' => array("round(avg(%s), 2)", "MAIN_RATING.VALUE")
				]
			]
		]);

		if ($rating = $obRes->Fetch()) {
			$obContact = new CDbzContact($userId);
			$obContact->updateContactField('UF_CRM_RATING_MAIN', $rating["RATING_AVG"]);
		}

		return true;
	}


	/**
	 * @throws \Bitrix\Main\SystemException
	 */
	private function validateUserId(): void {
		if ( ! $this->getUserId()) {
			throw new SystemException("Не определн пользоватеель");
		}
	}


	/**
	 * @throws \Bitrix\Main\SystemException
	 */
	private function validateOrderId(): void {
		if ( ! $this->getOrderId()) {
			throw new SystemException("Не определен заказ");
		}
	}


	/**
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function getIblockEntity(): string|\Bitrix\Iblock\ORM\CommonElementTable {
		Loader::includeModule('iblock');

		return Iblock::wakeUp($this->getIblockId())->getEntityDataClass();
	}


	#[Pure] private function getIblockId(): int {
		return CDbzConstants::DBZ_FEEDBACKS_IBLOCK_ID;
	}


	/**
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function isFeedbackExist(): bool {
		$arFeedback = $this->getUserOrderFeedback();

		return ! empty($arFeedback);
	}

	/**
	 * @param int $orderId
	 */
	public function setOrderId(int $orderId): void {
		$this->orderId = $orderId;
	}

	/**
	 * @return int
	 */
	public function getOrderId(): int {
		return $this->orderId;
	}


}