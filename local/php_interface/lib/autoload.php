<?php

use Bitrix\Main\Loader;

//Автозагрузка наших классов
Loader::registerAutoLoadClasses(null, [
	'lib\Helpers\CDbzBase'               => APP_CLASS_FOLDER . 'Helpers/CDbzBase.php',
	'lib\Helpers\CDbzHighloadHelper'     => APP_CLASS_FOLDER . 'Helpers/CDbzHighloadHelper.php',
	'lib\Helpers\CDbzConstants'          => APP_CLASS_FOLDER . 'Helpers/CDbzConstants.php',
	'lib\Helpers\CApiHelpers'            => APP_CLASS_FOLDER . 'Helpers/CApiHelpers.php',
	'lib\Helpers\CDadataHelpers'         => APP_CLASS_FOLDER . 'Helpers/CDadataHelpers.php',
	'lib\Helpers\CDbzFilesHelper'        => APP_CLASS_FOLDER . 'Helpers/CDbzFilesHelper.php',
	'lib\Helpers\CDbzResponse'           => APP_CLASS_FOLDER . 'Helpers/CDbzResponse.php',
	'lib\Location\CDbzLocationResponse'  => APP_CLASS_FOLDER . 'Location/CDbzLocationResponse.php',
	'lib\UserType\CUserTypeAddress'      => APP_CLASS_FOLDER . 'UserType/CUserTypeAddress.php',
	'lib\UserType\CUserTypeCheckbox'     => APP_CLASS_FOLDER . 'UserType/CUserTypeCheckbox.php',
	'lib\UserType\CUserTypeWorkType'     => APP_CLASS_FOLDER . 'UserType/CUserTypeWorkType.php',
	'lib\UserData\CUserData'             => APP_CLASS_FOLDER . 'UserData/CUserData.php',
	'lib\Order\CDbzOrder'                => APP_CLASS_FOLDER . 'Order/CDbzOrder.php',
	'lib\Order\CDbzOrders'               => APP_CLASS_FOLDER . 'Order/CDbzOrders.php',
	'lib\Order\CDbzOrderFavorites'       => APP_CLASS_FOLDER . 'Order/CDbzOrderFavorites.php',
	'lib\Order\CDbzOrderResponse'        => APP_CLASS_FOLDER . 'Order/CDbzOrderResponse.php',
	'lib\Contact\CDbzContact'            => APP_CLASS_FOLDER . 'Contact/CDbzContact.php',
	'lib\Contact\CDbzContactResponse'    => APP_CLASS_FOLDER . 'Contact/CDbzContactResponse.php',
	'lib\Packages\CDbzPackages'          => APP_CLASS_FOLDER . 'Packages/CDbzPackages.php',
	'lib\Responds\CDbzResponds'          => APP_CLASS_FOLDER . 'Responds/CDbzResponds.php',
	'lib\Feedbacks\CDbzFeedbacks'        => APP_CLASS_FOLDER . 'Feedbacks/CDbzFeedbacks.php',
	'lib\Feedbacks\CDbzFeedbackResponse' => APP_CLASS_FOLDER . 'Feedbacks/CDbzFeedbackResponse.php',
	'lib\Work\CDbzWork'                  => APP_CLASS_FOLDER . 'Work/CDbzWork.php',
	'lib\Work\CDbzWorkResponse'          => APP_CLASS_FOLDER . 'Work/CDbzWorkResponse.php',
	'lib\Work\CDbzCategoryResponse'      => APP_CLASS_FOLDER . 'Work/CDbzCategoryResponse.php',
	'lib\Notifications\CDbzNotification' => APP_CLASS_FOLDER . 'Notifications/CDbzNotification.php',
	'lib\News\CDbzNews'                  => APP_CLASS_FOLDER . 'News/CDbzNews.php',
	'lib\Main\CDbzMain'                  => APP_CLASS_FOLDER . 'Main/CDbzMain.php'
]);
