<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 09.08.2021
 * Time: 19:31
 * Project: dombezzabot.net
 */

namespace lib\packages;

use CIBlockElement;
use CModule;
use lib\helpers\CDbzConstants;

class CDbzPackages {

	private $contactId;
	private const PACKAGE_CONTACT_PROPERTY = "PROPERTY_KONTAKT"; // Код свойства "Контакт"
	private const PACKAGE_ACTIVE_PROPERTY = "PROPERTY_AKTIVEN"; // Код свойства "Актиность"
	private const BALANCE_RECORD_TYPE_PROPERTY = "PROPERTY_TIP_ZAPISI"; // Код свойства "Тип записи"
	private const PACKAGE_RECORD_TYPE_ID = 31232; // ID "типа баланса" - Пакет


	/**
	 * CDbzPackages constructor.
	 *
	 * @param $userId
	 */
	public function __construct($contactId) {
		$this->contactId = $contactId;
	}


	/**
	 * Информация о подключенных пакетах у контакта
	 */
	public function getUserPackagesInfo() {
		CModule::IncludeModule("crm");
		//активные пакеты мастера
		$arSelect = array("ID", "IBLOCK_ID", "NAME"); //пакет, осталось ед., время заверш.
		$arFilter = array(
			"IBLOCK_ID"                        => CDbzConstants::DBZ_BALANCE_IBLOCK_ID,
			self::PACKAGE_CONTACT_PROPERTY     => $this->contactId,
			self::PACKAGE_ACTIVE_PROPERTY      => CDbzConstants::DBZ_YES_VALUE,
			self::BALANCE_RECORD_TYPE_PROPERTY => self::PACKAGE_RECORD_TYPE_ID
		);

		// TODO Доработать вывод подрбоной информации для свойств Привязка к элементу
		$res        = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		$arPackages = [];
		while ($ob = $res->GetNextElement()) {
			$arProps = $ob->GetProperties();

			$arPackage = [];
			foreach ($arProps as $arProp) {
				$arPackage[ $arProp["CODE"] ] = $arProp["VALUE"];
			}
			$arPackages[] = $arPackage;
		};

		return $arPackages;
	}


	/**
	 * Возвращает строку которая сообщает на какие заказы распространаяется тариф
	 *
	 * @param int $priceStart
	 * @param int $priceEnd
	 *
	 * @return string
	 */
	public static function getPackageRangeTitle($priceStart = 0, $priceEnd = 0) {

		$title = "На любые заказы";

		if ( ! ! $priceStart && ! $priceEnd) {
			$title = "На заказы от {$priceStart}₽";
		} elseif ( ! $priceStart && ! ! $priceEnd) {
			$title = "На заказы до {$priceEnd}₽";
		} elseif ( ! ! $priceStart && ! ! $priceEnd) {
			$title = "На заказы от {$priceStart}₽ до {$priceEnd}₽";
		}

		return $title;
	}


	/**
	 * @param int $orderCount
	 * @param int $daysRemain
	 * @param int $hoursRemain
	 *
	 * @return string|null
	 */
	public static function getPackageRemainTitle($orderCount= 0, $daysRemain = 0, $hoursRemain = 0) {

		// Строка об остатке количества заявок
		$title = null;
		if ( ! ! $orderCount) {
			$title = "{$orderCount} заяв. на";
			$title .= $daysRemain ? " {$daysRemain} дн." : " {$hoursRemain} ч.}";
		}

		return $title;
	}



}