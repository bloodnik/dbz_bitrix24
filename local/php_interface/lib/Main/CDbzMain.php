<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 15.09.2021
 * Time: 20:39
 * Project: dombezzabot.net
 */

namespace lib\main;

use Bitrix\Main\SystemException;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use lib\contact\CDbzContact;
use lib\helpers\CApiHelpers;
use lib\helpers\CDbzBase;
use lib\helpers\CDbzConstants;
use lib\location\CDbzLocationResponse;
use lib\news\CDbzNews;

class CDbzMain extends CDbzBase {

	private ?string $appVersion = null;

	/**
	 * @throws \Bitrix\Main\SystemException
	 */
	#[ArrayShape(["counts" => "array", "news" => "array", "should_update_app" => "bool"])]
	public function getMain(): array {

		if ( ! $this->getAppVersion()) {
			throw new SystemException("Не определдена версия приложения");
		}

		return [
			"counts"            => $this->getCounts(),
			"news"              => CDbzNews::getNews(),
			"should_update_app" => CApiHelpers::checkIncorrectAppVersion($this->getAppVersion())
		];
	}


	/**
	 * @throws \Bitrix\Main\SystemException
	 */
	#[ArrayShape(["order_closed" => "null", "order_active" => "null", "notifications" => "null", "support" => "null", "profile" => "null", "my_orders" => "null"])]
	private function getCounts(): array {
		$notificationsCount = 0;
		if ($this->getUserId()) {
			$obContact = new CDbzContact($this->getUserId());
			$obContact->setUserType($this->getUserType());
			$notificationsCount = $obContact->getContactNotificationsCount();
		}


		return [
			"order_closed"  => 0,
			"order_active"  => 0,
			"notifications" => $notificationsCount,
		];
	}


	/**
	 * Контент для стартового слайдер
	 *
	 * @param \lib\location\CDbzLocationResponse $location
	 *
	 * @return array[]
	 */
	#[Pure]
	public function getStartContent(
		CDbzLocationResponse $location
	): array {

		if ($this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE) {
			$result = [
				[
					"title"   => "Дом Без Забот - Мастер",
					"summary" => "Это сервис для поиска работы, который сочетает в себе скорость, простоту и актуальные предложения.",
					"cover"   => CDbzConstants::DBZ_PORTAL_URL . "/upload/info_pictures/w1.png"
				],
				[
					"title"   => "Новые заказы",
					"summary" => "Мы подбираем для Вас заказы, основываясь на выбранном городе и выбранных специализациях.",
					"cover"   => CDbzConstants::DBZ_PORTAL_URL . "/upload/info_pictures/w2.png"
				],
				[
					"title"   => "Предлагайте свои услуги",
					"summary" => "Выбирайте подходящие заказы и предлагайте в них свои услуги. Так Вы сможете связаться с заказчиком и подробнее обсудить объем работ.",
					"cover"   => CDbzConstants::DBZ_PORTAL_URL . "/upload/info_pictures/w3.png"
				],
			];
		} else {
			$result = [
				[
					"title"   => "Дом Без Забот",
					"summary" => "Это сервис для поиска исполнителей по множеству направлений. Вы сможете быстро оформить заявку, а мы подберем для Вас лучших мастеров. ",
					"cover"   => CDbzConstants::DBZ_PORTAL_URL . "/upload/info_pictures/c1.png"
				],
				[
					"title"   => "Создавайте заказы",
					"summary" => "Воспользуйтесь поиском или выберите категорию, чтобы найти нужную Вам работу.",
					"cover"   => CDbzConstants::DBZ_PORTAL_URL . "/upload/info_pictures/c2.png"
				],
				[
					"title"   => "Выбирайте исполнителей",
					"summary" => "Мы будем сообщать Вам о новых мастерах, которые предложили свои услуги, Вы сможете сразу связаться с ними.",
					"cover"   => CDbzConstants::DBZ_PORTAL_URL . "/upload/info_pictures/c3.png"
				],
				[
					"title"   => "Скрывайте контакты",
					"summary" => "Вы можете отображать Ваши контакты только для выбранного исполнителя.",
					"cover"   => CDbzConstants::DBZ_PORTAL_URL . "/upload/info_pictures/c4.png"
				],
			];
		}


		return $result;
	}


	/**
	 * @return string|null
	 */
	public function getAppVersion(): ?string {
		return $this->appVersion;
	}

	/**
	 * @param string|null $appVersion
	 */
	public function setAppVersion(?string $appVersion): void {
		$this->appVersion = $appVersion;
	}


}