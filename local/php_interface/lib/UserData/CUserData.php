<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 14.07.2021
 * Time: 20:55
 * Project: dombezzabot.net
 */

namespace lib\userdata;


use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use CCrmContact;
use CCrmFieldMulti;
use CIBlockElement;
use CIBlockPropertyEnum;
use JetBrains\PhpStorm\Pure;
use lib\helpers\CApiHelpers;
use lib\helpers\CDbzBase;
use lib\helpers\CDbzConstants;

// Класс для работы с анкетой мастера
class CUserData extends CDbzBase {

	//	ID инфоблока анкеты
	public const MASTER_QUESTIONNARE_IBLOCK_ID = 140;
	public const CLIENT_QUESTIONNARE_IBLOCK_ID = 142;

	// Код свойства Фотография пользователя в анкете
	public const USER_PHOTO_PROPERTY_CODE = "PHOTO";

	/**
	 * Типы данных пользовательской анкеты
	 */
	public const NUMBER_TYPE = 0;
	public const STRING_TYPE = 1;
	public const PHONE_TYPE = 2;
	public const LIST_TYPE = 3;
	public const BOOLEAN_TYPE = 4;
	public const IMAGE_TYPE = 5;

	/**
	 * Соответствие типов данных с типами Битрикс
	 */
	public const TYPES_MAPPING = [
		'N' => self::NUMBER_TYPE,  // Число
		'S' => self::STRING_TYPE,  // Строка
		'L' => self::LIST_TYPE,  // Список
		'F' => self::IMAGE_TYPE  // Изображение
	];


	/**
	 * @param string $filterId - Индетификатор или код свойства, массив которого надо вернуть.
	 * Если пусто, то возвращем все свойтва
	 *
	 * @return array
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function getUserData(string $filterId = ""): array {

		return $this->getContactData($filterId);
	}


	/**
	 * Возвращает значение свойства из анкеты
	 *
	 * @param string $filterId
	 *
	 * @return mixed
	 */
	public function getUserDataValue(string $filterId = ""): mixed {
		$arProps = $this->getQuestionnaireProps();
		if ($filterId) {
			return $arProps[ $filterId ]["VALUE"];
		}

		return "";
	}


	/**
	 *  Данные контакта
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function getContactData($filterId): array {
		// Данные контката CRM
		$arProfileData = $this->getProfileData();

		// Данные анктеы мастера. Если не существует запись анкеты то создаем новую
		if ( ! ($arProps = $this->getQuestionnaireProps())) {
			$this->createContactQuestionnaire();
			$arProps = $this->getQuestionnaireProps();
		}
		$response = $this->getQuestionnaireData($arProps);

		$resultData = array_merge($arProfileData, $response);

		if ($filterId) {
			$resultData = array_values(array_filter($resultData, static function($item) use ($filterId) {
				return $item["id"] === $filterId;
			}));
		}

		return $resultData;
	}


	/**
	 * Возвращает масссив данных контакта CRM
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function getProfileData(): array {
		Loader::includeModule("crm");

		$arOrder          = array();
		$arFilter         = array("=ID" => $this->getUserId());
		$arGroupBy        = false;
		$arNavStartParams = false;
		$arSelectFields   = $this->getUserDataEditedFieldsArray();
		$res              = CCrmContact::GetListEx($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);

		$arUserDataItem = [];
		while ($ob = $res->GetNext()) {

			// Определяем номер телефона контакта
			$arFilterPhone = [
				'ENTITY_ID'  => 'CONTACT',
				'ELEMENT_ID' => $this->getUserId(),
				'TYPE_ID'    => 'PHONE',
				'VALUE_TYPE' => 'MOBILE',
			];
			$arPhone       = CCrmFieldMulti::GetListEx([], $arFilterPhone, false, ['nTopCount' => 1], ['VALUE'])->fetch();
			$phoneValue    = preg_replace("/[^\d]+/", '', $arPhone["VALUE"]);


			$arUserDataItem = array(
				array(
					"id"         => "NAME",
					"type"       => self::STRING_TYPE,
					"title"      => "Имя",
					"hint"       => "Укажите ваше имя",
					"value"      => $ob["NAME"] ?? "",
					"max_length" => 20
				),
				array(
					"id"         => "LAST_NAME",
					"type"       => self::STRING_TYPE,
					"title"      => "Фамилия",
					"hint"       => "Укажите вашу фамилию",
					"value"      => $ob["LAST_NAME"] ?? "",
					"max_length" => 20
				),
				array(
					"id"         => "SECOND_NAME",
					"type"       => self::STRING_TYPE,
					"title"      => "Отчество",
					"hint"       => "Укажите ваше отчество",
					"value"      => $ob["SECOND_NAME"] ?? "",
					"max_length" => 20
				),
				array(
					"id"         => "UF_CRM_CONTACT_PHONE",
					"type"       => self::PHONE_TYPE,
					"title"      => "Контактный номер",
					"hint"       => "Укажите контактный номер телефона",
					"value"      => CApiHelpers::formatPhoneNumber($ob["UF_CRM_CONTACT_PHONE"]) ?? CApiHelpers::formatPhoneNumber($arPhone["VALUE"]) ?? "",
					"max_length" => 10
				),
				array(
					"id"         => "UF_CRM_TELEGRAM",
					"type"       => self::STRING_TYPE,
					"title"      => "Логин Telegram",
					"hint"       => "",
					"value"      => $ob["UF_CRM_TELEGRAM"] ?? "",
					"max_length" => 30
				),
				array(
					"id"         => "UF_CRM_WHATSAPP",
					"type"       => self::PHONE_TYPE,
					"title"      => "Номер Whatsapp",
					"hint"       => "",
					"value"      => CApiHelpers::formatPhoneNumber($ob["UF_CRM_WHATSAPP"]) ?? CApiHelpers::formatPhoneNumber($arPhone["VALUE"]) ?? "",
					"max_length" => 10
				),
			);

			// Персональные поля для клиента
			if ($this->getUserType() === CDbzConstants::DBZ_CLIENT_TYPE) {
				$arUserDataItem[] = array(
					"id"         => "UF_CRM_HIDE_CONTACTS",
					"type"       => self::BOOLEAN_TYPE,
					"title"      => "Скрыть контакты",
					"hint"       => "Во всех заказах Ваши контактные данные будут видны только выбранному мастеру",
					"value"      => $ob["UF_CRM_HIDE_CONTACTS"] ? "true" : "false",
					"max_length" => 1
				);
			} else {
				//Персональные поля для мастера
			}
		}

		return $arUserDataItem;
	}


	/**
	 * Возвращает масссив допольнительных свойств элемента
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function getQuestionnaireProps(): array {
		Loader::includeModule("iblock");
		$arSelect = array("ID", "IBLOCK_ID");

		$arFilter = array("IBLOCK_ID" => $this->getIblockId(), "PROPERTY_CONTACT" => "C_" . $this->getUserId());
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

		$arProps = [];
		if ($ob = $res->GetNextElement()) {
			$arProps = $ob->GetProperties();
		}

		return $arProps;
	}


	/**
	 * Создаем новую запись анкеты мастера
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function createContactQuestionnaire(): void {
		Loader::includeModule("iblock");

		$el                 = new CIBlockElement;
		$PROP               = array();
		$PROP["CONTACT"]    = "C_" . $this->getUserId();
		$arLoadProductArray = array(
			"IBLOCK_ID"       => $this->getIblockId(),
			"PROPERTY_VALUES" => $PROP,
			"NAME"            => "Анкета мастера_" . $this->getUserId(),
			"ACTIVE"          => "Y",
		);

		$el->Add($arLoadProductArray);
	}


	/**
	 * Формирует массив с данными акнеты мастера
	 *
	 * @param $arProps
	 *
	 * @return array
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \JsonException
	 */
	private function getQuestionnaireData($arProps): array {
		$arResponse = [];

		foreach ($arProps as $arProp) {
			if ($arProp["CODE"] === 'CONTACT') {
				continue;
			}

			$arUserDataItem["id"]         = $arProp["ID"];
			$arUserDataItem["type"]       = self::TYPES_MAPPING[ $arProp["PROPERTY_TYPE"] ];
			$arUserDataItem["title"]      = $arProp["NAME"];
			$arUserDataItem["hint"]       = $arProp["HINT"];
			$arUserDataItem["max_length"] = (int) $arProp["SORT"];

			switch ($arProp["PROPERTY_TYPE"]) {
				case 'N': //Число

					if ($arProp["USER_TYPE"] === 'SASDCheckboxNum') {
						$arUserDataItem["type"]  = self::BOOLEAN_TYPE;
						$arUserDataItem["value"] = $arProp["VALUE"] ? "true" : "false";
					} else {
						$arUserDataItem["value"] = $arProp["VALUE"];
					}

					break;
				case 'S': //Строка
					$arUserDataItem["value"] = $arProp["VALUE"];
					break;
				case 'L': //Список
					$propEnums               = $this->getIblockListPropertyEnums($arProp["IBLOCK_ID"], $arProp["CODE"]);
					$arUserDataItem["value"] = array();
					$arUserDataItemTmp       = array();
					foreach ($propEnums as $propEnum) {
						$arValue = array(
							"id"      => $propEnum["ID"],
							"title"   => $propEnum["VALUE"],
							"enabled" => in_array($propEnum["ID"], (array) $arProp["VALUE_ENUM_ID"], true),
						);

						$arUserDataItemTmp[] = $arValue;
					}
					$arUserDataItem["value"] = json_encode($arUserDataItemTmp, JSON_THROW_ON_ERROR);

					$arProp["VALUE_ENUM_ID"] = is_array($arProp["VALUE_ENUM_ID"]) ? $arProp["VALUE_ENUM_ID"] : [];
					break;
				case 'F': //Файл
					// Если множественный файл
					$arUserDataItemTmp = array();
					if (is_array($arProp["VALUE"])) {
						foreach ($arProp["VALUE"] as $fileValue) {
							$arUserDataItemTmp[] = array(
								"id"  => (int) $fileValue,
								"url" => "https://" . $_SERVER["SERVER_NAME"] . \CFile::GetPath($fileValue),
							);
						}
					} elseif ( ! empty($arProp["VALUE"])) {
						$arUserDataItemTmp[] = [
							"id"  => (int) $arProp["VALUE"],
							"url" => "https://" . $_SERVER["SERVER_NAME"] . \CFile::GetPath($arProp["VALUE"]),
						];
					}
					$arUserDataItem["value"] = json_encode($arUserDataItemTmp, JSON_THROW_ON_ERROR);
					break;
				default:
					$arUserDataItem["value"]      = $arProp["VALUE"];
					$arUserDataItem["max_length"] = 20;
			}
			$arResponse[] = $arUserDataItem;
		}

		return $arResponse;
	}


	/**
	 * Сохранение данных контакта
	 *
	 * @param $data
	 *
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 * @throws \JsonException
	 */
	public function setUserData($data): void {
		$arProfileData       = [];
		$arQuestionnaireData = [];

		foreach ($data as $dataItem) {
			if (in_array($dataItem["id"], $this->getUserDataEditedFieldsArray(), true)) {
				$arProfileData[] = $dataItem;
			} else {
				$arQuestionnaireData[] = $dataItem;
			}
		}

		if ( ! empty($arProfileData)) {
			$this->updateUserProfile($arProfileData);
		}

		if ( ! empty($arQuestionnaireData)) {
			$this->updateUserQuestionnaire($arQuestionnaireData);
		}
	}


	/**
	 * Поля контакта CRM доступные для редактирвоания
	 * @return string[]
	 */
	#[Pure] private function getUserDataEditedFieldsArray(): array {

		// Общие поля
		$arEditedFields = ["NAME", "LAST_NAME", "SECOND_NAME", "UF_CRM_TELEGRAM", "UF_CRM_WHATSAPP", "UF_CRM_CONTACT_PHONE"];

		// Поля клиента
		if ($this->getUserType() === CDbzConstants::DBZ_CLIENT_TYPE) {
			$arEditedFields[] = "UF_CRM_HIDE_CONTACTS";
		}

		return $arEditedFields;
	}


	/**
	 * Обновление данных профиля контакта CRM
	 *
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	private function updateUserProfile($arProfileData): void {
		Loader::includeModule("crm");
		$entity = new CCrmContact;
		$fields = array();

		foreach ($arProfileData as $arProfileDatum) {

			$arProfileDatum                  = $this->prepareField($arProfileDatum);
			$fields[ $arProfileDatum["id"] ] = $arProfileDatum["value"];
		}

		$this->validateFields($fields);

		if ($entity->update($this->getUserId(), $fields)) {
			//обновление таблицы регистраций (корректировка)
			CApiHelpers::addProfileUpdateLog($this->getUserId());
		}

	}


	/**
	 * Приведение в соответствие поля перед обновлением сущностей
	 *
	 * @param $arData
	 *
	 * @return mixed
	 */
	private function prepareField($arData): mixed {

		switch ($arData["id"]) {
			case "UF_CRM_TELEGRAM":
				// Убираем @ перед логином телеграмм
				$arData["value"] = preg_replace("/^@/", '', $arData["value"]);
				$result          = $arData;
				break;
			case "UF_CRM_HIDE_CONTACTS":
				$arData["value"] = $arData["value"] === "true" ? 1 : 0;
				$result          = $arData;
				break;
			default:
				$result = $arData;
				break;
		}

		return $result;
	}


	/**
	 * Валидация полей
	 *
	 * @param $arFields
	 *
	 * @throws \Bitrix\Main\SystemException
	 */
	private function validateFields($arFields): void {

		$arErrors = [];

		if (empty($arFields["UF_CRM_TELEGRAM"]) && empty($arFields["UF_CRM_WHATSAPP"]) && empty($arFields["UF_CRM_CONTACT_PHONE"])) {
			$arErrors[] = "Укажите контактные даннные";
		}

		if (empty($arFields["NAME"]) || empty($arFields["LAST_NAME"])) {
			$arErrors[] = "Заполните имя и фамилию";
		}


		if ( ! empty($arErrors)) {
			throw new SystemException(implode("\n", $arErrors));
		}
	}


	/**
	 * Обновление анкеты мастера
	 *
	 * @param $data
	 *
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \JsonException
	 */
	private function updateUserQuestionnaire($data): void {
		Loader::includeModule("iblock");

		$arSelect = array("ID", "IBLOCK_ID");
		$arFilter = array("IBLOCK_ID" => $this->getIblockId(), "PROPERTY_CONTACT" => "C_" . $this->getUserId());
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$arProps  = $ob->GetProperties();

			foreach ($data as $arItem) {
				switch ($arItem["type"]) {
					case self::NUMBER_TYPE:
					case self::STRING_TYPE:
						$result = CIBlockElement::SetPropertyValueCode($arFields["ID"], $arItem["id"], $arItem["value"]);
						break;
					case self::LIST_TYPE:
						$arValuesId = array();
						foreach (json_decode($arItem["value"], true, 512, JSON_THROW_ON_ERROR) as $arValue) {
							if ( ! $arValue["enabled"]) {
								continue;
							}
							$arValuesId[] = $arValue['id'];
						}

						CIBlockElement::SetPropertyValueCode($arFields["ID"], $arItem["id"], $arValuesId);
						break;
					case self::BOOLEAN_TYPE:
						CIBlockElement::SetPropertyValueCode($arFields["ID"], $arItem["id"], $arItem["value"] === "true" ? 1 : 0);
						break;
					case self::IMAGE_TYPE:

						// Добавление файла
						if (isset($arItem["value"]["tmp_name"])) {
							CIBlockElement::SetPropertyValueCode($arFields["ID"], $arItem["id"], $arItem["value"]);
						} else {
							// Удаляем файлы.
							$value = json_decode($arItem["value"], true, 512, JSON_THROW_ON_ERROR);

							// Получаем текущие id файлов. Сравниваем с теми которые пришли из приложения, которых не хватает удаляем
							$arFilePropKey = array_search($arItem["id"], array_column($arProps, 'ID', "CODE"), true);

							if ($arProps[ $arFilePropKey ]["MULTIPLE"] === "N") {
								if (empty($value)) {
									CIBlockElement::SetPropertyValueCode($arFields["ID"], $arFilePropKey, [$arProps[ $arFilePropKey ]["PROPERTY_VALUE_ID"] => ["VALUE" => ["del" => "Y"]]]);
								}
							} else { // Множественноые свойство файл
								$currentFileIds = isset($arProps[ $arFilePropKey ]) ? $arProps[ $arFilePropKey ]["VALUE"] : "";
								$newFileIds     = array_map(static fn($fileData) => $fileData["id"], $value);

								// Ищем PROPERTY_VALUE_ID удаляемых файлов. Только зная их можно удалить файл
								$deletedFilePropId = [];
								foreach ($currentFileIds as $key => $currentFileId) {
									if ( ! in_array($currentFileId, $newFileIds, true)) {
										$deletedFilePropId[] = $arProps[ $arFilePropKey ]["PROPERTY_VALUE_ID"][ $key ];
									}
								}

								// Удаляем файл по его PROPERTY_ID файла
								$arFiles = [];
								foreach ($deletedFilePropId as $arDeletedFilePropertyId) {
									$arFiles[ $arDeletedFilePropertyId ] = array("VALUE" => ["del" => "Y"]);
								}
								CIBlockElement::SetPropertyValueCode($arFields["ID"], $arFilePropKey, $arFiles);
							}
						}
						break;
					default:
						break;
				}
			}
		}

	}


	/**
	 * Возвращет массив ссылки и id последного загруженного файла по ID свойства
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function getLastUserTypeFile($propID): array {
		Loader::includeModule("iblock");
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_" . $propID);

		$arFilter = array("IBLOCK_ID" => $this->getIblockId(), "PROPERTY_CONTACT" => "C_" . $this->getUserId());
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);

		$result = [];
		if ($ob = $res->GetNextElement()) {
			$arProperty = $ob->GetProperty($propID);
			if (is_array($arProperty["VALUE"])) {
				$result = ["id" => end($arProperty["VALUE"]), "url" => CDbzConstants::DBZ_PORTAL_URL . \CFile::GetPath(end($arProperty["VALUE"]))];
			} else {
				$result = ["id" => $arProperty["VALUE"], "url" => CDbzConstants::DBZ_PORTAL_URL . \CFile::GetPath($arProperty["VALUE"])];
			}
		}

		return $result;
	}


	/**
	 * Возвращает список значений пользовательского типа "Список"
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function getIblockListPropertyEnums($iblockId, $propertyCode): array {
		Loader::includeModule("iblock");

		$result         = [];
		$property_enums = CIBlockPropertyEnum::GetList(array("ID" => "ASC", "SORT" => "ASC"), array("IBLOCK_ID" => $iblockId, "CODE" => $propertyCode));
		while ($enum_fields = $property_enums->GetNext()) {
			$result[] = [
				"ID"    => $enum_fields["ID"],
				"VALUE" => $enum_fields["VALUE"]
			];
		}

		return $result;
	}


	#[Pure] private function getIblockId(): int {
		return $this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? self::MASTER_QUESTIONNARE_IBLOCK_ID : self::CLIENT_QUESTIONNARE_IBLOCK_ID;
	}


}