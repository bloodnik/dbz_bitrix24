<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 12.08.2021
 * Time: 20:53
 * Project: dombezzabot.net
 */

namespace lib\contact;

use lib\helpers\CDbzResponse;

class CDbzContactResponse extends CDbzResponse {

	protected $id = 0;
	protected $title = "";
	protected $summary = "";
	protected $rating = 0.0;
	protected $orders_amount = 0;
	protected $feedbacks_amount = 0;
	protected $contacts = null;
	protected $photo_url = null;
	protected $is_checked = false;

	/**
	 * @return bool
	 */
	public function isIsChecked(): bool {
		return $this->is_checked;
	}

	/**
	 * @param bool $is_checked
	 */
	public function setIsChecked(bool $is_checked): void {
		$this->is_checked = $is_checked;
	}

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getSummary(): string {
		return $this->summary;
	}

	/**
	 * @param string $summary
	 */
	public function setSummary(string $summary): void {
		$this->summary = $summary;
	}

	/**
	 * @return float
	 */
	public function getRating(): float {
		return $this->rating;
	}

	/**
	 * @param float $rating
	 */
	public function setRating(float $rating): void {
		$this->rating = $rating;
	}

	/**
	 * @return int
	 */
	public function getOrdersAmount(): int {
		return $this->orders_amount;
	}

	/**
	 * @param int $orders_amount
	 */
	public function setOrdersAmount(int $orders_amount): void {
		$this->orders_amount = $orders_amount;
	}

	/**
	 * @return int
	 */
	public function getFeedbacksAmount(): int {
		return $this->feedbacks_amount;
	}

	/**
	 * @param int $feedbacks_amount
	 */
	public function setFeedbacksAmount(int $feedbacks_amount): void {
		$this->feedbacks_amount = $feedbacks_amount;
	}

	/**
	 * @return array
	 */
	public function getContacts(): array|null {
		return $this->contacts;
	}

	/**
	 * @param array | null $contacts
	 */
	public function setContacts(array|null $contacts): void {
		$this->contacts = $contacts;
	}

	/**
	 * @return string
	 */
	public function getPhotoUrl(): string | null {
		return $this->photo_url;
	}

	/**
	 * @param string $photo_url
	 */
	public function setPhotoUrl(string | null $photo_url): void {
		$this->photo_url = $photo_url;
	}


}