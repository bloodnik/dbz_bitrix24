<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 09.08.2021
 * Time: 19:05
 * Project: dombezzabot.net
 */

namespace lib\contact;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\SystemException;
use CCrmContact;
use CCrmDeal;
use CModule;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use lib\feedbacks\CDbzFeedbacks;
use lib\helpers\CApiHelpers;
use lib\helpers\CDbzConstants;

class CDbzContact {

	private int $userId = 0;
	private int $userType = 0;

	// Значения поля "Регистрация в новом приложении"
	// ID элементов Закачик и Мастер
	public const NEW_APP_REGISTER_MASTER_ID = 31169;
	public const NEW_APP_REGISTER_CLIENT_ID = 31170;

	/**
	 * Код пользовательского поля "Вид работы"
	 */
	public const WORKS_USER_FIELD_CODE = "UF_CRM_WORK_TYPE";

	/**
	 * Код пользовательского поля "Категория работы"
	 */
	public const CATEGORIES_USER_FIELD_CODE = "UF_CRM_UNDER_CATEGORY";


	/**
	 * CDbzContact constructor.
	 *
	 * @param $userId
	 */
	public function __construct($userId) {
		$this->userId = $userId;
	}

	/**
	 * Возвращает струтуру OrderUser
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getContactResponse($showContactInfo = false): array {
		$arContact = $this->getContactData();

		$contactResponse = new CDbzContactResponse();
		$contactResponse->setId($this->userId);
		$contactResponse->setTitle($arContact["FULL_NAME"]);
		$contactResponse->setRating($arContact["UF_CRM_RATING_MAIN"] ? (float) $arContact["UF_CRM_RATING_MAIN"] : 0);

		$obFeedback = new CDbzFeedbacks($this->getUserId(), $this->userType);
		$contactResponse->setFeedbacksAmount($obFeedback->getUserFeedbacksCount());

		$arContact && $contactResponse->setPhotoUrl($arContact["PHOTO"] ? CDbzConstants::DBZ_PORTAL_URL . \CFile::GetPath($arContact["PHOTO"]) : null);

		$arContactPhone = $this->getContactPhone();
		$userContacts   = [
			"phone"     => $arContact["UF_CRM_CONTACT_PHONE"] ?? $arContactPhone,
			"whats_app" => $arContact["UF_CRM_WHATSAPP"] ?? $arContactPhone,
			"telegram"  => $arContact["UF_CRM_TELEGRAM"],
			"viber"     => null
		];
		$contactResponse->setContacts($showContactInfo ? $userContacts : null);

		$contactResponse->setOrdersAmount($this->getContactOrdersCount());

		$contactResponse->setIsChecked((bool) $arContact["UF_CRM_IS_PROFILE_CHECKED"]);

		return $contactResponse->toArray();
	}


	/**
	 * Массив всех данных по контакту CRM_CONTACT
	 *
	 * @return array
	 * @throws SystemException
	 */
	public function getContactData(): array {
		CModule::IncludeModule("crm");
		$arOrder         = array("id" => "asc");
		$arSelectFields  = ["*", "UF_*"];
		$arFilter['=ID'] = $this->userId;

		$obContact = \CCrmContact::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);
		if ($arContact = $obContact->Fetch()) {
			return $arContact;
		}

		throw new SystemException("Контакт не найден");
	}


	/**
	 * Возвращает номер телефона контакта
	 * @return string|null
	 */
	public function getContactPhone(): string|null {
		// Определяем номер телефона контакта
		$arFilterPhone = [
			'ENTITY_ID'  => 'CONTACT',
			'ELEMENT_ID' => $this->userId,
			'TYPE_ID'    => 'PHONE',
			'VALUE_TYPE' => 'WORK',
		];
		$arPhone       = \CCrmFieldMulti::GetListEx([], $arFilterPhone, false, ['nTopCount' => 1], ['VALUE'])->fetch();

		return $arPhone["VALUE"];
	}


	/**
	 * Установлен ли признак "Скрывать контакты"
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function isContactHidden(): bool {
		$arContact = $this->getContactData();

		return (bool) $arContact["UF_CRM_HIDE_CONTACTS"];
	}


	/**
	 * Количество выполненных заказов
	 * у Мастера - количество выполненных заказов
	 * у Заказчика - количество опубликованных заказов
	 * @throws LoaderException
	 */
	private function getContactOrdersCount(): int {
		Loader::includeModule("crm");

		$stages = array('C7:WON');
		if ($this->userType == CDbzConstants::DBZ_WORKER_TYPE) {
			$arFilter = array("=CONTACT_ID" => $this->userId, "STAGE_ID" => $stages);
		} else {
			$arFilter = array("=UF_CRM_MASTER" => $this->userId, "STAGE_ID" => $stages);
		}

		$res = CCrmDeal::GetListEx(array("id" => "asc"), $arFilter, false, false, ["ID"]);

		$cnt = [];
		while ($arFields = $res->GetNext()) {
			$cnt[] = $arFilter["ID"];
		}

		return count($cnt);
	}


	//получение города из контакта
	public function getContactCity(): int {
		$arFilter       = array("=ID" => $this->userId, "CHECK_PERMISSIONS" => 'N');
		$arSelectFields = array("UF_CRM_NEW_CITY");
		$res            = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);

		$city_id = null;
		while ($ob = $res->GetNext()) {
			$city_id = (int) $ob["UF_CRM_NEW_CITY"];
		}

		return $city_id;
	}


	/**
	 * Возвращает массив категорий видов работ на которые подписачн мастер
	 *
	 * @param $contactId
	 */
	#[ArrayShape(["WORK_CATEGORIES" => "array", "UNIT_WORKS" => "array"])] public static function getContactWorkCategories($contactId): array {

		$workCategories = [];
		$unitWorks      = [];
		$arFilter       = array("=ID" => $contactId);
		$arSelectFields = array(self::CATEGORIES_USER_FIELD_CODE, self::WORKS_USER_FIELD_CODE);
		$res2           = CCrmContact::GetListEx(array(), $arFilter, false, false, $arSelectFields);
		while ($ob2 = $res2->GetNext()) {
			$workCategories = $ob2[ self::CATEGORIES_USER_FIELD_CODE ];
			$unitWorks      = $ob2[ self::WORKS_USER_FIELD_CODE ];
		}

		return [
			"WORK_CATEGORIES" => $workCategories,
			"UNIT_WORKS"      => $unitWorks,
		];
	}


	/**
	 * проставить координаты в контакт
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function setContactCoordinates($lat, $lon): void {
		if ( ! ($lat) || ! ($lon)) {
			$cityId      = $this->getContactCity();
			$coordinates = CApiHelpers::getLocationByCityId($cityId);
			$lat         = $coordinates['lat'];
			$lon         = $coordinates['lon'];
		}

		$this->updateContactField('UF_CRM_REPORT_LAT', $lat);
		$this->updateContactField('UF_CRM_REPORT_LON', $lon);
	}


	/**
	 * @throws \Bitrix\Main\SystemException
	 */
	public function isContactRegistered(): bool {
		$arContact               = $this->getContactData();
		$contactUserRegisterType = $arContact["UF_CRM_NEW_APP_REGISTER"];

		return in_array($this->getUserRegisterId(), $contactUserRegisterType, true);
	}


	#[Pure] public function getUserRegisterId(): int {
		return $this->getUserId() === CDbzConstants::DBZ_WORKER_TYPE ? self::NEW_APP_REGISTER_MASTER_ID : self::NEW_APP_REGISTER_CLIENT_ID;
	}


	/**
	 * Возвращает количество непрочитанных уведомлений у контакта
	 *
	 * @return int
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getContactNotificationsCount(): int {
		$arContact = $this->getContactData();

		if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
			$count = count((array) $arContact["UF_CRM_NEW_NOTIF_WORKER"]);
		} else {
			$count = count((array) $arContact["UF_CRM_NEW_NOTIF_CLIENT"]);
		}

		return $count;
	}


	/**
	 * Обновление поля в контакте
	 *
	 * @param $fieldCode
	 * @param $value
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function updateContactField($fieldCode, $value): void {
		Loader::includeModule("crm");
		$entity = new CCrmContact();
		$fields = array(
			$fieldCode => $value,
		);
		$entity->update($this->userId, $fields);
	}


	/**
	 * @param int $userType
	 */
	public function setUserType(int $userType): void {
		$this->userType = $userType;
	}

	/**
	 * @return int
	 */
	public function getUserId(): int {
		return $this->userId;
	}

	/**
	 * @param int $userId
	 */
	public function setUserId(int $userId): void {
		$this->userId = $userId;
	}

}