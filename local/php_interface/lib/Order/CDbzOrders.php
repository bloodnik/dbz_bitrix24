<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 14.08.2021
 * Time: 16:05
 * Project: dombezzabot.net
 */

namespace lib\order;


use Bitrix\Main\Loader;
use JetBrains\PhpStorm\ArrayShape;
use lib\contact\CDbzContact;
use lib\helpers\CApiHelpers;
use lib\helpers\CDbzConstants;

class CDbzOrders {

	// Тип списка заказов
	public const ORDER_LIST_TYPE_NEW = 0; // Тип списка - Новые
	public const ORDER_LIST_TYPE_CURRENT = 1; // Тип списка - активные
	public const ORDER_LIST_TYPE_CLOSED = 2; // Тип списка - Закрытые
	public const ORDER_LIST_TYPE_FAVORITE = 3; // Тип списка - Избранные


	private int $zoom = 0; // Передается если поиск идет через карту
	private float $lat = 0.0; // Широта
	private float $lon = 0.0; // Долгота
	private int $cityId; // Id города мастера
	private ?int $userId; // Id пользователя
	private int $userType = CDbzConstants::DBZ_WORKER_TYPE; // Тип пользователя
	private int $ordersListType = self::ORDER_LIST_TYPE_NEW; // Тип списка заказов
	private int $page = 0;


	/**
	 * CDbzOrders constructor.
	 *
	 * @param int|null $userId
	 */
	public function __construct(?int $userId) {
		$this->userId = $userId;
	}


	/**
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getOrders(): array {
		Loader::includeModule('crm');

		// Постраничная навигация пока не требуется. Возвращаем все найденные заказы
		// Если пришла вторая страница, то просто выдаем пустой массив
		if ($this->page > 0) {
			return [];
		}

		if ($this->userId) {
			$obContact = new CDbzContact($this->userId);
			$obContact->setContactCoordinates($this->lat, $this->lon);
			$this->cityId = $obContact->getContactCity();
		} else {
			$this->cityId = CApiHelpers::getCityByCoordinates($this->lat, $this->lon);
		}

		// Актуализируем текущие переменные ID города и координаты.
		$this->setCityIdAndCoordinates();

		$arOrderSort = $this->setOrderListSort(); // Сортировка

		// Фильтрация возвращаются идентификаторы заявок в виде массива
		// <ID заявки> => int <значение сортировки>
		// Пример: ["123455" => 10, "222222" => 20]
		$arFilteredOrderIds = $this->getFilteredOrderIds(); //

		if (empty($arFilteredOrderIds)) {
			return [];
		}

		// Получаем все заявки по ключам масссива $arFilteredOrderIds (ID заявок)
		$arFilter       = [];
		$arFilter["ID"] = array_keys($arFilteredOrderIds);
		$resDealList    = \CCrmDeal::GetListEx($arOrderSort, $arFilter, false, false, ["*", "UF_*"]);
		$arOrders       = [];
		while ($arOrder = $resDealList->GetNext()) {
			$arOrders[ $arOrder["ID"] ] = $arOrder;
		}

		if (empty($arOrders)) {
			return [];
		}

		// Сортируем заказы по приоритету. По значениям массива $arFilteredOrderIds
		$arSortedOrders = [];
		foreach ($arFilteredOrderIds as $orderId => $sort) {
			$arSortedOrders[] = $arOrders[ $orderId ];
		}

		// Формируем список заказов для приложения по отсортированному списку
		$arOrderResponses = [];
		foreach ($arSortedOrders as $arOrder) {
			$dbzOrder = new CDbzOrder($arOrder["ID"]);
			$dbzOrder->setUserId($this->userId);
			$dbzOrder->setUserType($this->userType);
			$arOrderResponses[] = $dbzOrder->getOrderResponse($arOrder, true);
		}

		return $arOrderResponses;
	}


	private function setCityIdAndCoordinates(): void {
		if ($this->zoom && $this->lat and $this->lon) {
			$this->cityId = CApiHelpers::getCityByCoordinates($this->lat, $this->lon);
		} else {
			$cityCoord = CApiHelpers::getLocationByCityId($this->cityId);
			$this->lat = $cityCoord["lat"];
			$this->lon = $cityCoord["lon"];
		}
	}


	/**
	 * Сортировка списка заказов
	 * @return array
	 */
	private function setOrderListSort(): array {
		$arOrder                      = [];
		$arOrder["UF_CRM_ACTUALTIME"] = "DESC";

		return $arOrder;
	}


	/**
	 * Возвращает массив с отфильтрованными идентификаторами заказов отсортированных по приоритеу
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	private function getFilteredOrderIds(): array {
		$arFilter                 = [];
		$arFilter['=CATEGORY_ID'] = '7';

		$orderIds = [];

		switch ($this->ordersListType) {
			case self::ORDER_LIST_TYPE_NEW: // Новые заказы
				// Стадии сделки
				$arFilter['=STAGE_ID'] = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');

				// Скрывшие мастера
				$arFilter["!=UF_CRM_COUNTER_HIDEMASTERS"] = $this->userId;

				// Возвращает id заказов на которые подписан мастер из его города
				$arOrderIdsByCategories = $this->getOrderIdsByWorkCategories($arFilter);

				// Возвращает id всех заказов в городе мастера
				$arOrderIdsInMasterCity = $this->getOrderIdsInMasterCity($arFilter);

				// Возвращает id заказов по ближайшему радиусу
				$arOrderIdsByNearestRadius = $this->getOrderIdsByNearestRadius($arFilter);

				// Возвращает id всех заказов в работе в городе мастера
				$arOrderIdsExecutingInMasterCity = $this->getOrderIdsExecutingInMasterCity($arFilter);

				// Объединяем все массивы идентификторов в признаком сортрировки
				$orderIds = $arOrderIdsByCategories + $arOrderIdsInMasterCity + $arOrderIdsByNearestRadius + $arOrderIdsExecutingInMasterCity;

				break;
			case self::ORDER_LIST_TYPE_CURRENT: //Активные заказы
				if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
					$arFilter['=STAGE_ID']      = array('C7:FINAL_INVOICE', 'C7:2', 'C7:6', 'C7:1');
					$arFilter['=UF_CRM_MASTER'] = $this->userId;
				} else {
					$arFilter['=STAGE_ID']    = array('C7:NEW', 'C7:PREPARATION', 'C7:PREPAYMENT_INVOICE', 'C7:EXECUTING', 'C7:FINAL_INVOICE', 'C7:1', 'C7:2', 'C7:6', 'C7:3');
					$arFilter['!=CONTACT_ID'] = false;
					$arFilter['=CONTACT_ID']  = $this->userId;
				}
				$arActiveOrdersId = $this->getActiveOrdersIds($arFilter);

				$orderIds = $arActiveOrdersId;

				break;
			case self::ORDER_LIST_TYPE_CLOSED: // Закрытые заявки
				if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
					$arFilter['=STAGE_ID']      = array('C7:1', 'C7:WON', 'C7:3');
					$arFilter['=UF_CRM_MASTER'] = $this->userId;
				} else {
					$arFilter['=STAGE_ID']    = array('C7:WON', 'C7:3', 'C7:LOSE', 'C7:5', 'C7:1', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
					$arFilter['!=CONTACT_ID'] = false;
					$arFilter['=CONTACT_ID']  = $this->userId;
				}
				$arActiveOrdersId = $this->getActiveOrdersIds($arFilter);

				$orderIds = $arActiveOrdersId;
				break;
			case self::ORDER_LIST_TYPE_FAVORITE:
				// Избранные
				$orderIds = $this->getFavoriteOrdersIds();
				break;
			default:
				break;
		}

		return $orderIds;
	}

	/**
	 * Возвращает id заказов на которые подписан мастер из его города
	 */
	private function getOrderIdsByWorkCategories($arFilter): array {
		// Категории
		$arCategories = CDbzContact::getContactWorkCategories($this->userId);
		$arFilter[]   = array(
			"LOGIC" => "OR",
			array(CDbzContact::CATEGORIES_USER_FIELD_CODE => $arCategories["WORK_CATEGORIES"]),
			array(CDbzContact::WORKS_USER_FIELD_CODE => $arCategories["UNIT_WORKS"]),
		);

		//проверка города
		$arFilter["UF_CRM_NEW_CITY"] = $this->cityId;

		$resDealList = \CCrmDeal::GetListEx($this->setOrderListSort(), $arFilter, false, false, array("ID"));

		$arIds = [];
		while ($arOrder = $resDealList->GetNext()) {
			$arIds[ $arOrder["ID"] ] = 10;
		}

		return $arIds;
	}


	/**
	 * Возвращает id всех заказов в городе мастера
	 */
	private function getOrderIdsInMasterCity($arFilter): array {
		$arFilter["UF_CRM_NEW_CITY"] = $this->cityId;
		$resDealList                 = \CCrmDeal::GetListEx($this->setOrderListSort(), $arFilter, false, false, array("ID", "UF_CRM_LATITUDE", "UF_CRM_LONGITUDE"));
		$arIds                       = [];
		while ($arOrder = $resDealList->GetNext()) {
			$arIds[ $arOrder["ID"] ] = 20;
		}

		return $arIds;
	}

	/**
	 * Возвращает id заказов по ближайшему радиусу
	 */
	private function getOrderIdsByNearestRadius($arFilter): array {
		// Фильтр по координатам
		$coords                              = $this->getCalculatedSearchCoordinates();
		$arFilter["(int)>=UF_CRM_LATITUDE"]  = $coords["lat_min"];
		$arFilter["(int)<=UF_CRM_LATITUDE"]  = $coords["lat_max"];
		$arFilter["(int)>=UF_CRM_LONGITUDE"] = $coords["lon_min"];
		$arFilter["(int)<=UF_CRM_LONGITUDE"] = $coords["lon_max"];
		$resDealList                         = \CCrmDeal::GetListEx($this->setOrderListSort(), $arFilter, false, false, array("ID", "UF_CRM_LATITUDE", "UF_CRM_LONGITUDE"));

		$arIds = [];
		while ($arOrder = $resDealList->GetNext()) {

			$distance_lat = (abs($arOrder['UF_CRM_LATITUDE'] - $this->lat)) * 111.134;
			$distance_lon = (abs($arOrder['UF_CRM_LONGITUDE'] - $this->lon)) * 111.321 * cos(deg2rad($this->lat));
			$distance_sqr = pow($distance_lat, 2) + pow($distance_lon, 2);
			$distance     = sqrt($distance_sqr);
			//echo("dist ".$distance." rad ".$radius."<br>");
			if ($distance > $coords["radius"]) {
				continue;
			}

			$arIds[ $arOrder["ID"] ] = 30;
		}

		return $arIds;
	}


	/**
	 * Возвращает id всех заказов в работе в городе мастера
	 */
	private function getOrderIdsExecutingInMasterCity($arFilter): array {
		$arFilter["=STAGE_ID"] = array('C7:FINAL_INVOICE');

		$arFilter["UF_CRM_NEW_CITY"] = $this->cityId;
		$resDealList                 = \CCrmDeal::GetListEx($this->setOrderListSort(), $arFilter, false, false, array("ID"));
		$arIds                       = [];
		while ($arOrder = $resDealList->GetNext()) {
			$arIds[ $arOrder["ID"] ] = 40;
		}

		return $arIds;
	}


	/**
	 * Возвращает id активных заказов
	 */
	private function getActiveOrdersIds($arFilter): array {
		$resDealList = \CCrmDeal::GetListEx($this->setOrderListSort(), $arFilter, false, false, array("ID"));
		$arIds       = [];
		while ($arOrder = $resDealList->GetNext()) {
			$arIds[ $arOrder["ID"] ] = 10;
		}

		return $arIds;
	}


	/**
	 * Возвращает id избранных заказов
	 *
	 * @return array
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	private function getFavoriteOrdersIds(): array {
		$obFavorites = new CDbzOrderFavorites($this->userId);
		$arFavorites = $obFavorites->getUserFavoritesIds();

		$arIds = [];
		foreach ($arFavorites as $arFavorite) {
			$arIds[ $arFavorite ] = 10;
		}

		return $arIds;
	}


	// Расчет координат для поиска работ по радиусу
	#[ArrayShape(['lat_min' => "float", 'lat_max' => "float", 'lon_min' => "float", 'lon_max' => "float", 'radius' => "float|int|mixed"])]
	private function getCalculatedSearchCoordinates(): array {
		$radius = $this->getSearchRadius();

		$lat_min = $this->lat - ($radius / 111.134);
		$lat_max = $this->lat + ($radius / 111.134);
		$lon_min = $this->lon - ($radius / (111.321 * cos(deg2rad($this->lat))));
		$lon_max = $this->lon + ($radius / (111.321 * cos(deg2rad($this->lat))));
		$coord   = array(
			'lat_min' => (float) $lat_min,
			'lat_max' => (float) $lat_max,
			'lon_min' => (float) $lon_min,
			'lon_max' => (float) $lon_max,
			'radius'  => $radius
		);

		return $coord;
	}


	//рассчет радиуса по масштабу
	function getSearchRadius() {
		if ( ! ($this->zoom)) {
			return DBZ_GLOBAL_SETTINGS["RADIUS_POISKA_KM"];
		}
		if ($this->zoom >= 19) {
			$radius = 0.6;
		} else if ($this->zoom >= 16) {
			$radius = 5;
		} else if ($this->zoom >= 13) {
			$radius = 50;
		} else if ($this->zoom >= 10) {
			$radius = 200;
		} else if ($this->zoom >= 7) {
			$radius = 1500;
		} else {
			$radius = 50000;
		}

		return $radius;
	}


	/**
	 * @param int $zoom
	 */
	public function setZoom(int $zoom): void {
		$this->zoom = $zoom;
	}

	/**
	 * @param float $lat
	 */
	public function setLat(float $lat): void {
		$this->lat = $lat;
	}

	/**
	 * @param float $lon
	 */
	public function setLon(float $lon): void {
		$this->lon = $lon;
	}

	/**
	 * @param int $ordersListType
	 */
	public function setOrdersListType(int $ordersListType): void {
		$this->ordersListType = $ordersListType;
	}

	/**
	 * @param int $userType
	 */
	public function setUserType(int $userType): void {
		$this->userType = $userType;
	}

	/**
	 * @param int $page
	 */
	public function setPage(int $page): void {
		$this->page = $page;
	}


}