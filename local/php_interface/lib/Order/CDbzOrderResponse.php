<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 08.08.2021
 * Time: 19:35
 * Project: dombezzabot.net
 */

namespace lib\order;


use JetBrains\PhpStorm\Pure;
use lib\helpers\CDbzResponse;
use lib\location\CDbzLocationResponse;

class CDbzOrderResponse extends CDbzResponse {

	protected int $id; // Id заказа
	protected string $title = ""; // Заголовок
	protected string $cover; // url обложки
	protected int $price = 0; // стоимость
	protected int $state = -2; // состояние заказа
	protected string $state_title = ""; // текст состояния заказа
	protected int $state_color = 0; // цвет состояния заказа
	protected array|null $location; // местоположение
	protected string $summary = ""; // описание заказа
	protected int $date = 0; // дата timestamp
	protected array|null $schedule; // начало и конец работы
	protected bool $is_visited = false; // открывался ли заказ мастером
	protected int $count_new_events; // число новых событий
	protected bool $is_refund_available; // доступен ли запрос на возврат
	protected string $hint;
	protected int $views_count = 0; //Количество просмотров заявки
	protected int $type = 1; // Тип заказа. 0 - из приложения 1- из другого источника
	protected bool $is_favorite = false; // Заявка в избранном у мастера
	protected int $favorites_count = 0; // Число сохранений заказа
	protected array $work;  // Массив категории и вида работ


	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id): void {
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title): void {
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getCover(): string {
		return $this->cover;
	}

	/**
	 * @param string $cover
	 */
	public function setCover(string $cover): void {
		$this->cover = $cover;
	}

	/**
	 * @return int
	 */
	public function getPrice(): int {
		return $this->price;
	}

	/**
	 * @param int $price
	 */
	public function setPrice(int $price): void {
		$this->price = $price;
	}

	/**
	 * @return int
	 */
	public function getState(): int {
		return $this->state;
	}

	/**
	 * @param int $state
	 */
	public function setState(int $state): void {
		$this->state = $state;
	}

	/**
	 * @return string
	 */
	public function getStateTitle(): string {
		return $this->state_title;
	}

	/**
	 * @param string $state_title
	 */
	public function setStateTitle(string $state_title): void {
		$this->state_title = $state_title;
	}

	/**
	 * @return int
	 */
	public function getStateColor(): int {
		return $this->state_color;
	}

	/**
	 * @param int $state_color
	 */
	public function setStateColor(int $state_color): void {
		$this->state_color = $state_color;
	}

	/**
	 * @return array
	 */
	public function getLocation(): array {
		return $this->location;
	}

	/**
	 * @param array $location
	 */
	public function setLocation(array $location): void {
		$this->location = $location;
	}

	/**
	 * @return string
	 */
	public function getSummary(): string {
		return $this->summary;
	}

	/**
	 * @param string $summary
	 */
	public function setSummary(string $summary): void {
		$this->summary = $summary;
	}

	/**
	 * @return int
	 */
	public function getDate(): int {
		return $this->date;
	}

	/**
	 * @param int $date
	 */
	public function setDate(int $date): void {
		$this->date = $date;
	}

	/**
	 * @return array
	 */
	public function getSchedule(): array {
		return $this->schedule;
	}

	/**
	 * @param array|null $schedule
	 */
	public function setSchedule(array|null $schedule): void {
		$this->schedule = $schedule;
	}

	/**
	 * @return bool
	 */
	public function isIsVisited(): bool {
		return $this->is_visited;
	}

	/**
	 * @param bool $is_visited
	 */
	public function setIsVisited(bool $is_visited): void {
		$this->is_visited = $is_visited;
	}

	/**
	 * @return int
	 */
	public function getCountNewEvents(): int {
		return $this->count_new_events;
	}

	/**
	 * @param int $count_new_events
	 */
	public function setCountNewEvents(int $count_new_events): void {
		$this->count_new_events = $count_new_events;
	}

	/**
	 * @return bool
	 */
	public function isIsRefundAvailable(): bool {
		return $this->is_refund_available;
	}

	/**
	 * @param bool $is_refund_available
	 */
	public function setIsRefundAvailable(bool $is_refund_available): void {
		$this->is_refund_available = $is_refund_available;
	}

	/**
	 * @return string
	 */
	public function getHint(): string {
		return $this->hint;
	}

	/**
	 * @param string $hint
	 */
	public function setHint(string $hint): void {
		$this->hint = $hint;
	}

	/**
	 * @return int
	 */
	public function getViewsCount(): int {
		return $this->views_count;
	}

	/**
	 * @param int $views_count
	 */
	public function setViewsCount(int $views_count): void {
		$this->views_count = $views_count;
	}

	/**
	 * @return int
	 */
	public function getType(): int {
		return $this->type;
	}

	/**
	 * @param int $type
	 */
	public function setType(int $type): void {
		$this->type = $type;
	} //Количество просмотров заявки

	/**
	 * @return bool
	 */
	public function isIsFavorite(): bool {
		return $this->is_favorite;
	}

	/**
	 * @param bool $is_favorite
	 */
	public function setIsFavorite(bool $is_favorite): void {
		$this->is_favorite = $is_favorite;
	}

	/**
	 * @return array
	 */
	public function getWork(): array {
		return $this->work;
	}

	/**
	 * @param array $work
	 */
	public function setWork(array $work): void {
		$this->work = $work;
	}

	/**
	 * @return int
	 */
	public function getFavoritesCount(): int {
		return $this->favorites_count;
	}

	/**
	 * @param int $favorites_count
	 */
	public function setFavoritesCount(int $favorites_count): void {
		$this->favorites_count = $favorites_count;
	}

}