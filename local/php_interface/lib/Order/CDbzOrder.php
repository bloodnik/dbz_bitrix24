<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 03.08.2021
 * Time: 20:35
 * Project: dombezzabot.net
 */

namespace lib\order;

use Bitrix\Crm\Timeline\CommentEntry;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Web\Json;
use CBPDocument;
use CCrmContact;
use CCrmDeal;
use CFile;
use CIBlockElement;
use CModule;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use lib\contact\CDbzContact;
use lib\feedbacks\CDbzFeedbacks;
use lib\helpers\CApiHelpers;
use lib\helpers\CDbzBase;
use lib\helpers\CDbzConstants;
use lib\location\CDbzLocationResponse;
use lib\notifications\CDbzNotification;
use lib\responds\CDbzResponds;
use lib\work\CDbzWork;

class CDbzOrder {

	private int|null $orderId;
	private int $userType = CDbzConstants::DBZ_WORKER_TYPE;
	private ?int $userId = 0;
	private array $arOrder = [];

	public const REFUND_AVAILABLE_ELEMENT_ID = 49528; //ID элемета Возврат доступен

	public const ORDER_PHOTO_PROPERTY = "UF_CRM_ORDER_PHOTOS"; //UF_CRM_1551801303

	/**
	 * Свойства заказа
	 */
	private const ORDER_AVAILABLE_SERVICES_LIST = [
		"OFFER_AVAILABLE"    => 30952,
		"TELEGRAM_AVAILABLE" => 35153,
		"WHATSAPP_AVAILABLE" => 30955,
		"REFUND_AVAILABLE"   => 49528,
		"CALL_AVAILABLE"     => 30954,
		"CHAT_AVAILABLE"     => 30953,
	];


	/**
	 * Справочник типа заявки
	 */
	public const ORDER_MAIN_TYPE_LIST = [
		"MANUAL_ORDER"     => 34604,
		"CLIENT_APP_ORDER" => 34603
	];


	/**
	 * CDbzOrder constructor.
	 *
	 * @param int|null $orderId
	 */
	public function __construct(int|null $orderId = null) {
		$this->orderId = $orderId;
	}

	/**
	 * Возращает массив с данными заказа
	 * @throws Exception
	 */


	#[ArrayShape([
		"order"                => "array",
		"users"                => "array",
		"media"                => "array",
		'contacts_hint'        => "string",
		'is_contacts_hidden'   => "bool",
		'is_work_subscribed'   => "bool",
		'my_feedback'          => "array|null",
		'is_editing_available' => "bool",
		'info'                 => "array[]"
	])]
	public function getOrder(): array {
		$this->arOrder = $this->getOrderData();

		$isContactHidden = $this->isClientContactInfoHidden();
		$contactsHint     = $isContactHidden ? "Контакты будут доступны после того, как заказчик выберет Вас исполнителем" : "Контакты будут доступны после того, как Вы предложите услугу";

		return [
			"order"                => $this->getOrderResponse($this->arOrder),
			"users"                => $this->getOrderUsers(),
			"media"                => $this->getOrderMedia($this->arOrder[ self::ORDER_PHOTO_PROPERTY ]),
			'contacts_hint'        => $this->getUserType() === CDbzConstants::DBZ_WORKER_TYPE ? $contactsHint : null,
			'is_contacts_hidden'   => $isContactHidden,
			'is_work_subscribed'   => $this->getUserId() && $this->isMasterSubscribedToThisWork(),
			'my_feedback'          => $this->getUserId() ? $this->getUserFeedback() : null,
			'is_editing_available' => $this->isOrderEditingAvailable($this->getOrderData()),
			'info'                 => [
				[
					"title"        => "Образец договора",
					"summary"      => "Если потребуется",
					"button_title" => "Скачать",
					"url"          => "https://bit.ly/3w1wGxn",
					"cover"        => null
				]
			],
		];
	}


	/**
	 * Формируется массив данных по заявке для приложеня
	 *
	 * @param $arOrder - Массив с данными заявки
	 * @param false $fromList - Признак формирования массива для отображения в списке. Не требуется обновление показателей (просмотры и т.п.)
	 *
	 * @return array
	 * @throws ArgumentException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 * @throws Exception
	 */
	public function getOrderResponse($arOrder, bool $fromList = false): array {
		$orderResponse = new CDbzOrderResponse();
		$orderResponse->setId($arOrder["ID"]);
		$orderResponse->setTitle($arOrder["TITLE"]);

		$orderResponse->setCover($arOrder[ self::ORDER_PHOTO_PROPERTY ] ? CDbzConstants::DBZ_PORTAL_URL . CFile::GetPath(current($arOrder[ self::ORDER_PHOTO_PROPERTY ])) : "");
		$orderResponse->setPrice($arOrder["OPPORTUNITY"]);

		$state = $this->getOrderStateInfo($arOrder);
		$orderResponse->setState($state["state"]);
		$orderResponse->setStateColor($state["state_color"]);
		$orderResponse->setStateTitle($state["state_title"]);

		$hasSchedule = $arOrder['UF_PLANNED_START_DATE'] && $arOrder['UF_PLANNED_COMPLETION_DATE'];
		$orderResponse->setSchedule($hasSchedule ? [
			"work_start" => (float) DateTime::createFromPhp(new \DateTime($arOrder['UF_PLANNED_START_DATE']))->getTimestamp(),
			"work_end"   => (float) DateTime::createFromPhp(new \DateTime($arOrder['UF_PLANNED_COMPLETION_DATE']))->getTimestamp()
		] : null);

		// location

		if ($arOrder["UF_CRM_LATITUDE"] && $arOrder["UF_CRM_LONGITUDE"]) {
			$obLocation = new CDbzLocationResponse();
			$obLocation->setId($arOrder["UF_CRM_NEW_CITY"]);
			$obLocation->setTitle(current(explode("|", $arOrder["UF_CRM_DEAL_ADDRESS"])));
			$obLocation->setLat($arOrder["UF_CRM_LATITUDE"]);
			$obLocation->setLon($arOrder["UF_CRM_LONGITUDE"]);
		} else {
			$location   = CApiHelpers::getLocationByCityId($arOrder["UF_CRM_NEW_CITY"]);
			$obLocation = new CDbzLocationResponse($location);
		}
		$orderResponse->setLocation($obLocation->toArray());

		$orderResponse->setSummary(CApiHelpers::quotDecode($arOrder["UF_CRM_1548512221"]));

		// Дата
		if ($arOrder['UF_CRM_ACTUALTIME']) {
			$date = DateTime::createFromPhp(new \DateTime($arOrder['UF_CRM_ACTUALTIME']));
		} else if ($arOrder['UF_CRM_BEGINTIME']) {
			$date = DateTime::createFromPhp(new \DateTime($arOrder['UF_CRM_BEGINTIME']));
		} else {
			$date = DateTime::createFromPhp(new \DateTime($arOrder['BEGINDATE']));
		}
		$orderResponse->setDate($date->getTimestamp());

		//если открывался мастером
		$isVisited = false;
		if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE && in_array((string) $this->userId, (array) $arOrder['UF_CRM_COUNTER_WATCHEDMASTERS'], true)) {
			$isVisited = true;
		}
		$orderResponse->setIsVisited($isVisited);


		// доступен ли запрос возврата средств
		//-----------------провекра возврата-------------------
		$refundTimeCheck = false;
		if ($arOrder['UF_CRM_INWORK_TIME']) {
			$inwork_time             = DateTime::createFromPhp(new \DateTime($arOrder['UF_CRM_INWORK_TIME']));
			$refund_available_before = $inwork_time->add(DBZ_GLOBAL_SETTINGS["VOZVRAT_DOSTUPEN_DNEY"] . " days");

			$refundDateDiff = $refund_available_before->getDiff(DateTime::createFromPhp(new \DateTime()));

			if ((bool) $refundDateDiff->invert && $refundDateDiff->days >= 0) {
				$refundTimeCheck = true;
			}
		}
		$refundRespondCheck = $this->respondRefundCheck();

		$refund_stages = array('C7:2', 'C7:6');
		if (in_array(self::REFUND_AVAILABLE_ELEMENT_ID, $arOrder['UF_CRM_SERVICES_AVAILABLE'], true)
		    && ! (in_array($arOrder['STAGE_ID'], $refund_stages, true))
		    && $state["state"] === 3
		    && $refundTimeCheck
		    && $this->userType === CDbzConstants::DBZ_WORKER_TYPE
		    && $refundRespondCheck) {
			$isRefundAvailable = true;
		} else {
			$isRefundAvailable = false;
		}
		$orderResponse->setIsRefundAvailable($isRefundAvailable);


		$orderResponse->setHint($this->getOrderHint($state["state"]));

		$orderResponse->setViewsCount(count((array) $arOrder['UF_CRM_COUNTER_WATCHEDMASTERS']));

		$orderResponse->setType((int) $arOrder["UF_CRM_MAIN_TYPE"] === self::ORDER_MAIN_TYPE_LIST["CLIENT_APP_ORDER"] ? 0 : 1);

		$orderResponse->setWork($this->getOrderCategoryAndWork($arOrder[ CDbzContact::CATEGORIES_USER_FIELD_CODE ], $arOrder[ CDbzContact::WORKS_USER_FIELD_CODE ]));

		if ($this->getUserId()) {
			$obFavorites = new CDbzOrderFavorites($this->getUserId(), $this->getUserType());
			$obFavorites->setOrderId($this->getOrderId());
			$orderResponse->setIsFavorite($obFavorites->isFavoriteExist());

			$orderResponse->setFavoritesCount(CDbzOrderFavorites::getFavoritesCount($this->getOrderId()));
		}


		// Обновялем список просмотревших мастеров
		if ( ! $fromList && $this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
			$this->updateWatchedMastersField($arOrder['UF_CRM_COUNTER_WATCHEDMASTERS']);
			$this->setMasterRespondViewStatus();
		}

		$this->setOrderCompleteStatus($arOrder);


		return $orderResponse->toArray();
	}

	/**
	 * Изменение заявки.
	 * Создание или обновление текущей, если в $orderData есть $id
	 *
	 * $orderData = [$id, $work_id, $work_type, $summary, $price, $location, $schedule,  medio]
	 *
	 *
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function editOrder(array $orderData) {
		if ((int) $orderData["id"]) {
			$orderId = $this->updateOrder($orderData);
		} else {
			$orderId = $this->addOrder($orderData);
		}

		return $orderId;
	}


	/**
	 * Создание заявки (сделки CRM)
	 *
	 * $orderData = [$id, $work_id, $work_type, $summary, $price, $location, $schedule]
	 *
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function addOrder(array $orderData): int {

		$obContact = new CDbzContact($this->getUserId());
		$arContact = $obContact->getContactData();
		$city      = $obContact->getContactCity();


		// Если указаны координаты, находим город по ним
		$obLocation = new CDbzLocationResponse($orderData["location"]);
		if ($obLocation->getLat() && $obLocation->getLon()) {
			$city = CApiHelpers::getCityByCoordinates($obLocation->getLat(), $obLocation->getLon());
		}

		if ( ! $city) {
			throw new SystemException("Пожалуйста, укажите город");
		}


		["title" => $orderTitle, "category_id" => $categoryId] = CDbzWork::getWorkById($orderData["work_id"]);
		$orderTitle = $orderData["work_title"] ?? $orderTitle;

		$servicesAvailable = [self::ORDER_AVAILABLE_SERVICES_LIST["CALL_AVAILABLE"]];
		if (in_array(CDbzContact::NEW_APP_REGISTER_CLIENT_ID, $arContact["UF_CRM_NEW_APP_REGISTER"], true)) {
			$servicesAvailable[] = self::ORDER_AVAILABLE_SERVICES_LIST["OFFER_AVAILABLE"];
		}

		$currentDateTime = DateTime::createFromPhp(new \DateTime())->format("d.m.Y H:i:s");
		$entity          = new CCrmDeal;
		$fields          = array(
			'TITLE'                     => $orderTitle,
			'CATEGORY_ID'               => '7',
			'STAGE_ID'                  => 'C7:new',
			'UF_CRM_WORK_TYPE'          => $orderData["work_id"],
			'UF_CRM_UNDER_CATEGORY'     => $categoryId,
			'OPPORTUNITY'               => $orderData["price"],
			'UF_CRM_1548512221'         => $orderData["summary"],
			'CONTACT_ID'                => $this->getUserId(),
			'UF_CRM_NEW_CITY'           => $city,
			'UF_CRM_DEAL_ADDRESS'       => $obLocation->getTitle() . ' | ' . $obLocation->getLat() . ';' . $obLocation->getLon(),
			'UF_CRM_1564122612033'      => $obLocation->getTitle(),
			'UF_CRM_BEGINTIME'          => $currentDateTime,
			'UF_CRM_ACTUALTIME'         => $currentDateTime,
			'UF_CRM_LATITUDE'           => $obLocation->getLat(),
			'UF_CRM_LONGITUDE'          => $obLocation->getLon(),
			'UF_CRM_MAIN_TYPE'          => self::ORDER_MAIN_TYPE_LIST["CLIENT_APP_ORDER"],
			'UF_ORDER_FROM_CLIENT_APP'  => 1,
			'UF_CRM_SERVICES_AVAILABLE' => $servicesAvailable,
			'UF_CRM_REFUND_DECISION'    => 34541,
			'UF_CRM_RECOUNT_DECISION'   => 34620,
		);

		if ($orderData["schedule"]) {
			$fields["UF_PLANNED_START_DATE"]      = DateTime::createFromTimestamp($orderData["schedule"]["work_start"])->format("d.m.Y H:i:s");
			$fields["UF_PLANNED_COMPLETION_DATE"] = DateTime::createFromTimestamp($orderData["schedule"]["work_end"])->format("d.m.Y H:i:s");
		}

		if ($orderData["work_id"] && $orderData["summary"] && $orderData["price"]) {
			$fields['STAGE_ID']                = 'C7:PREPAYMENT_INVOICE';
			$fields['UF_CRM_FREEREQUEST_TIME'] = $currentDateTime;
			$fullOrder                         = true;
		} else {
			$fields['STAGE_ID'] = 'C7:PREPARATION'; //не заполнена - в уточнение
			$fullOrder          = false;
		}

		$orderId = $entity->add($fields);

		if ( ! $orderId) {
			throw new SystemException("Ошибка создания заказа");
		}

		$this->setOrderId($orderId);

		if ($fullOrder) {  //ВЫВЕШИВАЕМ ЗАПУСК ПРОЦЕССОВ ОТ СВОБОДНОЙ ЗАЯВКИ
			//запуск БП отправки сообщения в ватсап
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					238,
					array("crm", "CCrmDocumentDeal", "DEAL_" . $orderId),
					array_merge(),
					$arErrorsTmp
				);
			}
			//запуск БП перехода в "нет исполнителей"
			if (CModule::IncludeModule("bizproc")) {
				$wfId = CBPDocument::StartWorkflow(
					203,
					array("crm", "CCrmDocumentDeal", "DEAL_" . $orderId),
					array("target" => "noworkers_timer"),
					$arErrorsTmp
				);
				$this->updateOrderField("UF_CRM_ACTIVE_PROCESS", $wfId);
			}
		}

		$contactName = $arContact["NAME"] . ' ' . $arContact["LAST_NAME"];
		$this->addTimelineComment(
			'Создана заявка, заказчик: [url = ' . CDbzConstants::DBZ_PORTAL_URL . ' / crm / contact / details / ' . $this->getUserId() . ' /]' . $contactName . '[/url]~BR~Телефон: ' .
			$obContact->getContactPhone()
		);


		$arFilter                                            = [];
		$arFilter["=UF_CRM_NEW_APP_REGISTER"]                = CDbzContact::NEW_APP_REGISTER_MASTER_ID;
		$arFilter["=UF_CRM_NEW_CITY"]                        = $city;
		$arFilter[ CDbzContact::CATEGORIES_USER_FIELD_CODE ] = $categoryId;

		$right_workers = [];
		$res           = CCrmContact::GetListEx(array(), $arFilter, false, false, array("ID"));
		while ($ob = $res->GetNext()) {
			$right_workers[] = $ob['ID'];
		}

		// Создать класс для работы с оповещениями
		if (count($right_workers) === 0) {
			$obNotification = new CDbzNotification($this->getUserId(), CDbzConstants::DBZ_CLIENT_TYPE);
			$obNotification->setRecipients([$this->getUserId()]);
			$obNotification->addNotification(34408, $this->getOrderId(), $orderTitle);  //заказчику - нет мстеров
		} else {
			if ($fullOrder) {  //ВЫВЕШИВАЕМ ЗАПУСК ПРОЦЕССОВ ОТ СВОБОДНОЙ ЗАЯВКИ
				$obNotification = new CDbzNotification($this->getUserId(), CDbzConstants::DBZ_WORKER_TYPE);
				$obNotification->setRecipients($right_workers);
				$obNotification->addNotification(34417, $this->getOrderId(), $orderTitle);  //заказчику - нет мстеров
				$this->updateOrderField('UF_CRM_HAND_ORDER_PUSH', CDbzConstants::DBZ_YES_VALUE);
			}
		}

		return $this->getOrderId();
	}


	/**
	 * Обновление заявки (сделки CRM)
	 *
	 * $orderData = [$id, $work_id, $work_type, $summary, $price, $location, $schedule, media]
	 *
	 * @param array $orderData
	 *
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function updateOrder(array $orderData) {
		$this->setOrderId($orderData["id"]);
		$arOrder = $this->getOrderData();

		$obContact = new CDbzContact($this->getUserId());
		$city      = $obContact->getContactCity();

		// Если указаны координаты, находим город по ним
		$obLocation = new CDbzLocationResponse($orderData["location"]);
		if ($obLocation->getLat() && $obLocation->getLon()) {
			$city = CApiHelpers::getCityByCoordinates($obLocation->getLat(), $obLocation->getLon());
		}

		if ( ! $city) {
			throw new SystemException("Пожалуйста, укажите город");
		}

		if ( ! $this->isOrderEditingAvailable($arOrder)) {
			throw new SystemException("Заказ заблокировн на модификацию");
		}

		$currentDateTime = DateTime::createFromPhp(new \DateTime())->format("d.m.Y H:i:s");

		$entity = new CCrmDeal;
		$fields = array(
			'OPPORTUNITY'         => $orderData["price"],
			'UF_CRM_1548512221'   => $orderData["summary"],
			'UF_CRM_BEGINTIME'    => $currentDateTime,
			'UF_CRM_ACTUALTIME'   => $currentDateTime,
			'UF_CRM_NEW_CITY'     => $city,
			'UF_CRM_DEAL_ADDRESS' => $obLocation->getTitle() . ' | ' . $obLocation->getLat() . ';' . $obLocation->getLon(),
		);
		if ($orderData["schedule"]) {
			$fields["UF_PLANNED_START_DATE"]      = DateTime::createFromTimestamp($orderData["schedule"]["work_start"])->format("d.m.Y H:i:s");
			$fields["UF_PLANNED_COMPLETION_DATE"] = DateTime::createFromTimestamp($orderData["schedule"]["work_end"])->format("d.m.Y H:i:s");
		}


		if ($entity->Update($orderData["id"], $fields)) {
			$this->updateOrderPictures(array_map(static fn($file) => (int) $file["id"], (array) $orderData["media"]));

			return $orderData["id"];
		}


		throw new SystemException($entity->LAST_ERROR);
	}


	/**
	 * Массив всех данных по заказу CRM_DEAL
	 *
	 * @return array
	 * @throws SystemException
	 */
	public function getOrderData(): array {
		CModule::IncludeModule("crm");
		$arOrder         = array("id" => "asc");
		$arSelectFields  = ["*", "UF_*"];
		$arFilter['=ID'] = $this->orderId;

		$obOrder = CCrmDeal::GetListEx($arOrder, $arFilter, false, false, $arSelectFields);
		if ($arOrder = $obOrder->Fetch()) {
			return $arOrder;
		}

		throw new SystemException("Заказ не найден");
	}


	/**
	 * Возвращает статус заказа
	 * [state => [state_code], state_title => [state title], state_color => [state_color]]
	 *
	 * @param array $arOrder
	 *
	 * @return array
	 */
	#[ArrayShape(['state' => "int", 'state_color' => "int", 'state_title' => "string"])]
	private function getOrderStateInfo(
		array $arOrder = []
	): array {
		if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
			return $this->getWorkerOrderState($arOrder);
		} else {
			return $this->getClientOrderState($arOrder);
		}
	}


	/**
	 * Вычисление статуса заказа для мастера
	 */
	#[ArrayShape(['state' => "int", 'state_color' => "int", 'state_title' => "string"])] private function getWorkerOrderState($arOrder): array {
		$preparation_stages = array('C7:NEW', 'C7:PREPARATION');
		$active_stages      = array('C7:PREPAYMENT_INVOICE', 'C7:EXECUTING');
		$inwork_stages      = array('C7:FINAL_INVOICE', 'C7:3', 'C7:1');
		$refund_stages      = array('C7:2', 'C7:6');
		$success_stages     = array('C7:WON');
		$lose_stages        = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		$state_color        = 0;

		// ID Элементов активновти откликов
		$arRespondActivity = array(
			"ACTIVE"             => 31191, // Активен
			"SELECTED_BY_CLIENT" => 31193, // Выбран заказчиком
			"CONFIRM_TIME_OUT"   => 31212, // Время принятия истекло
			"INACTIVE"           => 31192 // Неактивен
		);

		//проверка откликов
		$active_responds   = array();
		$expired_responds  = array();
		$inactive_responds = array();

		if ($this->userId) {
			$arSelect_resp = array("ID", "IBLOCK_ID", "PROPERTY_AKTIVEN");
			$arFilter_resp = array("IBLOCK_ID" => CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID, "=PROPERTY_ZAKAZ" => $this->orderId, "=PROPERTY_MASTER" => $this->userId, "=PROPERTY_AKTIVEN" => array_values($arRespondActivity));

			$res_resp = CIBlockElement::GetList(array(), $arFilter_resp, false, false, $arSelect_resp);
			while ($ob_resp = $res_resp->GetNextElement()) {
				$arFields_resp = $ob_resp->GetFields();
				if ($arFields_resp["PROPERTY_AKTIVEN_VALUE"] == $arRespondActivity["CONFIRM_TIME_OUT"]) {
					$expired_responds[] = $arFields_resp["ID"];
				} else if ($arFields_resp["PROPERTY_AKTIVEN_VALUE"] == $arRespondActivity["ACTIVE"] or $arFields_resp["PROPERTY_AKTIVEN_VALUE"] == $arRespondActivity["SELECTED_BY_CLIENT"]) {
					$active_responds[] = $arFields_resp["ID"];
				} else if ($arFields_resp["PROPERTY_AKTIVEN_VALUE"] == $arRespondActivity["INACTIVE"]) {
					$inactive_responds[] = $arFields_resp["ID"];
				}
			}
		}

		$masterId = (int) $arOrder["UF_CRM_MASTER"];

		//---------РАСЧЕТ СТАТУСОВ---------
		//статусы в подготовке (защита от случайного попадания)
		if (in_array($arOrder["STAGE_ID"], $preparation_stages, true)) {
			$state       = -1;
			$state_color = 1;
			$state_title = "Заказ недоступен";
		} //статусы с выбранным текущим мастером
		else if ($this->userId && $masterId === $this->userId && in_array($arOrder["STAGE_ID"], $refund_stages, true)) {
			$state       = 7;
			$state_title = "По заказу был запрошен возврат";
		} else if ($this->userId && $masterId === $this->userId && (int) $arOrder['UF_CRM_CLOSED_BY_WORKER'] === 30957 && in_array($arOrder["STAGE_ID"], $inwork_stages, true)) {
			$state       = 4;
			$state_title = "Я являюсь исполнителем заказа, заказ завершен";
		} else if ($this->userId && $masterId === $this->userId && in_array($arOrder["STAGE_ID"], $inwork_stages, true)) {
			$state       = 3;
			$state_title = "Я являюсь исполнителем заказа, заказ активен";
		} else if ($this->userId && $masterId === $this->userId && in_array($arOrder["STAGE_ID"], $success_stages, true)) {
			$state       = 4;
			$state_title = "Я являюсь исполнителем заказа, заказ завершен";
		} else if ($this->userId && $masterId === $this->userId && in_array($arOrder["STAGE_ID"], $lose_stages, true)) {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ был отменен';
		} //статусы в свободных заказах
		else if ($this->userId && in_array($arOrder["STAGE_ID"], $active_stages, true) && in_array((string) $this->userId, $arOrder['UF_CRM_CANCELED_WORKERS'], true)) {
			$state_color = 1;
			$state       = 6;
			$state_title = 'Вы уже отказались от этого заказа';
		} else if ($active_responds && in_array($arOrder["STAGE_ID"], $active_stages, true)) {
			$state       = 1;
			$state_title = "Заказ принят";
		} else if ($expired_responds && in_array($arOrder["STAGE_ID"], $active_stages, true)) {
			$state       = 2;
			$state_title = "Время принятия заказа истекло";
		} else if (in_array($arOrder["STAGE_ID"], $active_stages, true)) {
			$state       = 0;
			$state_title = "Заказ не принят";
		} //прочие статусы, где выбран другой мастер
		else if ($inactive_responds) {
			$state       = 5;
			$state_color = 1;
			$state_title = "Я не являюсь исполнителем заказа: заказ отдан другому или отменен";
		} else if (in_array($arOrder["STAGE_ID"], $success_stages, true)) {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ уже выполнен';
		} else if (in_array($arOrder["STAGE_ID"], $lose_stages, true)) {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ был отменен';
		} else {
			$state       = -1;
			$state_color = 1;
			$state_title = 'Заказ уже взят в работу';
		}

		//возвращаем ответ
		return array(
			'state'       => $state,
			'state_color' => $state_color,
			'state_title' => $state_title,
		);
	}


	/**
	 * Вычисление статуса заказа для клиента
	 */
	#[ArrayShape(['state' => "int", 'state_color' => "int", 'state_title' => "string"])]
	private function getClientOrderState(
		$arOrder
	): array {

		$inwork_stages  = array('C7:FINAL_INVOICE', 'C7:2', 'C7:6');
		$border_stages  = array('C7:1', 'C7:3');
		$success_stages = array('C7:WON');
		$lose_stages    = array('C7:LOSE', 'C7:5', 'C7:7', 'C7:8', 'C7:9', 'C7:10', 'C7:11');
		$refund_stages  = array('C7:2', 'C7:6');

		$state_color = 0;
		if (in_array($arOrder["STAGE_ID"], $lose_stages, true)) {
			$state       = 5;
			$state_color = 1;
			$state_title = "Заказ отменен";
		} else if (in_array($arOrder["STAGE_ID"], $success_stages, true)) {
			$state       = 4;
			$state_title = "Заказ завершен";
		} else if (in_array($arOrder["STAGE_ID"], $refund_stages, true)) {
			$state       = 7;
			$state_title = "По заказу был запрошен возврат";
		} else if ((int) $arOrder["UF_CRM_FEEDBACK_PROCESS"] === 30957 && in_array($arOrder["STAGE_ID"], $border_stages, true)) {
			$state       = 4;
			$state_title = "Заказ завершен";
		} else if (in_array($arOrder["STAGE_ID"], $border_stages, true)) {
			$state       = 3;
			$state_title = "Заказ ожидает оценки";
		} else if (in_array($arOrder["STAGE_ID"], $inwork_stages, true)) {
			$state       = 3;
			$state_title = "Заказ выполняется";
		} else if ((int) $arOrder["UF_CRM_CONFIRMATION_ON"] === CDbzConstants::DBZ_YES_VALUE) {
			$state       = 6;
			$state_title = "Заказ доступен для принятия, но размещен давно: требуется актуализация";
		} else {
			$state       = 0;
			$state_title = "Заказ доступен для принятия";
		}

		//возвращаем ответ
		return array(
			'state'       => $state,
			'state_color' => $state_color,
			'state_title' => $state_title,
		);
	}


	/**
	 * проверка доступности возврата по отклику
	 * Возврат доступен если есть откклик и комиссия была списана комиссия по откликку
	 *
	 * @return bool
	 */
	private function respondRefundCheck(): bool {
		$PROPERTY_AKTIVEN     = 31193; // Выбран заказчиком
		$PROPERTY_TIP_OTKLIKA = 31188; // Процент от стоимости

		//получить отклик мастера
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_STOIMOST_DLYA_MASTERA");
		$arFilter = array(
			"IBLOCK_ID"             => CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID,
			"=PROPERTY_ZAKAZ"       => $this->orderId,
			"=PROPERTY_MASTER"      => $this->userId,
			"=PROPERTY_AKTIVEN"     => $PROPERTY_AKTIVEN,
			"=PROPERTY_TIP_OTKLIKA" => $PROPERTY_TIP_OTKLIKA
		);

		$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields  = $ob->GetFields();
			$respond   = $arFields["ID"];
			$workerTax = $arFields["PROPERTY_STOIMOST_DLYA_MASTERA_VALUE"];

			if ($respond && $workerTax > 0) {
				return true;
			}
		}

		return false;
	}


	/**
	 * получение текста подсказки по статусу
	 *
	 *
	 * @param $state
	 *
	 * @return string
	 */
	private function getOrderHint($state): string {
		//TODO Отрафакторить

		$hint = "";
		if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
			$hint = match ($state) {
				0, 4 => "",
				1 => "Вы отозвались на данный заказ. Если Вас выберут исполнителем, появятся контакты заказчика.",
				2 => "Если Вы все еще готовы выполнить данный заказ, Вам нужно повторно его принять, так как истек срок принятия, а заказчик еще не сделал выбор.",
				3 => "Вы являетесь исполнителем и можете позвонить или написать заказчику.",
				5 => "Вы отзывались на данный заказ, но он был отменен или отдан другому мастеру.",
				6 => "Вы отказались от выполнения данного заказа.",
				7 => "По данному заказу был запрошен возврат. Мы скоро рассмотрим Ваш запрос и сообщим о решении.",
				default => "Данный заказ больше не доступен.",
			};
		} else if ($this->userType === CDbzConstants::DBZ_CLIENT_TYPE) {
			switch ($state) {
				case 0:
					$active_responds = array();
					//получение списка мастеров
					$arSelect2 = array("ID", "IBLOCK_ID");
					$arFilter2 = array("IBLOCK_ID" => 73, "=PROPERTY_135" => $this->orderId, "=PROPERTY_132" => array(31191, 31193));
					$res2      = CIBlockElement::GetList(array(), $arFilter2, false, false, $arSelect2);
					while ($ob2 = $res2->GetNextElement()) {
						$arFields2         = $ob2->GetFields();
						$active_responds[] = $arFields2["ID"];
					}
					if ($active_responds) {
						$hint = "Выберите одного из мастеров в качестве исполнителя, затем Вам будут доступны его контакты и чат с ним.";
					} else {
						$hint = "Мы отправили Ваш заказ всем доступным мастерам. Как только они начнут отзываться, Вам придет уведомление.";
					}
					break;
				case 1:
				case 2:
				case 4:
					$hint = "";
					break;
				case 3:
					$hint = "Ваш заказ исполняется. Пожалуйста, завершите его, если исполнитель закончит работу.";
					break;
				case 5:
					$hint = "Данный заказ был отменен.";
					break;
				case 6:
					$hint = "Пожалуйста, подтвердите Ваш заказ, если он все еще актуальный.";
					break;
				case 7:
					$hint = "По данному заказу был запрошен возврат.";
					break;
			}
		}

		return $hint;
	}


	/**
	 * Возвращает массив контактов по заказу
	 * Для мастера - заказчика
	 * Для заказчика - список откликнувшихся или выбранного мастера
	 * @throws \Exception
	 */
	private function getOrderUsers(): array {
		$arUsers = [];
		if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
			$arContact = new CDbzContact($this->arOrder["CONTACT_ID"]);
			$arContact->setUserType(CDbzConstants::DBZ_CLIENT_TYPE);
			$arUsers[] = $arContact->getContactResponse($this->shouldShowContactInfo());
		} else {
			// Если указан мастер, тогда заказчику возвращаем информацию по выбранному мастеру. Иначе возвращаем данные по откликнувшимся мастерам
			if ( ! empty($this->arOrder["UF_CRM_MASTER"])) {
				$arContact = new CDbzContact($this->arOrder["UF_CRM_MASTER"]);
				$arContact->setUserType(CDbzConstants::DBZ_WORKER_TYPE);
				$arUsers[] = $arContact->getContactResponse($this->shouldShowContactInfo());
			} else {
				$PROPERTY_AKTIVEN = 31191; // Активный отклик
				$arSelect         = array("ID", "IBLOCK_ID", "PROPERTY_MASTER");
				$arFilter         = array(
					"IBLOCK_ID"         => CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID,
					"=PROPERTY_ZAKAZ"   => $this->orderId,
					"=PROPERTY_AKTIVEN" => $PROPERTY_AKTIVEN
				);
				$res              = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
				while ($ob = $res->GetNextElement()) {
					$arFields  = $ob->GetFields();
					$arContact = new CDbzContact($arFields["PROPERTY_MASTER_VALUE"]);
					$arContact->setUserType(CDbzConstants::DBZ_WORKER_TYPE);
					$arUsers[] = $arContact->getContactResponse($this->shouldShowContactInfo());
				}
			}
		}

		return $arUsers;
	}


	/**
	 * Показывать ли контакную информацию у OrderUser
	 * Мастер должен видеть контакты заказчика если:
	 *    1. Мастер откликнулся, а у закачика стоит Показать контакты
	 *    2, Мастер выбран, и не важно что стоит
	 *
	 * Заказчик видит все контакты откликнувшихся мастеров
	 *
	 * @return bool
	 * @throws \Exception
	 */
	private function shouldShowContactInfo(): bool {
		$result = false;

		if ( ! $this->userId) {
			return $result;
		}

		if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
			if ($this->userId === (int) $this->arOrder["UF_CRM_MASTER"]) {
				$result = true;
			} else {
				$hasActiveRespond = (bool) CDbzResponds::getMasterActiveRespondByOrderId($this->orderId, $this->userId);
				$result           = $hasActiveRespond && ! $this->isClientContactInfoHidden();
			}
		} else {
			$result = true;
		}

		return $result;
	}

	/**
	 * Стоит ли признак "Скрыть контакты" у клиента
	 *
	 * @return bool
	 * @throws Exception
	 */
	private function isClientContactInfoHidden(): bool {
		$obClient = new CDbzContact($this->arOrder["CONTACT_ID"]);

		return $obClient->isContactHidden();
	}


	/**
	 * Достумпен ли заказ для обновления (модификации)
	 *
	 * @param array $arOrder - массив заказа
	 *
	 * @return bool
	 */
	private function isOrderEditingAvailable(array $arOrder): bool {
		return in_array($arOrder["STAGE_ID"], ["C7:PREPARATION", "C7:PREPAYMENT_INVOICE", "C7:EXECUTING"]);
	}


	/**
	 * Подписан ли мастер на данную категорию заказа или вид работ
	 * @return bool
	 */
	private function isMasterSubscribedToThisWork(): bool {
		$isSubscribed = false;

		if ($this->userType === CDbzConstants::DBZ_WORKER_TYPE) {
			$masterWorks  = CDbzContact::getContactWorkCategories($this->userId);
			$isSubscribed = in_array($this->arOrder[ CDbzContact::CATEGORIES_USER_FIELD_CODE ], $masterWorks["WORK_CATEGORIES"], true) || in_array($this->arOrder[ CDbzContact::WORKS_USER_FIELD_CODE ], $masterWorks["UNIT_WORKS"], true);
		}

		return $isSubscribed;
	}


	/**
	 * Массив категории и вида работ заказа
	 *
	 * @param $category
	 * @param $work
	 *
	 * @return array
	 * @throws ArgumentException
	 * @throws LoaderException
	 * @throws ObjectPropertyException
	 * @throws SystemException
	 */
	private function getOrderCategoryAndWork($category, $work): array {
		$result = [];

		// Временно отключено, пока заявка возвращает только категорию
		// if ( ! empty($work)) {
		// 	$result = CDbzWork::getWorkById($work, $this->userId);
		// } elseif ($category) {
		// 	$result = CDbzWork::getWorkCategoryById($category, $this->userId);
		// }
		$result = CDbzWork::getWorkCategoryById($category, $this->userId);

		if (empty($result)) {
			throw new SystemException("Ошибка! Заказ не привязан к категории или виду работ");
		}

		return $result;
	}


	/**
	 * Возвращает массив файлов по заказу
	 *
	 * @param $arFilesId
	 *
	 * @return array
	 */
	private function getOrderMedia($arFilesId): array {
		$result = [];

		if (is_array($arFilesId)) {
			foreach ($arFilesId as $fileId) {
				$result[] = [
					"id"   => $fileId,
					"type" => 0,
					"url"  => CDbzConstants::DBZ_PORTAL_URL . CFile::GetPath($fileId)
				];
			}
		}

		return $result;
	}


	/**
	 * @throws \Bitrix\Main\LoaderException
	 * @throws \Bitrix\Main\SystemException
	 */
	private function getUserFeedback(): ?array {
		$obFeedback = new CDbzFeedbacks($this->getUserId());
		$obFeedback->setUserType($this->getUserType());
		$obFeedback->setOrderId($this->orderId);

		return $obFeedback->getUserOrderFeedback();
	}


	/**
	 * Увеличение числа просмотров заявки
	 *
	 * @param int $currentViewsCount
	 *
	 * @return int
	 * @throws LoaderException
	 */
	private function incrementOrderViewsCount(int $currentViewsCount = 0): int {
		$VIEWS_COUNT_CODE = "UF_ORDER_VIEWS_COUNT";
		$this->updateOrderField($VIEWS_COUNT_CODE, ++$currentViewsCount);

		return (int) self::getOrderUserFieldValue($this->orderId, $VIEWS_COUNT_CODE);
	}

	/**
	 * Добавление / обновление изображения заказа
	 *
	 * Т.к. не нашел метода для удаления фойла из закза по ID, то приходится сначала собрать массив $arFiles из сущесвующих изображений
	 * и заново их загрзуить. А все старые удалить
	 *
	 * @param array $arFile - массив с описанием файла CFile::MakeFileArray
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function updateOrderPicture($arFile = []): array {
		$currentPictures = self::getOrderUserFieldValue($this->getOrderId(), self::ORDER_PHOTO_PROPERTY);
		$arFiles         = [];
		$arDelFiles      = [];
		if (is_array($currentPictures)) {
			foreach ($currentPictures as $currentPicture) {
				$arDelFiles[] = $currentPicture;
				$arFiles[]    = CFile::MakeFileArray(CDbzConstants::DBZ_PORTAL_URL . CFile::GetPath($currentPicture));
			}
		}
		$arFiles[] = $arFile;

		$this->updateOrderField(self::ORDER_PHOTO_PROPERTY, $arFiles);
		$orderPictureId = self::getOrderUserFieldValue($this->getOrderId(), self::ORDER_PHOTO_PROPERTY);
		$result         = [];
		if ($orderPictureId) {
			if (is_array($orderPictureId)) {
				$result = ["id" => max($orderPictureId), "url" => "https://" . $_SERVER["SERVER_NAME"] . CFile::GetPath(end($orderPictureId))];
			} else {
				$result = ["id" => $orderPictureId, "url" => "https://" . $_SERVER["SERVER_NAME"] . CFile::GetPath($orderPictureId)];
			}
		}

		//TODO Костыль. Удаляем файлы
		foreach ($arDelFiles as $arDelFile) {
			CFile::Delete($arDelFile);
		}

		return $result;
	}


	/**
	 * Обнолвние существующих изображений заказа
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function updateOrderPictures(array $arUpdatedPictures): bool {
		//TODO В будущем передалть эти функции, если появятся методы для работы с файлами

		if (empty($arUpdatedPictures)) {
			return false;
		}

		$currentPictures = self::getOrderUserFieldValue($this->getOrderId(), self::ORDER_PHOTO_PROPERTY);

		$arFiles    = [];
		$arDelFiles = [];

		if (is_array($currentPictures)) {
			foreach ($currentPictures as $currentPicture) {
				if (in_array((int) $currentPicture, $arUpdatedPictures, true)) {
					$arFiles[] = CFile::MakeFileArray(CDbzConstants::DBZ_PORTAL_URL . CFile::GetPath($currentPicture));
				}
				$arDelFiles[] = $currentPicture;
			}
		}

		$this->updateOrderField(self::ORDER_PHOTO_PROPERTY, $arFiles);

		//TODO Костыль. Удаляем файлы
		foreach ($arDelFiles as $arDelFile) {
			CFile::Delete($arDelFile);
		}

		return true;
	}

	/**
	 * Добавляем мастера в список просмотревших
	 *
	 * @param $arWatchedMasters
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function updateWatchedMastersField($arWatchedMasters): void {
		if ( ! (in_array($this->userId, $arWatchedMasters, true))) {
			$arWatchedMasters[] = $this->userId;

			$this->updateOrderField("UF_CRM_COUNTER_WATCHEDMASTERS", array_unique($arWatchedMasters));
		}
	}


	/**
	 *
	 * Получение причин отказов / возвратов
	 *
	 * @param $type
	 *
	 * @return array
	 */
	#[ArrayShape(['list' => "array"])] private function getOrderHints($type): array {
		$MASTER_ID                = 31169;
		$CLIENT_ID                = 31170;
		$CANCEL_RESPOND_MASTER_ID = 92;
		$CANCEL_ORDER_CLIENT      = 93;
		$CANCEL_ORDER_MASTER      = 94;

		$list      = array();
		$user_code = 0;
		$type_code = 92;
		if ($type === 0) { // отмена отклика мастером
			$user_code = $MASTER_ID;
			$type_code = $CANCEL_RESPOND_MASTER_ID;
		} else if ($type === 1) { // отмена заказа клиентом
			$user_code = $CLIENT_ID;
			$type_code = $CANCEL_ORDER_CLIENT;
		} else if ($type === 2) { //Отказ от заявки в работе
			$user_code = $MASTER_ID;
			$type_code = $CANCEL_ORDER_MASTER;
		}

		$arSelect = array("ID", "IBLOCK_ID", "NAME");
		$arFilter = array("IBLOCK_ID" => CDbzConstants::DBZ_CANCEL_TYPES_IBLOCK_ID, "=PROPERTY_USER_TYPE" => $user_code, "=PROPERTY_TIPY_OTMENY" => $type_code, "=PROPERTY_AKTIVNOST_ZAPISI" => CDbzConstants::DBZ_YES_VALUE);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields = $ob->GetFields();
			$list[]   = $arFields["NAME"];
		}

		return array(
			'list' => $list
		);
	}


	/**
	 * Возвращает значение пользоватаельского поля заказа
	 *
	 * @param $orderId
	 * @param $userFieldId
	 *
	 * @return mixed
	 */
	public static function getOrderUserFieldValue($orderId, $userFieldId): mixed {
		$arFilter       = array("=ID" => $orderId);
		$arSelectFields = array($userFieldId);
		$res            = CCrmDeal::GetListEx(array("id" => "asc"), $arFilter, false, false, $arSelectFields);
		if ($arFields = $res->GetNext()) {
			return $arFields[ $userFieldId ];
		}

		return false;
	}


	/**
	 * Обновление поля в сделке
	 *
	 * @param $fieldCode
	 * @param $value
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function updateOrderField($fieldCode, $value): void {
		Loader::includeModule("crm");
		$entity = new CCrmDeal;
		$fields = array(
			$fieldCode => $value,
		);
		$entity->update($this->orderId, $fields);
	}


	/**
	 * Устанавливает значение свойтсва (Да/Нет) Закрытие просмотрено мастером для отклика
	 */
	private function setMasterRespondViewStatus(): void {
		//закрытые отклики - просмотрены мастером
		$arSelect = array("ID", "IBLOCK_ID", "PROPERTY_132");
		$arFilter = array("IBLOCK_ID" => CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID, "=PROPERTY_ZAKAZ" => $this->orderId, "=PROPERTY_MASTER" => $this->userId, "=PROPERTY_ZAKRYTIE_PROSMOTRENO_MASTEROM" => CDbzConstants::DBZ_NO_VALUE);
		$res      = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNextElement()) {
			$arFields                              = $ob->GetFields();
			$PROP                                  = array();
			$PROP["ZAKRYTIE_PROSMOTRENO_MASTEROM"] = CDbzConstants::DBZ_YES_VALUE;
			CIBlockElement::SetPropertyValuesEx($arFields["ID"], CDbzConstants::DBZ_ORDER_RESPONDS_IBLOCK_ID, $PROP);
		}
	}


	/**
	 * Установка признакоа "Завершение заказа (заказчик / мастер)"
	 *
	 * @param $arOrder
	 *
	 * @throws \Bitrix\Main\LoaderException
	 */
	private function setOrderCompleteStatus($arOrder): void {
		$IN_PROCESS_VALUE = 30956;
		$CONFIRM_VALUE    = 30957;

		if ($this->userType === CDbzConstants::DBZ_CLIENT_TYPE) {
			//закрытие просмотрено
			if ((int) $this->arOrder['UF_CRM_COUNTER_ORDERCOMPL'] === $IN_PROCESS_VALUE) {
				$this->updateOrderField("UF_CRM_COUNTER_ORDERCOMPL", $CONFIRM_VALUE);
			}
		} else {
			//закрытие просмотрено
			if ((int) $arOrder['UF_CRM_COUNTER_ORDERCOMPL_WORKER'] === $IN_PROCESS_VALUE) {
				$this->updateOrderField("UF_CRM_COUNTER_ORDERCOMPL_WORKER", $CONFIRM_VALUE);
			}
		}
	}


	/**
	 * добавление комментария в карточку сделки
	 * @throws \Bitrix\Main\ArgumentException
	 */
	private function addTimelineComment($text): void {
		if ( ! $this->getOrderId()) {
			return;
		}

		$text = Json::encode($text);
		$text = str_replace('~BR~', '\r\n', $text);
		$text = Json::decode($text);
		CommentEntry::create(
			array(
				'TEXT'      => $text,
				'SETTINGS'  => array('HAS_FILES' => 'N'),
				'AUTHOR_ID' => 146420,
				'BINDINGS'  => array(array('ENTITY_TYPE_ID' => 2, 'ENTITY_ID' => $this->getOrderId()))
			));
	}


	/**
	 * @param int $userType
	 */
	public function setUserType(int $userType): void {
		$this->userType = $userType;
	}

	/**
	 * @param int|null $userId
	 */
	public function setUserId(?int $userId): void {
		$this->userId = $userId;
	}

	/**
	 * @return int|null
	 */
	public function getOrderId(): ?int {
		return $this->orderId;
	}

	/**
	 * @return int
	 */
	public function getUserType(): int {
		return $this->userType;
	}

	/**
	 * @return int|null
	 */
	public function getUserId(): ?int {
		return $this->userId;
	}

	/**
	 * @return array
	 */
	public function getArOrder(): array {
		return $this->arOrder;
	}

	/**
	 * @param int $orderId
	 */
	public function setOrderId(int $orderId): void {
		$this->orderId = $orderId;
	}

}