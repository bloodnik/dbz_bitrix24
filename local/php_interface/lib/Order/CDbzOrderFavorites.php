<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 02.09.2021
 * Time: 20:00
 * Project: dombezzabot.net
 */

namespace lib\order;


use Bitrix\Main\ORM\Data\AddResult;
use Bitrix\Main\ORM\Data\DeleteResult;
use Bitrix\Main\SystemException;
use lib\helpers\CDbzBase;
use lib\helpers\CDbzHighloadHelper;

class CDbzOrderFavorites extends CDbzBase {

	public const FAVORITES_HL_NAME = "DbzOrderFavorites";

	private int $orderId = 0;


	/**
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function setOrderFavorite(bool $value): int|array {

		if ( ! $this->getOrderId()) {
			throw new SystemException("Не определен номер заказа");
		}

		$result = match ($value) {
			true => $this->addToFavorites(),
			false => $this->removeFromFavorites(),
			default => null,
		};

		if ($result->isSuccess()) {
			return true;
		}

		throw new SystemException(current($result->getErrorMessages()));
	}


	/**
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Exception
	 */
	private function addToFavorites(): AddResult {

		if ($this->isFavoriteExist()) {
			throw new SystemException("Заказ уже добвален в Избранное");
		}

		$entity_data_class = CDbzHighloadHelper::getHighloadEntityByName(self::FAVORITES_HL_NAME);
		$arData            = [
			"UF_MASTER" => $this->getUserId(),
			"UF_ORDER"  => $this->getOrderId(),
		];

		return $entity_data_class::add($arData);
	}


	/**
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Exception
	 */
	private function removeFromFavorites(): DeleteResult {
		$favoriteId        = $this->isFavoriteExist();
		$entity_data_class = CDbzHighloadHelper::getHighloadEntityByName(self::FAVORITES_HL_NAME);

		return $entity_data_class::delete($favoriteId);
	}


	/**
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function isFavoriteExist(): int {
		$entity_data_class = CDbzHighloadHelper::getHighloadEntityByName(self::FAVORITES_HL_NAME);
		$arFilter          = [
			"UF_MASTER" => $this->getUserId(),
			"UF_ORDER"  => $this->getOrderId(),
		];

		$rsData = $entity_data_class::getList([
			"filter" => $arFilter
		]);

		$result = false;
		if ($arData = $rsData->fetch()) {
			$result = $arData["ID"];
		}

		return $result;
	}


	/**
	 * Число сохранений заказа
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public static function getFavoritesCount($orderId): int {
		$entity_data_class = CDbzHighloadHelper::getHighloadEntityByName(self::FAVORITES_HL_NAME);
		$arFilter          = [
			"UF_ORDER" => $orderId,
		];

		return $entity_data_class::getCount($arFilter);
	}


	/**
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ObjectPropertyException
	 * @throws \Bitrix\Main\SystemException
	 */
	public function getUserFavoritesIds(): array {
		$entity_data_class = CDbzHighloadHelper::getHighloadEntityByName(self::FAVORITES_HL_NAME);
		$arFilter          = [
			"UF_MASTER" => $this->getUserId(),
		];

		$rsData = $entity_data_class::getList([
			"order"  => ["ID" => "DESC"],
			"select" => ["UF_ORDER"],
			"filter" => $arFilter
		]);

		$result = [];
		while ($arData = $rsData->fetch()) {
			$result[] = $arData["UF_ORDER"];
		}

		return $result;
	}


	/**
	 * @return int
	 */
	public function getOrderId(): int {
		return $this->orderId;
	}

	/**
	 * @param int $orderId
	 */
	public function setOrderId(int $orderId): void {
		$this->orderId = $orderId;
	}


}