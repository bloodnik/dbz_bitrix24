<?php
/**
 * User: SIRIL
 * Date: 20.01.2021
 * Time: 19:38
 * Project: dombezzabot.net
 */
if ( ! defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/newTabHandler.php")) {
	require_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/newTabHandler.php");
}

// Подключаем autoload composer
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/vendor/autoload.php');

//Подключение доп функций
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/highloadService.php');
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/functions.php');

//Константы
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/constants.php');
//Автозагрузка своих классов
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/autoload.php');
//Обработка событий
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/lib/event_handler.php');

//define('VUEJS_DEBUG', true);

// Региструем константу глобальных настроек
$globalSettings = \lib\helpers\CApiHelpers::getGlobalSettings();
define('DBZ_GLOBAL_SETTINGS', $globalSettings);


// For debug
function PR($item, $show_for = false) {
	global $USER;
	if ($USER->IsAdmin() || $show_for == 'all' || $USER->GetID() == 146420) {
		if ( ! $item) {
			echo ' <br />пусто <br />';
		} elseif (is_array($item) && empty($item)) {
			echo '<br />массив пуст <br />';
		} else {
			echo ' <pre>' . print_r($item, true) . ' </pre>';
		}
	}
}

/*
//log for mail cron fix
function custom_mail($to, $subject, $body, $headers) {
    $f=fopen($_SERVER["DOCUMENT_ROOT"]."/maillog.txt", "a+");
    fwrite($f, print_r(array('TO' => $to, 'SUBJECT' => $subject, 'BODY' => $body, 'HEADERS' => $headers),1)."\n========\n");
    fclose ($f);
    return mail($to, $subject, $body, $headers);
}
*/
