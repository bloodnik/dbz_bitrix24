<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 19.09.2021
 * Time: 19:27
 * Project: dombezzabot.net
 */

// Подключение ядра 1С-Битрикс
define('NOT_CHECK_PERMISSIONS', true);
define('NO_AGENT_CHECK', true);
$GLOBALS['DBType']        = 'mysql';
$_SERVER['DOCUMENT_ROOT'] = "/home/bitrix/www/";


require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

// Искуственная авторизация в роли админа
$_SESSION['SESS_AUTH']['USER_ID'] = 1;
// Подключение автозаргрузки Composer
//require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';