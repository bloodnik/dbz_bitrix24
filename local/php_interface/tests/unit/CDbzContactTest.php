<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 11.10.2021
 * Time: 0:23
 * Project: dombezzabot.net
 */

namespace dbz\tests;

use lib\contact\CDbzContact;
use lib\helpers\CDbzConstants;
use PHPUnit\Framework\TestCase;

class CDbzContactTest extends TestCase {
	private CDbzContact $obContact;
	private int $orderId = 340256;
	private int $userId = 187982;
	private int $userType = CDbzConstants::DBZ_WORKER_TYPE;
	const UFA_CITY = 31314;
	const ABAZA_CITY = 32261;

	protected function setUp(): void {
		parent::setUp();
		$this->obContact = new CDbzContact($this->userId);
	}


	public function testGetContactCity() {
		$city      = $this->obContact->getContactCity();

		self::assertEquals(self::UFA_CITY, $city);
	}

	public function testGetContactWorkCategories() {
		$arWorks = CDbzContact::getContactWorkCategories($this->userId);

		self::assertIsArray($arWorks);
		self::assertArrayHasKey("WORK_CATEGORIES", $arWorks);
		self::assertArrayHasKey("UNIT_WORKS", $arWorks);
	}

	public function testGetContactResponseWithoutContacts() {
		$arContact = $this->obContact->getContactResponse(false);

		self::assertIsArray($arContact);
		self::assertEquals($this->userId, $arContact["id"]);
		self::assertEquals("Ильдус Сираев", $arContact["title"]);
		self::assertIsFloat($arContact["rating"]);
		self::assertIsInt($arContact["feedbacks_amount"]);
		self::assertNull($arContact["contacts"]);
		self::assertIsInt($arContact["orders_amount"]);
	}

	public function testGetContactResponseWithContacts() {
		$arContact = $this->obContact->getContactResponse(true);

		self::assertIsArray($arContact);
		self::assertEquals($this->userId, $arContact["id"]);
		self::assertEquals("Ильдус Сираев", $arContact["title"]);
		self::assertIsFloat($arContact["rating"]);
		self::assertIsInt($arContact["feedbacks_amount"]);
		self::assertIsInt($arContact["orders_amount"]);
		self::assertIsArray($arContact["contacts"]);
		self::assertArrayHasKey("phone", $arContact["contacts"]);
	}

	public function testGetContactPhone() {
		$phone = $this->obContact->getContactPhone();
		self::assertEquals("79871389528", $phone);
	}

//	public function testGetContactNotificationsCount() {
//
//	}
//
//	public function testSetContactCoordinates() {
//
//	}
//
//	public function testGetUserRegisterId() {
//
//	}
//
//	public function testIsContactRegistered() {
//
//	}
//
//	public function testIsContactHidden() {
//
//	}

	public function testGetExistContactData() {
		$arContact = $this->obContact->getContactData();

		self::assertIsArray($arContact);
		self::assertEquals($this->userId, $arContact["ID"]);
	}

	public function testGetNotExistContactData() {
		$this->expectErrorMessage("Контакт не найден");

		$obContact = new CDbzContact(99999999999);
		$obContact->getContactData();
	}
}
