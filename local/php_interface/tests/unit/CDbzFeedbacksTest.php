<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 19.09.2021
 * Time: 21:08
 * Project: dombezzabot.net
 */

namespace dbz\tests;

use lib\feedbacks\CDbzFeedbacks;
use lib\helpers\CDbzBase;
use lib\helpers\CDbzConstants;
use PHPUnit\Framework\TestCase;

class CDbzFeedbacksTest extends TestCase {
	private int $orderId = 340256;
	private int $userId = 187982;
	private int $userType = CDbzConstants::DBZ_WORKER_TYPE;


	public function testGetUserOrderFeedbackNoOrderIdExp() {
		$this->expectErrorMessage("Не определен заказ");
		$obFeedback = new CDbzFeedbacks($this->userId, $this->userType);
		$result     = $obFeedback->getUserOrderFeedback();
	}

	public function testGetUserOrderFeedback() {
		$obFeedback = new CDbzFeedbacks($this->userId, $this->userType);
		$obFeedback->setOrderId($this->orderId);
		$result = $obFeedback->getUserOrderFeedback();

		self::assertIsArray($result);
		self::assertArrayHasKey("id", $result);
		self::assertArrayHasKey("ratings", $result);
	}

	public function testIsFeedbackExist() {
		$obFeedback = new CDbzFeedbacks($this->userId, $this->userType);
		$obFeedback->setOrderId($this->orderId);

		$result = $obFeedback->isFeedbackExist();
		self::assertTrue($result);
	}

	public function testGetUserFeedbacksCount() {
		$obFeedback = new CDbzFeedbacks($this->userId, $this->userType);
		$count = $obFeedback->getUserFeedbacksCount();
		self::assertIsInt($count);
	}

//
//	public function testAddFeedback() {
//
//	}
//
//	public function testGetRatingsFields() {
//
//	}
}
