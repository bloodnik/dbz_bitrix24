<?php
/**
 * Created by INGILE.
 * User: SIRIL
 * Date: 19.09.2021
 * Time: 20:04
 * Project: dombezzabot.net
 */

namespace dbz\tests;

use lib\helpers\CDbzBase;
use lib\helpers\CDbzConstants;
use lib\location\CDbzLocationResponse;
use lib\main\CDbzMain;
use PHPUnit\Framework\TestCase;

class CDbzMainTest extends TestCase {
	private int $userId = 187982;
	private int $userType = CDbzConstants::DBZ_WORKER_TYPE;
	private string $appVersion = "0.55a";


	public function testGetMainNoAppVersion(): void {
		$this->expectErrorMessage("Не определдена версия приложения");
		$obMain = new CDbzMain($this->userId, $this->userType);
		$obMain->getMain();
	}


	public function testGetMain(): void {
		$obMain = new CDbzMain($this->userId, $this->userType);
		$obMain->setAppVersion($this->appVersion);
		$result = $obMain->getMain();

		self::assertIsArray($result);
		self::assertArrayHasKey("counts", $result);
		self::assertArrayHasKey("news", $result);
		self::assertArrayHasKey("should_update_app", $result);
	}


	public function testGetAppVersion(): void {
		$obMain = new CDbzMain($this->userId, $this->userType);
		$obMain->setAppVersion($this->appVersion);
		self::assertEquals($this->appVersion, $obMain->getAppVersion());
	}

	public function testGetStartContent(): void {
		$location = \Mockery::mock(CDbzLocationResponse::class);

		$obMain = new CDbzMain($this->userId, $this->userType);
		$result = $obMain->getStartContent($location);

		self::assertIsArray($result);

		$firstEl = current($result);
		self::assertArrayHasKey("title", $firstEl);
		self::assertArrayHasKey("summary", $firstEl);
		self::assertArrayHasKey("cover", $firstEl);
	}


}
